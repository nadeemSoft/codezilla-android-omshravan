package com.zecast.live.om_saravanabava.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.squareup.picasso.Picasso;

import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.activity.eventdetails.EventDetailsActivity;
import com.zecast.live.om_saravanabava.model.EventList;

import java.util.ArrayList;


/**
 * Created by  Salman on 1/11/17.
 */

public class AdapterLiveCathupGrid extends RecyclerView.Adapter<AdapterLiveCathupGrid.MyViewHolder> {

    private ArrayList<EventList> mCathupSubCatList;
    private Context context;


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tvCatName,tvEventSeens;
        public ImageView iv,ivPlay;
        public LinearLayout ll;


        public MyViewHolder(View view) {
            super(view);

            ll = (LinearLayout)view.findViewById(R.id.llLayout);
            tvCatName = (TextView) view.findViewById(R.id.tv_catchup_name);
            tvEventSeens = (TextView)view.findViewById(R.id.tv_views);
            iv = (ImageView)view.findViewById(R.id.catchup_imageview_logo);
            ivPlay = (ImageView)view.findViewById(R.id.play_video_icon);

        }
    }

    public AdapterLiveCathupGrid(Context context, ArrayList<EventList> mCathupSubCatList) {
        this.context = context;
        this.mCathupSubCatList = mCathupSubCatList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapterlivecatchugrid, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final EventList el = mCathupSubCatList.get(position);

        holder.tvCatName.setText(el.getEventTitle());
        holder.tvEventSeens.setText(el.getEventSeens());


        if (!el.getEventImageURL().isEmpty())
        {
            Picasso.with(context)
                    .load(el.getEventImageURL())
                    .error(R.drawable.app_icon)
                    .placeholder(R.drawable.app_icon)
                    .into(holder.iv);
        }

        holder.ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent(context, EventDetailsActivity.class);
                in.putExtra("EventName",el.getEventTitle());
               in.putExtra("EventId",el.getEventId());
                context.startActivity(in);

            }
        });

    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mCathupSubCatList.size();
    }
}