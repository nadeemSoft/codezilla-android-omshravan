package com.zecast.live.om_saravanabava.asynctask;

public interface UrlConstant {
    int POST = 2;
    int CONNECTION_TIME_OUT = 20;
    int SOCKET_TIME_OUT = 25 ;
    String BASE_URL = "http://cdn.zecast.com/saravanabhava/";
    String API_URL = BASE_URL + "api/";
    String URL_GET_CALENDAR_EVENTS = "getCalendarEvents";
    String HEADER_AUTHORIZATION = "Authorization";
    String HEADER_DEVICE_ID = "deviceID";
}
