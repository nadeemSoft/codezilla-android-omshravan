package com.zecast.live.om_saravanabava.fragments;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.Utilities;
import com.zecast.live.om_saravanabava.activity.LiveCatchupGrid;
import com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity;
import com.zecast.live.om_saravanabava.activity.seealllists.CatchupSeeAllActivity;
import com.zecast.live.om_saravanabava.activity.seealllists.RecentEventSeeAllActivity;
import com.zecast.live.om_saravanabava.adapter.LiveCatchupProgramsAdapter;
import com.zecast.live.om_saravanabava.adapter.RecentEventsAdapter;
import com.zecast.live.om_saravanabava.model.CatchupModel;
import com.zecast.live.om_saravanabava.model.RecentEventsModel;
import com.zecast.live.om_saravanabava.utils.Const;
import com.zecast.live.om_saravanabava.utils.HTTPRequest;
import com.zecast.live.om_saravanabava.utils.Sharedprefrences;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.zecast.live.om_saravanabava.R.id.recent_container_linear;

public class LiveFragment extends Fragment implements View.OnClickListener {

    public LiveFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static VideoView videoLayout;
    RecyclerView recyclerViewRecentEvents, recyclerViewCatchupEvents;
    LiveCatchupProgramsAdapter catchupProgramsAdapter;
    RecentEventsAdapter recentEventsAdapter;
    LinearLayoutManager layoutManager;
    public static TextView tvDescriptionEventLive, tvHeadingEventLive, tvDateEventLive;
    public static LinearLayout dontateNowLayout;
    public static LinearLayout noticeLayoutCurentEvent;
    public static ImageView ivPlay, ivPause, ivVolumeControl, ivFullscreen;
    public static SeekBar seekBarVideo, seekbarVolume;
    public static TextView textViewSeeAll, textViewSeeAllCatchups;
    public static LinearLayout linearRecentContainer;
    public static LinearLayout linearcatchupContainer;
    public static NestedScrollView scrollView;
    public static FrameLayout frameVideoControls;
    public static TextView tvViewsLiveEvent;
    public static final ArrayList<CatchupModel> liveCatchuplist = new ArrayList<>();
    public static final ArrayList<RecentEventsModel> recentEventsModelList = new ArrayList<>();
    public static int durationVideo;
    private AudioManager mAudioManager;
    ProgressDialog progressDialog;
    public static TextView tvLiveTag;
    private static final String CURRENT = "duration";
    private Uri mURI;
    public static LinearLayout frameController;
    public static int llheight, llwidth;
    public static LinearLayout llVideoContainer, llContainer;
    String eventDonateStatus;
    Button btnDonateNow;
    TextView tvEventId;
    String eventLikeStatus;
    ImageView ivShare;
    String refresh;
    ProgressBar pb;
    String like;
    TextView tvCathupHeading, tvTimer, tvRemaining;
    private MediaPlayer mp;
    int catchup = 0; // if videos is catchup then it will be 1if videos is catchup then it will be 1

    String songTime, totalTime;
    private Handler mHandler = new Handler();
    private Utilities utils;

    //coem8Tv5QSU:APA91bHxQCdi2Gi7opJXU9yv6jlNIZW5sMMlyB_kYQbI2uvWrawrhB2vYeOd_tOmpgZEKN5m8nZd9so7CJcRH8Io2Y9HrDbOSWcF1VTAmhuDD96Jr94u-LTmgAO4kr_55lBuo6NALn4f

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.live2, container, false);
        getActivity().setTitle("Live");
        initVidgets(view);
        dontateNowLayout.setVisibility(View.GONE);
        setBottom();
        HomeDrawerActivity.ivSearch.setVisibility(View.GONE);


        refresh = "0";

        Const.UPCOMING_FRAGMENT_KEY = 0;

        if (!liveCatchuplist.isEmpty()) {

            tvCathupHeading.setVisibility(View.VISIBLE);
            textViewSeeAllCatchups.setVisibility(View.VISIBLE);

            catchupProgramsAdapter = new LiveCatchupProgramsAdapter(getActivity(), liveCatchuplist, LiveFragment.this);
            layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
            recyclerViewCatchupEvents.setLayoutManager(layoutManager);
            recyclerViewCatchupEvents.setAdapter(catchupProgramsAdapter);
            catchupProgramsAdapter.notifyDataSetChanged();
            Sharedprefrences.setDonateStatus(getContext(), eventDonateStatus);
            recentEventsAdapter = new RecentEventsAdapter(getActivity(), recentEventsModelList, LiveFragment.this);
            layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
            recyclerViewRecentEvents.setLayoutManager(layoutManager);
            recyclerViewRecentEvents.setAdapter(recentEventsAdapter);

            recentEventsAdapter.notifyDataSetChanged();

            if (!recentEventsModelList.isEmpty()) {
                if (liveCatchuplist.get(0).getEventDonateStatus().equals("Yes")) {
                    dontateNowLayout.setVisibility(View.VISIBLE);
                }

                tvLiveTag.setVisibility(View.VISIBLE);

                videoLayout.setVisibility(View.VISIBLE);

                String urls = recentEventsModelList.get(0).getEventURL();
                Sharedprefrences.setVideoUrl(getActivity(), urls);

                catchup = 0;

                RecentEventsAdapter.clickedPosition = 0;
                recentEventsAdapter.notifyDataSetChanged();

                LiveCatchupProgramsAdapter.clickedPosition = -2;
                catchupProgramsAdapter.notifyDataSetChanged();


                new BackgroundAsyncTask().execute(urls);

                like = "1";
                Sharedprefrences.setPositions(getActivity(), "1");


                tvHeadingEventLive.setText(recentEventsModelList.get(0).getEventName());
                tvDescriptionEventLive.setText(recentEventsModelList.get(0).getEventDescription());
                tvDateEventLive.setText(recentEventsModelList.get(0).getEventStartDate());
                tvViewsLiveEvent.setText(recentEventsModelList.get(0).getEventSeens());
                tvEventId.setText(recentEventsModelList.get(0).getEventId());
                Sharedprefrences.setliveLikeId(getContext(), recentEventsModelList.get(0).getEventId());
            }

            if (recentEventsModelList.isEmpty()) {
                like = "0";
                Sharedprefrences.setPositions(getActivity(), "0");

                if (liveCatchuplist.get(0).getEventDonateStatus().equals("Yes")) {
                    dontateNowLayout.setVisibility(View.VISIBLE);
                }

                tvLiveTag.setVisibility(View.GONE);

                String ur = liveCatchuplist.get(0).getEventURL();

                Sharedprefrences.setVideoUrl(getActivity(), ur);

                catchup = 1;
                LiveCatchupProgramsAdapter.clickedPosition = 0;
                catchupProgramsAdapter.notifyDataSetChanged();


                RecentEventsAdapter.clickedPosition = -2;
                recentEventsAdapter.notifyDataSetChanged();

                new BackgroundAsyncTask().execute(ur);

                tvHeadingEventLive.setText(liveCatchuplist.get(0).getEventName());
                tvDescriptionEventLive.setText(liveCatchuplist.get(0).getEventDescription());
                tvDateEventLive.setText(liveCatchuplist.get(0).getEventStartDate());
                tvViewsLiveEvent.setText(liveCatchuplist.get(0).getEventSeens());
                tvEventId.setText(liveCatchuplist.get(0).getEventId());

            }

        } else {
            pb.setVisibility(View.VISIBLE);
            llContainer.setVisibility(View.GONE);
            llVideoContainer.setVisibility(View.GONE);
            scrollView.setVisibility(View.GONE);

            fetchLiveVideos();
        }

        ivPause.setOnClickListener(this);
        ivPlay.setOnClickListener(this);
        textViewSeeAll.setOnClickListener(this);
        textViewSeeAllCatchups.setOnClickListener(this);
        btnDonateNow.setOnClickListener(this);
        mAudioManager = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);
        soundControl();
        setSoundControlFunction();

        progressDialog = new ProgressDialog(getContext());

        if (savedInstanceState != null) {

            mURI = Uri.parse(savedInstanceState.getString("URL"));
            Log.e("rotate", String.valueOf(mURI));
        }

        seekBarVideo.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                if (b) {

                    videoLayout.seekTo(i);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        setRetainInstance(true);

        return view;
    }

    private void setBottom() {
        HomeDrawerActivity.lineLive.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
        HomeDrawerActivity.iconTabLive.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_live_tab));
        HomeDrawerActivity.lineHome.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.White));
        HomeDrawerActivity.iconTabHome.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_home_unselected));
        HomeDrawerActivity.lineUpcoming.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.White));
        HomeDrawerActivity.iconTabUpcoming.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_upcoming_unselected));
        HomeDrawerActivity.lineVideo.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.White));
        HomeDrawerActivity.iconTabVideo.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_video_tab_unselected));
        HomeDrawerActivity.lineProfile.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.White));
        HomeDrawerActivity.iconTabProfile.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_profile_tab_unselected));
        HomeDrawerActivity.baseHomeText.setTextColor(ContextCompat.getColor(getActivity(), R.color.gray));
        HomeDrawerActivity.basevideostext.setTextColor(ContextCompat.getColor(getActivity(), R.color.gray));
        HomeDrawerActivity.baseUpcomingtext.setTextColor(ContextCompat.getColor(getActivity(), R.color.gray));
        HomeDrawerActivity.baseProfileText.setTextColor(ContextCompat.getColor(getActivity(), R.color.gray));
        HomeDrawerActivity.baseLivetext.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
    }

    private void setSoundControlFunction() {
        this.seekbarVolume.setMax(mAudioManager
                .getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        this.seekbarVolume.setProgress(mAudioManager
                .getStreamVolume(AudioManager.STREAM_MUSIC));
    }

    private void soundControl() {

        seekbarVolume.setMax(mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));

        seekbarVolume.setProgress(mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC));

        seekbarVolume.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if (progress == 0) {

                    ivVolumeControl.setImageResource(R.drawable.mute);


                } else if (progress != 0) {
                    ivVolumeControl.setImageResource(R.drawable.speaker);

                }
                mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                        progress, 0);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

        });
    }

    public void callAsnycTask(CatchupModel liveCatchuplist, String position) {

        if (liveCatchuplist.getEventHasMultipleParts().equals("true")) {
            Log.e("LKive", "1");
            Intent in = new Intent(getActivity(), LiveCatchupGrid.class);
            in.putExtra("CatchupName", liveCatchuplist.getEventName());
            in.putExtra("CatchupId", liveCatchuplist.getEventId());
            startActivity(in);
        } else {
            Log.e("LKive", "2");
            Toast.makeText(getActivity(), "Please wait video is starting", Toast.LENGTH_SHORT).show();
            new BackgroundAsyncTask().execute(liveCatchuplist.getEventURL());

            tvLiveTag.setVisibility(View.GONE);

            like = "0";

            seekBarVideo.setVisibility(View.VISIBLE);

            if (liveCatchuplist.getEventDonateStatus().equals("Yes")) {
                dontateNowLayout.setVisibility(View.VISIBLE);
            }
            RecentEventsAdapter.clickedPosition = -2;
            recentEventsAdapter.notifyDataSetChanged();


            videoLayout.setVisibility(View.VISIBLE);
            Sharedprefrences.setVideoUrl(getActivity(), liveCatchuplist.getEventURL());


            Sharedprefrences.setPositions(getActivity(), position);
            Log.e("livesss", liveCatchuplist.getEventLikeStatus());

            Sharedprefrences.setchangelikePosition(getContext(), position);

            Sharedprefrences.setVideoUrl(getContext(), liveCatchuplist.getEventURL());

            if (liveCatchuplist.getEventDonateStatus().equals("Yes")) {
                dontateNowLayout.setVisibility(View.VISIBLE);
            } else {
                dontateNowLayout.setVisibility(View.GONE);
            }

            catchup = 1;

            tvHeadingEventLive.setText(liveCatchuplist.getEventName());
            tvDescriptionEventLive.setText(liveCatchuplist.getEventDescription());
            tvDateEventLive.setText(liveCatchuplist.getEventStartDate());
            tvViewsLiveEvent.setText(liveCatchuplist.getEventSeens());
            tvEventId.setText(liveCatchuplist.getEventId());
            Log.e("Liklessss", liveCatchuplist.getEventLikes());

            getEventSeensCount();
        }
    }

    public void callAsnycTaskFromRecent(RecentEventsModel recentEventsModel, String position) {

        Log.e("likestatusss", recentEventsModel.getEventURL());
        like = "1";

        catchup = 0;

        seekBarVideo.setVisibility(View.VISIBLE);
        Sharedprefrences.setPositions(getActivity(), position);
        Sharedprefrences.setPositions(getActivity(), position);

        LiveCatchupProgramsAdapter.clickedPosition = -2;
        catchupProgramsAdapter.notifyDataSetChanged();


        Sharedprefrences.setVideoUrl(getActivity(), recentEventsModel.getEventURL());

        new BackgroundAsyncTask().execute(recentEventsModel.getEventURL());

        if (recentEventsModel.getEventDonateStatus().equals("Yes")) {
            dontateNowLayout.setVisibility(View.VISIBLE);
        } else {
            dontateNowLayout.setVisibility(View.GONE);
        }

        tvLiveTag.setVisibility(View.VISIBLE);

        tvHeadingEventLive.setText(recentEventsModel.getEventName());
        tvDescriptionEventLive.setText(recentEventsModel.getEventDescription());
        tvDateEventLive.setText(recentEventsModel.getEventStartDate());
        tvViewsLiveEvent.setText(recentEventsModel.getEventSeens());

        tvEventId.setText(recentEventsModel.getEventId());

        getEventSeensCount();
    }

    private void fetchLiveVideos() {
        RequestQueue queue = Volley.newRequestQueue(getContext());
        StringRequest sr = new StringRequest(Request.Method.POST, HTTPRequest.SERVICE_URL + "getLiveTv", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.e("LIVE RESPONS#E", response);

                    pb.setVisibility(View.GONE);
                    llContainer.setVisibility(View.VISIBLE);
                    llVideoContainer.setVisibility(View.VISIBLE);
                    scrollView.setVisibility(View.VISIBLE);

                    try {

                        JSONObject obj = new JSONObject(response);
                        JSONObject objMain = obj.optJSONObject("response");

                        recentEventsModelList.clear();
                        JSONArray recentEventsArray = objMain.optJSONArray("recent_events");

                        Log.e("LIST ITEM", " OUT OF LOOP" + recentEventsArray);


                        for (int i = 0; i < recentEventsArray.length(); i++) {

                            JSONObject objOpeningArray = recentEventsArray.optJSONObject(i);
                            String eventId = objOpeningArray.optString("eventId");
                            String eventName = objOpeningArray.optString("eventTitle");
                            String eventStartDate = objOpeningArray.optString("eventStartDate");
                            String eventEndDate = objOpeningArray.optString("eventEndDate");
                            String eventDescription = objOpeningArray.optString("eventDescription");
                            String eventLocation = objOpeningArray.optString("eventLocation");
                            String eventDuration = objOpeningArray.optString("eventDuration");
                            String eventCode = objOpeningArray.optString("eventCode");
                            String eventImageURL = objOpeningArray.optString("eventImageURL");
                            String eventURL = objOpeningArray.optString("eventURL");
                            String eventDonateStatus = objOpeningArray.optString("eventDonateStatus");
                            String eventGiftTypeId = objOpeningArray.optString("eventGiftTypeId");
                            String eventGiftRemarks = objOpeningArray.optString("eventGiftRemarks");
                            String eventTypeStatusId = objOpeningArray.optString("eventTypeStatusId");
                            String eventTypeStatus = objOpeningArray.optString("eventTypeStatus");
                            eventLikeStatus = objOpeningArray.optString("eventLikeStatus");
                            String eventLikes = objOpeningArray.optString("eventLikes");
                            String eventSeens = objOpeningArray.optString("eventSeens");

                            RecentEventsModel recentEventsModel = new RecentEventsModel(eventId, eventName, eventStartDate, eventEndDate, eventDescription, eventLocation, eventDuration, eventCode, eventImageURL, eventURL, eventDonateStatus, eventGiftTypeId, eventGiftRemarks, eventTypeStatusId, eventTypeStatus, eventLikeStatus, eventLikes, eventSeens);
                            recentEventsModelList.add(recentEventsModel);


                        }


//                        if (recentEventsModelList.get(0).getEventLikeStatus().equals("0")) {
//                            imgLike.setVisibility(View.VISIBLE);
//                            imgLikeOrange.setVisibility(View.GONE);
//                        } if (recentEventsModelList.get(0).getEventLikeStatus().equals("1")) {
//                            imgLike.setVisibility(View.GONE);
//                            imgLikeOrange.setVisibility(View.VISIBLE);
//                        }

                        if (recentEventsModelList.size() != 0) {
                            catchup = 0;
                            like = "1";
                            Sharedprefrences.setPositions(getActivity(), "0");
                            videoLayout.setVisibility(View.VISIBLE);

                            String url = recentEventsModelList.get(0).getEventURL();
                            Log.e("urlLive", url);
                            Sharedprefrences.setVideoUrl(getActivity(), url);
                            new BackgroundAsyncTask().execute(url);

                            tvHeadingEventLive.setText(recentEventsModelList.get(0).getEventName());
                            tvDescriptionEventLive.setText(recentEventsModelList.get(0).getEventDescription());
                            tvDateEventLive.setText(recentEventsModelList.get(0).getEventStartDate());
                            tvViewsLiveEvent.setText(recentEventsModelList.get(0).getEventSeens());
                            tvEventId.setText(recentEventsModelList.get(0).getEventId());

                            seekBarVideo.setVisibility(View.VISIBLE);

                            Sharedprefrences.setDonateStatus(getContext(), eventDonateStatus);
                            linearRecentContainer.setVisibility(View.VISIBLE);
                            if (recentEventsModelList.get(0).getEventDonateStatus().equals("Yes")) {
                                dontateNowLayout.setVisibility(View.VISIBLE);
                            } else {
                                dontateNowLayout.setVisibility(View.GONE);
                            }
                        } else {
                            like = "0";
                            Sharedprefrences.setPositions(getActivity(), "1");
                        }


                        recentEventsAdapter = new RecentEventsAdapter(getActivity(), recentEventsModelList, LiveFragment.this);
                        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                        recyclerViewRecentEvents.setLayoutManager(layoutManager);
                        recyclerViewRecentEvents.setAdapter(recentEventsAdapter);
                        recyclerViewRecentEvents.setNestedScrollingEnabled(false);
                        recentEventsAdapter.notifyDataSetChanged();

                        liveCatchuplist.clear();
                        JSONArray catchupArray = objMain.optJSONArray("catchups");

                        if (catchupArray.length() == 0) {
                            tvCathupHeading.setVisibility(View.GONE);
                            textViewSeeAllCatchups.setVisibility(View.GONE);
                        } else {
                            tvCathupHeading.setVisibility(View.VISIBLE);
                            textViewSeeAllCatchups.setVisibility(View.VISIBLE);
                        }

//                        }
                        for (int k = 0; k < catchupArray.length(); k++) {

                            JSONObject objOpeningArray = catchupArray.optJSONObject(k);
                            String eventId = objOpeningArray.optString("eventId");
                            String eventName = objOpeningArray.optString("eventTitle");
                            String eventStartDate = objOpeningArray.optString("eventStartDate");
                            String eventEndDate = objOpeningArray.optString("eventEndDate");
                            String eventDescription = objOpeningArray.optString("eventDescription");
                            String eventLocation = objOpeningArray.optString("eventLocation");
                            String eventDuration = objOpeningArray.optString("eventDuration");
                            String eventCode = objOpeningArray.optString("eventCode");
                            String eventImageURLCatchup = objOpeningArray.optString("eventImageURL");
                            String eventURL = objOpeningArray.optString("eventURL");
                            eventDonateStatus = objOpeningArray.optString("eventDonateStatus");
                            String eventGiftTypeId = objOpeningArray.optString("eventGiftTypeId");
                            String eventGiftRemarks = objOpeningArray.optString("eventGiftRemarks");
                            String eventTypeStatusId = objOpeningArray.optString("eventTypeStatusId");
                            String eventTypeStatus = objOpeningArray.optString("eventTypeStatus");
                            eventLikeStatus = objOpeningArray.optString("eventLikeStatus");
                            String eventLikes = objOpeningArray.optString("eventLikes");
                            String eventSeens = objOpeningArray.optString("eventSeens");
                            String eventHasMultipleParts = objOpeningArray.optString("eventHasMultipleParts");

                            CatchupModel catchupModel = new CatchupModel(eventId,
                                    eventName,
                                    eventStartDate,
                                    eventEndDate,
                                    eventDescription,
                                    eventLocation,
                                    eventDuration,
                                    eventCode,
                                    eventImageURLCatchup, eventURL,
                                    eventDonateStatus,
                                    eventGiftTypeId,
                                    eventGiftRemarks, eventTypeStatusId, eventTypeStatus, eventLikeStatus, eventLikes, eventSeens, eventHasMultipleParts);

                            liveCatchuplist.add(catchupModel);

                            Sharedprefrences.setVideoUrls(getActivity(), liveCatchuplist.get(0).getEventURL());

//                            else {
                            catchupProgramsAdapter = new LiveCatchupProgramsAdapter(getActivity(), liveCatchuplist, LiveFragment.this);
                            layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                            recyclerViewCatchupEvents.setLayoutManager(layoutManager);
                            recyclerViewCatchupEvents.setAdapter(catchupProgramsAdapter);
                            recyclerViewCatchupEvents.setNestedScrollingEnabled(false);
                            catchupProgramsAdapter.notifyDataSetChanged();

                            Sharedprefrences.setDonateStatus(getContext(), eventDonateStatus);

                            ivPlay.setVisibility(View.GONE);
                            ivPause.setVisibility(View.VISIBLE);
//                            }


                            if (!recentEventsModelList.isEmpty()) {

                                linearRecentContainer.setVisibility(View.VISIBLE);
                                if (liveCatchuplist.get(0).getEventDonateStatus().equals("Yes")) {
                                    dontateNowLayout.setVisibility(View.VISIBLE);
                                } else {
                                    dontateNowLayout.setVisibility(View.GONE);
                                }

                                tvLiveTag.setVisibility(View.VISIBLE);

                                tvHeadingEventLive.setText(recentEventsModelList.get(0).getEventName());
                                tvDescriptionEventLive.setText(recentEventsModelList.get(0).getEventDescription());
                                tvDateEventLive.setText(recentEventsModelList.get(0).getEventStartDate());
                                tvViewsLiveEvent.setText(recentEventsModelList.get(0).getEventSeens());
                                tvEventId.setText(recentEventsModelList.get(0).getEventId());
                            }

                            if (recentEventsModelList.isEmpty()) {
                                catchup = 1;
                                if (liveCatchuplist.get(0).getEventDonateStatus().equals("Yes")) {
                                    dontateNowLayout.setVisibility(View.VISIBLE);
                                } else {
                                    dontateNowLayout.setVisibility(View.GONE);
                                }

                                tvLiveTag.setVisibility(View.GONE);

                                seekBarVideo.setVisibility(View.VISIBLE);

//                                if (liveCatchuplist.get(0).getEventLikeStatus().equals("0")) {
//                                    imgLike.setVisibility(View.VISIBLE);
//                                    imgLikeOrange.setVisibility(View.GONE);
//                                }
//
//                                if (liveCatchuplist.get(0).getEventLikeStatus().equals("1")) {
//                                    imgLike.setVisibility(View.GONE);
//                                    imgLikeOrange.setVisibility(View.VISIBLE);
//                                }

                                String urls = liveCatchuplist.get(0).getEventURL();
                                Log.e("urlrecent", urls);
                                Sharedprefrences.setVideoUrl(getActivity(), urls);

                                new BackgroundAsyncTask().execute(urls);

                                tvHeadingEventLive.setText(liveCatchuplist.get(0).getEventName());
                                tvDescriptionEventLive.setText(liveCatchuplist.get(0).getEventDescription());
                                tvDateEventLive.setText(liveCatchuplist.get(0).getEventStartDate());
                                tvViewsLiveEvent.setText(liveCatchuplist.get(0).getEventSeens());
                                tvEventId.setText(liveCatchuplist.get(0).getEventId());
                            }

                            llContainer.setVisibility(View.VISIBLE);
                            pb.setVisibility(View.GONE);
                            llVideoContainer.setVisibility(View.VISIBLE);

                            scrollView.setVisibility(View.VISIBLE);

                            llVideoContainer.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                                @Override
                                public void onGlobalLayout() {
                                    llwidth = llVideoContainer.getWidth();
                                    llheight = llVideoContainer.getHeight();
                                    Log.e("h", String.valueOf(llheight));
                                    //you can add your code here on what you want to do to the height and width you can pass it as parameter or make width and height a global variable
                                    llVideoContainer.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                                }
                            });
                        }
                    } catch (Exception e) {
                        Log.e("Exception", String.valueOf(e));
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("EREROR", String.valueOf(error));
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();

                headers.put("vendorKey", Const.vendorKey);
                headers.put("deviceType", "A");
                headers.put("Authorization", Sharedprefrences.getUserKey(getActivity()));
                headers.put("Content-Type", Const.contentType);
                headers.put("userID", Sharedprefrences.getUserId(getActivity()));
                headers.put("deviceToken", Sharedprefrences.getFCMRegistrationId(getActivity()));
                headers.put("deviceMode", Const.deviceMode);


                return headers;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                return params;
            }
        };
        int socketTimeout = 20000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        sr.setRetryPolicy(policy);
        queue.add(sr);
    }

    private void getEventSeensCount() {
        RequestQueue queue = Volley.newRequestQueue(getContext());
        StringRequest sr = new StringRequest(Request.Method.POST, HTTPRequest.SERVICE_URL + "getEventDetails", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.e("ResponseEventDetails", response);

                    try {

                        fetchLiveVideosRefresh();

                    } catch (Exception e) {
                        Log.e("Exception", String.valueOf(e));
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("EREROR", String.valueOf(error));
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();

                headers.put("vendorKey", Const.vendorKey);
                headers.put("deviceType", "A");
                headers.put("Authorization", Sharedprefrences.getUserKey(getActivity()));
                headers.put("Content-Type", Const.contentType);

                return headers;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("eventId", tvEventId.getText().toString());
                return params;
            }
        };
        int socketTimeout = 20000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        sr.setRetryPolicy(policy);
        queue.add(sr);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(CURRENT, videoLayout.getCurrentPosition());
        outState.putString("URL", Sharedprefrences.getVideoUrl(getContext()));
        Log.e("rotatesss", Sharedprefrences.getVideoUrl(getContext()));
        super.onSaveInstanceState(outState);
    }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        final AudioManager audioManager = (AudioManager)getActivity().getSystemService(Context.AUDIO_SERVICE);
//        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
//            try{
//                seekbarVolume.setProgress(audioManager
//                        .getStreamVolume(AudioManager.STREAM_MUSIC) - 1);
//            } catch (Error e) {
//                // min value
//            }
//        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
//            try{
//                seekbarVolume.setProgress(audioManager
//                        .getStreamVolume(AudioManager.STREAM_MUSIC) + 1);
//            } catch (Error e) {
//                // max value
//            }
//        }
//
//        return false;
//    }

    private void initVidgets(View view) {

        ivShare = (ImageView) view.findViewById(R.id.liveShare);

        tvTimer = (TextView) view.findViewById(R.id.tvTimer);
        tvRemaining = (TextView) view.findViewById(R.id.tvRemaining);

        tvCathupHeading = (TextView) view.findViewById(R.id.heading_recycler_2);
        videoLayout = (VideoView) view.findViewById(R.id.videoview1);
        recyclerViewRecentEvents = (RecyclerView) view.findViewById(R.id.recycler_recent_events);
        recyclerViewCatchupEvents = (RecyclerView) view.findViewById(R.id.recycler_cat_events);
        tvHeadingEventLive = (TextView) view.findViewById(R.id.heading_event_live_1);
        tvDateEventLive = (TextView) view.findViewById(R.id.date_event_live_1);
        tvDescriptionEventLive = (TextView) view.findViewById(R.id.description_event_live_1);
        dontateNowLayout = (LinearLayout) view.findViewById(R.id.donate_now_layout);
        //noticeLayoutCurentEvent = (LinearLayout)view.findViewById(R.id.recent_event_banner_layout);
        textViewSeeAll = (TextView) view.findViewById(R.id.heading_see_all_recycler);
        textViewSeeAllCatchups = (TextView) view.findViewById(R.id.heading_see_all_recycler_2);
        linearRecentContainer = (LinearLayout) view.findViewById(recent_container_linear);
        linearcatchupContainer = (LinearLayout) view.findViewById(R.id.linearCatchupsContainer);
        scrollView = (NestedScrollView) view.findViewById(R.id.nestedscrollview);
        tvViewsLiveEvent = (TextView) view.findViewById(R.id.tv_views);
        pb = (ProgressBar) view.findViewById(R.id.progressbar);
        llVideoContainer = (LinearLayout) view.findViewById(R.id.frame_linear_container);
        //controls
        llContainer = (LinearLayout) view.findViewById(R.id.container_linear_rest_layout);
        ivPlay = (ImageView) view.findViewById(R.id.iv_play);
        ivPause = (ImageView) view.findViewById(R.id.iv_pause);
        ivFullscreen = (ImageView) view.findViewById(R.id.img_fullscreen);
        ivVolumeControl = (ImageView) view.findViewById(R.id.iv_sound);
        seekbarVolume = (SeekBar) view.findViewById(R.id.seekbarVolume);
        frameVideoControls = (FrameLayout) view.findViewById(R.id.framecontrols);
        tvLiveTag = (TextView) view.findViewById(R.id.tv_live_tag_img_view);
        frameController = (LinearLayout) view.findViewById(R.id.framescontroller);
        seekBarVideo = (SeekBar) view.findViewById(R.id.seekbarProgress);
        ivFullscreen.setOnClickListener(this);
        ivFullscreen.setEnabled(false);
        tvDescriptionEventLive.setMovementMethod(new ScrollingMovementMethod());
        progressDialog = new ProgressDialog(getActivity());
        btnDonateNow = (Button) view.findViewById(R.id.donate_now_btn);

        tvEventId = (TextView) view.findViewById(R.id.tvEventId);

        ivShare.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.iv_pause:
                ivPlay.setVisibility(View.VISIBLE);
                ivPause.setVisibility(View.GONE);
                videoLayout.pause();
                break;

            case R.id.iv_play:
                ivPause.setVisibility(View.VISIBLE);
                ivPlay.setVisibility(View.GONE);
                videoLayout.start();
                break;

            case R.id.heading_see_all_recycler:
                Intent intentSeeAll = new Intent(getActivity(), RecentEventSeeAllActivity.class);
                startActivity(intentSeeAll);
                break;

            case R.id.heading_see_all_recycler_2:
                Intent intentSeeAllCatchup = new Intent(getActivity(), CatchupSeeAllActivity.class);
                startActivity(intentSeeAllCatchup);
                break;

            case R.id.liveShare:

                List<Intent> targetShareIntents = new ArrayList<Intent>();
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                List<ResolveInfo> resInfos = getActivity().getPackageManager().queryIntentActivities(shareIntent, 0);
                if (!resInfos.isEmpty()) {
                    System.out.println("Have package");
                    for (ResolveInfo resInfo : resInfos) {
                        String packageName = resInfo.activityInfo.packageName;
                        Log.i("Package Name", packageName);

                        if (!(packageName.contains("com.google.android.apps.translate") || packageName.contains("com.google.android.gms.drive") || packageName.contains("com.google.android.hangouts"))) {

                            Intent intent = new Intent();
                            intent.setComponent(new ComponentName(packageName, resInfo.activityInfo.name));
                            intent.setAction(Intent.ACTION_SEND);
                            intent.setType("text/plain");
//                            intent.putExtra(Intent.EXTRA_TEXT,"watch the video - videoURL  "+ Sharedprefrences.getVideoUrl(getActivity()));
                            intent.putExtra(Intent.EXTRA_TEXT, "watch the video - " + "http://www.omsharavanabhavamatham.org.uk/dl/live");
                            intent.putExtra(Intent.EXTRA_SUBJECT, "watch the video -videoURL");
                            intent.setPackage(packageName);
                            targetShareIntents.add(intent);
                        }

                    }
                    if (!targetShareIntents.isEmpty()) {
                        System.out.println("Have Intent");
                        Intent chooserIntent = Intent.createChooser(targetShareIntents.remove(0), "Choose app to share");
                        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetShareIntents.toArray(new Parcelable[]{}));
                        startActivity(chooserIntent);
                    } else {
                        System.out.println("Do not Have Intent");
                    }
                }

                break;

            case R.id.img_fullscreen:

                int orientation = this.getResources().getConfiguration().orientation;

                if (orientation == Configuration.ORIENTATION_PORTRAIT) {

                    if (ivPause.getVisibility() == View.VISIBLE) {
                        ivPlay.setVisibility(View.GONE);
                        ivPause.setVisibility(View.VISIBLE);
                    }
                    if (ivPlay.getVisibility() == View.VISIBLE) {
                        ivPlay.setVisibility(View.VISIBLE);
                        ivPlay.setVisibility(View.VISIBLE);
                        ivPause.setVisibility(View.GONE);
                    }


                    // HomeDrawerActivity.drawer.setVisibility(View.GONE);
                    HomeDrawerActivity.drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                    getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

                    llVideoContainer.getLayoutParams().width = ViewGroup.LayoutParams.FILL_PARENT;
                    llVideoContainer.getLayoutParams().height = ViewGroup.LayoutParams.FILL_PARENT;
                    videoLayout.getLayoutParams().width = ViewGroup.LayoutParams.FILL_PARENT;
                    videoLayout.getLayoutParams().height = ViewGroup.LayoutParams.FILL_PARENT;
                    HomeDrawerActivity.bottombarContainerLinear.setVisibility(View.GONE);
                    HomeDrawerActivity.toolbar.setVisibility(View.GONE);
                    // HomeDrawerActivity.drawer.setVisibility(View.GONE);

                    HomeDrawerActivity.drawer.closeDrawer(GravityCompat.START);
                    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

                } else {

                    //  HomeDrawerActivity.drawer.openDrawer(GravityCompat.START);
                    HomeDrawerActivity.drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

                    if (ivPause.getVisibility() == View.VISIBLE) {
                        ivPlay.setVisibility(View.GONE);
                        ivPause.setVisibility(View.VISIBLE);
                    }
                    if (ivPlay.getVisibility() == View.VISIBLE) {
                        ivPlay.setVisibility(View.VISIBLE);
                        ivPause.setVisibility(View.GONE);
                    }

                    videoLayout.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    videoLayout.getLayoutParams().width = ViewGroup.LayoutParams.FILL_PARENT;
                    llVideoContainer.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;


                    HomeDrawerActivity.toolbar.setVisibility(View.VISIBLE);
                    HomeDrawerActivity.bottombarContainerLinear.setVisibility(View.VISIBLE);
                    getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                    getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
                    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }

                break;

            case R.id.donate_now_btn:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Const.donateLink));
                getContext().startActivity(browserIntent);
                break;


            default:
                break;

        }
    }

    public class BackgroundAsyncTask extends AsyncTask<String, Uri, Void> {

        protected void onPreExecute() {
            Log.e("Play", "Playing");
            seekBarVideo.setProgress(0);
            videoLayout.setVisibility(View.VISIBLE);
            llVideoContainer.getLayoutParams().width = ViewGroup.LayoutParams.FILL_PARENT;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }

        protected void onProgressUpdate(final Uri... uri) {

            try {

                videoLayout.setVideoURI(uri[0]);
                Log.e("uri", String.valueOf(uri));
                videoLayout.requestFocus();

                videoLayout.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    public void onPrepared(MediaPlayer mp) {

                        mp.setLooping(true);

                        videoLayout.start();
                        frameController.setVisibility(View.VISIBLE);
                        ivPause.setVisibility(View.VISIBLE);
                        ivPlay.setVisibility(View.GONE);
                        ivFullscreen.setEnabled(true);
                        videoLayout.requestFocus();

                        seekBarVideo.setMax(videoLayout.getDuration());


//                        seekUpdate(mp);

                        if (catchup == 0) {

                            tvTimer.setVisibility(View.GONE);
                            tvRemaining.setVisibility(View.GONE);

                        }

                        if (catchup == 1)

                        {
                            tvTimer.setVisibility(View.VISIBLE);
                            tvRemaining.setVisibility(View.VISIBLE);
                            seekBarVideo.setEnabled(true);
                            updateProgressBar();
                        }
                    }
                });

                videoLayout.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                    @Override
                    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                        videoLayout.start();
                        durationVideo = videoLayout.getDuration();
                        ivPause.setVisibility(View.VISIBLE);
                        ivPlay.setVisibility(View.GONE);

                        Log.e("Error", "error");
                        return true;
                    }
                });

            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(String... params) {
            try {
                Uri uri = Uri.parse(params[0]);
                publishProgress(uri);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }


    private void updateProgressBar() {
        mHandler.postDelayed(updateTimeTask, 100);
    }


    private Runnable updateTimeTask = new Runnable() {

        @Override
        public void run() {
            long totalDuration = videoLayout.getDuration();
            long currentDuration = videoLayout.getCurrentPosition();

//            SimpleDateFormat inputFormat = new SimpleDateFormat("HH:mm:ss");
//

            int Currentpos = videoLayout.getCurrentPosition();
            int TotalTime = videoLayout.getDuration();

            seekBarVideo.setProgress(Currentpos);


            int hrs = (int) (currentDuration / 3600000);
            int mns = (int) ((currentDuration / 60000) % 60000);
            int scs = (int) (currentDuration % 60000 / 1000);


            if (hrs == 0) {
                songTime = String.format("%02d:%02d", mns, scs);

            } else {
                songTime = String.format("%02d:%02d:%02d", hrs, mns, scs);

            }

            int timee = videoLayout.getDuration() - videoLayout.getCurrentPosition();
            int hrss = (int) (timee / 3600000);
            int mnss = (int) ((timee / 60000) % 60000);
            int scss = (int) (timee % 60000 / 1000);

            if (hrss == 0) {
                totalTime = String.format("%02d:%02d", mnss, scss);

            } else {
                totalTime = String.format("%02d:%02d:%02d", hrss, mnss, scss);

            }

            tvTimer.setText(songTime);
            tvRemaining.setText("-" + totalTime);
//            duration(currentDuration);
            mHandler.postDelayed(this, 100);

        }
    };

    public String duration(long duration) {
        String out = null;
        long hours = 0;
        try {
            hours = (duration / 3600000);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return out;
        }
        long remaining_minutes = (duration - (hours * 3600000)) / 60000;
        String minutes = String.valueOf(remaining_minutes);
        if (minutes.equals(0)) {
            minutes = "00";
        }
        long remaining_seconds = (duration - (hours * 3600000) - (remaining_minutes * 60000));
        String seconds = String.valueOf(remaining_seconds);
        if (seconds.length() < 2) {
            seconds = "00";
        } else {
            seconds = seconds.substring(0, 2);
        }

        if (hours > 0) {
            out = hours + ":" + minutes + ":" + seconds;
        } else {
            out = minutes + ":" + seconds;
        }

        tvTimer.setText(out);

        return out;

    }


    @Override
    public void onResume() {
        super.onResume();

        HomeDrawerActivity.lineHome.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.White));
        HomeDrawerActivity.iconTabHome.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_home_unselected));
        HomeDrawerActivity.lineLive.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
        HomeDrawerActivity.iconTabLive.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_live_tab));
        HomeDrawerActivity.lineUpcoming.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.White));
        HomeDrawerActivity.iconTabUpcoming.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_upcoming_unselected));
        HomeDrawerActivity.lineVideo.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.White));
        HomeDrawerActivity.iconTabVideo.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_video_tab_unselected));
        HomeDrawerActivity.lineProfile.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.White));
        HomeDrawerActivity.iconTabProfile.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_profile_tab_unselected));
        HomeDrawerActivity.baseHomeText.setTextColor(ContextCompat.getColor(getActivity(), R.color.gray));
        HomeDrawerActivity.basevideostext.setTextColor(ContextCompat.getColor(getActivity(), R.color.gray));
        HomeDrawerActivity.baseUpcomingtext.setTextColor(ContextCompat.getColor(getActivity(), R.color.gray));
        HomeDrawerActivity.baseProfileText.setTextColor(ContextCompat.getColor(getActivity(), R.color.gray));
        HomeDrawerActivity.baseLivetext.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
        videoLayout.start();
        if (refresh.equals("1")) {

            Refresh();
        }
//       fetchLiveVideosRefresh();
    }

    private void fetchLiveVideosRefresh() {
        RequestQueue queue = Volley.newRequestQueue(getContext());
        StringRequest sr = new StringRequest(Request.Method.POST, HTTPRequest.SERVICE_URL + "getLiveTv", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.e("Refresh", response);

                    try {

                        JSONObject obj = new JSONObject(response);
                        JSONObject objMain = obj.optJSONObject("response");

                        recentEventsModelList.clear();
                        JSONArray recentEventsArray = objMain.optJSONArray("recent_events");

                        for (int i = 0; i < recentEventsArray.length(); i++) {

                            JSONObject objOpeningArray = recentEventsArray.optJSONObject(i);
                            String eventId = objOpeningArray.optString("eventId");
                            String eventName = objOpeningArray.optString("eventTitle");
                            String eventStartDate = objOpeningArray.optString("eventStartDate");
                            String eventEndDate = objOpeningArray.optString("eventEndDate");
                            String eventDescription = objOpeningArray.optString("eventDescription");
                            String eventLocation = objOpeningArray.optString("eventLocation");
                            String eventDuration = objOpeningArray.optString("eventDuration");
                            String eventCode = objOpeningArray.optString("eventCode");
                            String eventImageURL = objOpeningArray.optString("eventImageURL");
                            String eventURL = objOpeningArray.optString("eventURL");
                            String eventDonateStatus = objOpeningArray.optString("eventDonateStatus");
                            String eventGiftTypeId = objOpeningArray.optString("eventGiftTypeId");
                            String eventGiftRemarks = objOpeningArray.optString("eventGiftRemarks");
                            String eventTypeStatusId = objOpeningArray.optString("eventTypeStatusId");
                            String eventTypeStatus = objOpeningArray.optString("eventTypeStatus");
                            eventLikeStatus = objOpeningArray.optString("eventLikeStatus");
                            String eventLikes = objOpeningArray.optString("eventLikes");
                            String eventSeens = objOpeningArray.optString("eventSeens");

                            RecentEventsModel recentEventsModel = new RecentEventsModel(eventId, eventName, eventStartDate, eventEndDate, eventDescription, eventLocation, eventDuration, eventCode, eventImageURL, eventURL, eventDonateStatus, eventGiftTypeId, eventGiftRemarks, eventTypeStatusId, eventTypeStatus, eventLikeStatus, eventLikes, eventSeens);
                            recentEventsModelList.add(recentEventsModel);

                        }
                        recentEventsAdapter.notifyDataSetChanged();

                        liveCatchuplist.clear();
                        JSONArray catchupArray = objMain.optJSONArray("catchups");

                        for (int k = 0; k < catchupArray.length(); k++) {

                            JSONObject objOpeningArray = catchupArray.optJSONObject(k);

                            String eventId = objOpeningArray.optString("eventId");
                            String eventName = objOpeningArray.optString("eventTitle");
                            String eventStartDate = objOpeningArray.optString("eventStartDate");
                            String eventEndDate = objOpeningArray.optString("eventEndDate");
                            String eventDescription = objOpeningArray.optString("eventDescription");
                            String eventLocation = objOpeningArray.optString("eventLocation");
                            String eventDuration = objOpeningArray.optString("eventDuration");
                            String eventCode = objOpeningArray.optString("eventCode");
                            String eventImageURLCatchup = objOpeningArray.optString("eventImageURL");
                            String eventURL = objOpeningArray.optString("eventURL");
                            eventDonateStatus = objOpeningArray.optString("eventDonateStatus");
                            String eventGiftTypeId = objOpeningArray.optString("eventGiftTypeId");
                            String eventGiftRemarks = objOpeningArray.optString("eventGiftRemarks");
                            String eventTypeStatusId = objOpeningArray.optString("eventTypeStatusId");
                            String eventTypeStatus = objOpeningArray.optString("eventTypeStatus");
                            eventLikeStatus = objOpeningArray.optString("eventLikeStatus");
                            String eventLikes = objOpeningArray.optString("eventLikes");
                            String eventSeens = objOpeningArray.optString("eventSeens");
                            String eventHasMultipleParts = objOpeningArray.optString("eventHasMultipleParts");

                            CatchupModel catchupModel = new CatchupModel(eventId,
                                    eventName,
                                    eventStartDate,
                                    eventEndDate,
                                    eventDescription,
                                    eventLocation,
                                    eventDuration,
                                    eventCode,
                                    eventImageURLCatchup, eventURL,
                                    eventDonateStatus,
                                    eventGiftTypeId,
                                    eventGiftRemarks, eventTypeStatusId, eventTypeStatus, eventLikeStatus, eventLikes, eventSeens, eventHasMultipleParts);

                            liveCatchuplist.add(catchupModel);

                            Sharedprefrences.setDonateStatus(getContext(), eventDonateStatus);

                            ivPlay.setVisibility(View.GONE);
                            ivPause.setVisibility(View.VISIBLE);


                            llVideoContainer.setVisibility(View.VISIBLE);

                            scrollView.setVisibility(View.VISIBLE);


                        }


                        if (like.equals("1")) {
                            try {
                                Log.e("star", "1");
                                RefreshLikeData(recentEventsModelList.get(Integer.parseInt(Sharedprefrences.getPostions(getActivity()))));

                            } catch (Exception e) {
                            }
                        }


                        if (like.equals("0")) {
                            try {
                                Log.e("star", "2");
                                RefreshData(liveCatchuplist.get(Integer.parseInt(Sharedprefrences.getPostions(getActivity()))));

                            } catch (Exception e) {
                            }
                        }


                    } catch (Exception e) {
                        Log.e("Exception", String.valueOf(e));
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("EREROR", String.valueOf(error));
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();


                headers.put("vendorKey", Const.vendorKey);
                headers.put("deviceType", "A");
                headers.put("Authorization", Sharedprefrences.getUserKey(getActivity()));
                headers.put("Content-Type", Const.contentType);
                headers.put("userID", Sharedprefrences.getUserId(getActivity()));
                headers.put("deviceToken", Sharedprefrences.getFCMRegistrationId(getActivity()));
                headers.put("deviceMode", Const.deviceMode);

                return headers;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                return params;
            }
        };
        int socketTimeout = 20000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        sr.setRetryPolicy(policy);
        queue.add(sr);
    }

    private void RefreshData(CatchupModel liveCatchuplist) {

        Log.e("running", "Catchup");


        catchupProgramsAdapter.notifyDataSetChanged();
        tvViewsLiveEvent.setText(liveCatchuplist.getEventSeens());
        tvEventId.setText(liveCatchuplist.getEventId());

    }

    private void RefreshLikeData(RecentEventsModel recentCatchuplist) {

        Log.e("running", "Live");


        recentEventsAdapter.notifyDataSetChanged();
        tvViewsLiveEvent.setText(recentCatchuplist.getEventSeens());
        tvEventId.setText(recentCatchuplist.getEventId());

    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e("pause", "onPause");
        videoLayout.pause();
        refresh = "1";
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    private void Refresh() {
        RequestQueue queue = Volley.newRequestQueue(getContext());
        StringRequest sr = new StringRequest(Request.Method.POST, HTTPRequest.SERVICE_URL + "getLiveTv", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.e("LIVE RESPONSE", response);

                    dontateNowLayout.setVisibility(View.GONE);

                    try {

                        JSONObject obj = new JSONObject(response);
                        JSONObject objMain = obj.optJSONObject("response");

                        recentEventsModelList.clear();
                        JSONArray recentEventsArray = objMain.optJSONArray("recent_events");

                        if (recentEventsArray.length() > 0) {
                            Log.e("LIST ITEM", " OUT OF LOOP" + recentEventsArray);

                            for (int i = 0; i < recentEventsArray.length(); i++) {

                                JSONObject objOpeningArray = recentEventsArray.optJSONObject(i);
                                String eventId = objOpeningArray.optString("eventId");
                                String eventName = objOpeningArray.optString("eventTitle");
                                String eventStartDate = objOpeningArray.optString("eventStartDate");
                                String eventEndDate = objOpeningArray.optString("eventEndDate");
                                String eventDescription = objOpeningArray.optString("eventDescription");
                                String eventLocation = objOpeningArray.optString("eventLocation");
                                String eventDuration = objOpeningArray.optString("eventDuration");
                                String eventCode = objOpeningArray.optString("eventCode");
                                String eventImageURL = objOpeningArray.optString("eventImageURL");
                                String eventURL = objOpeningArray.optString("eventURL");
                                String eventDonateStatus = objOpeningArray.optString("eventDonateStatus");
                                String eventGiftTypeId = objOpeningArray.optString("eventGiftTypeId");
                                String eventGiftRemarks = objOpeningArray.optString("eventGiftRemarks");
                                String eventTypeStatusId = objOpeningArray.optString("eventTypeStatusId");
                                String eventTypeStatus = objOpeningArray.optString("eventTypeStatus");
                                eventLikeStatus = objOpeningArray.optString("eventLikeStatus");
                                String eventLikes = objOpeningArray.optString("eventLikes");
                                String eventSeens = objOpeningArray.optString("eventSeens");


                                RecentEventsModel recentEventsModel = new RecentEventsModel(eventId, eventName, eventStartDate, eventEndDate, eventDescription, eventLocation, eventDuration, eventCode, eventImageURL, eventURL, eventDonateStatus, eventGiftTypeId, eventGiftRemarks, eventTypeStatusId, eventTypeStatus, eventLikeStatus, eventLikes, eventSeens);
                                recentEventsModelList.add(recentEventsModel);

                                if (recentEventsModelList.size() != 0) {

                                    Sharedprefrences.setDonateStatus(getContext(), eventDonateStatus);
                                    linearRecentContainer.setVisibility(View.VISIBLE);
                                    if (recentEventsModelList.get(0).getEventDonateStatus().equals("Yes")) {
                                        dontateNowLayout.setVisibility(View.VISIBLE);
                                    }
                                }
                            }
//                            recentEventsAdapter = new RecentEventsAdapter(getActivity(), recentEventsModelList, LiveFragment.this);
//                            layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
//                            recyclerViewRecentEvents.setLayoutManager(layoutManager);
//                            recyclerViewRecentEvents.setAdapter(recentEventsAdapter);
//                            recyclerViewRecentEvents.setNestedScrollingEnabled(false);
                            recentEventsAdapter.notifyDataSetChanged();
                        } else {

                            //  Toast.makeText(getActivity(), "recent events list not available", Toast.LENGTH_SHORT).show();
                        }

                        liveCatchuplist.clear();
                        JSONArray catchupArray = objMain.optJSONArray("catchups");


                        if (catchupArray.length() > 0) {
                            for (int k = 0; k < catchupArray.length(); k++) {

                                JSONObject objOpeningArray = catchupArray.optJSONObject(k);
                                String eventId = objOpeningArray.optString("eventId");
                                String eventName = objOpeningArray.optString("eventTitle");
                                String eventStartDate = objOpeningArray.optString("eventStartDate");
                                String eventEndDate = objOpeningArray.optString("eventEndDate");
                                String eventDescription = objOpeningArray.optString("eventDescription");
                                String eventLocation = objOpeningArray.optString("eventLocation");
                                String eventDuration = objOpeningArray.optString("eventDuration");
                                String eventCode = objOpeningArray.optString("eventCode");
                                String eventImageURLCatchup = objOpeningArray.optString("eventImageURL");
                                String eventURL = objOpeningArray.optString("eventURL");
                                eventDonateStatus = objOpeningArray.optString("eventDonateStatus");
                                String eventGiftTypeId = objOpeningArray.optString("eventGiftTypeId");
                                String eventGiftRemarks = objOpeningArray.optString("eventGiftRemarks");
                                String eventTypeStatusId = objOpeningArray.optString("eventTypeStatusId");
                                String eventTypeStatus = objOpeningArray.optString("eventTypeStatus");
                                eventLikeStatus = objOpeningArray.optString("eventLikeStatus");
                                String eventLikes = objOpeningArray.optString("eventLikes");
                                String eventSeens = objOpeningArray.optString("eventSeens");
                                String eventHasMultipleParts = objOpeningArray.optString("eventHasMultipleParts");

                                CatchupModel catchupModel = new CatchupModel(eventId,
                                        eventName,
                                        eventStartDate,
                                        eventEndDate,
                                        eventDescription,
                                        eventLocation,
                                        eventDuration,
                                        eventCode,
                                        eventImageURLCatchup, eventURL,
                                        eventDonateStatus,
                                        eventGiftTypeId,
                                        eventGiftRemarks, eventTypeStatusId, eventTypeStatus, eventLikeStatus, eventLikes, eventSeens, eventHasMultipleParts);
                                liveCatchuplist.add(catchupModel);

                                Sharedprefrences.setVideoUrls(getActivity(), liveCatchuplist.get(0).getEventURL());

//                            else {
//                                catchupProgramsAdapter = new LiveCatchupProgramsAdapter(getActivity(), liveCatchuplist, LiveFragment.this);
//                                layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
//                                recyclerViewCatchupEvents.setLayoutManager(layoutManager);
//                                recyclerViewCatchupEvents.setAdapter(catchupProgramsAdapter);
//                                recyclerViewCatchupEvents.setNestedScrollingEnabled(false);
                                catchupProgramsAdapter.notifyDataSetChanged();

                                Sharedprefrences.setDonateStatus(getContext(), eventDonateStatus);

                                ivPlay.setVisibility(View.GONE);
                                ivPause.setVisibility(View.VISIBLE);
//                            }

                                if (!recentEventsModelList.isEmpty()) {
                                    linearRecentContainer.setVisibility(View.VISIBLE);
                                    if (liveCatchuplist.get(0).getEventDonateStatus().equals("Yes")) {

                                        dontateNowLayout.setVisibility(View.VISIBLE);
                                    }

                                    RecentEventsAdapter.clickedPosition = 0;
                                    recentEventsAdapter.notifyDataSetChanged();

                                    LiveCatchupProgramsAdapter.clickedPosition = -2;
                                    catchupProgramsAdapter.notifyDataSetChanged();

                                    catchup = 0;

                                    tvLiveTag.setVisibility(View.VISIBLE);
                                    new BackgroundAsyncTask().execute(recentEventsModelList.get(0).getEventURL());
                                    tvHeadingEventLive.setText(recentEventsModelList.get(0).getEventName());
                                    tvDescriptionEventLive.setText(recentEventsModelList.get(0).getEventDescription());
                                    tvDateEventLive.setText(recentEventsModelList.get(0).getEventStartDate());
                                    tvViewsLiveEvent.setText(recentEventsModelList.get(0).getEventSeens());
                                    tvEventId.setText(recentEventsModelList.get(0).getEventId());
                                }

                                if (recentEventsModelList.isEmpty()) {

                                    RecentEventsAdapter.clickedPosition = -2;
                                    recentEventsAdapter.notifyDataSetChanged();

                                    LiveCatchupProgramsAdapter.clickedPosition = 0;
                                    catchupProgramsAdapter.notifyDataSetChanged();


                                    if (liveCatchuplist.get(0).getEventDonateStatus().equals("Yes")) {
                                        dontateNowLayout.setVisibility(View.VISIBLE);
                                    }

                                    tvLiveTag.setVisibility(View.GONE);


//dQfdqDPjreU:APA91bHbG-mVoJDc31ueLHa4kmhGjBQIKld4l6MCCdxCzfHFG5nszr4m0nGD6JOtao7Z-Ym26TOCvPJfkOjAsUfOpfEr1_eqWvqUOvD56ULvjnWFvIOna2KiIqSx2zO7bk44Bhmfx_L-
                                    catchup = 1;
                                    new BackgroundAsyncTask().execute(liveCatchuplist.get(0).getEventURL());

                                    tvHeadingEventLive.setText(liveCatchuplist.get(0).getEventName());
                                    tvDescriptionEventLive.setText(liveCatchuplist.get(0).getEventDescription());
                                    tvDateEventLive.setText(liveCatchuplist.get(0).getEventStartDate());
                                    tvViewsLiveEvent.setText(liveCatchuplist.get(0).getEventSeens());
                                    tvEventId.setText(liveCatchuplist.get(0).getEventId());
                                }

                                llVideoContainer.setVisibility(View.VISIBLE);

                                scrollView.setVisibility(View.VISIBLE);
                            }
                        } else {
                            // Toast.makeText(getActivity(), "catchup list not available", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        Log.e("Exception", String.valueOf(e));
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("EREROR", String.valueOf(error));
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();

                headers.put("vendorKey", Const.vendorKey);
                headers.put("deviceType", "A");
                headers.put("Authorization", Sharedprefrences.getUserKey(getActivity()));
                headers.put("Content-Type", Const.contentType);
                headers.put("userID", Sharedprefrences.getUserId(getActivity()));
                headers.put("deviceToken", Sharedprefrences.getFCMRegistrationId(getActivity()));
                headers.put("deviceMode", Const.deviceMode);

                return headers;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                return params;
            }
        };
        int socketTimeout = 20000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        sr.setRetryPolicy(policy);
        queue.add(sr);
    }
}

