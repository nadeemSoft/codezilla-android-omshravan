package com.zecast.live.om_saravanabava.model;

public interface UrlConstant {
    int GET = 1;
    int POST = 2;
    int DELETE = 3;
    int PUT = 4;

    int CONNECTION_TIME_OUT = 20;
    int SOCKET_TIME_OUT = 25 ;

    String BASE_URL = "http://cdn.zecast.com/saravanabhava/";
    //String BASE_URL = "http://cdn.zecast.com/zecast/";
    Integer DEVICE_DEBUG_MODE = 1;
    Integer DEVICE_RELEASE_MODE = 2;

    String API_URL = BASE_URL + "api/";
    //String API_URL = BASE_URL + "api/";

}
