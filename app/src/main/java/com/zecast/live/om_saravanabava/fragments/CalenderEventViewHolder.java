package com.zecast.live.om_saravanabava.fragments;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.zecast.live.om_saravanabava.R;



public class CalenderEventViewHolder extends RecyclerView.ViewHolder {

    public TextView visibleText;
    public TextView reminderText;
    public TextView timingText;
    public TextView heading;
    public TextView subHeading;
    public TextView likeCount;
    public TextView commentCount;
    public TextView day;
    public TextView month;
    public TextView year;
    public TextView liveTag;
    public ImageButton likeBtn;
    public LinearLayout llParent;
    public RelativeLayout llReminder;
    public ImageView reminderIcon;
     public ImageView logoImg;

    /*calender_event_item_view*/

    public CalenderEventViewHolder(LayoutInflater inflater, ViewGroup parent) {
        super(inflater.inflate(R.layout.single_row_item_calender_event_adapter_2, parent, false));
      /*  visibleText = (TextView) itemView.findViewById(R.id.calender_visible_text);
        reminderText = (TextView) itemView.findViewById(R.id.calender_reminder_text);
     */   timingText = (TextView) itemView.findViewById(R.id.calender_time_text);
        heading = (TextView) itemView.findViewById(R.id.calender_heading_text);
        subHeading = (TextView) itemView.findViewById(R.id.calender_sub_heading_text);
        likeCount = (TextView) itemView.findViewById(R.id.calender_like);
        month = (TextView) itemView.findViewById(R.id.calender_month_text);
        year = (TextView) itemView.findViewById(R.id.calender_year_text);
        visibleText = (TextView) itemView.findViewById(R.id.calender_visible_text);
        logoImg = (ImageView) itemView.findViewById(R.id.img_event_poster);
        //   commentCount = (TextView) itemView.findViewById(R.id.calender_comment);
        /*day = (TextView) itemView.findViewById(R.id.calender_day_text);
        month = (TextView) itemView.findViewById(R.id.calender_month_text);
        year = (TextView) itemView.findViewById(R.id.calender_year_text);
        likeBtn = (ImageButton) itemView.findViewById(R.id.calender_like_icon);
        llReminder = (RelativeLayout) itemView.findViewById(R.id.reminder_layout);*/
        llParent = (LinearLayout) itemView.findViewById(R.id.parent_layout);
        liveTag = (TextView)itemView.findViewById(R.id.taglive);
        //reminderIcon = (ImageView) itemView.findViewById(R.id.calender_reminder_icon);
    }
}
