package com.zecast.live.om_saravanabava.adapter;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.squareup.picasso.Picasso;
import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.model.ExperienceModel;
import com.zecast.live.om_saravanabava.players.YoutubeActivity;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class ExperiecnceAdapter extends RecyclerView.Adapter<ExperiecnceAdapter.MyViewHolder> {

    private Context context;
    ArrayList<ExperienceModel> arrayListData;
    public static SeekBar seekBarVideo, seekbarVolume;
    public static ImageButton ivPlay, ivPause, ivVolumeControl, ivFullscreen;

    public int clickedPosition = -1;
    public boolean val = false;

    public ExperiecnceAdapter(Context context, ArrayList<ExperienceModel> mAudioList) {
        this.context = context;
        this.arrayListData = mAudioList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = View.inflate(context, R.layout.single_row_item_experience_adapter, null);
        return new ExperiecnceAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final DisplayMetrics dm = new DisplayMetrics();
        ((FragmentActivity)context).getWindowManager().getDefaultDisplay().getMetrics(dm);
        double x = Math.pow(dm.widthPixels/dm.xdpi,2);
        double y = Math.pow(dm.heightPixels/dm.ydpi,2);
        final double screenInches = Math.sqrt(x+y);
        Log.e("debug","Screen inches : " + screenInches);

        Display display =  ((FragmentActivity)context).getWindowManager().getDefaultDisplay();
        final int width = display.getWidth();
        int height = display.getHeight();
        Log.e("sizes",String.valueOf(width+" "+height));

        if (position == clickedPosition) {
            val = true;

            holder.btn_play.setBackgroundResource(R.color.colorPrimary);
            holder.frameCointrols.setVisibility(View.GONE);
            holder.btnPlay.setVisibility(View.GONE);
            holder.btnPause.setVisibility(View.GONE);
            holder.seekbar.setVisibility(View.GONE);
            holder.frameCointrols.setVisibility(View.VISIBLE);

        }
        else
        {

            holder.btn_play.setBackgroundResource(R.color.gray);
            holder.frameCointrols.setVisibility(View.GONE);
        }


        holder.tvViewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (arrayListData.get(position).getExpreiencedesc().isEmpty()) {
                    return;
                }

                else {

                    if (screenInches < 5)
                    {
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        params.height = 680;
                        params.width=params.WRAP_CONTENT;

                        params.setMargins(15,5,15,5);
                        holder.llContainer.setLayoutParams(params);
                        holder.llContainer.setElevation(5);
                        holder.llContainer.setVisibility(View.VISIBLE);
                        holder.tvViewDetails.setVisibility(View.GONE);
                        holder.tvDetails1.setVisibility(View.VISIBLE);
                    }

                    if (screenInches > 5)
                    {
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        params.height = 1200;
                        params.width=params.WRAP_CONTENT;

                        params.setMargins(15,5,15,5);
                        holder.llContainer.setLayoutParams(params);
                        holder.llContainer.setElevation(5);
                        holder.llContainer.setVisibility(View.VISIBLE);
                        holder.tvViewDetails.setVisibility(View.GONE);
                        holder.tvDetails1.setVisibility(View.VISIBLE);
                    }
                }


            }
        });

        holder.tvDetails1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (screenInches < 5) {
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    params.height =  680;
                    params.width=params.WRAP_CONTENT;
                    params.setMargins(15,2,15,2);
                    holder.llContainer.setLayoutParams(params);
                    holder.llContainer.setElevation(5);
                    holder.llContainer.setVisibility(View.VISIBLE);
                    holder.tvViewDetails.setVisibility(View.VISIBLE);
                    holder.tvDetails1.setVisibility(View.GONE);
                }

                if (screenInches > 5)
                {
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    params.height = -1200;
                    params.width=params.WRAP_CONTENT;

                    params.setMargins(15,2,15,2);
                    holder.llContainer.setLayoutParams(params);
                    holder.llContainer.setElevation(5);
                    holder.llContainer.setVisibility(View.VISIBLE);
                    holder.tvViewDetails.setVisibility(View.VISIBLE);
                    holder.tvDetails1.setVisibility(View.GONE);
                }
            }
        });


        final ExperienceModel experienceModel = arrayListData.get(position);
        holder.tvTitle.setText(experienceModel.getExperienceName());
       // holder.tvTitle1.setText(experienceModel.getExperienceName());

        Picasso.with(context)
                .load(experienceModel.getExperienceLogo())
                .error(R.drawable.app_icon)
                .fit()
                .placeholder(R.drawable.app_icon)
                .into(holder.imgViewCharityLogo);

        holder.tvDescription.setText(experienceModel.getExpreiencedesc());

        holder.btn_play.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                if (experienceModel.getExperiencevideoId().isEmpty()) {

                    holder.tvName.setText(experienceModel.getExperienceName());

                    if (holder.frameCointrols.getVisibility()==View.VISIBLE)
                    {
                        holder.btn_play.setBackgroundResource(R.color.gray);
                        holder.frameCointrols.setVisibility(View.GONE);
                    }

                    else {

                        if (position == 0)
                        {


                            if (screenInches < 5)
                            {
                                holder.tvName.getLayoutParams().width = ViewGroup.LayoutParams.FILL_PARENT;
                                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                                params.height = 322;
                                params.width=params.FILL_PARENT;
                                holder.videoLayout.setLayoutParams(params);

                            }

                            if (screenInches > 5)
                            {
                                holder.tvName.getLayoutParams().width = ViewGroup.LayoutParams.FILL_PARENT;
                                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                                params.height = 710;
                                params.width=params.FILL_PARENT;
                                holder.videoLayout.setLayoutParams(params);
                            }
                        }

                        else
                        {




                            if (screenInches < 5)
                            {
                                holder.tvName.getLayoutParams().width = ViewGroup.LayoutParams.FILL_PARENT;
                                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                                params.height = 370;
                                params.width=params.FILL_PARENT;

                                holder.videoLayout.setLayoutParams(params);

                            }

                            if (screenInches > 5)
                            {
                                holder.tvName.getLayoutParams().width = ViewGroup.LayoutParams.FILL_PARENT;
                                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                                params.height = 820;
                                params.width=params.FILL_PARENT;

                                holder.videoLayout.setLayoutParams(params);

                            }
                        }
                        Toast.makeText(context,"Please wait Video is Starting..!",Toast.LENGTH_SHORT).show();
                        Uri video = Uri.parse(experienceModel.getExpreienceUrl());
                        holder.videoLayout.setVideoURI(video);
                        holder.videoLayout.getLayoutParams().width = ViewGroup.LayoutParams.FILL_PARENT;
                        clickedPosition = position;

                        holder.videoLayout.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                            @Override
                            public void onPrepared(MediaPlayer mp) {

                                mp.setLooping(true);

                                holder.btnPlay.setVisibility(View.GONE);
                                holder.btnPause.setVisibility(View.VISIBLE);
                                holder.seekbar.setVisibility(View.VISIBLE);
                                holder.frameCointrols.setBackgroundResource(R.color.gray);
                                holder.tvName.setText(experienceModel.getExperienceName());
                                holder.videoLayout.start();
                                holder.seekbar.setMax(holder.videoLayout.getDuration());
                            }
                        });

                        holder.videoLayout.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                            @Override
                            public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {

                                holder.btnPlay.setVisibility(View.VISIBLE);
                                holder.btnPause.setVisibility(View.GONE);
                                Toast.makeText(context,"Cant play Video",Toast.LENGTH_SHORT).show();

                                Log.e("Error", "error");
                                return true;
                            }
                        });

                        notifyDataSetChanged();
                    }

                } else {

                    holder.frameCointrols.setVisibility(View.GONE);
                    Intent in = new Intent(context, YoutubeActivity.class);
                    in.putExtra("URL", experienceModel.getExperiencevideoId());
                    context.startActivity(in);
                }
            }
        });

        holder.btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                holder.btnPause.setVisibility(View.VISIBLE);
                holder.btnPlay.setVisibility(View.GONE);
                holder.videoLayout.start();

            }
        });

        holder.btnPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                holder.btnPause.setVisibility(View.GONE);
                holder.btnPlay.setVisibility(View.VISIBLE);
                holder.videoLayout.pause();

            }
        });


        holder.seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {


                if (b)
                {

                    holder.videoLayout.seekTo(i);
                }

//                holder.videoLayout.start();

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayListData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle, tvDescription, tvViewDetails,tvName,tvDetails1,tvTitle1;

        ImageView btnPlay,btnPause,btnImageButton;
        Button btn_play;
        FrameLayout frameCointrols;
        CircleImageView imgViewCharityLogo;
        VideoView videoLayout;
        SeekBar seekbar;
        CardView llContainer;

        public MyViewHolder(View itemView) {

            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.title_tv);
           // tvTitle1 = (TextView) itemView.findViewById(R.id.title_tv_1);
            tvDescription = (TextView) itemView.findViewById(R.id.desc_tv);
            tvViewDetails = (TextView) itemView.findViewById(R.id.tv_view_details);
            imgViewCharityLogo = (CircleImageView) itemView.findViewById(R.id.image__charity_logo);
            btnPlay = (ImageView)itemView.findViewById(R.id.iv_play);
            btnPause = (ImageView)itemView.findViewById(R.id.iv_pause);
            btn_play = (Button)itemView.findViewById(R.id.btn_play);
            frameCointrols = (FrameLayout) itemView.findViewById(R.id.framecontrols);
            seekbar = (SeekBar)itemView.findViewById(R.id.seekbarProgress_1);
            videoLayout = (VideoView) itemView.findViewById(R.id.videoview2_1);
            btnImageButton = (ImageView)itemView.findViewById(R.id.play_live);
            tvName = (TextView)itemView.findViewById(R.id.name_live);
            llContainer = (CardView) itemView.findViewById(R.id.card_view_row_3);
            tvDetails1 = (TextView)itemView.findViewById(R.id.tv_view_details1);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }



}