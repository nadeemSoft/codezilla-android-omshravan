package com.zecast.live.om_saravanabava.helper;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;


public class Utility {

    private static int widthPixels = 0;

    public static float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    public static LinearLayout.LayoutParams setLinearTopImageSize(View img, Activity context, int widthRatio, int heightRatio) {
        Log.d("ratio", "width : " + widthRatio + " height " + heightRatio);
        int width = getDeviceWidth(context);
        Log.d("ratio", "screen width : " + width);

        if(widthRatio != 0) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, (((width * heightRatio) / widthRatio) * 100) / 100);
            img.setLayoutParams(params);
            Log.d("ratio", "calculate width : " + width + " calculate height : " + (((width * heightRatio) / widthRatio) * 100)/100);
            return params;
        }else {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, (((width * 1) / 1) * 100) / 100);
            img.setLayoutParams(params);
            return params;
        }
    }

    public static LinearLayout.LayoutParams setLinearMiddleImageSize(View img, Activity context, int widthRatio, int heightRatio) {
        Log.d("ratio", "width : " + widthRatio + " height " + heightRatio);
        int width = getDeviceWidth(context);
        Log.d("ratio", "screen width : " + width);

        if(widthRatio != 0) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, (((width * heightRatio) / widthRatio) * 100) / 100);
            img.setLayoutParams(params);
            Log.d("ratio", "setMiddleImageSize width : " + width + " setMiddleImageSize height : " + (((width * heightRatio) / widthRatio) * 100)/100);
            return params;
        }else {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, (((width * 1) / 1) * 100) / 100);
            img.setLayoutParams(params);
            return params;
        }
    }

    public static LinearLayout.LayoutParams setLinearBottomImageSize(View img, Activity context, int widthRatio, int heightRatio) {
        Log.d("ratio", "width : " + widthRatio + " height " + heightRatio);
        int width = getDeviceWidth(context);
        Log.d("ratio", "screen width : " + width);

        if(widthRatio != 0) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, (((width * heightRatio) / widthRatio) * 100) / 100);
            img.setLayoutParams(params);
            Log.d("ratio", "setMiddleImageSize width : " + width + " setMiddleImageSize height : " + (((width * heightRatio) / widthRatio) * 100)/100);
            return params;
        }else {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, (((width * 1) / 1) * 100) / 100);
            img.setLayoutParams(params);
            return params;
        }
    }

    public static int getDeviceWidth(Activity context) {
        if (widthPixels == 0) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            widthPixels = displayMetrics.widthPixels;
        }
        return widthPixels;
    }

}