package com.zecast.live.om_saravanabava.activity.welcome;

import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;

import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.fragments.ProfileFragment;
import com.zecast.live.om_saravanabava.helper.ValidationUtils;
import com.zecast.live.om_saravanabava.activity.signup.SignupActivity;

public class WelcomeActivity extends Fragment implements View.OnClickListener {

    Button buttonJoinNow;
    LinearLayout containerLinear;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_welcome,container,false);

        buttonJoinNow = (Button)view.findViewById(R.id.btn_join_now);
        containerLinear = (LinearLayout)view.findViewById(R.id.container_linear);

        getActivity().setTitle("Welcome");

        buttonJoinNow.setOnClickListener(this);

        Window window = getActivity().getWindow();
        window.setStatusBarColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (!ValidationUtils.isInternetAvailable(getActivity())) {

                    //  buttonJoinNow.setEnabled(false);
                    Snackbar snackbar = Snackbar.make(containerLinear, "Please check your internet connection", Snackbar.LENGTH_LONG);

                    snackbar.setAction("SETTINGS", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);

                            // buttonJoinNow.setEnabled(true);
                        }
                    });
                    snackbar.setActionTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimary));
                    snackbar.show();
                }
                if (ValidationUtils.isInternetAvailable(getActivity())){
                    //  buttonJoinNow.setEnabled(true);

                }
            }
        },600);



        return view;
    }


     @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btn_join_now:

                if (!ValidationUtils.isInternetAvailable(getActivity())) {

                 //   buttonJoinNow.setEnabled(false);
                    Snackbar snackbar = Snackbar.make(containerLinear, "Please check your internet connection", Snackbar.LENGTH_LONG);

                    snackbar.setAction("SETTINGS", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);

                          //  buttonJoinNow.setEnabled(true);
                        }
                    });
                    snackbar.setActionTextColor(ContextCompat.getColor(getActivity(),R.color.colorPrimary));
                    snackbar.show();
                }
                if (ValidationUtils.isInternetAvailable(getActivity())){

                    FragmentTransaction fg5 = getActivity().getSupportFragmentManager().beginTransaction(); //to begin home page with fragment
                    fg5.replace(R.id.frame_drawer, new SignupActivity());
                    fg5.commit();

//                    Intent navigatetoSignup = new Intent(WelcomeActivity.this, SignupActivity.class);
//                    startActivity(navigatetoSignup);
                  //  buttonJoinNow.setEnabled(true);
                }

                break;
        }

    }

}
