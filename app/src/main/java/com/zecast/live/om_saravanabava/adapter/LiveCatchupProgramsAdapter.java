package com.zecast.live.om_saravanabava.adapter;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Parcelable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.fragments.LiveFragment;
import com.zecast.live.om_saravanabava.model.CatchupModel;

import java.util.ArrayList;
import java.util.List;

public class LiveCatchupProgramsAdapter extends RecyclerView.Adapter<LiveCatchupProgramsAdapter.MyViewHolder>  {

    private Context context;
    ArrayList<CatchupModel> arrayListData;
    LiveFragment fragment;

    public static int clickedPosition = -1;
    public boolean val = false;

    public LiveCatchupProgramsAdapter(Context context, ArrayList<CatchupModel> mCatchupList, LiveFragment fragment) {
        this.context = context;
        this.arrayListData = mCatchupList;
        this.fragment = fragment;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = View.inflate(context, R.layout.single_row_item_live_catchup_videos,null);
        return new LiveCatchupProgramsAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder,final  int position) {

        final CatchupModel catchupVideosModel = arrayListData.get(position);
        holder.tvLikeessCatchup.setText(catchupVideosModel.getEventLikes());

        if (LiveFragment.recentEventsModelList.isEmpty()) {
            if (clickedPosition == -2) {
                val = true;
                clickedPosition = 2;
                holder.llMain.setBackgroundResource(0);
            }

            if (clickedPosition == -1) {
                val = true;
                clickedPosition = 0;

                holder.llMain.setBackgroundResource(R.color.colorPrimary);
            }
        }
        if (position == clickedPosition) {
            val = true;

            holder.llMain.setBackgroundResource(R.color.colorPrimary);
        } else {
            holder.llMain.setBackgroundResource(0);
        }

        Log.e("EVENT IMAGE", "onBindViewHolder: " + catchupVideosModel.getEventImageURL());

        Picasso.with(context)
                .load(catchupVideosModel.getEventImageURL())
                .error(R.drawable.app_icon)
                .placeholder(R.drawable.app_icon)
                .into(holder.imageViewCatchuplogo);

        holder.tvTitle.setText(catchupVideosModel.getEventName());
        holder.tvViews.setText(catchupVideosModel.getEventSeens());

        if (catchupVideosModel.getEventDonateStatus().equals("N0")) {

        }

        holder.imageViewPlayIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LiveFragment.tvLiveTag.setVisibility(View.GONE);
                clickedPosition = position;
                fragment.callAsnycTask(catchupVideosModel, String.valueOf(position));;

                notifyDataSetChanged();
            }
        });

        holder.llLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("Layout","YEs");

                LiveFragment.tvLiveTag.setVisibility(View.GONE);
                clickedPosition = position;
                notifyDataSetChanged();
                fragment.callAsnycTask(catchupVideosModel, String.valueOf(position));
                //  fragment.imgLike.setColorFilter(ContextCompat.getColor(context,R.color.gray));
            }
        });

        holder.ivshare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                List<Intent> targetShareIntents = new ArrayList<Intent>();
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                List<ResolveInfo> resInfos = context.getPackageManager().queryIntentActivities(shareIntent, 0);
                if (!resInfos.isEmpty()) {
                    System.out.println("Have package");
                    for (ResolveInfo resInfo : resInfos) {
                        String packageName = resInfo.activityInfo.packageName;
                        Log.i("Package Name", packageName);

                        if (!(packageName.contains("com.google.android.apps.translate") || packageName.contains("com.google.android.gms.drive") || packageName.contains("com.google.android.hangouts"))) {

                            Intent intent = new Intent();
                            intent.setComponent(new ComponentName(packageName, resInfo.activityInfo.name));
                            intent.setAction(Intent.ACTION_SEND);
                            intent.setType("text/plain");
                            intent.putExtra(Intent.EXTRA_TEXT,"watch the video- videoURL"+ catchupVideosModel.getEventURL()+"\n");
                            intent.putExtra(Intent.EXTRA_SUBJECT, "watch the video- videoURL");
                            intent.setPackage(packageName);
                            targetShareIntents.add(intent);
                        }

                    }
                    if (!targetShareIntents.isEmpty()) {
                        System.out.println("Have Intent");
                        Intent chooserIntent = Intent.createChooser(targetShareIntents.remove(0), "Choose app to share");
                        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetShareIntents.toArray(new Parcelable[]{}));
                        context.startActivity(chooserIntent);
                    } else {
                        System.out.println("Do not Have Intent");
                    }
                }


            }
        });
    }



    @Override
    public int getItemCount() {
        return arrayListData.size();
    }


    /*public void changeLikeIMgColor (int color) {
        // color == createViewHolder().ColorImg;
    }*/
    public class MyViewHolder extends RecyclerView.ViewHolder {

        LinearLayout containerBottomLayout,llLayout,llMain;
        TextView tvLike,tvViews,tvTitle;
        ImageView imageViewCatchuplogo,ivshare;
        ImageView imageViewPlayIcon;
        CardView cardViewContainer;
        ImageView imgLikeCatchup;
        TextView tvLikeessCatchup;
        public int ColorImg;


        public MyViewHolder(View itemView) {

            super(itemView);

            containerBottomLayout = (LinearLayout)itemView.findViewById(R.id.bottomlayout_container_linear);
            //  tvLike = (TextView) itemView.findViewById(R.id.tv_like);
            tvViews = (TextView) itemView.findViewById(R.id.tv_views);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_catchup_name);
            imageViewCatchuplogo = (ImageView) itemView.findViewById(R.id.catchup_imageview_logo);
            imageViewPlayIcon = (ImageView) itemView.findViewById(R.id.play_video_icon);
            cardViewContainer = (CardView) itemView.findViewById(R.id.card_view);
            llLayout = (LinearLayout)itemView.findViewById(R.id.llLayout);
            ivshare = (ImageView)itemView.findViewById(R.id.liveShare);
            imgLikeCatchup = (ImageView) itemView.findViewById(R.id.img_like_catchup);
            tvLikeessCatchup = (TextView) itemView.findViewById(R.id.tv_like_catchup);
            llMain = (LinearLayout)itemView.findViewById(R.id.llMain);

        }
    }
}