package com.zecast.live.om_saravanabava.helper;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import static com.zecast.live.om_saravanabava.helper.Utility.getDeviceWidth;

/**
 * Created by ist on 23/8/17.
 */

public class AppUtil {

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    public static void showMsgAlert(View view, String msg) {
       // Snackbar.make(view, msg, Snackbar.LENGTH_SHORT).show();
    }

    public static RelativeLayout.LayoutParams setImageSizeForPopularVIew(View img, Activity context, int widthRatio, int heightRatio) {
        Log.d("ratio", "width : " + widthRatio + " height " + heightRatio);
        int width = getDeviceWidth(context);
        Log.d("ratio", "screen width : " + width);

        if (widthRatio != 0) {
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(width, ((((width * heightRatio) / widthRatio) * 100) / 100) + 85);
            img.setLayoutParams(params);
            Log.d("ratio", "calculate width : " + width + " calculate height : " + (((width * heightRatio) / widthRatio) * 100) / 100);
            return params;
        } else {
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(width, (((width * 1) / 1) * 100) / 100);
            img.setLayoutParams(params);
            return params;
        }
    }

    public static LinearLayout.LayoutParams setImageSizeForHomeView(View img, Activity context, int widthRatio, int heightRatio) {
        Log.d("ratio", "width : " + widthRatio + " height " + heightRatio);
        int width = getDeviceWidth(context);
        Log.d("ratio", "screen width : " + width);

        if (widthRatio != 0) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, ((((width * heightRatio) / widthRatio) * 100) / 100) + 85);
            img.setLayoutParams(params);
            Log.d("ratio", "calculate width : " + width + " calculate height : " + (((width * heightRatio) / widthRatio) * 100) / 100);
            return params;
        } else {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, (((width * 1) / 1) * 100) / 100);
            img.setLayoutParams(params);
            return params;
        }
    }

   }
