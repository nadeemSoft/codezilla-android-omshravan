package com.zecast.live.om_saravanabava.model;

import java.util.ArrayList;

/**
 * Created by codezilla-12 on 19/9/17.
 */

public class VideosCategoryModel {

    String categoryId;
    String categoryImage;
    String categoryName;
    String categoryOrder;
    ArrayList<VideoProgramsModel> subcategorylist;

    public VideosCategoryModel(String categoryId, String categoryImage, String categoryName, String categoryOrder, ArrayList<VideoProgramsModel> subcategorylist) {
        this.categoryId = categoryId;
        this.categoryImage = categoryImage;
        this.categoryName = categoryName;
        this.categoryOrder = categoryOrder;
        this.subcategorylist = subcategorylist;
    }



    public ArrayList<VideoProgramsModel> getSubcategorylist() {
        return subcategorylist;
    }

    public void setSubcategorylist(ArrayList<VideoProgramsModel> subcategorylist) {
        this.subcategorylist = subcategorylist;
    }


    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryOrder() {
        return categoryOrder;
    }

    public void setCategoryOrder(String categoryOrder) {
        this.categoryOrder = categoryOrder;
    }
}
