package com.zecast.live.om_saravanabava.model;

import java.util.ArrayList;

public class HomeBanner {
    int bannerRowId;
    ArrayList<Banner> bannerArrayList;

    public int getBannerRowId() {
        return bannerRowId;
    }

    public void setBannerRowId(int bannerRowId) {
        this.bannerRowId = bannerRowId;
    }

    public ArrayList<Banner> getBannerArrayList() {
        return bannerArrayList;
    }

    public void setBannerArrayList(ArrayList<Banner> bannerArrayList) {
        this.bannerArrayList = bannerArrayList;
    }
}
