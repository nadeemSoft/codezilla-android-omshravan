package com.zecast.live.om_saravanabava.model;

import java.util.ArrayList;

public class Banner {
    int id;
    int type;
    ArrayList<String> imageUrl;
    String adKey;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public ArrayList<String> getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(ArrayList<String> imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getAdKey() {
        return adKey;
    }

    public void setAdKey(String adKey) {
        this.adKey = adKey;
    }

    @Override
    public String toString() {
        return "Banner{" +
                "id=" + id +
                ", type=" + type +
                ", imageUrl='" + imageUrl + '\'' +
                ", adKey='" + adKey + '\'' +
                '}';
    }
}
