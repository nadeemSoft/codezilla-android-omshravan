package com.zecast.live.om_saravanabava.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity;
import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.model.NavDrawerDataModel;
import com.zecast.live.om_saravanabava.activity.aboutus.AboutusActivity;
import com.zecast.live.om_saravanabava.activity.audio.AudioActivity;
import com.zecast.live.om_saravanabava.activity.charity.CharityActivity;
import com.zecast.live.om_saravanabava.activity.experience.ExperienceActivity;
import com.zecast.live.om_saravanabava.fragments.HomeFragment;
import com.zecast.live.om_saravanabava.fragments.LiveFragment;
import java.util.ArrayList;
import static com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity.baseHomeText;
import static com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity.baseLivetext;
import static com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity.baseProfileText;
import static com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity.baseUpcomingtext;
import static com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity.basevideostext;
import static com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity.drawer;
import static com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity.iconTabHome;
import static com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity.iconTabLive;
import static com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity.iconTabProfile;
import static com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity.iconTabUpcoming;
import static com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity.iconTabVideo;
import static com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity.lineHome;
import static com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity.lineLive;
import static com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity.lineProfile;
import static com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity.lineUpcoming;
import static com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity.lineVideo;

public class HomeSideNavDrawerAdapter extends RecyclerView.Adapter<HomeSideNavDrawerAdapter.MyViewHolder> {

    private Context context;
    ArrayList<NavDrawerDataModel> drawerDataList;
    ArrayList<Integer> mDrawerIcons = new ArrayList<>();

    public HomeSideNavDrawerAdapter(Context context, ArrayList<NavDrawerDataModel> drawerDataList) {
        this.context = context;
        this.drawerDataList = drawerDataList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

       /* View view = View.inflate(context, R.layout.single_row_item_home_side_nav_drawer_adapter,parent);
        return new HomeSideNavDrawerAdapter.MyViewHolder(view);
*/
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_row_item_home_side_nav_drawer_adapter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        mDrawerIcons.add( R.drawable.ic_home_tab);
        mDrawerIcons.add( R.drawable.ic_live_tab);
        mDrawerIcons.add( R.drawable.ic_charity_tab);
        mDrawerIcons.add( R.drawable.ic_expre);
        mDrawerIcons.add( R.drawable.ic_audio_tab);
        mDrawerIcons.add( R.drawable.ic_about_us_tab);

        holder.tvTitleDrawer.setText(drawerDataList.get(position).getTitle());
        holder.imgDrawer.setImageResource(mDrawerIcons.get(position));


        holder.linearContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (drawerDataList.get(position).getNavItemId().equals("1")) {

                    HomeFragment fragment2 = new HomeFragment();
                    Bundle bundle = new Bundle();
                    fragment2.setArguments(bundle);
                    FragmentTransaction quote_fm = ((HomeDrawerActivity) context).getSupportFragmentManager().beginTransaction();
                    quote_fm.replace(R.id.frame_drawer, fragment2);
                    quote_fm.commit();
                    if (drawer.isDrawerOpen(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    }

                    lineHome.setBackgroundColor(ContextCompat.getColor(context,R.color.colorPrimary));
                    iconTabHome.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_home_tab));
                    lineLive.setBackgroundColor(ContextCompat.getColor(context,R.color.White));
                    iconTabLive.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_live_unselected));
                    lineUpcoming.setBackgroundColor(ContextCompat.getColor(context,R.color.White));
                    iconTabUpcoming.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_upcoming_unselected));
                    lineVideo.setBackgroundColor(ContextCompat.getColor(context,R.color.White));
                    iconTabVideo.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_video_tab_unselected));
                    lineProfile.setBackgroundColor(ContextCompat.getColor(context,R.color.White));
                    iconTabProfile.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_profile_tab_unselected));
                    baseHomeText.setTextColor(ContextCompat.getColor(context,R.color.colorPrimary));
                    basevideostext.setTextColor(ContextCompat.getColor(context,R.color.gray));
                    baseUpcomingtext.setTextColor(ContextCompat.getColor(context,R.color.gray));
                    baseProfileText.setTextColor(ContextCompat.getColor(context,R.color.gray));
                    baseLivetext.setTextColor(ContextCompat.getColor(context,R.color.gray));


                }

                if (drawerDataList.get(position).getNavItemId().equals("2")) {

                    LiveFragment fragment2 = new LiveFragment();
                    Bundle bundle = new Bundle();
                    fragment2.setArguments(bundle);
                    FragmentTransaction quote_fm = ((HomeDrawerActivity)context).getSupportFragmentManager().beginTransaction();
                    quote_fm.replace(R.id.frame_drawer, fragment2);
                    quote_fm.commit();
                    if (drawer.isDrawerOpen(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    }
                    lineLive.setBackgroundColor(ContextCompat.getColor(context,R.color.colorPrimary));
                    iconTabLive.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_live_tab));
                    lineHome.setBackgroundColor(ContextCompat.getColor(context,R.color.White));
                    iconTabHome.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_home_unselected));
                    lineUpcoming.setBackgroundColor(ContextCompat.getColor(context,R.color.White));
                    iconTabUpcoming.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_upcoming_unselected));
                    lineVideo.setBackgroundColor(ContextCompat.getColor(context,R.color.White));
                    iconTabVideo.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_video_tab_unselected));
                    lineProfile.setBackgroundColor(ContextCompat.getColor(context,R.color.White));
                    iconTabProfile.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_profile_tab_unselected));
                    baseHomeText.setTextColor(ContextCompat.getColor(context,R.color.gray));
                    basevideostext.setTextColor(ContextCompat.getColor(context,R.color.gray));
                    baseUpcomingtext.setTextColor(ContextCompat.getColor(context,R.color.gray));
                    baseProfileText.setTextColor(ContextCompat.getColor(context,R.color.gray));
                    baseLivetext.setTextColor(ContextCompat.getColor(context,R.color.colorPrimary));

                }

                if (drawerDataList.get(position).getNavItemId().equals("3")) {

                    Intent intent = new Intent(context, CharityActivity.class);
                    context.startActivity(intent);

                    if (drawer.isDrawerOpen(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    }

                    lineHome.setBackgroundColor(ContextCompat.getColor(context,R.color.colorPrimary));
                    iconTabHome.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_home_tab));
                    lineLive.setBackgroundColor(ContextCompat.getColor(context,R.color.White));
                    iconTabLive.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_live_unselected));
                    lineUpcoming.setBackgroundColor(ContextCompat.getColor(context,R.color.White));
                    iconTabUpcoming.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_upcoming_unselected));
                    lineVideo.setBackgroundColor(ContextCompat.getColor(context,R.color.White));
                    iconTabVideo.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_video_tab_unselected));
                    lineProfile.setBackgroundColor(ContextCompat.getColor(context,R.color.White));
                    iconTabProfile.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_profile_tab_unselected));
                }

                if (drawerDataList.get(position).getNavItemId().equals("4")) {


                    Intent intent = new Intent(context, ExperienceActivity.class);
                    context.startActivity(intent);

                    if (drawer.isDrawerOpen(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    }

                    // quote_fm.commit();

                    lineHome.setBackgroundColor(ContextCompat.getColor(context,R.color.colorPrimary));
                    iconTabHome.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_home_tab));
                    lineLive.setBackgroundColor(ContextCompat.getColor(context,R.color.White));
                    iconTabLive.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_live_unselected));
                    lineUpcoming.setBackgroundColor(ContextCompat.getColor(context,R.color.White));
                    iconTabUpcoming.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_upcoming_unselected));
                    lineVideo.setBackgroundColor(ContextCompat.getColor(context,R.color.White));
                    iconTabVideo.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_video_tab_unselected));
                    lineProfile.setBackgroundColor(ContextCompat.getColor(context,R.color.White));
                    iconTabProfile.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_profile_tab_unselected));

                }

                if (drawerDataList.get(position).getNavItemId().equals("5")) {
                    Log.e("CLICKED position", "onClick: "+position);


                    Intent intent = new Intent(context, AudioActivity.class);
                    context.startActivity(intent);

                    if (drawer.isDrawerOpen(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    }

                    lineHome.setBackgroundColor(ContextCompat.getColor(context,R.color.colorPrimary));
                    iconTabHome.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_home_tab));
                    lineLive.setBackgroundColor(ContextCompat.getColor(context,R.color.White));
                    iconTabLive.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_live_unselected));
                    lineUpcoming.setBackgroundColor(ContextCompat.getColor(context,R.color.White));
                    iconTabUpcoming.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_upcoming_unselected));
                    lineVideo.setBackgroundColor(ContextCompat.getColor(context,R.color.White));
                    iconTabVideo.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_video_tab_unselected));
                    lineProfile.setBackgroundColor(ContextCompat.getColor(context,R.color.White));
                    iconTabProfile.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_profile_tab_unselected));

                }

                if (drawerDataList.get(position).getNavItemId().equals("6")) {


                    Intent intent = new Intent(context, AboutusActivity.class);
                    context.startActivity(intent);


                    if (drawer.isDrawerOpen(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    }

                    lineHome.setBackgroundColor(ContextCompat.getColor(context,R.color.colorPrimary));
                    iconTabHome.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_home_tab));
                    lineLive.setBackgroundColor(ContextCompat.getColor(context,R.color.White));
                    iconTabLive.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_live_unselected));
                    lineUpcoming.setBackgroundColor(ContextCompat.getColor(context,R.color.White));
                    iconTabUpcoming.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_upcoming_unselected));
                    lineVideo.setBackgroundColor(ContextCompat.getColor(context,R.color.White));
                    iconTabVideo.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_video_tab_unselected));
                    lineProfile.setBackgroundColor(ContextCompat.getColor(context,R.color.White));
                    iconTabProfile.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_profile_tab_unselected));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return drawerDataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitleDrawer;
        ImageView imgDrawer;
        int positionClicked;
        LinearLayout linearContainer,drawerContainerinner;
        LinearLayout card_view_container_linear;

        public MyViewHolder(View itemView) {
            super(itemView);
                tvTitleDrawer = (TextView)itemView.findViewById(R.id.titleDrawer1);
                imgDrawer = (ImageView) itemView.findViewById(R.id.logo_drawer);
                linearContainer = (LinearLayout)itemView.findViewById(R.id.datacontainer);

        }

        /*@Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.card_view_container_linear:

                break;

            }
        }*/
    }
}
