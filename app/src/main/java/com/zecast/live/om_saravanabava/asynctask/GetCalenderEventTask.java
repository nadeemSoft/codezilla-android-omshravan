package com.zecast.live.om_saravanabava.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import com.zecast.live.om_saravanabava.helper.AsyncCallback;
import com.zecast.live.om_saravanabava.helper.KeyConstant;
import com.zecast.live.om_saravanabava.helper.MessageConstant;
import com.zecast.live.om_saravanabava.helper.ProgressDialogUtils;
import com.zecast.live.om_saravanabava.model.User;
import com.zecast.live.om_saravanabava.utils.Const;
import com.zecast.live.om_saravanabava.utils.HTTPRequest;
import com.zecast.live.om_saravanabava.utils.Sharedprefrences;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import java.util.concurrent.TimeUnit;


public class GetCalenderEventTask extends AsyncTask<Void, Void, Void> {

    private String TAG = this.getClass().getSimpleName();
    private User user;
    private AsyncCallback asyncCallback;
    private Context context;

    public GetCalenderEventTask(Context context, User user, AsyncCallback asyncCallback) {
        this.user = user;
        this.asyncCallback = asyncCallback;
        this.context = context;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        try {
         //  ProgressDialogUtils.showProgressDialog(context, MessageConstant.MESSAGE_PLEASE_WAIT);

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Void doInBackground(Void... voids) {
        getPopularData();
        return null;
    }

    public String getPopularData() {
        String url = HTTPRequest.SERVICE_URL + UrlConstant.URL_GET_CALENDAR_EVENTS;
        Log.d(TAG, "url " + url);
        try {

            OkHttpClient client = new OkHttpClient();
            client.setConnectTimeout(UrlConstant.CONNECTION_TIME_OUT, TimeUnit.SECONDS);
            client.setReadTimeout(UrlConstant.SOCKET_TIME_OUT, TimeUnit.SECONDS);
            RequestBody requestBody = null;

            requestBody = new FormEncodingBuilder()
                    .add(KeyConstant.KEY_USER_ID, Sharedprefrences.getUserId(context)+ "")
                    .build();

            Request request = new Request.Builder()
                    .url(url)
                    .addHeader("vendorKey", Const.vendorKey)
                    .addHeader(UrlConstant.HEADER_AUTHORIZATION, Sharedprefrences.getUserKey(context))
                    .addHeader(UrlConstant.HEADER_DEVICE_ID, Sharedprefrences.getDeviceId(context))
                    .addHeader("userID", Sharedprefrences.getUserId(context)+ "")
                    .post(requestBody)
                    .build();

            Response response = client.newCall(request).execute();
            String result = response.body().string().toString();

            Log.e(TAG, "responseStr " + result);
            Log.e(TAG, "response.code() :" + response.code());
            asyncCallback.setResponse(response.code(), result);

           // ProgressDialogUtils.hideProgressDialog();

        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "exception " + e.getMessage());
          //  ProgressDialogUtils.hideProgressDialog();
            asyncCallback.setException(e.getMessage());
        }
        return null;
    }

}
