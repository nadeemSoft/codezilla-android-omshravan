package com.zecast.live.om_saravanabava.players;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.zecast.live.om_saravanabava.R;

public class VideoPlayFullScreenActivity extends AppCompatActivity {

    VideoView videoLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_video_play_full_screen);

        videoLayout = (VideoView)findViewById(R.id.fullScreeenVideoPlay);

        MediaController mediaController = new
                MediaController(VideoPlayFullScreenActivity.this);

        mediaController.setAnchorView(videoLayout);
        videoLayout.setMediaController(mediaController);

        Intent intent = getIntent();
        String URLOFVIDEO =  intent.getStringExtra("VIDEOURL");
        initWidgets();

        Log.e("INTENT RECIEVED DATA", "onClick: "+URLOFVIDEO);

        new BackgroundAsyncTask().execute(URLOFVIDEO);

          }

    private void initWidgets() {

    }

    class BackgroundAsyncTask extends AsyncTask<String, Uri, Void> {

        protected void onPreExecute() {
          //  linearVideoLoader.setVisibility(View.GONE);
        }

        protected void onProgressUpdate(final Uri... uri) {
            try {
                videoLayout.setVideoURI(uri[0]);
                videoLayout.requestFocus();

                videoLayout.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

                    public void onPrepared(MediaPlayer arg0) {
                        videoLayout.start();

                    }
                });

                videoLayout.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                    @Override
                    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {

                           videoLayout.start();
                        Toast.makeText(VideoPlayFullScreenActivity.this, "Cant play Video", Toast.LENGTH_SHORT).show();
                        Log.e("Error", "error");
                        return true;
                    }
                });

            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(String... params) {
            try {
                Uri uri = Uri.parse(params[0]);
                publishProgress(uri);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }
    }

}
