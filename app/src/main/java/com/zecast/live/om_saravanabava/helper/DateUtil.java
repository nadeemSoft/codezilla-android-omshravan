package com.zecast.live.om_saravanabava.helper;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateUtil {

    public static String getLocalDate(String serverDate) {
        String localDate;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date value = formatter.parse(serverDate);

            formatter.setTimeZone(TimeZone.getDefault());
            localDate = formatter.format(value);
        } catch (Exception e) {
            localDate = "00-00-0000 00:00";
        }
        return localDate;
    }


    public static String getDay(String dateStr) {
        dateStr = getLocalDate(dateStr);
        String[] date = dateStr.split("-");
        return date[0];
    }

    public static String getMonth(String dateStr) {
        dateStr = getLocalDate(dateStr);
        String[] monthArray = new String[]{"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

        String[] date = dateStr.split("-");
        Integer month = Integer.parseInt(date[1].replace("0", ""));
        return monthArray[month - 1];
    }

    public static String getYear(String dateStr) {
        dateStr = getLocalDate(dateStr);
        String[] date = dateStr.split("-");
        return date[2];
    }

    public static String getTime(String dateStr) {
        dateStr = getLocalDate(dateStr);
        String[] date = dateStr.split(" ");
        return date[1];
    }


    public static boolean isEndDateGreater(String startDate, String endDate) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

            Date date1 = formatter.parse(startDate);
            Date date2 = formatter.parse(endDate);

            if (date1.compareTo(date2) <= 0) {
                Log.d("date2", " is Greater than my date1");
                return true;
            } else {
                return false;
            }

        } catch (ParseException e1) {
            e1.printStackTrace();
            return false;
        }
    }
}
