package com.zecast.live.om_saravanabava.activity.aboutus;

import android.Manifest;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Parcelable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.model.AboutUsModel;
import com.zecast.live.om_saravanabava.utils.Const;
import com.zecast.live.om_saravanabava.utils.HTTPRequest;
import com.zecast.live.om_saravanabava.utils.Sharedprefrences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AboutusActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tvDescription, tvHeadingAboutUS;
    ImageView imageViewLogo;
    ImageView imgCallIcon, imgMailIcon, imgShareIcon;
    AboutUsModel aboutusModel;
    private static final int STORAGE_PERMISSION_CODE = 23;
    LinearLayout linearDataContainer,linearLoaderContainer;

    ProgressBar pb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aboutus);

        setTitle("About us");

        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        initWidgets();
        getAboutUsData();

        imgCallIcon.setOnClickListener(this);
        imgMailIcon.setOnClickListener(this);
        imgShareIcon.setOnClickListener(this);
    }

    private void fillWidgets() {

        if (!(aboutusModel.equals("") || aboutusModel == null)) {

            tvDescription.setText(aboutusModel.getAboutUs());


        }
    }

    private void getAboutUsData() {

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest sr = new StringRequest(Request.Method.POST, HTTPRequest.SERVICE_URL + "getAboutUs", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.e("response about us ", response);
                    try {
                        JSONObject response1 = new JSONObject(response);
                        String responseStatus = response1.getString("status");
                        String errorType = response1.optString("error_type");
                        String responseMessage = response1.optString("message");

                        JSONObject openingObj = response1.optJSONObject("response");

                        for (int i = 0; i < openingObj.length(); i++) {
                            String venderId = openingObj.optString("vendor_id");
                            String venderAboutUs = openingObj.optString("vendor_about_us");
                            String venderMobile = openingObj.optString("vendor_mobile");
                            String venderEmail = openingObj.optString("vendor_email");

                            aboutusModel = new AboutUsModel(venderId, venderAboutUs, venderMobile, venderEmail);
                            fillWidgets();

                            linearDataContainer.setVisibility(View.VISIBLE);
                            //pb.setVisibility(View.GONE);

                            linearLoaderContainer.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("EREROR", String.valueOf(error));
                linearDataContainer.setVisibility(View.VISIBLE);
                linearLoaderContainer.setVisibility(View.GONE);
               // pb.setVisibility(View.GONE);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();



                headers.put("vendorKey", Const.vendorKey);
                headers.put("Authorization",Sharedprefrences.getUserKey(getBaseContext()));
                headers.put("userID", Sharedprefrences.getUserId(getBaseContext()));
                headers.put("Content-Type", Const.contentType);

                return headers;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                //  params.put("channelId", "1");

                return params;
            }
        };

        int socketTimeout = 20000; //30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        sr.setRetryPolicy(policy);
        queue.add(sr);
    }

    private void initWidgets() {
        imgCallIcon = (ImageView)findViewById(R.id.phone_imageview);
        imgMailIcon = (ImageView) findViewById(R.id.email_imageview);
        imgShareIcon = (ImageView) findViewById(R.id.share_imageview);
        imageViewLogo = (ImageView) findViewById(R.id.imageview_logoAbout_us);
        tvDescription = (TextView) findViewById(R.id.tv_description_about_us);
        tvHeadingAboutUS = (TextView) findViewById(R.id.Heading_about_us);
        linearDataContainer = (LinearLayout) findViewById(R.id.linearDatacontainer);
        pb = (ProgressBar) findViewById(R.id.progressbar1);
        linearLoaderContainer = (LinearLayout) findViewById(R.id.linearpbContainer);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.phone_imageview:
                requestStoragePermission();
                call();
                break;

            case R.id.email_imageview:
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", aboutusModel.getVenderEmail(), null));
                startActivity(Intent.createChooser(emailIntent, "Send email..."));

                break;

            case R.id.share_imageview:
                List<Intent> targetShareIntents = new ArrayList<Intent>();
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                List<ResolveInfo> resInfos = this.getPackageManager().queryIntentActivities(shareIntent, 0);
                if (!resInfos.isEmpty()) {
                    System.out.println("Have package");
                    for (ResolveInfo resInfo : resInfos) {
                        String packageName = resInfo.activityInfo.packageName;
                        Log.i("Package Name", packageName);

                        if (!(packageName.contains("com.google.android.apps.translate") || packageName.contains("com.google.android.gms.drive") || packageName.contains("com.google.android.hangouts"))) {
                            Intent intent = new Intent();
                            intent.setComponent(new ComponentName(packageName, resInfo.activityInfo.name));
                            intent.setAction(Intent.ACTION_SEND);
                            intent.setType("text/plain");
                            intent.putExtra(Intent.EXTRA_TEXT,"Please Download Sri Sharavana Baba's app " +"https://play.google.com/store/apps/details?id=com.zecast.live.om_saravanabava&hl=en");
                            intent.putExtra(Intent.EXTRA_SUBJECT, "Please Download Sri Sharavana Baba's app");
                            intent.setPackage(packageName);
                            targetShareIntents.add(intent);
                        }

                    }
                    if (!targetShareIntents.isEmpty()) {
                        System.out.println("Have Intent");
                        Intent chooserIntent = Intent.createChooser(targetShareIntents.remove(0), "downloadable link of android");
                        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetShareIntents.toArray(new Parcelable[]{}));
                        startActivity(chooserIntent);
                    } else {
                        System.out.println("Do not Have Intent");
                    }
                }

                break;
        }
    }

    public void call() {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + aboutusModel.getVenderMobile()));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(intent);
    }

    private void requestStoragePermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CALL_PHONE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }

        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, STORAGE_PERMISSION_CODE);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                this.finish();
                return true;

            default:
                Log.e("SELECTEDDDDD", "onOptionsItemSelected: "+item.getItemId());
                return super.onOptionsItemSelected(item);
        }
    }

}


