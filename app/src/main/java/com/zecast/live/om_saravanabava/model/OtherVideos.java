package com.zecast.live.om_saravanabava.model;

import java.io.Serializable;

/**
 * Created by codezilla-12 on 18/10/17.
 */

public class OtherVideos implements Serializable
{
    String episode_id,episode_name,episode_image,episode_date,video_id,episode_type,episode_url,episode_seen,episode_desc,episode_like_status,episode_like;


    public OtherVideos() {
    }

    public OtherVideos(String episode_id, String episode_name, String episode_image, String episode_date, String video_id, String episode_type, String episode_url, String episode_seen, String episode_desc, String episode_like_status, String episode_like) {
        this.episode_id = episode_id;
        this.episode_name = episode_name;
        this.episode_image = episode_image;
        this.episode_date = episode_date;
        this.video_id = video_id;
        this.episode_type = episode_type;
        this.episode_url = episode_url;
        this.episode_seen = episode_seen;
        this.episode_desc = episode_desc;
        this.episode_like_status = episode_like_status;
        this.episode_like = episode_like;
    }

    public String getEpisode_id() {
        return episode_id;
    }

    public void setEpisode_id(String episode_id) {
        this.episode_id = episode_id;
    }

    public String getEpisode_name() {
        return episode_name;
    }

    public void setEpisode_name(String episode_name) {
        this.episode_name = episode_name;
    }

    public String getEpisode_image() {
        return episode_image;
    }

    public void setEpisode_image(String episode_image) {
        this.episode_image = episode_image;
    }

    public String getEpisode_date() {
        return episode_date;
    }

    public void setEpisode_date(String episode_date) {
        this.episode_date = episode_date;
    }

    public String getVideo_id() {
        return video_id;
    }

    public void setVideo_id(String video_id) {
        this.video_id = video_id;
    }

    public String getEpisode_type() {
        return episode_type;
    }

    public void setEpisode_type(String episode_type) {
        this.episode_type = episode_type;
    }

    public String getEpisode_url() {
        return episode_url;
    }

    public void setEpisode_url(String episode_url) {
        this.episode_url = episode_url;
    }

    public String getEpisode_seen() {
        return episode_seen;
    }

    public void setEpisode_seen(String episode_seen) {
        this.episode_seen = episode_seen;
    }

    public String getEpisode_desc() {
        return episode_desc;
    }

    public void setEpisode_desc(String episode_desc) {
        this.episode_desc = episode_desc;
    }

    public String getEpisode_like_status() {
        return episode_like_status;
    }

    public void setEpisode_like_status(String episode_like_status) {
        this.episode_like_status = episode_like_status;
    }

    public String getEpisode_like() {
        return episode_like;
    }

    public void setEpisode_like(String episode_like) {
        this.episode_like = episode_like;
    }

    @Override
    public String toString() {
        return "OtherVideos{" +
                "episode_id='" + episode_id + '\'' +
                ", episode_name='" + episode_name + '\'' +
                ", episode_image='" + episode_image + '\'' +
                ", episode_date='" + episode_date + '\'' +
                ", video_id='" + video_id + '\'' +
                ", episode_type='" + episode_type + '\'' +
                ", episode_url='" + episode_url + '\'' +
                ", episode_seen='" + episode_seen + '\'' +
                ", episode_desc='" + episode_desc + '\'' +
                ", episode_like_status='" + episode_like_status + '\'' +
                ", episode_like='" + episode_like + '\'' +
                '}';
    }
}

