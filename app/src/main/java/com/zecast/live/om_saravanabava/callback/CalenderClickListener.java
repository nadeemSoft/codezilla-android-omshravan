package com.zecast.live.om_saravanabava.callback;

public interface CalenderClickListener {
    public void setClickedDate(String date);
}
