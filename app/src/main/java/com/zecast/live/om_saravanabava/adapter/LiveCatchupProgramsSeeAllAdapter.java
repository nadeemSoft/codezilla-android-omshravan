package com.zecast.live.om_saravanabava.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.activity.LiveCatchupGrid;
import com.zecast.live.om_saravanabava.activity.eventdetails.EventDetailsActivity;
import com.zecast.live.om_saravanabava.model.CatchupModel;

import java.util.ArrayList;

public class LiveCatchupProgramsSeeAllAdapter extends RecyclerView.Adapter<LiveCatchupProgramsSeeAllAdapter.MyViewHolder>  {

    private Context context;
    ArrayList<CatchupModel> arrayListData;

    public LiveCatchupProgramsSeeAllAdapter(Context context, ArrayList<CatchupModel> mCatchupList) {
        this.context = context;
        this.arrayListData = mCatchupList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = View.inflate(context, R.layout.single_row_item_live_catchup_videos_see_all,null);
        return new LiveCatchupProgramsSeeAllAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder,final  int position) {

        final CatchupModel catchupVideosModel = arrayListData.get(position);

        Picasso.with(context)
                .load(catchupVideosModel.getEventImageURL())
                .error(R.drawable.app_icon)
                .placeholder(R.drawable.app_icon)
                .into(holder.imageViewCatchuplogo);

        holder.tvTitle.setText(catchupVideosModel.getEventName());
        holder.tvViews.setText(catchupVideosModel.getEventSeens());

        holder.imageViewPlayIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (catchupVideosModel.getEventHasMultipleParts().equalsIgnoreCase("true")) {
                    Log.e("Event",catchupVideosModel.getEventHasMultipleParts());

                    Intent in = new Intent(context, LiveCatchupGrid.class);
                    in.putExtra("CatchupName",catchupVideosModel.getEventName());
                    in.putExtra("CatchupId",catchupVideosModel.getEventId());
                    in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(in);
                }
                else {

                    Intent in = new Intent(context, EventDetailsActivity.class);
                    in.putExtra("EventName",catchupVideosModel.getEventName());
                    in.putExtra("EventId",catchupVideosModel.getEventId());
                    context.startActivity(in);
                }
            }
        });


        holder.linearLayoutImgContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (catchupVideosModel.getEventHasMultipleParts().equalsIgnoreCase("true")) {
                    Log.e("Event",catchupVideosModel.getEventHasMultipleParts());

                    Intent in = new Intent(context, LiveCatchupGrid.class);
                    in.putExtra("CatchupName",catchupVideosModel.getEventName());
                    in.putExtra("CatchupId",catchupVideosModel.getEventId());
                    in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(in);
                }
                else {

                    Intent in = new Intent(context, EventDetailsActivity.class);
                    in.putExtra("EventName",catchupVideosModel.getEventName());
                    in.putExtra("EventId",catchupVideosModel.getEventId());
                    context.startActivity(in);
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayListData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        LinearLayout containerBottomLayout;
        TextView tvLike,tvViews,tvTitle;
        ImageView imageViewCatchuplogo;
        ImageView imageViewPlayIcon;
        LinearLayout linearLayoutImgContainer;


        public MyViewHolder(View itemView) {

            super(itemView);

              containerBottomLayout = (LinearLayout)itemView.findViewById(R.id.bottomlayout_container_linear);
              tvTitle = (TextView) itemView.findViewById(R.id.tv_catchup_name);
              imageViewCatchuplogo = (ImageView) itemView.findViewById(R.id.catchup_imageview_logo);
              imageViewPlayIcon = (ImageView) itemView.findViewById(R.id.play_video_icon);
              linearLayoutImgContainer = (LinearLayout)itemView.findViewById(R.id.linear_container_img);
              tvViews = (TextView) itemView.findViewById(R.id.tv_views);

             }
    }
}

 /* tvLike = (TextView) itemView.findViewById(R.id.tv_like);
              tvViews = (TextView) itemView.findViewById(R.id.tv_views);
           */

   /*      Intent intentplayvideo = new Intent(context,VideoPlayFullScreenActivity.class);
                Log.e("INTENT SENT DATA", "onClick: "+arrayListData.get(position).getEventURL() );
                intentplayvideo.putExtra("VIDEOURL",arrayListData.get(position).getEventURL());
              *//*  Intent intent = Intent.createChooser(intentplayvideo, "Complete With");
         *//*       Log.e("INTENT SENT DATA", "onClick: "+arrayListData.get(position).getEventURL() );
               *//* intentplayvideo.putExtra("VIDEOURL",arrayListData.get(position).getEventURL());
             *//*   *//*intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);*//*
                    context.startActivity(intentplayvideo);*/