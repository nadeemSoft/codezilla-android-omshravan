package com.zecast.live.om_saravanabava.activity.seealllists;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.adapter.LiveCatchupProgramsSeeAllAdapter;

import static com.zecast.live.om_saravanabava.fragments.LiveFragment.liveCatchuplist;

public class CatchupSeeAllActivity extends AppCompatActivity {

    RecyclerView recyclerViewSeeAllCatachup;
    GridLayoutManager layoutManager;
    LiveCatchupProgramsSeeAllAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catchup_see_all);
        setTitle("Catchups");
        initWidgets();
        setRecyclerAdapter();
    }

    private void setRecyclerAdapter() {
        adapter = new LiveCatchupProgramsSeeAllAdapter(getBaseContext(), liveCatchuplist);
        layoutManager = new GridLayoutManager(getBaseContext(),2);


        recyclerViewSeeAllCatachup.setLayoutManager(layoutManager);
        recyclerViewSeeAllCatachup.setAdapter(adapter);
        recyclerViewSeeAllCatachup.setNestedScrollingEnabled(false);
        adapter.notifyDataSetChanged();
    }

    private void initWidgets() {
        recyclerViewSeeAllCatachup = (RecyclerView)findViewById(R.id.recycler_catchups_see_all);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}



    /*@Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        System.exit(0);
    }*/


   /* @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(getBaseContext(), HomeDrawerActivity.class));
    }*/
