package com.zecast.live.om_saravanabava.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.model.Search;
import com.zecast.live.om_saravanabava.utils.Sharedprefrences;
import com.zecast.live.om_saravanabava.activity.searchdetails.VideoDetailsSearchActivity;
import java.util.ArrayList;


/**
 * Created by codezilla-12 on 18/10/17.
 */

public class VideoDetailsSearchAdapter extends RecyclerView.Adapter<VideoDetailsSearchAdapter.MyViewHolder> {

    private ArrayList<Search> mOtherVideos;
    VideoDetailsSearchActivity details;
    private Context context;
    public int clickedPosition = -1;
    public boolean val = false;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView ivHolderEpisodeLogo;
        public TextView tvEpisodeName,tvLike,tvView;
        ImageView playVideoIcon,ivShare;
        FrameLayout frameLayoutImg;
        CardView cd;
        LinearLayout linearLayoutContainer;

        public MyViewHolder(View view) {
            super(view);

            ivHolderEpisodeLogo =(ImageView)itemView.findViewById(R.id.adapter_categories_imageview1);
            tvEpisodeName = (TextView) itemView.findViewById(R.id.tv_episode_name1);
            tvLike = (TextView) itemView.findViewById(R.id.tv_like1);
            tvView = (TextView)itemView.findViewById(R.id.tv_views1);
            playVideoIcon = (ImageView) itemView.findViewById(R.id.imageViewPlayVideo_pro1);
            frameLayoutImg = (FrameLayout) itemView.findViewById(R.id.frame_img1);
            ivShare = (ImageView)itemView.findViewById(R.id.videoShare1);
            cd = (CardView)itemView.findViewById(R.id.card_view);
            linearLayoutContainer = (LinearLayout)itemView.findViewById(R.id.layout_container);
        }
    }

    public VideoDetailsSearchAdapter(Context context, VideoDetailsSearchActivity activity, ArrayList<Search> mOtherVideos) {
        this.context = context;
        this.mOtherVideos = mOtherVideos;
        this.details = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_row_item_videodetails_search_adapter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        if (position == clickedPosition) {
            val = true;
            holder.cd.setBackgroundResource(R.color.colorPrimary);
        } else {
            holder.cd.setBackgroundResource(0);
        }
        final Search ccn = mOtherVideos.get(position);
        holder.tvEpisodeName.setText(ccn.getEpisode_name());
        holder.tvLike.setText(ccn.getEpisode_like());
        holder.tvView.setText(ccn.getEpisode_seen());

        Sharedprefrences.setepisodeId(context,ccn.getEpisode_id());
        Picasso.with(context)
                .load(ccn.getEpisode_image())
                .error(R.drawable.app_icon)
                .fit()
                .placeholder(R.drawable.app_icon)
                .into(holder.ivHolderEpisodeLogo);

        holder.frameLayoutImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                details.playLiveChannel(ccn, String.valueOf(position));
                clickedPosition = position;

                if (ccn.getEpisode_type().equals("direct")) {
                   /* VideoDetailsSearchActivity.imgPlayYoutubeVideo.setVisibility(View.GONE);
                    VideoDetailsSearchActivity.vv.setVisibility(View.VISIBLE);*/

                    VideoDetailsSearchActivity.youtubePlayerContainer.setVisibility(View.GONE);
                    VideoDetailsSearchActivity.videoviewFullContainer.setVisibility(View.VISIBLE);
                    VideoDetailsSearchActivity.vv.setVisibility(View.VISIBLE);


                }  else if (ccn.getEpisode_type().equals("youtube")){
                 /*   VideoDetailsSearchActivity.imgPlayYoutubeVideo.setVisibility(View.VISIBLE);
                    VideoDetailsSearchActivity.vv.setVisibility(View.INVISIBLE);
                    VideoDetailsSearchActivity.vv.stopPlayback();*/


                    VideoDetailsSearchActivity.youtubePlayerContainer.setVisibility(View.VISIBLE);
                    VideoDetailsSearchActivity.videoviewFullContainer.setVisibility(View.GONE);
                    VideoDetailsSearchActivity.vv.stopPlayback();
                    VideoDetailsSearchActivity.vv.setVisibility(View.INVISIBLE);


                }

                details.tvDescription.setText(ccn.getEpisode_desc());
                details.tvEpisodeName.setText(ccn.getEpisode_name());
                details.tvEpisodeDate.setText(ccn.getEpisode_date());

                notifyDataSetChanged();
            }
        });

        holder.playVideoIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickedPosition = position;
                details.playLiveChannel(ccn, String.valueOf(position));
                if (ccn.getEpisode_type().equals("direct")) {
                   /* VideoDetailsSearchActivity.imgPlayYoutubeVideo.setVisibility(View.GONE);
                    VideoDetailsSearchActivity.vv.setVisibility(View.VISIBLE);*/


                    VideoDetailsSearchActivity.youtubePlayerContainer.setVisibility(View.GONE);
                    VideoDetailsSearchActivity.videoviewFullContainer.setVisibility(View.VISIBLE);
                    VideoDetailsSearchActivity.vv.setVisibility(View.VISIBLE);



                }  else if (ccn.getEpisode_type().equals("youtube")){
                    /*VideoDetailsSearchActivity.imgPlayYoutubeVideo.setVisibility(View.VISIBLE );
                    VideoDetailsSearchActivity.vv.setVisibility(View.INVISIBLE);
                    VideoDetailsSearchActivity.vv.stopPlayback();*/

                    VideoDetailsSearchActivity.youtubePlayerContainer.setVisibility(View.VISIBLE);
                    VideoDetailsSearchActivity.videoviewFullContainer.setVisibility(View.GONE);
                    VideoDetailsSearchActivity.vv.stopPlayback();
                    VideoDetailsSearchActivity.vv.setVisibility(View.INVISIBLE);
                    Sharedprefrences.setYoutubeVideoId(context,ccn.getVideo_id());


                }
                holder.frameLayoutImg.performClick();

                details.tvDescription.setText(ccn.getEpisode_desc());
                details.tvEpisodeName.setText(ccn.getEpisode_name());
                details.tvEpisodeDate.setText(ccn.getEpisode_date());

                notifyDataSetChanged();
            }
        });

        holder.linearLayoutContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickedPosition = position;
                if (ccn.getEpisode_type().equals("direct")) {
                   /* VideoDetailsSearchActivity.imgPlayYoutubeVideo.setVisibility(View.GONE);
                    VideoDetailsSearchActivity.vv.setVisibility(View.VISIBLE);*/

                    VideoDetailsSearchActivity.youtubePlayerContainer.setVisibility(View.GONE);
                    VideoDetailsSearchActivity.videoviewFullContainer.setVisibility(View.VISIBLE);
                    VideoDetailsSearchActivity.vv.setVisibility(View.VISIBLE);


                }  else if (ccn.getEpisode_type().equals("youtube")){
                    /*VideoDetailsSearchActivity.imgPlayYoutubeVideo.setVisibility(View.VISIBLE );
                    VideoDetailsSearchActivity.vv.setVisibility(View.INVISIBLE);
                    VideoDetailsSearchActivity.vv.stopPlayback();*/

                    VideoDetailsSearchActivity.youtubePlayerContainer.setVisibility(View.VISIBLE);
                    VideoDetailsSearchActivity.videoviewFullContainer.setVisibility(View.GONE);
                    Sharedprefrences.setYoutubeVideoId(context,ccn.getVideo_id());

                    VideoDetailsSearchActivity.vv.stopPlayback();
                    VideoDetailsSearchActivity.vv.setVisibility(View.INVISIBLE);


                }
                holder.frameLayoutImg.performClick();
                 details.tvDescription.setText(ccn.getEpisode_desc());
                 details.tvEpisodeName.setText(ccn.getEpisode_name());
                 details.tvEpisodeDate.setText(ccn.getEpisode_date());
                 details.tvSeen.setText(ccn.getEpisode_seen());
                 details.tvLikes.setText(ccn.getEpisode_like());
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mOtherVideos.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}