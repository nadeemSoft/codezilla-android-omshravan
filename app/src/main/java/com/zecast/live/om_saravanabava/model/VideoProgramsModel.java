package com.zecast.live.om_saravanabava.model;

import java.io.Serializable;

/**
 * Created by codezilla-12 on 19/9/17.
 */

public class VideoProgramsModel implements Serializable {

    String episodeId;
    String getEpisodeName;
    String episodeImage;
    String episodeNumber;
    String videoId;
    String episodeType;
    String episodeUrl;
    String episode_like;
    String episode_seen;
    String episode_like_status;

    public VideoProgramsModel() {
    }

    public VideoProgramsModel(String episodeId, String getEpisodeName, String episodeImage, String episodeNumber, String videoId, String episodeType, String episodeUrl, String episode_like,
                              String episode_seen, String  episode_like_status) {
        this.episodeId = episodeId;
        this.getEpisodeName = getEpisodeName;
        this.episodeImage = episodeImage;
        this.episodeNumber = episodeNumber;
        this.videoId = videoId;
        this.episodeType = episodeType;
        this.episodeUrl = episodeUrl;
        this.episode_like = episode_like;
        this.episode_seen = episode_seen;
        this.episode_like_status = episode_like_status;
    }

    public String getEpisodeId() {
        return episodeId;
    }

    public void setEpisodeId(String episodeId) {
        this.episodeId = episodeId;
    }

    public String getGetEpisodeName() {
        return getEpisodeName;
    }

    public void setGetEpisodeName(String getEpisodeName) {
        this.getEpisodeName = getEpisodeName;
    }

    public String getEpisodeImage() {
        return episodeImage;
    }

    public void setEpisodeImage(String episodeImage) {
        this.episodeImage = episodeImage;
    }

    public String getEpisodeNumber() {
        return episodeNumber;
    }

    public void setEpisodeNumber(String episodeNumber) {
        this.episodeNumber = episodeNumber;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public String getEpisodeType() {
        return episodeType;
    }

    public void setEpisodeType(String episodeType) {
        this.episodeType = episodeType;
    }

    public String getEpisodeUrl() {
        return episodeUrl;
    }

    public void setEpisodeUrl(String episodeUrl) {
        this.episodeUrl = episodeUrl;
    }

    public String getEpisode_like() {
        return episode_like;
    }

    public void setEpisode_like(String episode_like) {
        this.episode_like = episode_like;
    }

    public String getEpisode_seen() {
        return episode_seen;
    }

    public void setEpisode_seen(String episode_seen) {
        this.episode_seen = episode_seen;
    }

    public String getEpisode_like_status() {
        return episode_like_status;
    }

    public void setEpisode_like_status(String episode_seen) {
        this.episode_like_status = episode_like_status;
    }

    @Override
    public String toString() {
        return "VideoProgramsModel{" +
                "episodeId='" + episodeId + '\'' +
                ", getEpisodeName='" + getEpisodeName + '\'' +
                ", episodeImage='" + episodeImage + '\'' +
                ", episodeNumber='" + episodeNumber + '\'' +
                ", videoId='" + videoId + '\'' +
                ", episodeType='" + episodeType + '\'' +
                ", episodeUrl='" + episodeUrl + '\'' +
                ", episode_like='" + episode_like + '\'' +
                ", episode_seen='" + episode_seen + '\'' +
                '}';
    }
}