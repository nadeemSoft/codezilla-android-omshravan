package com.zecast.live.om_saravanabava.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.helper.MessageConstant;
import com.zecast.live.om_saravanabava.model.UrlConstant;
import com.zecast.live.om_saravanabava.utils.Const;
import com.zecast.live.om_saravanabava.utils.Sharedprefrences;
import com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity;
import com.zecast.live.om_saravanabava.callback.CalenderClickListener;
import com.zecast.live.om_saravanabava.callback.RecyclerClickListener;
import com.zecast.live.om_saravanabava.helper.AppUtil;
import com.zecast.live.om_saravanabava.helper.KeyConstant;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class UpcomingFragment extends Fragment implements View.OnClickListener, CalenderClickListener {

    private final String TAG = this.getClass().getSimpleName();
    private Context context;
    private ExpandableHeightGridView calendarView;
    private CalenderAdapter adapter;
    private Calendar calendar;
    private int month, year;
    private RecyclerView recyclerView;
    private CalenderEventAdapter calenderEventAdapter;
    private JSONArray publicEventArray = new JSONArray();
    private TextView toolbarTitle;
    private TextView msg;
    private int eventId = 0;
    private String eventTime = "";
    private String clickedDate;
    String eventTitle,eventStartDate,eventEndDate,eventDescription,eventLocation,eventDuration,eventCode,eventImageURL,eventURL,eventTypeStatusId,eventTypeStatus,eventStatus,eventLikeStatus,eventLikes,eventSeens;
    private String[] monthArray = new String[]{"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    public static String dateee;
    LinearLayout linearLayoutDataContainer;
    LinearLayout linearLayoutProgressContainer;


    String s="0";

    @Override
    public void onStart() {
        super.onStart();
        if (month < 10) {
            Log.e(TAG, /*+"0"*/calendar.get(Calendar.DAY_OF_MONTH) + "-" + "0" + month + "-" + year );
            fetchLiveVideos(calendar.get(Calendar.DAY_OF_MONTH) + "-" + "0" + month + "-" + year);
           // getEventListByDate(calendar.get(Calendar.DAY_OF_MONTH) + "-" /*+ "0"*/ + month + "-" + year);
        } else {
            Log.e(TAG, "checkkk date hereeee: "+calendar.get(Calendar.DAY_OF_MONTH) + "-" + month + "-" + year );
             fetchLiveVideos(calendar.get(Calendar.DAY_OF_MONTH) + "-" + month + "-" + year);
           // getEventListByDate(calendar.get(Calendar.DAY_OF_MONTH) + "-" /*+ "0"*/ + month + "-" + year);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_calender, container, false);
        getActivity().setTitle("Upcoming Events");
        initToolBar(view);
        context = getActivity();
        initView(view);
        initCalender(view);

        HomeDrawerActivity.ivSearch.setVisibility(View.GONE);
        toolbarTitle.setText(monthArray[month - 1] + " " + year);

        if (month < 10) {
             dateee = /*"0"+*/calendar.get(Calendar.DAY_OF_MONTH) + "-" + "0" + month + "-" + year;

            Log.e(TAG, "onCreateView: less then 10 "+dateee );
        } else {
            dateee =calendar.get(Calendar.DAY_OF_MONTH) + "-" + month + "-" + year;

            Log.e(TAG, "onCreateView: greater then 10 "+dateee);
        }

        Log.e(TAG, "checkkk date hereeee: "+calendar.get(Calendar.DAY_OF_MONTH) + "-" + month + "-" + year );

        return view;
    }

    private void initToolBar(View view) {
        ImageButton leftImage = (ImageButton) view.findViewById(R.id.toolbar_left_image);
        ImageButton rightImage = (ImageButton) view.findViewById(R.id.toolbar_right_image);
        toolbarTitle = (TextView) view.findViewById(R.id.toolbar_title);

        leftImage.setOnClickListener(this);
        rightImage.setOnClickListener(this);
    }

    @Override
    public void onResume() {

        super.onResume();

        Const.UPCOMING_FRAGMENT_KEY = 1;

        if (clickedDate != null) {
            fetchLiveVideos(clickedDate);
        }

        fetchLiveVideos(calendar.get(Calendar.DAY_OF_MONTH) + "-" /*+ "0"*/ + month + "-" + year);

        HomeDrawerActivity.lineLive.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.White));
        HomeDrawerActivity.iconTabLive.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_live_unselected));
        HomeDrawerActivity.lineHome.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.White));
        HomeDrawerActivity.iconTabHome.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_home_unselected));
        HomeDrawerActivity.lineUpcoming.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        HomeDrawerActivity.iconTabUpcoming.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_upcoming_tab));
        HomeDrawerActivity.baseUpcomingtext.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        HomeDrawerActivity.lineVideo.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.White));
        HomeDrawerActivity.iconTabVideo.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_video_tab_unselected));
        HomeDrawerActivity.lineProfile.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.White));
        HomeDrawerActivity.iconTabProfile.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_profile_tab_unselected));
        HomeDrawerActivity.baseHomeText.setTextColor(ContextCompat.getColor(getContext(), R.color.gray));
        HomeDrawerActivity.basevideostext.setTextColor(ContextCompat.getColor(getContext(), R.color.gray));
        HomeDrawerActivity.baseProfileText.setTextColor(ContextCompat.getColor(getContext(), R.color.gray));
        HomeDrawerActivity.baseLivetext.setTextColor(ContextCompat.getColor(getContext(), R.color.gray));

    }

    private void initCalender(View view) {
        calendar = Calendar.getInstance(Locale.getDefault());

        month = calendar.get(Calendar.MONTH) + 1;
        year = calendar.get(Calendar.YEAR);
        calendarView = (ExpandableHeightGridView)view.findViewById(R.id.calendar);
        adapter = new CalenderAdapter(context, month, year,this);
    }

    private void initView(View view) {
        msg = (TextView)view.findViewById(R.id.calender_msg);
        recyclerView = (RecyclerView)view.findViewById(R.id.calender_recycler_view);
        linearLayoutDataContainer = (LinearLayout)view.findViewById(R.id.data_container_linear);
        linearLayoutProgressContainer = (LinearLayout)view.findViewById(R.id.progressContainer);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
    }

    private void setGridCellAdapterToDate(int month, int year) {
        adapter = new CalenderAdapter(context, month, year,this);
        calendar.set(year, month - 1, calendar.get(Calendar.DAY_OF_MONTH));
        adapter.notifyDataSetChanged();
        calendarView.setAdapter(adapter);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.toolbar_left_image:
                if (month <= 1) {
                    month = 12;
                    year--;
                } else {
                    month--;
                }
                toolbarTitle.setText(monthArray[month - 1] + " " + year);
                setGridCellAdapterToDate(month, year);
                break;
            case R.id.toolbar_right_image:
                if (month > 11) {
                    month = 1;
                    year++;
                } else {
                    month++;
                }
                toolbarTitle.setText(monthArray[month - 1] + " " + year);
                setGridCellAdapterToDate(month, year);

                break;
            default:
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {}
        return super.onOptionsItemSelected(menuItem);
    }

    private boolean dateMatch;

    private void getEventListByDate(String dateString) {

        Log.e(TAG, "getEventListByDate: dateString"+dateString);
        String newDateString =/* 0+*/dateString;
        int length = publicEventArray.length();
        dateMatch = false;

        for (int i = 0; i < length; i++) {
            final JSONObject jsonObject = publicEventArray.optJSONObject(i);
            String date = jsonObject.optString(KeyConstant.KEY_EVENT_DATE);

            Sharedprefrences.setEventDateObject(context,publicEventArray.toString());
            Sharedprefrences.setEventDateArray(context, String.valueOf(jsonObject));

            if (newDateString.contains(date)) {

                if (getActivity()!=null)
                    {

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            JSONArray jsonArray = jsonObject.optJSONArray(KeyConstant.KEY_PUBLIC_EVENT);
                            Log.d(TAG, "getEventListByDate: jsonArray" + jsonArray.length());
                            dateMatch = true;
                            updateUI(jsonArray);
                        }

                    });
                    return;
                    }
            }
        }

        if (!dateMatch) {
            recyclerView.setVisibility(View.GONE);
            msg.setVisibility(View.VISIBLE);
        }
    }

    private void fetchLiveVideos(final String dateString) {
        RequestQueue queue = Volley.newRequestQueue(getContext());
        StringRequest sr = new StringRequest(com.android.volley.Request.Method.POST, UrlConstant.API_URL+"getCalendarEvents", new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                calendarView.setAdapter(adapter);
                if (response != null) {
                    try {

                        linearLayoutProgressContainer.setVisibility(View.GONE);
                        linearLayoutDataContainer.setVisibility(View.VISIBLE);

                        JSONObject resp = new JSONObject(response);
                        Log.e(TAG, "onResponse: "+response);


                        String errorType = resp.optString(KeyConstant.KEY_ERROR_TYPE);

                        if (errorType.equalsIgnoreCase(KeyConstant.KEY_RESPONSE_CODE_200))
                        {
                            JSONObject responseObj = resp.getJSONObject(KeyConstant.KEY_RESPONSE);

                            if (responseObj != null) {
                                publicEventArray = responseObj.optJSONArray(KeyConstant.KEY_PUBLIC_EVENTS);
                                Log.e(TAG, "onResponse: "+"arraaaayyyy"+publicEventArray );
                                getEventListByDate(dateString);
                               // getEventOnClickDate(dateString);
                                adapter.notifyDataSetChanged();
                            }
                        } else {
                            if (KeyConstant.KEY_MESSAGE_FALSE.equalsIgnoreCase(resp.optString(KeyConstant.KEY_STATUS))) {
                                AppUtil.showMsgAlert(calendarView, resp.optString(KeyConstant.KEY_MESSAGE));
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        AppUtil.showMsgAlert(calendarView, MessageConstant.MESSAGE_SOMETHING_WRONG);
                    }
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("EREROR",String.valueOf(error));
                calendarView.setAdapter(adapter);
                linearLayoutProgressContainer.setVisibility(View.GONE);
                linearLayoutDataContainer.setVisibility(View.VISIBLE);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("vendorKey", Const.vendorKey);
                headers.put("deviceId", Sharedprefrences.getDeviceId(getContext()));
                headers.put("deviceType", "A");
                headers.put("Authorization", Sharedprefrences.getUserKey(getActivity()));
                headers.put("userID", Sharedprefrences.getUserId(getContext()));
                headers.put("Content-Type", Const.contentType);
                return headers;
            }

            @Override
            public Map<String, String> getParams(){
                Map<String, String> params = new HashMap<>();
                return params;
            }
        };

        int socketTimeout = 20000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        sr.setRetryPolicy(policy);
        queue.add(sr);
    }

    private void updateUI(JSONArray jsonArray) {

        Log.e(TAG, "updateUI: calleddd"+jsonArray );

        if (jsonArray != null && jsonArray.length() > 0) {

            calenderEventAdapter = new CalenderEventAdapter(context, getActivity(), jsonArray, new RecyclerClickListener() {

                @Override
                public void setCellClicked(JSONObject newsJSONObject) {
                    eventId = newsJSONObject.optInt("eventId");
                    Log.e("eventId", String.valueOf(eventId));
                    eventTitle = newsJSONObject.optString("eventTitle");
                    eventStartDate = newsJSONObject.optString("eventStartDate");
                    eventEndDate = newsJSONObject.optString("eventEndDate");
                    eventDescription = newsJSONObject.optString("eventDescription");
                    eventLocation = newsJSONObject.optString("eventLocation");
                    eventDuration = newsJSONObject.optString("eventDuration");
                    eventCode = newsJSONObject.optString("eventCode");
                    eventImageURL = newsJSONObject.optString("eventImageURL");
                    eventURL = newsJSONObject.optString("eventURL");
                    eventTypeStatusId = newsJSONObject.optString("eventTypeStatusId");
                    eventStatus = newsJSONObject.optString("eventStatus");
                    eventLikeStatus = newsJSONObject.optString("eventLikeStatus");
                    eventLikes = newsJSONObject.optString("eventLikes");
                    eventSeens = newsJSONObject.optString("eventSeens");
                }
            });

            recyclerView.setAdapter(calenderEventAdapter);
            msg.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            adapter.notifyDataSetChanged();
        }
        else {
            msg.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void setClickedDate(String date) {
        clickedDate = date;
        Log.e(TAG, "setClickedDate: "+date );
        getEventListByDate(date);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Sharedprefrences.clearEventDateArray(context);
        Sharedprefrences.clearEventDateObject(context);
    }

}



/*

    private void getEventListByDate(String dateString) {

        Log.e(TAG, "getEventListByDate: dateString"+dateString);
        String newDateString =*/
/* 0+*//*
dateString;
        int length = publicEventArray.length();
         dateMatch = false;

        for (int i = 0; i < length; i++) {
            final JSONObject jsonObject = publicEventArray.optJSONObject(i);
            String date = jsonObject.optString(KeyConstant.KEY_EVENT_DATE);

           */
/* Sharedprefrences.setEventDateObject(context,publicEventArray.toString());
            Sharedprefrences.setEventDateArray(context, String.valueOf(jsonObject));
*//*

            if (newDateString.contains(date)) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        JSONArray jsonArray = jsonObject.optJSONArray(KeyConstant.KEY_PUBLIC_EVENT);
                        Log.d(TAG, "getEventListByDate: jsonArray" + jsonArray.length());
                        dateMatch = true;
                        updateUI(jsonArray);
                    }
                });
                return;
            }
        }

        if (!dateMatch) {
            recyclerView.setVisibility(View.GONE);
            msg.setVisibility(View.VISIBLE);
       }
    }
*/



//update ui

/*
if (jsonArray != null && jsonArray.length() > 0) {

        calenderEventAdapter = new CalenderEventAdapter(context, getActivity(), jsonArray, new RecyclerClickListener() {

@Override
public void setCellClicked(JSONObject newsJSONObject) {
        eventId = newsJSONObject.optInt("eventId");
        Log.e("eventId", String.valueOf(eventId));
        eventTitle = newsJSONObject.optString("eventTitle");
        eventStartDate = newsJSONObject.optString("eventStartDate");
        eventEndDate = newsJSONObject.optString("eventEndDate");
        eventDescription = newsJSONObject.optString("eventDescription");
        eventLocation = newsJSONObject.optString("eventLocation");
        eventDuration = newsJSONObject.optString("eventDuration");
        eventCode = newsJSONObject.optString("eventCode");
        eventImageURL = newsJSONObject.optString("eventImageURL");
        eventURL = newsJSONObject.optString("eventURL");
        eventTypeStatusId = newsJSONObject.optString("eventTypeStatusId");
        eventStatus = newsJSONObject.optString("eventStatus");
        eventLikeStatus = newsJSONObject.optString("eventLikeStatus");
        eventLikes = newsJSONObject.optString("eventLikes");
        eventSeens = newsJSONObject.optString("eventSeens");
        }
        });

        recyclerView.setAdapter(calenderEventAdapter);
        msg.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        adapter.notifyDataSetChanged();
        }
        else {
        msg.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        }*/


 /*private void getEventListByDate(String dateString) {
        Log.e(TAG, "getEventListByDate: dateString" + dateString);
        int length = publicEventArray.length();
          Log.e("start","1");
        dateMatch = false;
          Log.e("start","2");
        for (int i = 0; i < length; i++) {
            //  Log.e("start","3");
           // Log.e(TAG, "getEventListByDate: "+publicEventArray);
            final JSONObject jsonObject = publicEventArray.optJSONObject(i);
            //Log.e("start","4");
            String date = jsonObject.optString(KeyConstant.KEY_EVENT_DATE);


        *//*    if (dateString.equalsIgnoreCase(date)) {
                JSONArray jsonArray = jsonObject.optJSONArray(KeyConstant.KEY_PUBLIC_EVENT);
                //  Log.e("start","7");
                Log.d(TAG, "getEventListByDate: jsonArray" + jsonArray.length());
                dateMatch = true;
                updateUI(jsonArray);
            }
*//*

            Sharedprefrences.setEventDateObject(context,publicEventArray.toString());
            Sharedprefrences.setEventDateArray(context, String.valueOf(jsonObject));

            // Log.e("start","5");
            if (dateString.equalsIgnoreCase(date)) {
                Log.e(TAG, "getEventListByDate: "+dateString );
              //  Log.e("start","6");
                getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        JSONArray jsonArray = jsonObject.optJSONArray(KeyConstant.KEY_PUBLIC_EVENT);
                        Sharedprefrences.setPublicEventArray(getContext(),jsonArray.toString());
                        Log.d(TAG, "getEventListByDate: jsonArray" + jsonArray.length());
                        dateMatch = true;
                        updateUI();
                    }
                });
                return;
            }
        }

        *//*if (!dateMatch) {
            recyclerView.setVisibility(View.GONE);
            msg.setVisibility(View.VISIBLE);
        }*//*
    }
*/



/*

    private void getEventOnClickDate(final String dateString) {
     //   Log.e("newwww", "getEventOnClickDate: "+dateString);
        User user = PreferenceHelper.getInstance(context).getUserDetail();
        if (AppUtil.isNetworkAvailable(context)) {
            new GetCalenderEventTask(context, user, new AsyncCallback() {
                @Override
                public void setResponse(Integer responseCode, String responseStr) {
                    if (responseStr != null) {
                        try {
                            JSONObject resp = new JSONObject(responseStr);
                            String errorType = resp.optString(KeyConstant.KEY_ERROR_TYPE);
                            if (errorType.equalsIgnoreCase(KeyConstant.KEY_RESPONSE_CODE_200)) {
                                JSONObject responseObj = resp.getJSONObject(KeyConstant.KEY_RESPONSE);
                                if (responseObj != null) {
                                    publicEventArray = responseObj.optJSONArray(KeyConstant.KEY_PUBLIC_EVENTS);

                                    for (int i=0;i<publicEventArray.length();i++) {

                                        final JSONObject jsonObject = publicEventArray.optJSONObject(i);
                                        String date = jsonObject.optString(KeyConstant.KEY_EVENT_DATE);

                                        Sharedprefrences.setEventDateObject(context,publicEventArray.toString());
                                        Sharedprefrences.setEventDateArray(context, String.valueOf(jsonObject));

                                        if (dateString.contains(date)) {
                                            getActivity().runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    JSONArray jsonArray = jsonObject.optJSONArray(KeyConstant.KEY_PUBLIC_EVENT);
                                                    Log.d(TAG, "getEventListByDate: jsonArray" + jsonArray.length());
                                                    dateMatch = true;
                                                    updateUI(jsonArray);
                                                }
                                            });
                                            return;
                                        }


                                    }

                              //      Log.e(TAG, "clickeddateeee: "+dateString);




                                    //getEventListByDate(dateString);
                                   // getEventOnClickDate(dateString);
                                }
                            } else {

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    else {
                    }
                }

                @Override
                public void setException(String e) {
                }
            }).execute();
        } else {
        }
    }
*/
