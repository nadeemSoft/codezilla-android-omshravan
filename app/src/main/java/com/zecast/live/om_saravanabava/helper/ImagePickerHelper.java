package com.zecast.live.om_saravanabava.helper;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.util.TypedValue;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Calendar;

public class ImagePickerHelper {
    /**
     * Calculate a final image multiplied with which both dimensions larger than
     * or equal to the requested height and width.
     *
     * @param options
     *            {@link BitmapFactory} containing the
     *            outHieght & outWidth of the image
     * @param reqWidth
     *            new width of the image to cropped
     * @param reqHeight
     *            new height of the image to cropped
     * @return the smallest multiplier possible.
     */
    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight, boolean limitSize)
    {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth)
        {
            // Calculate ratios of height and width to requested height and
            // width
            final int heightRatio = Math.round((float) height
                    / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will
            // guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            if(limitSize)
            {
                inSampleSize = heightRatio > widthRatio ? heightRatio : widthRatio;
            }
            else
            {
                inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
            }
        }
        return inSampleSize;
    }
    public static Bitmap decodeSampledBitmapFromResource(Resources res,
                                                         int resId, int reqWidth, int reqHeight, boolean limitSize)

    {
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);
        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight, limitSize);
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    /**
     * Given a resource image returns a optimized
     * {@link Bitmap} object
     *
     * @param res
     *            {@link Resources} object to access the
     *            resorce
     * @param resId
     *            id of the image resource
     * @param reqWidth
     *            width of the required optimized image
     * @param reqHeight
     *            height of the required optimized image
     * @return {@link Bitmap}
     */
    public static Bitmap decodeSampledBitmapFromResource(Resources res,
                                                         int resId, int reqWidth, int reqHeight)
    {
        return ImagePickerHelper.decodeSampledBitmapFromResource(res, resId, reqWidth, reqHeight, false);
    }
    /**
     * Given a Image Uri returns a optimized {@link Bitmap}
     * object
     *
     * @param contentUri
     *            {@link Uri} pointing to the image
     * @param context
     *            current context
     * @param reqWidth
     *            width of the required optimized image
     * @param reqHeight
     *            height of the required optimized image
     * @param limitSize
     *            Should the both the width and height be less than or equal to the reqd.
     * @return {@link Bitmap}
     */
    public static Bitmap decodeSampledBitmapFromURI(Uri contentUri,
                                                    Context context, int reqWidth, int reqHeight, boolean limitSize)
    {
        try {
            InputStream in = context.getContentResolver().openInputStream(
                    contentUri);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, options);
            options.inSampleSize = calculateInSampleSize(options, reqWidth,
                    reqHeight,limitSize);

            InputStream in2 = context.getContentResolver().openInputStream(
                    contentUri);
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeStream(in2, null, options);
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static File getImageFile(boolean isTemp)
    {
        File imageDir = new File(Environment.getExternalStorageDirectory()
                .getAbsolutePath() + File.separator + "omsharawanababa" + File.separator);
        if (!imageDir.exists())
        {
            imageDir.mkdirs();
        }
        Calendar c = Calendar.getInstance();
        String prefix = "IMG_";
        if (isTemp)
        {
            prefix = "TEMP_";
        }
        String cameraImageFileName = prefix
                + c.get(Calendar.YEAR)
                + "-"
                + ((c.get(Calendar.MONTH) + 1 < 10) ? ("0" + (c
                .get(Calendar.MONTH) + 1))
                : (c.get(Calendar.MONTH) + 1))
                + "-"
                + ((c.get(Calendar.DAY_OF_MONTH) < 10) ? ("0" + c
                .get(Calendar.DAY_OF_MONTH)) : c
                .get(Calendar.DAY_OF_MONTH))
                + "_"
                + ((c.get(Calendar.HOUR_OF_DAY) < 10) ? ("0" + c
                .get(Calendar.HOUR_OF_DAY)) : c
                .get(Calendar.HOUR_OF_DAY))
                + "-"
                + ((c.get(Calendar.MINUTE) < 10) ? ("0" + c
                .get(Calendar.MINUTE)) : c.get(Calendar.MINUTE))
                + "-"
                + ((c.get(Calendar.SECOND) < 10) ? ("0" + c
                .get(Calendar.SECOND)) : c.get(Calendar.SECOND)) + "-"
                + c.get(Calendar.MILLISECOND) + ".jpg";
        File imageFile = new File(imageDir, cameraImageFileName);
        return imageFile;
    }

    /**
     * Creates a File in the external sd card directory of the device in this
     * apps folder with a IMG prefix
     *
     * @return {@link File} for an jpg image
     */
    public static File getImageFile()
    {
        return getImageFile(false);
    }

    /**
     * Creates a File in the external sd card directory of the device in this
     * apps folder with a TEMP prefix
     *
     * @return {@link File} for an jpg image
     */
    public static File getTemporaryFile()
    {
        return getImageFile(true);
    }

    public static int getPixels(int dps, Context context)
    {
        int pixels = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, dps, context.getResources()
                        .getDisplayMetrics()
        );
        return pixels;
    }
}