package com.zecast.live.om_saravanabava.model;

/**
 * Created by codezilla-12 on 3/10/17.
 */

public class MembershipTypeDetailsModel {

    String membershiTypeId;
    String membershiTypeTitle;
    String membershiTypeName;
    String membershiTypeDescription;
    String membershiTypeStatusId;
    String membershiTypeStatus;


    public MembershipTypeDetailsModel(String membershiTypeId, String membershiTypeTitle, String membershiTypeName, String membershiTypeDescription, String membershiTypeStatusId, String membershiTypeStatus) {
        this.membershiTypeId = membershiTypeId;
        this.membershiTypeTitle = membershiTypeTitle;
        this.membershiTypeName = membershiTypeName;
        this.membershiTypeDescription = membershiTypeDescription;
        this.membershiTypeStatusId = membershiTypeStatusId;
        this.membershiTypeStatus = membershiTypeStatus;
    }

    public String getMembershiTypeId() {
        return membershiTypeId;
    }

    public void setMembershiTypeId(String membershiTypeId) {
        this.membershiTypeId = membershiTypeId;
    }

    public String getMembershiTypeTitle() {
        return membershiTypeTitle;
    }

    public void setMembershiTypeTitle(String membershiTypeTitle) {
        this.membershiTypeTitle = membershiTypeTitle;
    }

    public String getMembershiTypeName() {
        return membershiTypeName;
    }

    public void setMembershiTypeName(String membershiTypeName) {
        this.membershiTypeName = membershiTypeName;
    }

    public String getMembershiTypeDescription() {
        return membershiTypeDescription;
    }

    public void setMembershiTypeDescription(String membershiTypeDescription) {
        this.membershiTypeDescription = membershiTypeDescription;
    }

    public String getMembershiTypeStatusId() {
        return membershiTypeStatusId;
    }

    public void setMembershiTypeStatusId(String membershiTypeStatusId) {
        this.membershiTypeStatusId = membershiTypeStatusId;
    }

    public String getMembershiTypeStatus() {
        return membershiTypeStatus;
    }

    public void setMembershiTypeStatus(String membershiTypeStatus) {
        this.membershiTypeStatus = membershiTypeStatus;
    }
}
