package com.zecast.live.om_saravanabava.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.model.MembershipTypeDetailsModel;
import com.zecast.live.om_saravanabava.activity.membership.MembershipDetailActivity;
import java.util.ArrayList;

import static com.android.volley.VolleyLog.TAG;

public class ProfileMembershipAdapter extends RecyclerView.Adapter<ProfileMembershipAdapter.MyViewHolder> {

    private Context context;
    ArrayList<MembershipTypeDetailsModel> arrayListData;
    private ArrayList<Integer> mDataset;

    public ProfileMembershipAdapter(Context context, ArrayList<MembershipTypeDetailsModel> mAudioList, ArrayList<Integer> mDataset) {
        this.context = context;
        this.arrayListData = mAudioList;
        this.mDataset = mDataset;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = View.inflate(context, R.layout.single_row_item_membership_type_adapter, null);
        return new ProfileMembershipAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder( final MyViewHolder holder, final int position) {

        final MembershipTypeDetailsModel model = arrayListData.get(position);

        Log.e(TAG, "onBindViewHolder: "+mDataset.size()+"viewholder positionnn"+holder.getAdapterPosition() );
        holder.tvTitleMembership.setText(model.getMembershiTypeName());

        mDataset.clear();
        mDataset.add(holder.getAdapterPosition());

        switch (model.getMembershiTypeStatusId()) {

            case "2":
                holder.imgTypeMembership.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_update_status_pending));
                holder.imgTypeMembership.setColorFilter(ContextCompat.getColor(context, R.color.red));
                holder.tvTitleMembership.setTextColor(ContextCompat.getColor(context, R.color.gray));
                holder.tvTitleMembership.setEnabled(false);
                break;

            case "3":
                holder.imgTypeMembership.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_success_membership));
                holder.imgTypeMembership.setColorFilter(null);
                holder.tvTitleMembership.setTextColor(ContextCompat.getColor(context, R.color.green));
                break;

            case "1":
                holder.imgTypeMembership.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_membership_add));
                holder.imgTypeMembership.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary));
                holder.tvTitleMembership.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                break;

            default:
                holder.imgTypeMembership.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_membership_add));
                holder.imgTypeMembership.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary));
                holder.tvTitleMembership.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                break;
        }



        holder.lllsubcribemembershipImgContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("re", model.getMembershiTypeStatus());

                if (model.getMembershiTypeStatus().equals("Requested") || model.getMembershiTypeStatus().equals("requested"))
                {
                    return;
                }

                if (model.getMembershiTypeStatus().equals("Accepted") ||model.getMembershiTypeStatus().equals("accepted") )
                {
                    return;
                }
                else
                {
                    Log.e("yes","2");
                    Intent intent = new Intent(context, MembershipDetailActivity.class);
                    intent.putExtra("titlemembershiptype", model.getMembershiTypeName());
                    intent.putExtra("membershiTypeId", model.getMembershiTypeId());
                    intent.putExtra("membershiTypeDescription", model.getMembershiTypeDescription());
                    intent.putExtra("membershiTypeStatus", model.getMembershiTypeStatus());
                    intent.putExtra("membershiTypeTitle", model.getMembershiTypeTitle());
                    intent.putExtra("membershiTypeStatusId", model.getMembershiTypeStatusId());

                    context.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayListData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

       TextView tvTitleMembership;
       ImageView imgTypeMembership;
        LinearLayout lllsubcribemembershipImgContainer;

        public MyViewHolder(View itemView)  {

            super(itemView);
            tvTitleMembership = (TextView)itemView.findViewById(R.id.membership_text_view);
            imgTypeMembership = (ImageView)itemView.findViewById(R.id.membership_status_logo_1);
            lllsubcribemembershipImgContainer = (LinearLayout)itemView.findViewById(R.id.container_subscribe_membership);

        }


        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.tv_view_details:
                    break;
            }
        }
    }
}

//   android:tint="@color/colorPrimary"

/*if (position == 0) {
               if (isClicked1 == true) {
                   holder.lllsubcribemembershipImgContainer.setEnabled(false);
                   Log.d(TAG, "onBindViewHolder: " + "inside loop");
               }
           }*/
