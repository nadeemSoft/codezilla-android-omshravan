package com.zecast.live.om_saravanabava.activity.homescreen;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;
import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.activity.eventdetails.EventDetailsActivity;
import com.zecast.live.om_saravanabava.activity.search.SearchActivity;
import com.zecast.live.om_saravanabava.activity.videodetails.VideoDetailsActivity;
import com.zecast.live.om_saravanabava.activity.welcome.WelcomeActivity;
import com.zecast.live.om_saravanabava.adapter.HomeSideNavDrawerAdapter;
import com.zecast.live.om_saravanabava.fragments.HomeFragment;
import com.zecast.live.om_saravanabava.fragments.LiveFragment;
import com.zecast.live.om_saravanabava.fragments.ProfileFragment;
import com.zecast.live.om_saravanabava.fragments.UpcomingFragment;
import com.zecast.live.om_saravanabava.fragments.VideoFragment;
import com.zecast.live.om_saravanabava.model.NavDrawerDataModel;
import com.zecast.live.om_saravanabava.utils.Const;
import com.zecast.live.om_saravanabava.utils.HTTPRequest;
import com.zecast.live.om_saravanabava.utils.Sharedprefrences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;


public class HomeDrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,View.OnClickListener {

    public static RelativeLayout relativeHometab,relativeLivetab,relativeVideotab,relativeUpcomingTab,relativeProfileTab;
    Fragment fragmentHome,fragmentLive,fragmentVideo,fragmentUpcoming,fragmentProfile;
    public static ImageView iconTabHome,iconTabLive,iconTabVideo,iconTabUpcoming,iconTabProfile;
    public static View lineHome,lineLive,lineVideo,lineUpcoming,lineProfile;
    RecyclerView recyclerHomeDideDrawer;
    LinearLayoutManager layoutManager;
    public static final ArrayList<NavDrawerDataModel> drawerList = new ArrayList<>();
    HomeSideNavDrawerAdapter adapterSideDrawer;
    NavigationView navigationView;
    public static DrawerLayout drawer;
    public static TextView tvUserCountry,tvUserName,tvUserEamil;
    public static LinearLayout bottombarContainerLinear;
    public static Toolbar toolbar;
    public static FrameLayout frameContainer;
    public static TextView baseHomeText,baseLivetext,basevideostext,baseUpcomingtext,baseProfileText;
    private ActionBarDrawerToggle mDrawerToggle;
    CoordinatorLayout coor;
    public static ImageView imgprofileSideDrawer;
    String liveDrect;
   public static ImageView ivSearch;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_drawer);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        try
        {
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                liveDrect = extras.getString("goLive");
                Log.e("direct",liveDrect);
            }

            }catch (Exception e){}


        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        ivSearch = (ImageView)findViewById(R.id.imageSearch);
        ivSearch.setOnClickListener(this);
        HomeDrawerActivity.ivSearch.setVisibility(View.GONE);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Home");
        coor = (CoordinatorLayout)findViewById(R.id.coor);
        toolbar.setTitleTextColor(Color.WHITE);
        Window window = this.getWindow();
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.colorPrimary));

        ToggleButton Btn=new ToggleButton(this); // or get it from the layout by ToggleButton Btn=(ToggleButton) findViewById(R.id.IDofButton);
        Btn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // TODO Auto-generated method stub
                if(isChecked)
                    buttonView.setBackgroundColor(Color.BLUE);
                else buttonView.setBackgroundColor(Color.WHITE);
            }
        });

        initWidgets();
        fetchDfrawerData();
        initializeFragments();
        getrootFragment();
        setDefaultBottombarColor();
        initializeNavigationAdapter();
        sslCertificateInstall();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        mDrawerToggle = new ActionBarDrawerToggle(this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerClosed(View view) {
                supportInvalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                supportInvalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                coor.setTranslationX(slideOffset * drawerView.getWidth());
                coor.bringChildToFront(drawerView);
                coor.requestLayout();
                //below line used to remove shadow of drawer
                //   drawerLayout.setScrimColor(Color.TRANSPARENT);
            }
        };

        drawer.setDrawerListener(mDrawerToggle);

        //fill drawer data

        if(Sharedprefrences.getUserProfile(getApplicationContext())!="") {
            Picasso.with(this)
                    .load(Sharedprefrences.getUserProfile(getApplicationContext()))
                    .error(R.drawable.app_icon)
                    .placeholder(R.drawable.app_icon)
                    .into(imgprofileSideDrawer);
        }

        navigationView.setNavigationItemSelectedListener(this);
        relativeHometab.setOnClickListener(this);
        relativeLivetab.setOnClickListener(this);
        relativeVideotab.setOnClickListener(this);
        relativeUpcomingTab.setOnClickListener(this);
        relativeProfileTab.setOnClickListener(this);
    }

    private void sslCertificateInstall() {

        TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }
                    public void checkClientTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }
                    public void checkServerTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }
                }
        };

        // Install the all-trusting trust manager
        // Try "SSL" or Replace with "TLS"
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            Log.e("hhtps","Install");
        } catch (Exception e) {
        }

    }

    private void initializeNavigationAdapter() {
        tvUserCountry.setText(Sharedprefrences.getUserCountry(getBaseContext()));
        tvUserName.setText(Sharedprefrences.getUserName(getBaseContext()));
        tvUserEamil.setText(Sharedprefrences.getUserEmail(getBaseContext()));

        Log.d("ACTIVITY SHAREDPREF", "initializeNavigationAdapter: "+Sharedprefrences.getUserCountry(getBaseContext()));
    }

    private void setDefaultBottombarColor() {
        lineHome.setBackgroundColor(ContextCompat.getColor(this,R.color.colorPrimary));
        iconTabHome.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_home_tab));
        lineLive.setBackgroundColor(ContextCompat.getColor(this,R.color.White));
        iconTabLive.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_live_unselected));
        lineUpcoming.setBackgroundColor(ContextCompat.getColor(this,R.color.White));
        iconTabUpcoming.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_upcoming_unselected));
        lineVideo.setBackgroundColor(ContextCompat.getColor(this,R.color.White));
        iconTabVideo.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_video_tab_unselected));
        lineProfile.setBackgroundColor(ContextCompat.getColor(this,R.color.White));
        iconTabProfile.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_profile_tab_unselected));
        baseHomeText.setTextColor(ContextCompat.getColor(this,R.color.colorPrimary));
        basevideostext.setTextColor(ContextCompat.getColor(this,R.color.gray));
        baseUpcomingtext.setTextColor(ContextCompat.getColor(this,R.color.gray));
        baseProfileText.setTextColor(ContextCompat.getColor(this,R.color.gray));
        baseLivetext.setTextColor(ContextCompat.getColor(this,R.color.gray));
    }

    private void initializeFragments() {
        fragmentHome = new HomeFragment();
        fragmentLive = new LiveFragment();
        fragmentVideo = new VideoFragment();
        fragmentUpcoming = new UpcomingFragment();
        fragmentProfile = new ProfileFragment();
    }

    private void getrootFragment() {

       FragmentTransaction fg = getSupportFragmentManager().beginTransaction();

        if (liveDrect!=null) {
                fg.replace(R.id.frame_drawer, new LiveFragment());
                fg.commit();

            FragmentTransaction fg2 = getSupportFragmentManager().beginTransaction(); //to begin home page with fragment
            fg2.replace(R.id.frame_drawer, new LiveFragment());
            fg2.commit();
            }

            else {
                fg.replace(R.id.frame_drawer, new HomeFragment());
                fg.commit();
            }

                //to begin home page with fragment


    }

    //TODO: initialized the views

    private void initWidgets() {
        relativeHometab = (RelativeLayout) findViewById(R.id.rl_home);
        relativeLivetab = (RelativeLayout) findViewById(R.id.rl_live);
        relativeVideotab = (RelativeLayout) findViewById(R.id.rl_videos);
        relativeUpcomingTab = (RelativeLayout) findViewById(R.id.rl_upcoming);
        relativeProfileTab = (RelativeLayout) findViewById(R.id.rl_profile);
        frameContainer = (FrameLayout) findViewById(R.id.frame_drawer);
        iconTabHome = (ImageView)findViewById(R.id.base_home_image);
        iconTabLive = (ImageView)findViewById(R.id.base_live_image);
        iconTabVideo = (ImageView)findViewById(R.id.base_video_image);
        iconTabUpcoming = (ImageView)findViewById(R.id.base_upcoming_image);
        iconTabProfile = (ImageView)findViewById(R.id.base_profile_image);
        lineHome = (View) findViewById(R.id.line_home);
        lineLive = (View) findViewById(R.id.line_live);
        lineVideo = (View) findViewById(R.id.line_video);
        lineUpcoming = (View) findViewById(R.id.line_upcoming);
        lineProfile = (View) findViewById(R.id.line_profile);
        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        recyclerHomeDideDrawer = (RecyclerView)findViewById(R.id.navigation_drawer_recycler);
        tvUserCountry = (TextView) findViewById(R.id.tvUserCountry);
        tvUserEamil = (TextView) findViewById(R.id.tvUseremail);
        tvUserName = (TextView)findViewById(R.id.tvUsername);
        bottombarContainerLinear = (LinearLayout)findViewById(R.id.linear_bottom_bar_container);
        frameContainer = (FrameLayout)findViewById(R.id.frame_drawer);
        baseHomeText = (TextView)findViewById(R.id.base_home_text);
        baseLivetext = (TextView)findViewById(R.id.base_live_text);
        baseUpcomingtext = (TextView)findViewById(R.id.base_upcoming_text);
        baseProfileText = (TextView)findViewById(R.id.base_profile_text);
        basevideostext = (TextView)findViewById(R.id.base_video_text);
        imgprofileSideDrawer = (ImageView)findViewById(R.id.account_profile_side_drawer);
    }

    private void fetchDfrawerData() {

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest sr = new StringRequest(Request.Method.POST, HTTPRequest.SERVICE_URL+"getLiveTv", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.e("response_catcup", response);
                    drawerList.clear();

                    try {
                        JSONObject response1 = new JSONObject(response);
                        String responseStatus = response1.getString("status");
                        String errorType = response1.optString("error_type");
                        String responseMessage = response1.optString("message");

                        JSONObject openingObj = response1.optJSONObject("response");
                        JSONArray jsonLeftMenuArray = openingObj.getJSONArray("left_menu");

                            for (int j = 0;j<jsonLeftMenuArray.length();j++) {
                                JSONObject openingObjArrayLeftMenu = jsonLeftMenuArray.getJSONObject(j);

                                    String leftMenuId = openingObjArrayLeftMenu.optString("left_menu_id");
                                    String leftMenuName = openingObjArrayLeftMenu.optString("left_menu_name");
                                    String leftMenuService = openingObjArrayLeftMenu.optString("left_menu_service");
                                    NavDrawerDataModel model = new NavDrawerDataModel(leftMenuName,leftMenuId);
                                    drawerList.add(model);
                            }

                        adapterSideDrawer = new HomeSideNavDrawerAdapter(HomeDrawerActivity.this,drawerList);
                        layoutManager = new LinearLayoutManager(HomeDrawerActivity.this,LinearLayoutManager.VERTICAL, false);
                        recyclerHomeDideDrawer.setLayoutManager(layoutManager);
                        recyclerHomeDideDrawer.setAdapter(adapterSideDrawer);
                        recyclerHomeDideDrawer.setNestedScrollingEnabled(false);
                        adapterSideDrawer.notifyDataSetChanged();

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("EXCEPTION",e.getMessage());
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("EREROR HOME LEFTY MENU ",String.valueOf(error));
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("vendorKey", Const.vendorKey);
                headers.put("deviceType", "A");
                headers.put("Authorization",Sharedprefrences.getUserKey(getBaseContext()));
                headers.put("Content-Type",Const.contentType);

                return headers;
            }

            @Override
            public Map<String, String> getParams(){
                Map<String, String> params = new HashMap<>();

                return params;
            }
        };

        int socketTimeout = 20000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        sr.setRetryPolicy(policy);
        queue.add(sr);
    }

    @Override
    public void onBackPressed() {

        int orientation = this.getResources().getConfiguration().orientation;
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {

            HomeDrawerActivity.drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

            if (LiveFragment.ivPause.getVisibility()==View.VISIBLE) {
                LiveFragment.ivPlay.setVisibility(View.GONE);
                LiveFragment.ivPause.setVisibility(View.VISIBLE);
            }if (LiveFragment.ivPlay.getVisibility()==View.VISIBLE) {
                LiveFragment.ivPlay.setVisibility(View.VISIBLE);
                LiveFragment.ivPause.setVisibility(View.GONE);
            }

            LiveFragment.videoLayout.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
            LiveFragment.videoLayout.getLayoutParams().width =  ViewGroup.LayoutParams.FILL_PARENT;
            LiveFragment.llVideoContainer.getLayoutParams().height= ViewGroup.LayoutParams.WRAP_CONTENT;

            HomeDrawerActivity.toolbar.setVisibility(View.VISIBLE);
            HomeDrawerActivity.bottombarContainerLinear.setVisibility(View.VISIBLE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        else {

            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//***Change Here***
            startActivity(intent);
            System.exit(0);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

               switch (item.getItemId()) {

            case R.id.rl_home:
                FragmentTransaction fg1 = getSupportFragmentManager().beginTransaction(); //to begin home page with fragment
                fg1.replace(R.id.frame_drawer, new HomeFragment());
                fg1.commit();

                lineHome.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
                iconTabHome.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_home_tab));
                lineLive.setBackgroundColor(ContextCompat.getColor(this, R.color.White));
                iconTabLive.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_live_unselected));
                lineUpcoming.setBackgroundColor(ContextCompat.getColor(this, R.color.White));
                iconTabUpcoming.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_upcoming_unselected));
                lineVideo.setBackgroundColor(ContextCompat.getColor(this, R.color.White));
                iconTabVideo.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_video_tab_unselected));
                lineProfile.setBackgroundColor(ContextCompat.getColor(this, R.color.White));
                iconTabProfile.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_profile_tab_unselected));
                baseHomeText.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
                basevideostext.setTextColor(ContextCompat.getColor(this, R.color.gray));
                baseUpcomingtext.setTextColor(ContextCompat.getColor(this, R.color.gray));
                baseProfileText.setTextColor(ContextCompat.getColor(this, R.color.gray));
                baseLivetext.setTextColor(ContextCompat.getColor(this, R.color.gray));

                break;

            case R.id.rl_live:
                FragmentTransaction fg2 = getSupportFragmentManager().beginTransaction(); //to begin home page with fragment
                fg2.replace(R.id.frame_drawer, new LiveFragment());
                fg2.commit();

                lineLive.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
                iconTabLive.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_live_tab));
                lineHome.setBackgroundColor(ContextCompat.getColor(this, R.color.White));
                iconTabHome.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_home_unselected));
                lineUpcoming.setBackgroundColor(ContextCompat.getColor(this, R.color.White));
                iconTabUpcoming.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_upcoming_unselected));
                lineVideo.setBackgroundColor(ContextCompat.getColor(this, R.color.White));
                iconTabVideo.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_video_tab_unselected));
                lineProfile.setBackgroundColor(ContextCompat.getColor(this, R.color.White));
                iconTabProfile.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_profile_tab_unselected));
                baseHomeText.setTextColor(ContextCompat.getColor(this, R.color.gray));
                basevideostext.setTextColor(ContextCompat.getColor(this, R.color.gray));
                baseUpcomingtext.setTextColor(ContextCompat.getColor(this, R.color.gray));
                baseProfileText.setTextColor(ContextCompat.getColor(this, R.color.gray));
                baseLivetext.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));

                break;

            case R.id.rl_videos:
                FragmentTransaction fg3 = getSupportFragmentManager().beginTransaction(); //to begin home page with fragment
                fg3.replace(R.id.frame_drawer, new VideoFragment());
                fg3.commit();

                lineLive.setBackgroundColor(ContextCompat.getColor(this, R.color.White));
                iconTabLive.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_live_unselected));
                lineHome.setBackgroundColor(ContextCompat.getColor(this, R.color.White));
                iconTabHome.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_home_unselected));
                lineUpcoming.setBackgroundColor(ContextCompat.getColor(this, R.color.White));
                iconTabUpcoming.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_upcoming_unselected));
                lineVideo.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
                iconTabVideo.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_video_tab));
                lineProfile.setBackgroundColor(ContextCompat.getColor(this, R.color.White));
                iconTabProfile.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_profile_tab_unselected));
                baseHomeText.setTextColor(ContextCompat.getColor(this, R.color.gray));
                basevideostext.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
                baseUpcomingtext.setTextColor(ContextCompat.getColor(this, R.color.gray));
                baseProfileText.setTextColor(ContextCompat.getColor(this, R.color.gray));
                baseLivetext.setTextColor(ContextCompat.getColor(this, R.color.gray));
                break;

            case R.id.rl_upcoming:

                FragmentTransaction fg4 = getSupportFragmentManager().beginTransaction(); //to begin home page with fragment
                fg4.replace(R.id.frame_drawer, new UpcomingFragment());
                fg4.commit();

                lineLive.setBackgroundColor(ContextCompat.getColor(this, R.color.White));
                iconTabLive.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_live_unselected));
                lineHome.setBackgroundColor(ContextCompat.getColor(this, R.color.White));
                iconTabHome.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_home_unselected));
                lineUpcoming.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
                iconTabUpcoming.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_upcoming_tab));
                lineVideo.setBackgroundColor(ContextCompat.getColor(this, R.color.White));
                iconTabVideo.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_video_tab_unselected));
                lineProfile.setBackgroundColor(ContextCompat.getColor(this, R.color.White));
                iconTabProfile.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_profile_tab_unselected));
                baseHomeText.setTextColor(ContextCompat.getColor(this, R.color.gray));
                basevideostext.setTextColor(ContextCompat.getColor(this, R.color.gray));
                baseUpcomingtext.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
                baseProfileText.setTextColor(ContextCompat.getColor(this, R.color.gray));
                baseLivetext.setTextColor(ContextCompat.getColor(this, R.color.gray));

                break;

            case R.id.rl_profile:

//                if (SplashScreenActivity.isLogin != 0)
//                {
//                    FragmentTransaction fg5 = getSupportFragmentManager().beginTransaction(); //to begin home page with fragment
//                    fg5.replace(R.id.frame_drawer, new ProfileFragment());
//                    fg5.commit();
//                }
//
//                else {
//
//                    Intent in = new Intent(HomeDrawerActivity.this, WelcomeActivity.class);
//                    in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(in);
//                }

                lineLive.setBackgroundColor(ContextCompat.getColor(this, R.color.White));
                iconTabLive.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_live_unselected));
                lineHome.setBackgroundColor(ContextCompat.getColor(this, R.color.White));
                iconTabHome.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_home_unselected));
                lineUpcoming.setBackgroundColor(ContextCompat.getColor(this, R.color.White));
                iconTabUpcoming.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_upcoming_unselected));
                lineVideo.setBackgroundColor(ContextCompat.getColor(this, R.color.White));
                iconTabVideo.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_video_tab_unselected));
                lineProfile.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
                iconTabProfile.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_profile_tab));
                baseHomeText.setTextColor(ContextCompat.getColor(this, R.color.gray));
                basevideostext.setTextColor(ContextCompat.getColor(this, R.color.gray));
                baseUpcomingtext.setTextColor(ContextCompat.getColor(this, R.color.gray));
                baseProfileText.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
                baseLivetext.setTextColor(ContextCompat.getColor(this, R.color.gray));
                break;

            default:
                break;

        }

        return false;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.rl_home:
                FragmentTransaction fg1 = getSupportFragmentManager().beginTransaction(); //to begin home page with fragment
                fg1.replace(R.id.frame_drawer, new HomeFragment());
                fg1.commit();

                lineHome.setBackgroundColor(ContextCompat.getColor(this,R.color.colorPrimary));
                iconTabHome.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_home_tab));
                lineLive.setBackgroundColor(ContextCompat.getColor(this,R.color.White));
                iconTabLive.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_live_unselected));
                 lineUpcoming.setBackgroundColor(ContextCompat.getColor(this,R.color.White));
                iconTabUpcoming.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_upcoming_unselected));
                lineVideo.setBackgroundColor(ContextCompat.getColor(this,R.color.White));
                iconTabVideo.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_video_tab_unselected));
                lineProfile.setBackgroundColor(ContextCompat.getColor(this,R.color.White));
                iconTabProfile.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_profile_tab_unselected));
                baseHomeText.setTextColor(ContextCompat.getColor(this,R.color.colorPrimary));
                basevideostext.setTextColor(ContextCompat.getColor(this,R.color.gray));
                baseUpcomingtext.setTextColor(ContextCompat.getColor(this,R.color.gray));
                baseProfileText.setTextColor(ContextCompat.getColor(this,R.color.gray));
                baseLivetext.setTextColor(ContextCompat.getColor(this,R.color.gray));

                break;

            case R.id.rl_live:
                FragmentTransaction fg2 = getSupportFragmentManager().beginTransaction(); //to begin home page with fragment
                fg2.replace(R.id.frame_drawer, new LiveFragment());
                fg2.commit();

                lineLive.setBackgroundColor(ContextCompat.getColor(this,R.color.colorPrimary));
                iconTabLive.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_live_tab));
                lineHome.setBackgroundColor(ContextCompat.getColor(this,R.color.White));
                iconTabHome.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_home_unselected));
                lineUpcoming.setBackgroundColor(ContextCompat.getColor(this,R.color.White));
                iconTabUpcoming.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_upcoming_unselected));
                lineVideo.setBackgroundColor(ContextCompat.getColor(this,R.color.White));
                iconTabVideo.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_video_tab_unselected));
                lineProfile.setBackgroundColor(ContextCompat.getColor(this,R.color.White));
                iconTabProfile.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_profile_tab_unselected));
                baseHomeText.setTextColor(ContextCompat.getColor(this,R.color.gray));
                basevideostext.setTextColor(ContextCompat.getColor(this,R.color.gray));
                baseUpcomingtext.setTextColor(ContextCompat.getColor(this,R.color.gray));
                baseProfileText.setTextColor(ContextCompat.getColor(this,R.color.gray));
                baseLivetext.setTextColor(ContextCompat.getColor(this,R.color.colorPrimary));

                break;

            case R.id.rl_videos:
                FragmentTransaction fg3 = getSupportFragmentManager().beginTransaction(); //to begin home page with fragment
                fg3.replace(R.id.frame_drawer, new VideoFragment());
                fg3.commit();

                lineLive.setBackgroundColor(ContextCompat.getColor(this,R.color.White));
                iconTabLive.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_live_unselected));
                lineHome.setBackgroundColor(ContextCompat.getColor(this,R.color.White));
                iconTabHome.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_home_unselected));
                lineUpcoming.setBackgroundColor(ContextCompat.getColor(this,R.color.White));
                iconTabUpcoming.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_upcoming_unselected));
                lineVideo.setBackgroundColor(ContextCompat.getColor(this,R.color.colorPrimary));
                iconTabVideo.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_video_tab));
                lineProfile.setBackgroundColor(ContextCompat.getColor(this,R.color.White));
                iconTabProfile.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_profile_tab_unselected));
                baseHomeText.setTextColor(ContextCompat.getColor(this,R.color.gray));
                basevideostext.setTextColor(ContextCompat.getColor(this,R.color.colorPrimary));
                baseUpcomingtext.setTextColor(ContextCompat.getColor(this,R.color.gray));
                baseProfileText.setTextColor(ContextCompat.getColor(this,R.color.gray));
                baseLivetext.setTextColor(ContextCompat.getColor(this,R.color.gray));
                break;

            case R.id.rl_upcoming:
                FragmentTransaction fg4 = getSupportFragmentManager().beginTransaction(); //to begin home page with fragment
                fg4.replace(R.id.frame_drawer, new UpcomingFragment()/*UpcomingFragment()*/);
                fg4.commit();

                lineLive.setBackgroundColor(ContextCompat.getColor(this,R.color.White));
                iconTabLive.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_live_unselected));
                lineHome.setBackgroundColor(ContextCompat.getColor(this,R.color.White));
                iconTabHome.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_home_unselected));
                lineUpcoming.setBackgroundColor(ContextCompat.getColor(this,R.color.colorPrimary));
                iconTabUpcoming.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_upcoming_tab));
                lineVideo.setBackgroundColor(ContextCompat.getColor(this,R.color.White));
                iconTabVideo.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_video_tab_unselected));
                lineProfile.setBackgroundColor(ContextCompat.getColor(this,R.color.White));
                iconTabProfile.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_profile_tab_unselected));
                baseHomeText.setTextColor(ContextCompat.getColor(this,R.color.gray));
                basevideostext.setTextColor(ContextCompat.getColor(this,R.color.gray));
                baseUpcomingtext.setTextColor(ContextCompat.getColor(this,R.color.colorPrimary));
                baseProfileText.setTextColor(ContextCompat.getColor(this,R.color.gray));
                baseLivetext.setTextColor(ContextCompat.getColor(this,R.color.gray));
                break;

            case R.id.rl_profile:

                if (!Sharedprefrences.getUserEmail(HomeDrawerActivity.this).isEmpty())
                {
                    FragmentTransaction fg5 = getSupportFragmentManager().beginTransaction(); //to begin home page with fragment
                    fg5.replace(R.id.frame_drawer, new ProfileFragment());
                    fg5.commit();
                }

                else {
                    FragmentTransaction fg5 = getSupportFragmentManager().beginTransaction(); //to begin home page with fragment
                    fg5.replace(R.id.frame_drawer, new WelcomeActivity());
                    fg5.commit();

                }


                lineLive.setBackgroundColor(ContextCompat.getColor(this,R.color.White));
                iconTabLive.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_live_unselected));
                lineHome.setBackgroundColor(ContextCompat.getColor(this,R.color.White));
                iconTabHome.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_home_unselected));
                lineUpcoming.setBackgroundColor(ContextCompat.getColor(this,R.color.White));
                iconTabUpcoming.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_upcoming_unselected));
                lineVideo.setBackgroundColor(ContextCompat.getColor(this,R.color.White));
                iconTabVideo.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_video_tab_unselected));
                lineProfile.setBackgroundColor(ContextCompat.getColor(this,R.color.colorPrimary));
                iconTabProfile.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_profile_tab));
                baseHomeText.setTextColor(ContextCompat.getColor(this,R.color.gray));
                basevideostext.setTextColor(ContextCompat.getColor(this,R.color.gray));
                baseUpcomingtext.setTextColor(ContextCompat.getColor(this,R.color.gray));
                baseProfileText.setTextColor(ContextCompat.getColor(this,R.color.colorPrimary));
                baseLivetext.setTextColor(ContextCompat.getColor(this,R.color.gray));
                break;

            case R.id.imageSearch:
                startActivity(new Intent(HomeDrawerActivity.this, SearchActivity.class));
                break;

            default:
                break;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Sharedprefrences.clearEventDateArray(getBaseContext());
        Sharedprefrences.clearEventDateObject(getBaseContext());
    }

    @Override
    protected void onResume() {
        super.onResume();

        Intent in = getIntent();
        Uri data = in.getData();
        try {
            String protocol = data.getScheme();
            String server = data.getAuthority();
            String path = data.getPath();
            String[] str = path.split("/");
            String id = str[1];
            Log.e("id", id);

            Intent intent;
            if (server.equalsIgnoreCase("live")) {
                FragmentTransaction fg2 = getSupportFragmentManager().beginTransaction(); //to begin home page with fragment
                fg2.replace(R.id.frame_drawer, new LiveFragment());
                fg2.commit();
            }
            else if (server.equalsIgnoreCase("eventId")) {
                intent = new Intent(this, EventDetailsActivity.class);
                intent.putExtra("EventId", "id");
                startActivity(intent);
            }

            else if (server.equalsIgnoreCase("videoId")) {
                intent = new Intent(this, VideoDetailsActivity.class);
                intent.putExtra("EpisodeId", "id");
                startActivity(intent);
            }


        } catch (Exception e) {

        }

     //   fragmentUpcoming.getData(Sharedprefrences.getDateOfEVEMatch(getBaseContext()));

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Sharedprefrences.clearEventDateArray(getBaseContext());
        Sharedprefrences.clearEventDateObject(getBaseContext());
        //Sharedprefrences.clearDateOfEVEMatch(getBaseContext());
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        //startActivity(new Intent(this,HomeDrawerActivity.class));

        if(Const.UPCOMING_FRAGMENT_KEY == 1) {
            Const.UPCOMING_FRAGMENT_KEY = 0;
            Sharedprefrences.clearEventDateArray(getBaseContext());
            Sharedprefrences.clearEventDateObject(getBaseContext());
            startActivity(new Intent(this, HomeDrawerActivity.class));
        }

    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        final AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            try{
                LiveFragment.seekbarVolume.setProgress(audioManager
                        .getStreamVolume(AudioManager.STREAM_MUSIC) - 1);
            } catch (Error e) {
                // min value
            }
        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
            try{
                LiveFragment.seekbarVolume.setProgress(audioManager
                        .getStreamVolume(AudioManager.STREAM_MUSIC) + 1);
            } catch (Error e) {
                // max value
            }
        }
        return super.onKeyDown(keyCode, event);
    }

}

