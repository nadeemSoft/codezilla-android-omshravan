package com.zecast.live.om_saravanabava.model;

/**
 * Created by codezilla-12 on 23/9/17.
 */

public class AboutUsModel {

    String venderId;
    String aboutUs;
    String venderMobile;
    String venderEmail;

    public AboutUsModel(String venderId, String aboutUs,String venderMobile, String venderEmail) {
        this.venderId = venderId;
        this.aboutUs = aboutUs;
        this.venderMobile = venderMobile;
        this.venderEmail = venderEmail;
    }

    public String getVenderId() {
        return venderId;
    }

    public void setVenderId(String venderId) {
        this.venderId = venderId;
    }

    public String getAboutUs() {
        return aboutUs;
    }

    public void setAboutUs(String aboutUs) {
        this.aboutUs = aboutUs;
    }

    public String getVenderMobile() {
        return venderMobile;
    }

    public void setVenderMobile(String venderMobile) {
        this.venderMobile = venderMobile;
    }

    public String getVenderEmail() {
        return venderEmail;
    }

    public void setVenderEmail(String venderEmail) {
        this.venderEmail = venderEmail;
    }
}
