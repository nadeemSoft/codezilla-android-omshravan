package com.zecast.live.om_saravanabava.helper;

import android.app.ProgressDialog;
import android.content.Context;

public class ProgressDialogUtils {
    static ProgressDialog dialog;

    public static void showProgressDialog(Context pContext, String msg) {
        try {
            dialog = new ProgressDialog(pContext);
            dialog.setCancelable(false);
            dialog.setMessage(msg);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void hideProgressDialog() {
        try {
            if (ifDialogIsNotNullAndIsShowing()) {
                dialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static boolean ifDialogIsNotNullAndIsShowing() throws Exception {
        return ((dialog != null) && (dialog.isShowing()));
    }
}
