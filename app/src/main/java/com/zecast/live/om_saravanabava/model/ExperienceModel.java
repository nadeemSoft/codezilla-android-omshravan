package com.zecast.live.om_saravanabava.model;

/**
 * Created by codezilla-12 on 23/9/17.
 */

public class ExperienceModel {


    String experienceId;
    String experienceName;
    String expreienceUrl;
    String experienceLogo;
    String experiencevideoId;
    String expreiencedesc;


    public ExperienceModel(String experienceId, String experienceName, String expreienceUrl, String experienceLogo, String experiencevideoId, String expreiencedesc) {
        this.experienceId = experienceId;
        this.experienceName = experienceName;
        this.expreienceUrl = expreienceUrl;
        this.experienceLogo = experienceLogo;
        this.experiencevideoId = experiencevideoId;
        this.expreiencedesc = expreiencedesc;
    }


    public String getExperienceId() {
        return experienceId;
    }

    public void setExperienceId(String experienceId) {
        this.experienceId = experienceId;
    }

    public String getExperienceName() {
        return experienceName;
    }

    public void setExperienceName(String experienceName) {
        this.experienceName = experienceName;
    }

    public String getExpreienceUrl() {
        return expreienceUrl;
    }

    public void setExpreienceUrl(String expreienceUrl) {
        this.expreienceUrl = expreienceUrl;
    }

    public String getExperienceLogo() {
        return experienceLogo;
    }

    public void setExperienceLogo(String experienceLogo) {
        this.experienceLogo = experienceLogo;
    }

    public String getExperiencevideoId() {
        return experiencevideoId;
    }

    public void setExperiencevideoId(String experiencevideoId) {
        this.experiencevideoId = experiencevideoId;
    }

    public String getExpreiencedesc() {
        return expreiencedesc;
    }

    public void setExpreiencedesc(String expreiencedesc) {
        this.expreiencedesc = expreiencedesc;
    }
}
