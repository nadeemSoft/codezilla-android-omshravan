package com.zecast.live.om_saravanabava.activity.videodetails;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatCallback;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.adapter.VideoDetailsAdapter;
import com.zecast.live.om_saravanabava.model.OtherVideos;
import com.zecast.live.om_saravanabava.utils.Const;
import com.zecast.live.om_saravanabava.utils.HTTPRequest;
import com.zecast.live.om_saravanabava.utils.Sharedprefrences;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by codezilla-12 on 18/10/17.
 */

/*extends AppCompatActivity implements View.OnClickListener*/

public class VideoDetailsActivity  extends YouTubeBaseActivity implements View.OnClickListener,AppCompatCallback {

    public static VideoView vv;
    public static TextView tvEpisodeName, tvEpisodeDate, tvSeen, tvDescription;
    RecyclerView rv;
    String episodeName, episodeId;
    String httpLiveUrl;
    ArrayList<OtherVideos> mVideoDetails = new ArrayList<>();
    LinearLayoutManager layoutManager;
    VideoDetailsAdapter adap;
    LinearLayout videoContainer, frameContainer;
    ImageView ivPlay, ivPause, ivFullScreenDetails, ivSpeaker;
    TextView tvtitle;
    SeekBar seekbar, progressSeekbar;
    private AudioManager mAudioManager;
    LinearLayout linearYuoutubePlayerContainer;
    public static ImageView imgPlayYoutubeVideo;

    ImageView ivShare;
    TextView tvIds;
    String video_id;
    LinearLayout toolbar;
    ImageView imgBackIcon;
    TextView tvTitleToolbar;
    String videoId;

    //private AppCompatDelegate delegate;

    //youtube releted initialization

    private YouTubePlayer youTubePlayer;
    private YouTubePlayerFragment youTubePlayerFragment;
    private static final int RQS_ErrorDialog = 1;
    private VideoDetailsActivity.MyPlayerStateChangeListener myPlayerStateChangeListener;
    private VideoDetailsActivity.MyPlaybackEventListener myPlaybackEventListener;

    //controlss for youtubee and video view

    //it will be visible by default
    public static  FrameLayout videoviewFullContainer;
    public static  FrameLayout youtubePlayerContainer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.videodetails);

        init_Components();
        httpLiveUrl = getIntent().getStringExtra("URL");
        getIntentData();
        mAudioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
        soundControl();
        setSoundControlFunction();
        getData();

        ivPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ivPause.setVisibility(View.GONE);
                ivPlay.setVisibility(View.VISIBLE);
                vv.pause();
            }
        });

        ivPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ivPause.setVisibility(View.VISIBLE);
                ivPlay.setVisibility(View.GONE);
                vv.start();
            }
        });

        progressSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (b) {
                    vv.seekTo(i);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        ivFullScreenDetails.setOnClickListener(this);
        ivFullScreenDetails.setEnabled(false);
       // imgPlayYoutubeVideo.setOnClickListener(this);

        if (Sharedprefrences.getEpisodeTypeStored(getApplicationContext()).equals("direct")) {
          //  VideoDetailsActivity.imgPlayYoutubeVideo.setVisibility(View.GONE);
            vv.setVisibility(View.VISIBLE);
        } else {
           // VideoDetailsActivity.imgPlayYoutubeVideo.setVisibility(View.VISIBLE);

            vv.setVisibility(View.GONE);
        }

        myPlayerStateChangeListener = new VideoDetailsActivity.MyPlayerStateChangeListener();
        myPlaybackEventListener = new VideoDetailsActivity.MyPlaybackEventListener();

        tvTitleToolbar.setText("Video details");
        tvTitleToolbar.setTextColor(ContextCompat.getColor(getBaseContext(),R.color.White));

    }  //end of on create

    public void PlayYoutubeVideos()

    {
        youTubePlayerFragment = (YouTubePlayerFragment)getFragmentManager()
                .findFragmentById(R.id.youtubeplayerfragment);

        youTubePlayerFragment.onDestroy();

        youTubePlayerFragment.initialize(Const.YOUTUBE_VIDEO_CODE,
                new YouTubePlayer.OnInitializedListener() {
                    @Override
                    public void onInitializationSuccess(YouTubePlayer.Provider provider,
                                                        YouTubePlayer youTubePlayer, boolean b) {
                        // do any work here to cue video, play video, etc.

                        youTubePlayer.loadVideo(videoId);
                        Log.e("start","1");

                        Log.e("start",videoId);

                    }
                    @Override
                    public void onInitializationFailure(YouTubePlayer.Provider provider,
                                                        YouTubeInitializationResult youTubeInitializationResult) {
                    }
                });
    }

    private void getIntentData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            episodeId = extras.getString("EpisodeId");

        }
    }

    private void init_Components() {
        rv = (RecyclerView) findViewById(R.id.recycler_recent_events);
        vv = (VideoView) findViewById(R.id.videoview);
        tvEpisodeName = (TextView) findViewById(R.id.heading_event_live_1);
        tvEpisodeDate = (TextView) findViewById(R.id.date_event_live_1);

        tvSeen = (TextView) findViewById(R.id.tv_views);
        tvDescription = (TextView) findViewById(R.id.description_event_live_1);
        videoContainer = (LinearLayout) findViewById(R.id.video_Container);
        frameContainer = (LinearLayout) findViewById(R.id.frame_linear_container);
        ivPlay = (ImageView) findViewById(R.id.iv_play1);
        ivPause = (ImageView) findViewById(R.id.iv_pause1);
        ivFullScreenDetails = (ImageView) findViewById(R.id.video_fullscreen);
        seekbar = (SeekBar) findViewById(R.id.seekbar);
        ivSpeaker = (ImageView) findViewById(R.id.seekbarVolume);
        progressSeekbar = (SeekBar) findViewById(R.id.seekbarProgress);
        tvtitle = (TextView) findViewById(R.id.toolbar_title);
        linearYuoutubePlayerContainer = (LinearLayout) findViewById(R.id.linear_container_youtube_player);
        //youtube_view_video_details = (YouTubePlayer) findViewById(R.id.youtube_view_video_details);
        imgPlayYoutubeVideo = (ImageView) findViewById(R.id.ic_youtube_play);

        ivShare = (ImageView)findViewById(R.id.liveShare);
        tvIds = (TextView)findViewById(R.id.episodeId);



        youtubePlayerContainer = (FrameLayout)findViewById(R.id.youtube_player_container);
        videoviewFullContainer = (FrameLayout)findViewById(R.id.framecontrols);
        youTubePlayerFragment = (YouTubePlayerFragment)getFragmentManager()
                .findFragmentById(R.id.youtubeplayerfragment);

        toolbar = (LinearLayout) findViewById(R.id.toolbar);
        tvTitleToolbar = (TextView) findViewById(R.id.toolbar_title);


        imgBackIcon = (ImageView)findViewById(R.id.back_action);


      //  toolbar.setho

        ivShare.setOnClickListener(this);
        imgBackIcon.setOnClickListener(this);
    }

    private void getData() {

        RequestQueue queue = Volley.newRequestQueue(VideoDetailsActivity.this);
        StringRequest sr = new StringRequest(Request.Method.POST, HTTPRequest.SERVICE_URL + "getVideoDetails", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.e("responseVideosDetials", response);
                    try {
                        mVideoDetails.clear();
                        JSONObject obj = new JSONObject(response);
                        String message = obj.optString("message");
                        String status = obj.optString("status");
                        JSONObject objResponse = obj.optJSONObject("response");
                        String episode_id = objResponse.optString("episode_id");
                        String episode_name = objResponse.optString("episode_name");
                        String episode_image = objResponse.optString("episode_image");
                        String episode_date = objResponse.optString("episode_date");
                        video_id = objResponse.optString("video_id");
                        String episode_type = objResponse.optString("episode_type");
                        String episode_url = objResponse.optString("episode_url");
                        String episode_seen = objResponse.optString("episode_seen");
                        String episode_desc = objResponse.optString("episode_desc");
                        String episode_like_status = objResponse.optString("episode_like_status");
                        String episode_like = objResponse.optString("episode_like");


                       tvtitle.setText(episode_name);
                        tvIds.setText(episode_id);
                        tvEpisodeName.setText(episode_name);
                        tvSeen.setText(episode_seen);
                        tvDescription.setText(episode_desc);
                        tvEpisodeDate.setText(episode_date);
//
                        Sharedprefrences.setVideoUrls(VideoDetailsActivity.this, episode_url);
                        if (!(video_id.equals(""))) {
                            Sharedprefrences.setYoutubeVideoId(getBaseContext(), video_id);

                        }
                       // imgPlayYoutubeVideo.setVisibility(View.GONE);



                        if (episode_type.equals("youtube")) {
                            youtubePlayerContainer.setVisibility(View.VISIBLE);
                            videoviewFullContainer.setVisibility(View.GONE);
                            vv.setVisibility(View.INVISIBLE);
                            Sharedprefrences.setYoutubeVideoId(getBaseContext(),video_id);
                            videoId = video_id;
                           PlayYoutubeVideos();

                        } else {
                            youtubePlayerContainer.setVisibility(View.GONE);
                            videoviewFullContainer.setVisibility(View.VISIBLE);
                            vv.setVisibility(View.VISIBLE);
                            new BackgroundAsyncTask().execute(episode_url);
                        }




                      /*  }*/

                        JSONArray arr = objResponse.optJSONArray("otherVideos");

                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject ob = arr.getJSONObject(i);
                            String episode_ids = ob.optString("episode_id");
                            String episode_names = ob.optString("episode_name");
                            String episode_images = ob.optString("episode_image");
                            String episode_dates = ob.optString("episode_date");
                            String video_ids = ob.optString("video_id");
                            String episode_types = ob.optString("episode_type");
                            String episode_urls = ob.optString("episode_url");
                            String episode_seens = ob.optString("episode_seen");
                            String episode_descs = ob.optString("episode_desc");
                            String episode_like_statuss = ob.optString("episode_like_status");
                            String episode_likes = ob.optString("episode_like");
                            OtherVideos vd = new OtherVideos(episode_ids, episode_names, episode_images, episode_dates, video_ids, episode_types, episode_urls, episode_seens, episode_descs, episode_like_statuss, episode_likes);
                            mVideoDetails.add(vd);
                        }

                        adap = new VideoDetailsAdapter(VideoDetailsActivity.this, VideoDetailsActivity.this, mVideoDetails);
                        layoutManager = new LinearLayoutManager(VideoDetailsActivity.this, LinearLayoutManager.HORIZONTAL, false);
                        rv.setLayoutManager(layoutManager);
                        rv.setAdapter(adap);
                        rv.setNestedScrollingEnabled(false);
                        adap.notifyDataSetChanged();
                    } catch (Exception e) {
                        Log.e("Exception", e + "");
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", String.valueOf(error));
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                headers.put("vendorKey", Const.vendorKey);
                headers.put("Authorization", Sharedprefrences.getUserKey(VideoDetailsActivity.this));
                headers.put("userID", Sharedprefrences.getUserId(VideoDetailsActivity.this));
                headers.put("deviceToken",Sharedprefrences.getFCMRegistrationId(VideoDetailsActivity.this));
                headers.put("deviceType", Const.deviceType);
                headers.put("deviceId", Sharedprefrences.getDeviceId(VideoDetailsActivity.this));
                headers.put("Content-Type", Const.contentType);
                return headers;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("videoId", episodeId);
                Log.e("ids",episodeId);

                return params;
            }
        };
        queue.add(sr);
    }

    public void playLiveChannel(OtherVideos ov, String position) {
        try {
           /* vv.setVisibility(View.VISIBLE);
       */

            progressSeekbar.setProgress(0);

            DoRefreshList(ov.getEpisode_id());


//            tvtitle.setText(ov.getEpisode_name());
//            tvIds.setText(ov.getEpisode_id());
//
//            Sharedprefrences.setVideoUrls(VideoDetailsActivity.this, ov.getEpisode_url());
//            Sharedprefrences.setPositions(VideoDetailsActivity.this, position);
//
//            tvEpisodeName.setText(ov.getEpisode_name());
//            tvDescription.setText(ov.getEpisode_desc());
//            tvEpisodeDate.setText(ov.getEpisode_date());
//            tvSeen.setText(ov.getEpisode_seen());
//
//
//                if (ov.getEpisode_type().equals("youtube")) {
//                    youtubePlayerContainer.setVisibility(View.VISIBLE);
//                    videoviewFullContainer.setVisibility(View.GONE);
//
//                    videoId =ov.getVideo_id();
//                    PlayYoutubeVideos();
//
//                } else {
//                    youtubePlayerContainer.setVisibility(View.GONE);
//                    videoviewFullContainer.setVisibility(View.VISIBLE);
//
//                    new BackgroundAsyncTask().execute(ov.getEpisode_url());
//                    Log.e("live", ov.getEpisode_url());
//                    progressSeekbar.setProgress(0);
//                }
        } catch (Exception e) {
        }

    }


    private void DoRefreshList(final String episodeid) {

        RequestQueue queue = Volley.newRequestQueue(VideoDetailsActivity.this);
        StringRequest sr = new StringRequest(Request.Method.POST, HTTPRequest.SERVICE_URL + "getVideoDetails", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.e("responseVideosDetials", response);
                    try {
                        mVideoDetails.clear();
                        JSONObject obj = new JSONObject(response);
                        String message = obj.optString("message");
                        String status = obj.optString("status");
                        JSONObject objResponse = obj.optJSONObject("response");
                        String episode_id = objResponse.optString("episode_id");
                        String episode_name = objResponse.optString("episode_name");
                        String episode_image = objResponse.optString("episode_image");
                        String episode_date = objResponse.optString("episode_date");
                        video_id = objResponse.optString("video_id");
                        String episode_type = objResponse.optString("episode_type");
                        String episode_url = objResponse.optString("episode_url");
                        String episode_seen = objResponse.optString("episode_seen");
                        String episode_desc = objResponse.optString("episode_desc");
                        String episode_like_status = objResponse.optString("episode_like_status");
                        String episode_like = objResponse.optString("episode_like");


                        tvtitle.setText(episode_name);
                        tvIds.setText(episode_id);
                        tvEpisodeName.setText(episode_name);
                        tvSeen.setText(episode_seen);
                        tvDescription.setText(episode_desc);
                        tvEpisodeDate.setText(episode_date);
//
                        Sharedprefrences.setVideoUrls(VideoDetailsActivity.this, episode_url);
                        if (!(video_id.equals(""))) {
                            Sharedprefrences.setYoutubeVideoId(getBaseContext(), video_id);

                        }
                        // imgPlayYoutubeVideo.setVisibility(View.GONE);



                        if (episode_type.equals("youtube")) {
                            youtubePlayerContainer.setVisibility(View.VISIBLE);
                            videoviewFullContainer.setVisibility(View.GONE);
                            vv.setVisibility(View.INVISIBLE);
                            Sharedprefrences.setYoutubeVideoId(getBaseContext(),video_id);
                            videoId = video_id;
                            PlayYoutubeVideos();

                        } else {
                            youtubePlayerContainer.setVisibility(View.GONE);
                            videoviewFullContainer.setVisibility(View.VISIBLE);
                            vv.setVisibility(View.VISIBLE);
                            new BackgroundAsyncTask().execute(episode_url);
                        }




                      /*  }*/

                        JSONArray arr = objResponse.optJSONArray("otherVideos");

                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject ob = arr.getJSONObject(i);
                            String episode_ids = ob.optString("episode_id");
                            String episode_names = ob.optString("episode_name");
                            String episode_images = ob.optString("episode_image");
                            String episode_dates = ob.optString("episode_date");
                            String video_ids = ob.optString("video_id");
                            String episode_types = ob.optString("episode_type");
                            String episode_urls = ob.optString("episode_url");
                            String episode_seens = ob.optString("episode_seen");
                            String episode_descs = ob.optString("episode_desc");
                            String episode_like_statuss = ob.optString("episode_like_status");
                            String episode_likes = ob.optString("episode_like");
                            OtherVideos vd = new OtherVideos(episode_ids, episode_names, episode_images, episode_dates, video_ids, episode_types, episode_urls, episode_seens, episode_descs, episode_like_statuss, episode_likes);
                            mVideoDetails.add(vd);
                        }
                        VideoDetailsAdapter.clickedPosition = -1;
                        adap.notifyDataSetChanged();
                    } catch (Exception e) {
                        Log.e("Exception", e + "");
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", String.valueOf(error));
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                headers.put("vendorKey", Const.vendorKey);
                headers.put("Authorization", Sharedprefrences.getUserKey(VideoDetailsActivity.this));
                headers.put("userID", Sharedprefrences.getUserId(VideoDetailsActivity.this));
                headers.put("deviceToken",Sharedprefrences.getFCMRegistrationId(VideoDetailsActivity.this));
                headers.put("deviceType", Const.deviceType);
                headers.put("deviceId", Sharedprefrences.getDeviceId(VideoDetailsActivity.this));
                headers.put("Content-Type", Const.contentType);
                return headers;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("videoId", episodeid);
                Log.e("ids", episodeid);

                return params;
            }
        };
        queue.add(sr);
    }




    @Override
    public void onSupportActionModeStarted(ActionMode mode) {

    }

    @Override
    public void onSupportActionModeFinished(ActionMode mode) {

    }


    @Nullable
    @Override
    public ActionMode onWindowStartingSupportActionMode(ActionMode.Callback callback) {
        return null;
    }

    public class BackgroundAsyncTask extends AsyncTask<String, Uri, Void> {


        protected void onPreExecute() {

//            pb.setVisibility(View.VISIBLE);

        }

        protected void onProgressUpdate(final Uri... uri) {

            try {

                vv.setVideoURI(uri[0]);

                vv.requestFocus();

                vv.getLayoutParams().width = ViewGroup.LayoutParams.FILL_PARENT;

                vv.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

                    public void onPrepared(MediaPlayer mp) {
                        mp.setLooping(true);
                        vv.start();
                        vv.setVisibility(View.VISIBLE);
                        ivFullScreenDetails.setEnabled(true);
//                        ivFulllscreen.setVisibility(View.VISIBLE);
                        ivPlay.setVisibility(View.GONE);
                        ivPause.setVisibility(View.VISIBLE);
                        progressSeekbar.setMax(vv.getDuration());
                    }
                });

                vv.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                    @Override
                    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {

                        vv.setVisibility(View.VISIBLE);
                        vv.start();
                        Toast.makeText(VideoDetailsActivity.this, "Cant play Video", Toast.LENGTH_SHORT).show();
//                        ivFulllscreen.setVisibility(View.GONE);
                        ivPlay.setVisibility(View.GONE);
                        ivPause.setVisibility(View.GONE);

                        Log.e("Error", "error");
                        return true;
                    }
                });

            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(String... params) {
            try {
                Uri uri = Uri.parse(params[0]);
                publishProgress(uri);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.video_fullscreen:
                int orientation = this.getResources().getConfiguration().orientation;
                if (orientation == Configuration.ORIENTATION_PORTRAIT) {

                    //delegate.getSupportActionBar().hide();
                    toolbar.setVisibility(View.GONE);
                 //   toolbar.setTitle("Video details");


                    getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                    vv.getLayoutParams().width = ViewGroup.LayoutParams.FILL_PARENT;
                    frameContainer.getLayoutParams().height = ViewGroup.LayoutParams.FILL_PARENT;
                    vv.getLayoutParams().height = ViewGroup.LayoutParams.FILL_PARENT;
                    videoContainer.setVisibility(View.GONE);
                    frameContainer.getLayoutParams().height = ViewGroup.LayoutParams.FILL_PARENT;
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                } else {
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                    videoContainer.setVisibility(View.VISIBLE);
                    vv.getLayoutParams().width = ViewGroup.LayoutParams.FILL_PARENT;
                    frameContainer.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    vv.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                   // delegate.getSupportActionBar().show();
                    toolbar.setVisibility(View.VISIBLE);
                    frameContainer.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }
                break;

            case R.id.liveShare:

                List<Intent> targetShareIntents = new ArrayList<Intent>();
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                List<ResolveInfo> resInfos = getPackageManager().queryIntentActivities(shareIntent, 0);
                if (!resInfos.isEmpty()) {
                    System.out.println("Have package");
                    for (ResolveInfo resInfo : resInfos) {
                        String packageName = resInfo.activityInfo.packageName;
                        Log.i("Package Name", packageName);

                        if (!(packageName.contains("com.google.android.apps.translate") || packageName.contains("com.google.android.gms.drive") || packageName.contains("com.google.android.hangouts"))) {

                            Intent intents = new Intent();
                            intents.setComponent(new ComponentName(packageName, resInfo.activityInfo.name));
                            intents.setAction(Intent.ACTION_SEND);
                            intents.setType("text/plain");
                            intents.putExtra(Intent.EXTRA_TEXT,"watch the video- videoURL "+ "http://www.omsharavanabhavamatham.org.uk/dl/videoId/"+tvIds.getText().toString());
                            intents.putExtra(Intent.EXTRA_SUBJECT, "watch the video- videoURL");
                            intents.setPackage(packageName);
                            targetShareIntents.add(intents);
                        }

                    }
                    if (!targetShareIntents.isEmpty()) {
                        System.out.println("Have Intent");
                        Intent chooserIntent = Intent.createChooser(targetShareIntents.remove(0), "Choose app to share");
                        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetShareIntents.toArray(new Parcelable[]{}));
                        startActivity(chooserIntent);
                    } else {
                        System.out.println("Do not Have Intent");
                    }
                }


                break;


            case R.id.back_action:
                   this.finish();
                break;

            default:
                break;
        }
    }

    private void soundControl() {

        seekbar.setMax(mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));

        seekbar.setProgress(mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC));

        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if (progress == 0) {

                    ivSpeaker.setImageResource(R.drawable.mute);


                } else if (progress != 0) {
                    ivSpeaker.setImageResource(R.drawable.speaker);

                }
                mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                        progress, 0);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

        });
    }

    private void setSoundControlFunction() {

        this.seekbar.setMax(mAudioManager
                .getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        this.seekbar.setProgress(mAudioManager
                .getStreamVolume(AudioManager.STREAM_MUSIC));
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        final AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            try{
                seekbar.setProgress(audioManager
                        .getStreamVolume(AudioManager.STREAM_MUSIC) - 1);
            } catch (Error e) {
                // min value
            }
        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
            try{
                seekbar.setProgress(audioManager
                        .getStreamVolume(AudioManager.STREAM_MUSIC) + 1);
            } catch (Error e) {
                // max value
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {

        int orientation = this.getResources().getConfiguration().orientation;

        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {

            videoContainer.setVisibility(View.VISIBLE);
            vv.getLayoutParams().width = ViewGroup.LayoutParams.FILL_PARENT;
            frameContainer.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
            vv.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
          //  delegate.getSupportActionBar().show();
            toolbar.setVisibility(View.VISIBLE);
            frameContainer.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            super.onBackPressed();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                this.finish();
                return true;

            case R.id.action_back:
                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void getLike() {
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest sr = new StringRequest(Request.Method.POST, HTTPRequest.SERVICE_URL + "likeVideo", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.e("like responseeee", response);
                    try {
                        JSONObject obj = new JSONObject(response);
                        String message = obj.optString("message");
                        String error_type = obj.optString("error_type");
                        String status = obj.optString("status");

                  //      Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT).show();

                        if (error_type.equals("200")) {
                            fetchLiveVideosAfterLike(/*position,likecount*/);
                        }
                    } catch (Exception e) {
                        Log.e("Exception", e + "");
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", String.valueOf(error));
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();

                headers.put("vendorKey",Const.vendorKey);
                headers.put("Authorization",Sharedprefrences.getUserKey(VideoDetailsActivity.this));
                headers.put("userID",Sharedprefrences.getUserId(VideoDetailsActivity.this));
                headers.put("deviceId", Sharedprefrences.getDeviceId(VideoDetailsActivity.this));
                headers.put("deviceToken",Sharedprefrences.getFCMRegistrationId(VideoDetailsActivity.this));
                headers.put("deviceType", Const.deviceType);
                headers.put("Content-Type", Const.contentType);
                return headers;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                Log.e("videoid",tvIds.getText().toString());
                params.put("videoId", tvIds.getText().toString());

                return params;
            }
        };
        queue.add(sr);
    }


    private void fetchLiveVideosAfterLike() {

        RequestQueue queue = Volley.newRequestQueue(VideoDetailsActivity.this);

        StringRequest sr = new StringRequest(Request.Method.POST, HTTPRequest.SERVICE_URL + "getVideoDetails", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {

                    Log.e("responseAfterLikes", response);
                    try {

                        mVideoDetails.clear();
                        JSONObject obj = new JSONObject(response);

                        String message = obj.optString("message");
                        String status = obj.optString("status");

                        JSONObject objResponse = obj.optJSONObject("response");

                        String episode_id = objResponse.optString("episode_id");
                        String episode_name = objResponse.optString("episode_name");
                        String episode_image = objResponse.optString("episode_image");
                        String episode_date = objResponse.optString("episode_date");
                        String video_id = objResponse.optString("video_id");
                        String episode_type = objResponse.optString("episode_type");
                        String episode_url = objResponse.optString("episode_url");
                        String episode_seen = objResponse.optString("episode_seen");
                        String episode_desc = objResponse.optString("episode_desc");
                        String episode_like_status = objResponse.optString("episode_like_status");
                        String episode_like = objResponse.optString("episode_like");


                        JSONArray arr = objResponse.optJSONArray("otherVideos");

                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject ob = arr.getJSONObject(i);

                            String episode_ids = ob.optString("episode_id");
                            String episode_names = ob.optString("episode_name");
                            String episode_images = ob.optString("episode_image");
                            String episode_dates = ob.optString("episode_date");
                            String video_ids = ob.optString("video_id");
                            String episode_types = ob.optString("episode_type");
                            String episode_urls = ob.optString("episode_url");
                            String episode_seens = ob.optString("episode_seen");
                            String episode_descs = ob.optString("episode_desc");
                            String episode_like_statuss = ob.optString("episode_like_status");
                            String episode_likes = ob.optString("episode_like");

                            OtherVideos vd = new OtherVideos(episode_ids, episode_names, episode_images, episode_dates, video_ids, episode_types, episode_urls, episode_seens, episode_descs, episode_like_statuss, episode_likes);
                            mVideoDetails.add(vd);
                        }

                        try
                        {
                            changeLikes(mVideoDetails.get(Integer.parseInt(Sharedprefrences.getPostions(VideoDetailsActivity.this))));

                        } catch (Exception e) {
                        }

                    } catch (Exception e) {
                        Log.e("Exception", e + "");
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("error", String.valueOf(error));
            }
        }) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                headers.put("vendorKey", Const.vendorKey);
                headers.put("Authorization", Sharedprefrences.getUserKey(VideoDetailsActivity.this));
                headers.put("userID", Sharedprefrences.getUserId(VideoDetailsActivity.this));
                headers.put("deviceToken",Sharedprefrences.getFCMRegistrationId(VideoDetailsActivity.this));
                headers.put("deviceType", Const.deviceType);
                headers.put("deviceId", Sharedprefrences.getDeviceId(VideoDetailsActivity.this));
                headers.put("Content-Type", Const.contentType);
                return headers;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("videoId", episodeId);


                return params;
            }
        };
        queue.add(sr);
    }

    private void changeLikes(OtherVideos otherVideos) {

        Log.e("Likes",otherVideos.getEpisode_like_status());

        tvDescription.setText(otherVideos.getEpisode_desc());
        tvSeen.setText(otherVideos.getEpisode_seen());
        tvIds.setText(otherVideos.getEpisode_id());
        adap.notifyDataSetChanged();
        Log.e("asas",otherVideos.getEpisode_like());

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    private final class MyPlayerStateChangeListener implements YouTubePlayer.PlayerStateChangeListener {

        /*private void updateLog(String prompt){
            log +=  "MyPlayerStateChangeListener" + "\n" +
                    prompt + "\n\n=====";
            textVideoLog.setText(log);
        };*/

        @Override
        public void onAdStarted() {
         /*   updateLog("onAdStarted()");*/
        }

        @Override
        public void onError(
                com.google.android.youtube.player.YouTubePlayer.ErrorReason arg0) {
          //  updateLog("onError(): " + arg0.toString());
        }

        @Override
        public void onLoaded(String arg0) {
           // updateLog("onLoaded(): " + arg0);
        }

        @Override
        public void onLoading() {
            //updateLog("onLoading()");
        }

        @Override
        public void onVideoEnded() {
            //updateLog("onVideoEnded()");
        }

        @Override
        public void onVideoStarted() {
          //  updateLog("onVideoStarted()");
        }

    }

    private final class MyPlaybackEventListener implements YouTubePlayer.PlaybackEventListener {


        @Override
        public void onBuffering(boolean arg0) {
           // updateLog("onBuffering(): " + String.valueOf(arg0));
        }

        @Override
        public void onPaused() {
            //updateLog("onPaused()");
        }

        @Override
        public void onPlaying() {

            //updateLog("onPlaying()");
        }

        @Override
        public void onSeekTo(int arg0) {
           // updateLog("onSeekTo(): " + String.valueOf(arg0));
        }

        @Override
        public void onStopped() {
           // updateLog("onStopped()");
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menubardetails, menu);
        return true;
    }


}