package com.zecast.live.om_saravanabava.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity;

import org.json.JSONObject;

import java.util.Map;

/**
 * Created by codezilla-8 on 27/9/17.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //Displaying data in log
        //It is optional

        playNotificationSound(this);

        Log.e("noti", remoteMessage.getData() + "");

        if (remoteMessage.getNotification() != null) {
            Log.e("Message Body: ", remoteMessage.getNotification().getBody());
        }

        String[] trim = remoteMessage.getData().toString().split("=");// because the json not have  "=" so we have to split it
        String jsonStr = trim[1].substring(0, trim[1].length() - 1);
        Log.e("JsonStr",jsonStr);
        Log.e("dataChat",remoteMessage.getData().toString());

        try{

            JSONObject obj = new JSONObject(jsonStr);

            String title = obj.optString("title");
            String appname = obj.optString("notificationTitle");
            Log.e("title",title);
            sendNotification(title,appname);

        }catch (Exception e){}

    }

    private void sendNotification(String appname, String messageBody) {

        Intent intent = new Intent(this, HomeDrawerActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("goLive","1");
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP );
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setContentTitle(appname)
                .setSmallIcon(R.drawable.app_icon)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)

                .setContentIntent(pendingIntent);


        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }

    public void playNotificationSound(Context context) {
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}