package com.zecast.live.om_saravanabava.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.adapter.AdapterLiveCathupGrid;
import com.zecast.live.om_saravanabava.model.CatchupModel;
import com.zecast.live.om_saravanabava.model.EventList;
import com.zecast.live.om_saravanabava.model.RecentEventsModel;
import com.zecast.live.om_saravanabava.utils.Const;
import com.zecast.live.om_saravanabava.utils.HTTPRequest;
import com.zecast.live.om_saravanabava.utils.Sharedprefrences;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by codezilla-8 on 11/1/18.
 */

public class LiveCatchupGrid extends AppCompatActivity
{

    RecyclerView rv;
    String CatchupName,CatchupId;
    AdapterLiveCathupGrid adap;
    ArrayList<EventList> mEventList = new ArrayList<>();
    int numberOfColumns = 2;
    ProgressBar pb;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.livecatcupgrid);

        initWidgets();

        getBundleData();



        pb.setVisibility(View.VISIBLE);
        rv.setVisibility(View.GONE);


    }

    private void getBundleData() {

        Bundle extras = getIntent().getExtras();

        if (extras!=null)
        {
            CatchupName = extras.getString("CatchupName");
            CatchupId = extras.getString("CatchupId");
        }

        LiveCatchupGrid.this.setTitle(CatchupName);
    }

    private void initWidgets()
    {
        rv = (RecyclerView)findViewById(R.id.liveCatchupGrid);
        pb =(ProgressBar)findViewById(R.id.progressbar);

    }


    private void getData() {
        RequestQueue queue = Volley.newRequestQueue(LiveCatchupGrid.this);
        StringRequest sr = new StringRequest(Request.Method.POST, HTTPRequest.SERVICE_URL+"getEventList", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.e("ResponseEventList", response);

                    try {

                        JSONObject obj = new JSONObject(response);
                        mEventList.clear();
                        JSONObject objResponse = obj.optJSONObject("response");

                        JSONArray arr = objResponse.optJSONArray("eventList");

                        for (int i = 0; i < arr.length(); i++)
                        {
                            JSONObject objMain = arr.optJSONObject(i);


                            EventList el =new EventList(objMain.optString("eventId"),objMain.optString("eventTitle"),objMain.optString("eventStartDate"),objMain.optString("eventEndDate"),objMain.optString("eventDescription"),objMain.optString("eventLocation"),objMain.optString("eventDuration"),objMain.optString("eventCode"),objMain.optString("eventImageURL"),objMain.optString("eventURL"),objMain.optString("eventDonateStatus"),
                            objMain.optString("eventGiftTypeId"),objMain.optString("eventGiftRemarks"),objMain.optString("eventTypeStatusId"),objMain.optString("eventTypeStatus"),objMain.optString("eventLikeStatus"),objMain.optString("eventLikes"),objMain.optString("eventSeens"));

                            mEventList.add(el);

                        }

                        adap = new AdapterLiveCathupGrid(LiveCatchupGrid.this,mEventList);
                        rv.setLayoutManager(new GridLayoutManager(LiveCatchupGrid.this,numberOfColumns));
                        rv.setAdapter(adap);
                        adap.notifyDataSetChanged();


                        if (!mEventList.isEmpty())
                        {
                            pb.setVisibility(View.GONE);
                            rv.setVisibility(View.VISIBLE);

                        }

                        else
                        {
                            pb.setVisibility(View.GONE);
                            rv.setVisibility(View.GONE);
                            Toast.makeText(LiveCatchupGrid.this, "No Data...!", Toast.LENGTH_SHORT).show();
                        }
                        }

                    catch (Exception e) {
                        Log.e("Exception", String.valueOf(e));
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("EREROR",String.valueOf(error));
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();


                headers.put("vendorKey", Const.vendorKey);
                headers.put("deviceType",  "A");
                headers.put("Authorization",Sharedprefrences.getUserKey(LiveCatchupGrid.this));
                headers.put("Content-Type",Const.contentType);
                headers.put("userID",Sharedprefrences.getUserId(LiveCatchupGrid.this));
                headers.put("deviceToken",Sharedprefrences.getFCMRegistrationId(LiveCatchupGrid.this));
                headers.put("deviceMode",Const.deviceMode);

                return headers;
            }

            @Override
            public Map<String, String> getParams(){
                Map<String, String> params = new HashMap<>();

                params.put("eventId",CatchupId);

                return params;
            }
        };
        int socketTimeout = 20000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        sr.setRetryPolicy(policy);
        queue.add(sr);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:


                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        this.finish();
    }


    @Override
    protected void onResume() {
        super.onResume();

        getData();
    }
}
