package com.zecast.live.om_saravanabava.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.zecast.live.om_saravanabava.utils.Sharedprefrences;
import com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity;
import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.adapter.VideoCategoriesAdapter;
import com.zecast.live.om_saravanabava.adapter.VideoCategoriesSeeAllAdapter;
import com.zecast.live.om_saravanabava.model.VideoProgramsModel;
import com.zecast.live.om_saravanabava.model.VideosCategoryModel;
import com.zecast.live.om_saravanabava.utils.Const;
import com.zecast.live.om_saravanabava.utils.HTTPRequest;

import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class VideoFragment extends Fragment implements View.OnClickListener {

    RecyclerView recyclerViewVideoCategories;
    VideoCategoriesAdapter adapterMain;
    LinearLayoutManager linearLayoutManagaer;
    TextView videoCategoriesTv,seeAllCategories;
    public static  ArrayList<VideosCategoryModel> modelList = new ArrayList<>();
    VideoCategoriesSeeAllAdapter adapter;
    String mainCategoryName;
    ProgressBar pb;
    LinearLayout llContainer;
    private ProgressDialog dialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video, container, false);
        getActivity().setTitle("Video");
        initWidgets(view);
        seeAllCategories.setOnClickListener(this);
        HomeDrawerActivity.ivSearch.setVisibility(View.VISIBLE);

        Const.UPCOMING_FRAGMENT_KEY = 0;

        if (modelList.isEmpty()) {
            pb.setVisibility(View.VISIBLE);
            llContainer.setVisibility(View.GONE);
            fetchLiveVideos();
        }
          else {
            adapterMain = new VideoCategoriesAdapter(VideoFragment.this,getContext(), modelList);
            linearLayoutManagaer = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
            recyclerViewVideoCategories.setLayoutManager(linearLayoutManagaer);
            recyclerViewVideoCategories.setAdapter(adapterMain);
            recyclerViewVideoCategories.setNestedScrollingEnabled(false);
            adapterMain.notifyDataSetChanged();
             }

        return view;
    }

    private void initWidgets(View view) {
        recyclerViewVideoCategories = (RecyclerView) view.findViewById(R.id.rvTodaysPrograms);
        videoCategoriesTv = (TextView)view.findViewById(R.id.heading_recycler_category_type);
        seeAllCategories = (TextView)view.findViewById(R.id.heading_see_all_recycler);
        pb = (ProgressBar)view.findViewById(R.id.progressbar);
        llContainer = (LinearLayout)view.findViewById(R.id.llmain);
        dialog = new ProgressDialog(getContext());
    }


    public void likeButtonHit(int position, String  episodeid,String likecount) {

       /* dialog.setMessage("Doing something, please wait.");
        dialog.show();*/
        getLike(position,episodeid,likecount);
    }


    /*
     method like video
     */

    private void getLike(final int position,final String  episodeid,final String likecount) {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest sr = new StringRequest(Request.Method.POST, HTTPRequest.SERVICE_URL+"likeVideo", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.e("like responseeee",response);
                    try {
                        JSONObject obj = new JSONObject(response);
                        String message = obj.optString("message");
                        String error_type = obj.optString("error_type");
                        String status = obj.optString("status");

                        if (error_type.equals("200")) {
                            fetchLiveVideosAfterLike(position,likecount);
                        }
                    }
                    catch (Exception e) {
                        Log.e("Exception", e + "");
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error",String.valueOf(error));
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("vendorKey",Const.vendorKey);
                headers.put("deviceId", Sharedprefrences.getDeviceId(getActivity()));
                headers.put("deviceType", Const.deviceType);
                headers.put("Authorization",Sharedprefrences.getUserKey(getContext()));
                headers.put("userID",Sharedprefrences.getUserId(getContext()));
                headers.put("deviceToken",Sharedprefrences.getFCMRegistrationId(getContext()));
                headers.put("deviceMode",Const.deviceMode);
                headers.put("Content-Type", Const.contentType);
                return headers;
            }

            @Override
            public Map<String, String> getParams(){
                Map<String, String> params = new HashMap<>();
                Log.e("EPISODE IDD", "getParams: "+episodeid );
                params.put("videoId",episodeid);
                params.put("type","2");
                return params;
            }
        };
        queue.add(sr);
    }

    /*
    *method to fetch videos after like
    * */

    private void fetchLiveVideosAfterLike(final int position, final String likecount) {

        RequestQueue queue = Volley.newRequestQueue(getContext());
        StringRequest sr = new StringRequest(Request.Method.POST, HTTPRequest.SERVICE_URL+"getLiveTv", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.e("Video RESP AFTER LIKE", response);

                    try {
                        JSONObject obj = new JSONObject(response);
                        JSONObject objMain = obj.optJSONObject("response");
                        modelList.clear();
                        JSONArray videosCategoryArray = objMain.optJSONArray("video_category");

                        Log.e("LIST ITEM"," OUT OF LOOP"+videosCategoryArray);

                        for (int i = 0; i < videosCategoryArray.length(); i++) {

                            JSONObject categoriesOpeningObj = videosCategoryArray.optJSONObject(i);

                            String categoryId  = categoriesOpeningObj.optString("category_id");
                            mainCategoryName  = categoriesOpeningObj.optString("category_name");
                            String categoryLogo  = categoriesOpeningObj.optString("category_logo");
                            String categoryLogoType  = categoriesOpeningObj.optString("category_logo_type");
                            String categoryOrder  = categoriesOpeningObj.optString("category_order");

                            ArrayList<VideoProgramsModel> arrayEpisodeVideos = new ArrayList<>();
                            JSONArray arrayVideoPrograms = categoriesOpeningObj.getJSONArray("video_programs");

                            for (int k = 0; k<arrayVideoPrograms.length();k++) {

                                JSONObject objectOPeningVideoProgrms = arrayVideoPrograms.getJSONObject(k);

                                String episodeId = objectOPeningVideoProgrms.optString("episode_id");
                                String episodeName = objectOPeningVideoProgrms.optString("episode_name");
                                String episodeImage = objectOPeningVideoProgrms.optString("episode_image");
                                String episodeNumber = objectOPeningVideoProgrms.optString("episode_no");
                                String videoID = objectOPeningVideoProgrms.optString("video_id");
                                String epidsodeType = objectOPeningVideoProgrms.optString("episode_type");
                                String episodeUrl = objectOPeningVideoProgrms.optString("episode_url");
                                String episode_like = objectOPeningVideoProgrms.optString("episode_like");
                                String episode_seen = objectOPeningVideoProgrms.optString("episode_seen");
                                String episode_like_status = objectOPeningVideoProgrms.optString("episode_like_status");

                                VideoProgramsModel modelPrograms = new VideoProgramsModel(episodeId, episodeName,episodeImage,episodeNumber,
                                        videoID,epidsodeType,episodeUrl,
                                        episode_like,episode_seen,episode_like_status);

                                arrayEpisodeVideos.add(modelPrograms);
                                arrayEpisodeVideos.size();
                            }

                            VideosCategoryModel model = new VideosCategoryModel(categoryId,categoryLogo,mainCategoryName,categoryOrder,arrayEpisodeVideos);
                            modelList.add(model);
                            modelList.size();

                            adapterMain = new VideoCategoriesAdapter(VideoFragment.this,getContext(),modelList);
                            linearLayoutManagaer = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
                            recyclerViewVideoCategories.setLayoutManager(linearLayoutManagaer);
                            recyclerViewVideoCategories.setAdapter(adapterMain);
                            recyclerViewVideoCategories.setNestedScrollingEnabled(false);
                            recyclerViewVideoCategories.scrollToPosition(position);
                            adapterMain.notifyDataSetChanged();
                            pb.setVisibility(View.GONE);
                            llContainer.setVisibility(View.VISIBLE);

                    /* if (modelList.get(position).getSubcategorylist().get(position).getEpisode_like_status().equals("1"))
                     {
                         Log.e("LIKED VIDEO ", "EVENT LIKE STATUS PRINTTT: " +modelList.get(position).getSubcategorylist().get(position).getEpisode_like_status());
                            };*/
                        }
                    }
                    catch (Exception e) {
                        Log.e("Exception", String.valueOf(e));
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("EREROR",String.valueOf(error));
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();


                headers.put("vendorKey",Const.vendorKey);
                headers.put("deviceId", Sharedprefrences.getDeviceId(getActivity()));
                headers.put("deviceType", Const.deviceType);
                headers.put("Authorization",Sharedprefrences.getUserKey(getContext()));
                headers.put("userID",Sharedprefrences.getUserId(getContext()));
                headers.put("deviceToken",Sharedprefrences.getFCMRegistrationId(getContext()));
                headers.put("deviceMode",Const.deviceMode);
                headers.put("Content-Type", Const.contentType);

                return headers;
            }

            @Override
            public Map<String, String> getParams(){
                Map<String, String> params = new HashMap<>();
                return params;
            }
        };

        int socketTimeout = 20000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        sr.setRetryPolicy(policy);
        queue.add(sr);
    }

    private void fetchLiveVideos() {
        RequestQueue queue = Volley.newRequestQueue(getContext());
        StringRequest sr = new StringRequest(Request.Method.POST, HTTPRequest.SERVICE_URL+"getLiveTv", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.e("Video RESPONS#E", response);

                    try {

                        JSONObject obj = new JSONObject(response);
                        JSONObject objMain = obj.optJSONObject("response");
                         modelList.clear();
                        JSONArray videosCategoryArray = objMain.optJSONArray("video_category");

                        Log.e("LIST ITEM"," OUT OF LOOP"+videosCategoryArray);

                        for (int i = 0; i < videosCategoryArray.length(); i++) {

                            JSONObject categoriesOpeningObj = videosCategoryArray.optJSONObject(i);

                            String categoryId  = categoriesOpeningObj.optString("category_id");
                            mainCategoryName  = categoriesOpeningObj.optString("category_name");
                            String categoryLogo  = categoriesOpeningObj.optString("category_logo");
                            String categoryLogoType  = categoriesOpeningObj.optString("category_logo_type");
                            String categoryOrder  = categoriesOpeningObj.optString("category_order");


                            ArrayList<VideoProgramsModel> arrayEpisodeVideos = new ArrayList<>();

                            JSONArray arrayVideoPrograms = categoriesOpeningObj.getJSONArray("video_programs");

                            for (int k = 0; k<arrayVideoPrograms.length();k++) {

                                JSONObject objectOPeningVideoProgrms = arrayVideoPrograms.getJSONObject(k);

                                String episodeId = objectOPeningVideoProgrms.optString("episode_id");

                                String episodeName = objectOPeningVideoProgrms.optString("episode_name");

                                String episodeImage = objectOPeningVideoProgrms.optString("episode_image");

                                String episodeNumber = objectOPeningVideoProgrms.optString("episode_no");
                                String videoID = objectOPeningVideoProgrms.optString("video_id");

                                String epidsodeType = objectOPeningVideoProgrms.optString("episode_type");
                                String episodeUrl = objectOPeningVideoProgrms.optString("episode_url");
                                String episode_like = objectOPeningVideoProgrms.optString("episode_like");
                                String episode_seen = objectOPeningVideoProgrms.optString("episode_seen");
                                String episode_like_status = objectOPeningVideoProgrms.optString("episode_like_status");

                                VideoProgramsModel modelPrograms = new VideoProgramsModel(episodeId,episodeName,episodeImage,episodeNumber,videoID,epidsodeType,episodeUrl,episode_like,episode_seen,episode_like_status);
                                arrayEpisodeVideos.add(modelPrograms);
                                arrayEpisodeVideos.size();
                            }

                            VideosCategoryModel model = new VideosCategoryModel(categoryId,categoryLogo,mainCategoryName,categoryOrder,arrayEpisodeVideos);
                            modelList.add(model);
                            modelList.size();

                            Log.e("REACHED HERE", "onResponse: ");

                            adapterMain = new VideoCategoriesAdapter(VideoFragment.this,getContext(),modelList);
                            linearLayoutManagaer = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
                            recyclerViewVideoCategories.setLayoutManager(linearLayoutManagaer);
                            recyclerViewVideoCategories.setAdapter(adapterMain);
                            recyclerViewVideoCategories.setNestedScrollingEnabled(false);
                            adapterMain.notifyDataSetChanged();

                            pb.setVisibility(View.GONE);
                            llContainer.setVisibility(View.VISIBLE);
                        }
                    }
                    catch (Exception e) {
                        Log.e("Exception", String.valueOf(e));
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("EREROR",String.valueOf(error));
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();

                headers.put("vendorKey",Const.vendorKey);
                headers.put("deviceId", Sharedprefrences.getDeviceId(getActivity()));
                headers.put("deviceType", Const.deviceType);
                headers.put("Authorization",Sharedprefrences.getUserKey(getContext()));
                headers.put("userID",Sharedprefrences.getUserId(getContext()));
                headers.put("deviceToken",Sharedprefrences.getFCMRegistrationId(getContext()));
                headers.put("deviceMode",Const.deviceMode);
                headers.put("Content-Type", Const.contentType);

                return headers;
            }

            @Override
            public Map<String, String> getParams(){
                Map<String, String> params = new HashMap<>();
                return params;
            }
        };

        int socketTimeout = 20000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        sr.setRetryPolicy(policy);
        queue.add(sr);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.heading_see_all_recycler:

               }
        }

    @Override
    public void onResume() {
        super.onResume();
        HomeDrawerActivity.lineLive.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.White));
        HomeDrawerActivity.iconTabLive.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_live_unselected));
        HomeDrawerActivity.lineHome.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.White));
        HomeDrawerActivity.iconTabHome.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_home_unselected));
        HomeDrawerActivity.lineUpcoming.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.White));
        HomeDrawerActivity.iconTabUpcoming.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_upcoming_unselected));
        HomeDrawerActivity.lineVideo.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        HomeDrawerActivity.iconTabVideo.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_video_tab));
        HomeDrawerActivity.lineProfile.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.White));
        HomeDrawerActivity.iconTabProfile.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_profile_tab_unselected));
        HomeDrawerActivity.baseHomeText.setTextColor(ContextCompat.getColor(getContext(), R.color.gray));
        HomeDrawerActivity.basevideostext.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        HomeDrawerActivity.baseUpcomingtext.setTextColor(ContextCompat.getColor(getContext(), R.color.gray));
        HomeDrawerActivity.baseProfileText.setTextColor(ContextCompat.getColor(getContext(), R.color.gray));
        HomeDrawerActivity.baseLivetext.setTextColor(ContextCompat.getColor(getContext(), R.color.gray));


        Refresh();

    }

    private void Refresh() {
        RequestQueue queue = Volley.newRequestQueue(getContext());
        StringRequest sr = new StringRequest(Request.Method.POST, HTTPRequest.SERVICE_URL+"getLiveTv", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.e("Video RESPONS#E", response);

                    try {

                        JSONObject obj = new JSONObject(response);
                        JSONObject objMain = obj.optJSONObject("response");
                        modelList.clear();
                        JSONArray videosCategoryArray = objMain.optJSONArray("video_category");

                        Log.e("LIST ITEM"," OUT OF LOOP"+videosCategoryArray);

                        for (int i = 0; i < videosCategoryArray.length(); i++) {

                            JSONObject categoriesOpeningObj = videosCategoryArray.optJSONObject(i);

                            String categoryId  = categoriesOpeningObj.optString("category_id");
                            mainCategoryName  = categoriesOpeningObj.optString("category_name");
                            String categoryLogo  = categoriesOpeningObj.optString("category_logo");
                            String categoryLogoType  = categoriesOpeningObj.optString("category_logo_type");
                            String categoryOrder  = categoriesOpeningObj.optString("category_order");


                            ArrayList<VideoProgramsModel> arrayEpisodeVideos = new ArrayList<>();

                            JSONArray arrayVideoPrograms = categoriesOpeningObj.getJSONArray("video_programs");

                            for (int k = 0; k<arrayVideoPrograms.length();k++) {

                                JSONObject objectOPeningVideoProgrms = arrayVideoPrograms.getJSONObject(k);

                                String episodeId = objectOPeningVideoProgrms.optString("episode_id");

                                String episodeName = objectOPeningVideoProgrms.optString("episode_name");

                                String episodeImage = objectOPeningVideoProgrms.optString("episode_image");

                                String episodeNumber = objectOPeningVideoProgrms.optString("episode_no");
                                String videoID = objectOPeningVideoProgrms.optString("video_id");

                                String epidsodeType = objectOPeningVideoProgrms.optString("episode_type");
                                String episodeUrl = objectOPeningVideoProgrms.optString("episode_url");
                                String episode_like = objectOPeningVideoProgrms.optString("episode_like");
                                String episode_seen = objectOPeningVideoProgrms.optString("episode_seen");
                                String episode_like_status = objectOPeningVideoProgrms.optString("episode_like_status");

                                VideoProgramsModel modelPrograms = new VideoProgramsModel(episodeId,episodeName,episodeImage,episodeNumber,videoID,epidsodeType,episodeUrl,episode_like,episode_seen,episode_like_status);
                                arrayEpisodeVideos.add(modelPrograms);
                                arrayEpisodeVideos.size();
                            }

                            VideosCategoryModel model = new VideosCategoryModel(categoryId,categoryLogo,mainCategoryName,categoryOrder,arrayEpisodeVideos);
                            modelList.add(model);
                            modelList.size();

                            Log.e("REACHED HERE", "onResponse: ");


                            adapterMain.notifyDataSetChanged();


                        }
                    }
                    catch (Exception e) {
                        Log.e("Exception", String.valueOf(e));
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("EREROR",String.valueOf(error));
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();

                headers.put("vendorKey",Const.vendorKey);
                headers.put("deviceId", Sharedprefrences.getDeviceId(getActivity()));
                headers.put("deviceType", Const.deviceType);
                headers.put("Authorization",Sharedprefrences.getUserKey(getContext()));
                headers.put("userID",Sharedprefrences.getUserId(getContext()));
                headers.put("deviceToken",Sharedprefrences.getFCMRegistrationId(getContext()));
                headers.put("deviceMode",Const.deviceMode);
                headers.put("Content-Type", Const.contentType);

                return headers;
            }

            @Override
            public Map<String, String> getParams(){
                Map<String, String> params = new HashMap<>();
                return params;
            }
        };

        int socketTimeout = 20000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        sr.setRetryPolicy(policy);
        queue.add(sr);
    }


}
