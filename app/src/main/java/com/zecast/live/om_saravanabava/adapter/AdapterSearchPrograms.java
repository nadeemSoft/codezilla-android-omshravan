package com.zecast.live.om_saravanabava.adapter;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.callback.SearchListener;
import com.zecast.live.om_saravanabava.model.Search;
import com.zecast.live.om_saravanabava.model.VideoProgramsModel;
import com.zecast.live.om_saravanabava.activity.searchdetails.VideoDetailsSearchActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by  Salman on 28/7/17.
 */

   //searchingadapter

public class AdapterSearchPrograms extends RecyclerView.Adapter<AdapterSearchPrograms.MyViewHolder> {
    private Context context;
    ArrayList<VideoProgramsModel> arrayListData;
    ArrayList<Search> mSearchList;
    private SearchListener mSearchListener;
    LayoutInflater inflater;

    public AdapterSearchPrograms(Context context, ArrayList<Search> mSearchList) {
        this.context = context;
        this.mSearchList =mSearchList;
        inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public AdapterSearchPrograms.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(context, R.layout.searchingadapter/*single_row_item_video__programs_sub_categories*/,null);
        return new AdapterSearchPrograms.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final AdapterSearchPrograms.MyViewHolder holder, final int position) {

        holder.tvLike.setText(mSearchList.get(position).getEpisode_like());
        holder.tvEpisodeName.setText(mSearchList.get(position).getEpisode_name());
        holder.tvView.setText(mSearchList.get(position).getEpisode_seen());

        Picasso.with(context)
                .load(mSearchList.get(position).getEpisode_image())
                .error(R.drawable.app_icon)
                .placeholder(R.drawable.app_icon)
                .into(holder.ivHolderEpisodeLogo);

        holder.playVideoIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  Sharedprefrences.setEpisodeTypeStoredSearch(context,arrayListData.get(position).getEpisodeType());
                Intent in = new Intent(context, VideoDetailsSearchActivity.class);
                in.putExtra("EpisodeName",mSearchList.get(position).getEpisode_name());
                in.putExtra("EpisodeId",mSearchList.get(position).getEpisode_id());
                in.putExtra ("URL", mSearchList.get(position).getEpisode_url());
              //  in.putExtra("object",mSearchList.get(position));

                Log.e("URL FROM SEARCH ADAPTER", mSearchList.get(position).getEpisode_url());

                context.startActivity(in);

            }
        });


        holder.frameLayoutImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mSearchList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView ivHolderEpisodeLogo;
        public TextView tvEpisodeName,tvLike,tvView;
        ImageView playVideoIcon,ivShare;
        FrameLayout frameLayoutImg;
        public  ImageView imgLike;

        public MyViewHolder(View itemView) {

            super(itemView);
            ivHolderEpisodeLogo =(ImageView)itemView.findViewById(R.id.adapter_categories_imageview);
            tvEpisodeName = (TextView) itemView.findViewById(R.id.tv_episode_name);
            tvLike = (TextView) itemView.findViewById(R.id.tv_like);
            tvView = (TextView)itemView.findViewById(R.id.tv_views);
            playVideoIcon = (ImageView) itemView.findViewById(R.id.imageViewPlayVideo_pro);
            frameLayoutImg = (FrameLayout)itemView.findViewById(R.id.frame_img);
            ivShare = (ImageView)itemView.findViewById(R.id.videoShare);
            imgLike = (ImageView)itemView.findViewById(R.id.img_like);
        }
    }
}