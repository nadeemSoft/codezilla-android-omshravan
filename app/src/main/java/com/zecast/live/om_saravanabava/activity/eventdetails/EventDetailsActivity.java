package com.zecast.live.om_saravanabava.activity.eventdetails;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatCallback;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.adapter.EventsDetailsAdapter;
import com.zecast.live.om_saravanabava.fragments.LiveFragment;
import com.zecast.live.om_saravanabava.model.EventDetailsModel;
import com.zecast.live.om_saravanabava.utils.Const;
import com.zecast.live.om_saravanabava.utils.HTTPRequest;
import com.zecast.live.om_saravanabava.utils.Sharedprefrences;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by codezilla-12 on 18/10/17.
 */

public class EventDetailsActivity extends AppCompatActivity implements View.OnClickListener,AppCompatCallback {

    public static VideoView vv;
    public static TextView tvEpisodeName, tvEpisodeDate, tvSeen,tvDescription;
    RecyclerView rv;
    String eventId;
    ArrayList<EventDetailsModel> mEventDetails = new ArrayList<>();
    LinearLayoutManager layoutManager;
    EventsDetailsAdapter adap;
    LinearLayout videoContainer, frameContainer;
    ImageView ivPlay, ivPause, ivFullScreenDetails, ivSpeaker;
    TextView tvtitle;
    SeekBar seekbar, progressSeekbar;
    private AudioManager mAudioManager;

    ProgressBar pb;
    ImageView ivShare;
    TextView tvIds;


    @Override
    protected void onCreate( Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.eventdetails);

        init_Components();

        pb.setVisibility(View.VISIBLE);
        frameContainer.setVisibility(View.GONE);
        videoContainer.setVisibility(View.GONE);

        getIntentData();

        mAudioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
        soundControl();
        setSoundControlFunction();

        getData();

        ivPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ivPause.setVisibility(View.GONE);
                ivPlay.setVisibility(View.VISIBLE);
                vv.pause();
            }
        });

        ivPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ivPause.setVisibility(View.VISIBLE);
                ivPlay.setVisibility(View.GONE);
                vv.start();
            }
        });

        progressSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (b) {
                    vv.seekTo(i);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        ivFullScreenDetails.setOnClickListener(this);
        ivFullScreenDetails.setEnabled(false);
       // imgPlayYoutubeVideo.setOnClickListener(this);

    }

    private void getIntentData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            eventId = extras.getString("EventId");
        }
    }

    private void init_Components() {
        rv = (RecyclerView) findViewById(R.id.recycler_recent_events);
        vv = (VideoView) findViewById(R.id.videoview);
        tvEpisodeName = (TextView) findViewById(R.id.heading_event_live_1);
        tvEpisodeDate = (TextView) findViewById(R.id.date_event_live_1);
        tvSeen = (TextView) findViewById(R.id.tv_views);
        tvDescription = (TextView) findViewById(R.id.description_event_live_1);
        videoContainer = (LinearLayout) findViewById(R.id.video_Container);
        frameContainer = (LinearLayout) findViewById(R.id.frame_linear_container);
        ivPlay = (ImageView) findViewById(R.id.iv_play1);
        ivPause = (ImageView) findViewById(R.id.iv_pause1);
        ivFullScreenDetails = (ImageView) findViewById(R.id.video_fullscreen);
        seekbar = (SeekBar) findViewById(R.id.seekbar);
        ivSpeaker = (ImageView) findViewById(R.id.seekbarVolume);
        progressSeekbar = (SeekBar) findViewById(R.id.seekbarProgress);
        tvtitle = (TextView) findViewById(R.id.toolbar_title);
         //youtube_view_video_details = (YouTubePlayer) findViewById(R.id.youtube_view_video_details);

        pb = (ProgressBar) findViewById(R.id.progressbar);

        ivShare = (ImageView)findViewById(R.id.liveShare);
        tvIds = (TextView)findViewById(R.id.episodeId);



      //  toolbar.setho

        ivShare.setOnClickListener(this);
    }

    private void getData() {

        RequestQueue queue = Volley.newRequestQueue(EventDetailsActivity.this);
        StringRequest sr = new StringRequest(Request.Method.POST, HTTPRequest.SERVICE_URL + "getEventDetails", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.e("responseVideosDetials", response);
                    try {
                        mEventDetails.clear();
                        JSONObject obj = new JSONObject(response);
                        String message = obj.optString("message");
                        String status = obj.optString("status");

                        JSONObject objResponse = obj.optJSONObject("response");

                        JSONArray arr = objResponse.optJSONArray("otherVideos");

                        for (int i = 0; i < arr.length(); i++) {

                            JSONObject ob = arr.getJSONObject(i);

                            String eventId = ob.optString("eventId");
                            String eventTitle = ob.optString("eventTitle");
                            String eventStartDate = ob.optString("eventStartDate");
                            String eventEndDate = ob.optString("eventEndDate");
                            String eventDescription = ob.optString("eventDescription");
                            String eventLocation = ob.optString("eventLocation");
                            String eventDuration = ob.optString("eventDuration");
                            String eventCode = ob.optString("eventCode");
                            String eventImageURL = ob.optString("eventImageURL");
                            String eventURL = ob.optString("eventURL");
                            String eventDonateStatus = ob.optString("eventDonateStatus");
                            String eventGiftTypeId = ob.optString("eventGiftTypeId");
                            String eventGiftRemarks = ob.optString("eventGiftRemarks");
                            String eventTypeStatusId = ob.optString("eventTypeStatusId");
                            String eventTypeStatus = ob.optString("eventTypeStatus");
                            String eventLikeStatus = ob.optString("eventLikeStatus");
                            String eventLikes = ob.optString("eventLikes");
                            String eventSeens = ob.optString("eventSeens");

                            EventDetailsModel ed = new EventDetailsModel(eventId, eventTitle, eventStartDate, eventEndDate, eventDescription, eventLocation, eventDuration, eventCode, eventImageURL, eventURL, eventDonateStatus,eventGiftTypeId,eventGiftRemarks,eventTypeStatusId,eventTypeStatus,eventLikeStatus,eventLikes,eventSeens);
                            mEventDetails.add(ed);
                        }

                        adap = new EventsDetailsAdapter(EventDetailsActivity.this, mEventDetails);
                        layoutManager = new LinearLayoutManager(EventDetailsActivity.this, LinearLayoutManager.HORIZONTAL, false);
                        rv.setLayoutManager(layoutManager);
                        rv.setAdapter(adap);
                        rv.setNestedScrollingEnabled(false);
                        adap.notifyDataSetChanged();

                        pb.setVisibility(View.GONE);
                        frameContainer.setVisibility(View.VISIBLE);
                        videoContainer.setVisibility(View.VISIBLE);

                        String eventId = objResponse.optString("eventId");
                        String eventTitle = objResponse.optString("eventTitle");
                        String eventStartDate = objResponse.optString("eventStartDate");
                        String eventEndDate = objResponse.optString("eventEndDate");
                        String eventDescription = objResponse.optString("eventDescription");
                        String eventLocation = objResponse.optString("eventLocation");
                        String eventDuration = objResponse.optString("eventDuration");
                        String eventCode = objResponse.optString("eventCode");
                        String eventImageURL = objResponse.optString("eventImageURL");
                        String eventURL = objResponse.optString("eventURL");
                        String eventDonateStatus = objResponse.optString("eventDonateStatus");
                        String eventGiftTypeId = objResponse.optString("eventGiftTypeId");
                        String eventGiftRemarks = objResponse.optString("eventGiftRemarks");
                        String eventTypeStatusId = objResponse.optString("eventTypeStatusId");
                        String eventTypeStatus = objResponse.optString("eventTypeStatus");
                        String eventLikeStatus = objResponse.optString("eventLikeStatus");
                        String eventLikes = objResponse.optString("eventLikes");
                        String eventSeens = objResponse.optString("eventSeens");


                        EventDetailsActivity.this.setTitle(eventTitle);
                        tvIds.setText(eventId);
                        tvEpisodeName.setText(eventTitle);
                        tvSeen.setText(eventSeens);
                        tvDescription.setText(eventDescription);
                        tvEpisodeDate.setText(eventStartDate);

                        Sharedprefrences.setVideoUrls(EventDetailsActivity.this, eventURL);

                        Toast.makeText(EventDetailsActivity.this,"Please wait Video is Starting....!",Toast.LENGTH_SHORT).show();

                        new BackgroundAsyncTask().execute(eventURL);


                    } catch (Exception e) {
                        Log.e("Exception", e + "");
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", String.valueOf(error));
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                headers.put("vendorKey", Const.vendorKey);
                headers.put("Authorization", Sharedprefrences.getUserKey(EventDetailsActivity.this));
                headers.put("userID", Sharedprefrences.getUserId(EventDetailsActivity.this));
                headers.put("deviceToken",Sharedprefrences.getFCMRegistrationId(EventDetailsActivity.this));
                headers.put("deviceType", Const.deviceType);
                headers.put("deviceId", Sharedprefrences.getDeviceId(EventDetailsActivity.this));
                headers.put("Content-Type", Const.contentType);
                return headers;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("eventId", eventId);
                return params;
            }
        };
        queue.add(sr);
    }

    public void playLiveChannel(EventDetailsModel ov) {
        try {


            progressSeekbar.setProgress(0);

            getRefreshData(ov.getEventId());

            EventDetailsActivity.this.setTitle(ov.getEventTitle());

            tvIds.setText(eventId);

            tvEpisodeName.setText(ov.getEventTitle());

            tvSeen.setText(ov.getEventSeens());
            tvDescription.setText(ov.getEventDescription());
            tvEpisodeDate.setText(ov.getEventStartDate());

            Toast.makeText(EventDetailsActivity.this,"Please wait Video is Starting....!",Toast.LENGTH_SHORT).show();


            new BackgroundAsyncTask().execute(ov.getEventURL());



        } catch (Exception e) {
        }

    }

    private void getRefreshData(final String eventId) {

        RequestQueue queue = Volley.newRequestQueue(EventDetailsActivity.this);
        StringRequest sr = new StringRequest(Request.Method.POST, HTTPRequest.SERVICE_URL + "getEventDetails", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if (response != null) {

                    Log.e("Refresh",response);

                    try {
                        mEventDetails.clear();

                        JSONObject obj = new JSONObject(response);
                        String message = obj.optString("message");
                        String status = obj.optString("status");

                        JSONObject objResponse = obj.optJSONObject("response");

                        JSONArray arr = objResponse.optJSONArray("otherVideos");

                        for (int i = 0; i < arr.length(); i++) {

                            JSONObject ob = arr.getJSONObject(i);

                            String eventId = ob.optString("eventId");
                            String eventTitle = ob.optString("eventTitle");
                            String eventStartDate = ob.optString("eventStartDate");
                            String eventEndDate = ob.optString("eventEndDate");
                            String eventDescription = ob.optString("eventDescription");
                            String eventLocation = ob.optString("eventLocation");
                            String eventDuration = ob.optString("eventDuration");
                            String eventCode = ob.optString("eventCode");
                            String eventImageURL = ob.optString("eventImageURL");
                            String eventURL = ob.optString("eventURL");
                            String eventDonateStatus = ob.optString("eventDonateStatus");
                            String eventGiftTypeId = ob.optString("eventGiftTypeId");
                            String eventGiftRemarks = ob.optString("eventGiftRemarks");
                            String eventTypeStatusId = ob.optString("eventTypeStatusId");
                            String eventTypeStatus = ob.optString("eventTypeStatus");
                            String eventLikeStatus = ob.optString("eventLikeStatus");
                            String eventLikes = ob.optString("eventLikes");
                            String eventSeens = ob.optString("eventSeens");

                            EventDetailsModel ed = new EventDetailsModel(eventId, eventTitle, eventStartDate, eventEndDate, eventDescription, eventLocation, eventDuration, eventCode, eventImageURL, eventURL, eventDonateStatus,eventGiftTypeId,eventGiftRemarks,eventTypeStatusId,eventTypeStatus,eventLikeStatus,eventLikes,eventSeens);
                            mEventDetails.add(ed);
                        }

                        adap.notifyDataSetChanged();


                    } catch (Exception e) {
                        Log.e("Exception", e + "");
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", String.valueOf(error));
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                headers.put("vendorKey", Const.vendorKey);
                headers.put("Authorization", Sharedprefrences.getUserKey(EventDetailsActivity.this));
                headers.put("userID", Sharedprefrences.getUserId(EventDetailsActivity.this));
                headers.put("deviceToken",Sharedprefrences.getFCMRegistrationId(EventDetailsActivity.this));
                headers.put("deviceType", Const.deviceType);
                headers.put("deviceId", Sharedprefrences.getDeviceId(EventDetailsActivity.this));
                headers.put("Content-Type", Const.contentType);
                return headers;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("eventId", eventId);
                Log.e("EventId",eventId);
                return params;
            }
        };
        queue.add(sr);
    }

    public class BackgroundAsyncTask extends AsyncTask<String, Uri, Void> {


        protected void onPreExecute() {

//            pb.setVisibility(View.VISIBLE);

        }

        protected void onProgressUpdate(final Uri... uri) {

            try {

                vv.setVideoURI(uri[0]);

                vv.requestFocus();

                vv.getLayoutParams().width = ViewGroup.LayoutParams.FILL_PARENT;

                vv.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

                    public void onPrepared(MediaPlayer mp) {
                        mp.setLooping(true);
                        vv.start();
                        vv.setVisibility(View.VISIBLE);
                        ivFullScreenDetails.setEnabled(true);
//
                        ivPlay.setVisibility(View.GONE);
                        ivPause.setVisibility(View.VISIBLE);
                        progressSeekbar.setMax(vv.getDuration());
                    }
                });

                vv.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                    @Override
                    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {

                        vv.setVisibility(View.VISIBLE);
                        vv.start();
                        Toast.makeText(EventDetailsActivity.this, "Cant play Video", Toast.LENGTH_SHORT).show();
//                        ivFulllscreen.setVisibility(View.GONE);
                        ivPlay.setVisibility(View.GONE);
                        ivPause.setVisibility(View.GONE);


                        Log.e("Error", "error");
                        return true;
                    }
                });

            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(String... params) {
            try {
                Uri uri = Uri.parse(params[0]);
                publishProgress(uri);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.video_fullscreen:
                int orientation = this.getResources().getConfiguration().orientation;

                if (orientation == Configuration.ORIENTATION_PORTRAIT) {

                    getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                    getSupportActionBar().hide();

                    vv.getLayoutParams().width = ViewGroup.LayoutParams.FILL_PARENT;
                    vv.getLayoutParams().height = ViewGroup.LayoutParams.FILL_PARENT;
                    frameContainer.getLayoutParams().height = ViewGroup.LayoutParams.FILL_PARENT;
                    frameContainer.getLayoutParams().height = ViewGroup.LayoutParams.FILL_PARENT;
                    videoContainer.getLayoutParams().height = ViewGroup.LayoutParams.FILL_PARENT;
                    videoContainer.getLayoutParams().width = ViewGroup.LayoutParams.FILL_PARENT;

                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                }
                else {

                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                    getSupportActionBar().show();

                    vv.getLayoutParams().width = ViewGroup.LayoutParams.FILL_PARENT;
                    vv.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    videoContainer.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    videoContainer.getLayoutParams().width = ViewGroup.LayoutParams.FILL_PARENT;
                    frameContainer.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    frameContainer.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }

                break;


            case R.id.liveShare:

                List<Intent> targetShareIntents = new ArrayList<Intent>();
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                List<ResolveInfo> resInfos = getPackageManager().queryIntentActivities(shareIntent, 0);
                if (!resInfos.isEmpty()) {
                    System.out.println("Have package");
                    for (ResolveInfo resInfo : resInfos) {
                        String packageName = resInfo.activityInfo.packageName;
                        Log.i("Package Name", packageName);

                        if (!(packageName.contains("com.google.android.apps.translate") || packageName.contains("com.google.android.gms.drive") || packageName.contains("com.google.android.hangouts"))) {

                            Intent intents = new Intent();
                            intents.setComponent(new ComponentName(packageName, resInfo.activityInfo.name));
                            intents.setAction(Intent.ACTION_SEND);
                            intents.setType("text/plain");
                            intents.putExtra(Intent.EXTRA_TEXT,"watch the video- videoURL "+ "http://www.omsharavanabhavamatham.org.uk/dl/eventId/"+tvIds.getText().toString());
                            intents.putExtra(Intent.EXTRA_SUBJECT, "watch the video- videoURL");
                            intents.setPackage(packageName);
                            targetShareIntents.add(intents);
                        }

                    }
                    if (!targetShareIntents.isEmpty()) {
                        System.out.println("Have Intent");
                        Intent chooserIntent = Intent.createChooser(targetShareIntents.remove(0), "Choose app to share");
                        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetShareIntents.toArray(new Parcelable[]{}));
                        startActivity(chooserIntent);
                    } else {
                        System.out.println("Do not Have Intent");
                    }
                }

                break;
            default:
                break;
        }
    }

    private void soundControl() {

        seekbar.setMax(mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));

        seekbar.setProgress(mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC));

        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if (progress == 0) {

                    ivSpeaker.setImageResource(R.drawable.mute);


                } else if (progress != 0) {
                    ivSpeaker.setImageResource(R.drawable.speaker);

                }
                mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                        progress, 0);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

        });
    }

    private void setSoundControlFunction() {

        this.seekbar.setMax(mAudioManager
                .getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        this.seekbar.setProgress(mAudioManager
                .getStreamVolume(AudioManager.STREAM_MUSIC));
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        final AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            try{
                seekbar.setProgress(audioManager
                        .getStreamVolume(AudioManager.STREAM_MUSIC) - 1);
            } catch (Error e) {
                // min value
            }
        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
            try{
                seekbar.setProgress(audioManager
                        .getStreamVolume(AudioManager.STREAM_MUSIC) + 1);
            } catch (Error e) {
                // max value
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {

        int orientation = this.getResources().getConfiguration().orientation;

        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {

            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getSupportActionBar().show();

            vv.getLayoutParams().width = ViewGroup.LayoutParams.FILL_PARENT;
            vv.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
            videoContainer.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
            videoContainer.getLayoutParams().width = ViewGroup.LayoutParams.FILL_PARENT;
            frameContainer.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
            frameContainer.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            super.onBackPressed();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}