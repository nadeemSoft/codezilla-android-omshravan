package com.zecast.live.om_saravanabava.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.helper.Utility;
import com.zecast.live.om_saravanabava.model.Banner;
import com.zecast.live.om_saravanabava.model.HomeBanner;
import com.zecast.live.om_saravanabava.utils.Sharedprefrences;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class HomeRecyclerBannerAdapter extends RecyclerView.Adapter<HomeRecyclerBannerAdapter.ViewHolder>  {

    private Context context;
    ArrayList<HomeBanner> homeBannerArrayList;
    LayoutInflater inflater;
    static FragmentManager fm;
    Activity activity;

    public HomeRecyclerBannerAdapter(FragmentManager fm, Activity activity, Context context, ArrayList<HomeBanner> homeBannerArrayList) {
        this.context = context;
        this.fm = fm;
        this.homeBannerArrayList = homeBannerArrayList;
        this.activity  = activity;
        inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public HomeRecyclerBannerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_banner_view_pager, null);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);

        return viewHolder;
    }

    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        ArrayList<ViewPager> viewPagersList = new ArrayList<>();
        viewPagersList.add(viewHolder.topLeftPager);
        viewPagersList.add(viewHolder.topRightPager);
        viewPagersList.add(viewHolder.viewPager);
        viewPagersList.add(viewHolder.bottomLowerPager);
        viewPagersList.add(viewHolder.bottomUpperPager);
        viewPagersList.add(viewHolder.bottomRightPager);

        ArrayList<ViewPager> viewPagersList2 = new ArrayList<>();
        viewPagersList2.add(viewHolder.topLeftPager2);
        viewPagersList2.add(viewHolder.topRightPager2);
        viewPagersList2.add(viewHolder.viewPager2);
        viewPagersList2.add(viewHolder.bottomLowerPager2);
        viewPagersList2.add(viewHolder.bottomUpperPager2);
        viewPagersList2.add(viewHolder.bottomRightPager2);

        ArrayList<ViewPager> viewPagersList3 = new ArrayList<>();
        viewPagersList3.add(viewHolder.topLeftPager3);
        viewPagersList3.add(viewHolder.topRightPager3);
        viewPagersList3.add(viewHolder.viewPager3);
        viewPagersList3.add(viewHolder.bottomLowerPager3);
        viewPagersList3.add(viewHolder.bottomUpperPager3);
        viewPagersList3.add(viewHolder.bottomRightPager3);

        ArrayList<ViewPager> viewPagersList4 = new ArrayList<>();
        viewPagersList4.add(viewHolder.topLeftPager4);
        viewPagersList4.add(viewHolder.topRightPager4);
        viewPagersList4.add(viewHolder.viewPager4);
        viewPagersList4.add(viewHolder.bottomLowerPager4);
        viewPagersList4.add(viewHolder.bottomUpperPager4);
        viewPagersList4.add(viewHolder.bottomRightPager4);

        ArrayList<ViewPager> viewPagersList5 = new ArrayList<>();
        viewPagersList5.add(viewHolder.topLeftPager5);
        viewPagersList5.add(viewHolder.topRightPager5);
        viewPagersList5.add(viewHolder.viewPager5);
        viewPagersList5.add(viewHolder.bottomLowerPager5);
        viewPagersList5.add(viewHolder.bottomUpperPager5);
        viewPagersList5.add(viewHolder.bottomRightPager5);

        ArrayList<ViewPager> viewPagersList6 = new ArrayList<>();
        viewPagersList6.add(viewHolder.topLeftPager6);
        viewPagersList6.add(viewHolder.topRightPager6);
        viewPagersList6.add(viewHolder.viewPager6);
        viewPagersList6.add(viewHolder.bottomLowerPager6);
        viewPagersList6.add(viewHolder.bottomUpperPager6);
        viewPagersList6.add(viewHolder.bottomRightPager6);

        for(int j = 0; j < homeBannerArrayList.size(); j++) {

            if (j == 0) {
                HomeBanner homeBanner = homeBannerArrayList.get(j);
                ArrayList<Banner> bannerArrayList = homeBanner.getBannerArrayList();

                LinearLayout.LayoutParams topLayoutParams = Utility.setLinearTopImageSize(viewHolder.topLayout, activity, 8, 3);
                viewHolder.topLayout.setLayoutParams(topLayoutParams);
                   LinearLayout.LayoutParams middleLayoutParams = Utility.setLinearMiddleImageSize(viewHolder.middleLayout, activity, 16, 7);
                viewHolder.middleLayout.setLayoutParams(middleLayoutParams);
                LinearLayout.LayoutParams bottomLayoutParams = Utility.setLinearBottomImageSize(viewHolder.bottomLayout, activity, 4, 3);
                viewHolder.bottomLayout.setLayoutParams(bottomLayoutParams);



                for (int i = 0; i < bannerArrayList.size(); i++) {
                    viewPagersList.get(i).setVisibility(View.VISIBLE);
                    setPagerAdapter(viewPagersList.get(i), bannerArrayList.get(i).getImageUrl(), bannerArrayList.get(i).getType(), bannerArrayList.get(i).getAdKey());
                    Sharedprefrences.setBAnnerpositionStored(context, String.valueOf(i));

                }

            }else if (j == 1) {
                HomeBanner homeBanner = homeBannerArrayList.get(j);
                ArrayList<Banner> bannerArrayList = homeBanner.getBannerArrayList();

                LinearLayout.LayoutParams topLayoutParams2 = Utility.setLinearTopImageSize(viewHolder.topLayout2, activity, 8, 3);
                viewHolder.topLayout2.setLayoutParams(topLayoutParams2);
                LinearLayout.LayoutParams middleLayoutParams2 = Utility.setLinearMiddleImageSize(viewHolder.middleLayout2, activity, 16, 7);
                viewHolder.middleLayout2.setLayoutParams(middleLayoutParams2);
                LinearLayout.LayoutParams bottomLayoutParams2 = Utility.setLinearBottomImageSize(viewHolder.bottomLayout2, activity, 4, 3);
                viewHolder.bottomLayout2.setLayoutParams(bottomLayoutParams2);


                for (int i = 0; i < bannerArrayList.size(); i++) {
                    viewPagersList2.get(i).setVisibility(View.VISIBLE);
                    setPagerAdapter(viewPagersList2.get(i), bannerArrayList.get(i).getImageUrl(), bannerArrayList.get(i).getType(), bannerArrayList.get(i).getAdKey());
                }
            }else if (j == 2) {
                HomeBanner homeBanner = homeBannerArrayList.get(j);
                ArrayList<Banner> bannerArrayList = homeBanner.getBannerArrayList();

                LinearLayout.LayoutParams topLayoutParams3 = Utility.setLinearTopImageSize(viewHolder.topLayout3, activity, 8, 3);
                viewHolder.topLayout3.setLayoutParams(topLayoutParams3);
                LinearLayout.LayoutParams middleLayoutParams3 = Utility.setLinearMiddleImageSize(viewHolder.middleLayout3, activity, 16, 7);
                viewHolder.middleLayout3.setLayoutParams(middleLayoutParams3);
                LinearLayout.LayoutParams bottomLayoutParams3 = Utility.setLinearBottomImageSize(viewHolder.bottomLayout3, activity, 4, 3);
                viewHolder.bottomLayout3.setLayoutParams(bottomLayoutParams3);

                for (int i = 0; i < bannerArrayList.size(); i++) {
                    viewPagersList3.get(i).setVisibility(View.VISIBLE);
                    setPagerAdapter(viewPagersList3.get(i), bannerArrayList.get(i).getImageUrl(), bannerArrayList.get(i).getType(), bannerArrayList.get(i).getAdKey());
                }
            }else if (j == 3) {
                HomeBanner homeBanner = homeBannerArrayList.get(j);
                ArrayList<Banner> bannerArrayList = homeBanner.getBannerArrayList();

                LinearLayout.LayoutParams topLayoutParams4 = Utility.setLinearTopImageSize(viewHolder.topLayout4, activity, 8, 4);
                viewHolder.topLayout4.setLayoutParams(topLayoutParams4);
                LinearLayout.LayoutParams middleLayoutParams4 = Utility.setLinearMiddleImageSize(viewHolder.middleLayout4, activity, 16, 7);
                viewHolder.middleLayout4.setLayoutParams(middleLayoutParams4);
                LinearLayout.LayoutParams bottomLayoutParams4 = Utility.setLinearBottomImageSize(viewHolder.bottomLayout4, activity, 4, 4);
                viewHolder.bottomLayout4.setLayoutParams(bottomLayoutParams4);

                for (int i = 0; i < bannerArrayList.size(); i++) {
                    viewPagersList4.get(i).setVisibility(View.VISIBLE);
                    setPagerAdapter(viewPagersList4.get(i), bannerArrayList.get(i).getImageUrl(), bannerArrayList.get(i).getType(), bannerArrayList.get(i).getAdKey());
                }
            }else if (j == 4) {
                HomeBanner homeBanner = homeBannerArrayList.get(j);
                ArrayList<Banner> bannerArrayList = homeBanner.getBannerArrayList();

                LinearLayout.LayoutParams topLayoutParams5 = Utility.setLinearTopImageSize(viewHolder.topLayout5, activity, 8, 5);
                viewHolder.topLayout5.setLayoutParams(topLayoutParams5);
                LinearLayout.LayoutParams middleLayoutParams5 = Utility.setLinearMiddleImageSize(viewHolder.middleLayout5, activity, 16, 7);
                viewHolder.middleLayout5.setLayoutParams(middleLayoutParams5);
                LinearLayout.LayoutParams bottomLayoutParams5 = Utility.setLinearBottomImageSize(viewHolder.bottomLayout5, activity, 4, 5);
                viewHolder.bottomLayout5.setLayoutParams(bottomLayoutParams5);

                for (int i = 0; i < bannerArrayList.size(); i++) {
                    viewPagersList5.get(i).setVisibility(View.VISIBLE);
                    setPagerAdapter(viewPagersList5.get(i), bannerArrayList.get(i).getImageUrl(), bannerArrayList.get(i).getType(), bannerArrayList.get(i).getAdKey());
                }
            }else if (j == 5) {
                HomeBanner homeBanner = homeBannerArrayList.get(j);
                ArrayList<Banner> bannerArrayList = homeBanner.getBannerArrayList();

                LinearLayout.LayoutParams topLayoutParams6 = Utility.setLinearTopImageSize(viewHolder.topLayout6, activity, 8, 6);
                viewHolder.topLayout6.setLayoutParams(topLayoutParams6);
                LinearLayout.LayoutParams middleLayoutParams6 = Utility.setLinearMiddleImageSize(viewHolder.middleLayout6, activity, 16, 7);
                viewHolder.middleLayout6.setLayoutParams(middleLayoutParams6);
                LinearLayout.LayoutParams bottomLayoutParams6 = Utility.setLinearBottomImageSize(viewHolder.bottomLayout6, activity, 4, 6);
                viewHolder.bottomLayout6.setLayoutParams(bottomLayoutParams6);

                for (int i = 0; i < bannerArrayList.size(); i++) {
                    viewPagersList6.get(i).setVisibility(View.VISIBLE);
                    setPagerAdapter(viewPagersList6.get(i), bannerArrayList.get(i).getImageUrl(), bannerArrayList.get(i).getType(), bannerArrayList.get(i).getAdKey());
                }
            }
        }
    }

    int NUM_PAGES;
    int currentPage;
    private void setPagerAdapter(final ViewPager viewPager, ArrayList<String> itemData, int type, String adUnit){
        ViewPagerAdapter adapter = new ViewPagerAdapter(fm, itemData,type, adUnit, context);
        viewPager.setAdapter(adapter);
        //viewPager.startAutoScroll(10000);
        NUM_PAGES = itemData.size();
        currentPage = 0;

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                Log.v("currentPage",currentPage+"xs"+NUM_PAGES);
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                    Log.v("currentPage",currentPage+"xs"+NUM_PAGES);
                }
                viewPager.setCurrentItem(currentPage++, true);
                Log.v("currentPage++",currentPage+"xs"+NUM_PAGES);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//                currentPage=position;

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public final static class ViewHolder extends RecyclerView.ViewHolder {

        ViewPager viewPager;
        ViewPager topLeftPager;
        ViewPager topRightPager;
        ViewPager bottomLowerPager;
        ViewPager bottomUpperPager;
        ViewPager bottomRightPager;
        LinearLayout topLayout;
        LinearLayout middleLayout;
        LinearLayout bottomLayout;

        ViewPager viewPager2;
        ViewPager topLeftPager2;
        ViewPager topRightPager2;
        ViewPager bottomLowerPager2;
        ViewPager bottomUpperPager2;
        ViewPager bottomRightPager2;
        LinearLayout topLayout2;
        LinearLayout middleLayout2;
        LinearLayout bottomLayout2;

        ViewPager viewPager3;
        ViewPager topLeftPager3;
        ViewPager topRightPager3;
        ViewPager bottomLowerPager3;
        ViewPager bottomUpperPager3;
        ViewPager bottomRightPager3;
        LinearLayout topLayout3;
        LinearLayout middleLayout3;
        LinearLayout bottomLayout3;

        ViewPager viewPager4;
        ViewPager topLeftPager4;
        ViewPager topRightPager4;
        ViewPager bottomLowerPager4;
        ViewPager bottomUpperPager4;
        ViewPager bottomRightPager4;
        LinearLayout topLayout4;
        LinearLayout middleLayout4;
        LinearLayout bottomLayout4;

        ViewPager viewPager5;
        ViewPager topLeftPager5;
        ViewPager topRightPager5;
        ViewPager bottomLowerPager5;
        ViewPager bottomUpperPager5;
        ViewPager bottomRightPager5;
        LinearLayout topLayout5;
        LinearLayout middleLayout5;
        LinearLayout bottomLayout5;

        ViewPager viewPager6;
        ViewPager topLeftPager6;
        ViewPager topRightPager6;
        ViewPager bottomLowerPager6;
        ViewPager bottomUpperPager6;
        ViewPager bottomRightPager6;
        LinearLayout topLayout6;
        LinearLayout middleLayout6;
        LinearLayout bottomLayout6;

        public ViewHolder(View view) {
            super(view);

            this.viewPager = (ViewPager)view.findViewById(R.id.viewpager);
            this.topLeftPager = (ViewPager) view.findViewById(R.id.top_left_viewpager);
            this.topRightPager = (ViewPager) view.findViewById(R.id.top_right_viewpager);
            this.bottomLowerPager = (ViewPager)view.findViewById(R.id.bottom_lower_viewpager);
            this.bottomUpperPager = (ViewPager) view.findViewById(R.id.bottom_upper_viewpager);
            this.bottomRightPager = (ViewPager) view.findViewById(R.id.bottom_right_viewpager);

            this.topLayout = (LinearLayout) view.findViewById(R.id.top_banner_layout);
            this.middleLayout = (LinearLayout) view.findViewById(R.id.banner_middle_layout);
            this.bottomLayout = (LinearLayout) view.findViewById(R.id.bottom_banner_layout);

            this.viewPager2 = (ViewPager)view.findViewById(R.id.viewpager_2);
            this.topLeftPager2 = (ViewPager) view.findViewById(R.id.top_left_viewpager_2);
            this.topRightPager2 = (ViewPager) view.findViewById(R.id.top_right_viewpager_2);
            this.bottomLowerPager2 = (ViewPager)view.findViewById(R.id.bottom_lower_viewpager_2);
            this.bottomUpperPager2 = (ViewPager) view.findViewById(R.id.bottom_upper_viewpager_2);
            this.bottomRightPager2 = (ViewPager) view.findViewById(R.id.bottom_right_viewpager_2);

            this.topLayout2 = (LinearLayout) view.findViewById(R.id.top_banner_layout_2);
            this.middleLayout2 = (LinearLayout) view.findViewById(R.id.banner_middle_layout_2);
            this.bottomLayout2 = (LinearLayout) view.findViewById(R.id.bottom_banner_layout_2);

            this.viewPager3 = (ViewPager)view.findViewById(R.id.viewpager_3);
            this.topLeftPager3 = (ViewPager) view.findViewById(R.id.top_left_viewpager_3);
            this.topRightPager3 = (ViewPager) view.findViewById(R.id.top_right_viewpager_3);
            this.bottomLowerPager3 = (ViewPager)view.findViewById(R.id.bottom_lower_viewpager_3);
            this.bottomUpperPager3 = (ViewPager) view.findViewById(R.id.bottom_upper_viewpager_3);
            this.bottomRightPager3 = (ViewPager) view.findViewById(R.id.bottom_right_viewpager_3);

            this.topLayout3 = (LinearLayout) view.findViewById(R.id.top_banner_layout_3);
            this.middleLayout3 = (LinearLayout) view.findViewById(R.id.banner_middle_layout_3);
            this.bottomLayout3 = (LinearLayout) view.findViewById(R.id.bottom_banner_layout_3);

            this.viewPager4 = (ViewPager)view.findViewById(R.id.viewpager_4);
            this.topLeftPager4 = (ViewPager) view.findViewById(R.id.top_left_viewpager_4);
            this.topRightPager4 = (ViewPager) view.findViewById(R.id.top_right_viewpager_4);
            this.bottomLowerPager4 = (ViewPager)view.findViewById(R.id.bottom_lower_viewpager_4);
            this.bottomUpperPager4 = (ViewPager) view.findViewById(R.id.bottom_upper_viewpager_4);
            this.bottomRightPager4 = (ViewPager) view.findViewById(R.id.bottom_right_viewpager_4);

            this.topLayout4 = (LinearLayout) view.findViewById(R.id.top_banner_layout_4);
            this.middleLayout4 = (LinearLayout) view.findViewById(R.id.banner_middle_layout_4);
            this.bottomLayout4 = (LinearLayout) view.findViewById(R.id.bottom_banner_layout_4);

            this.viewPager5 = (ViewPager)view.findViewById(R.id.viewpager_5);
            this.topLeftPager5 = (ViewPager) view.findViewById(R.id.top_left_viewpager_5);
            this.topRightPager5 = (ViewPager) view.findViewById(R.id.top_right_viewpager_5);
            this.bottomLowerPager5 = (ViewPager)view.findViewById(R.id.bottom_lower_viewpager_5);
            this.bottomUpperPager5 = (ViewPager) view.findViewById(R.id.bottom_upper_viewpager_5);
            this.bottomRightPager5 = (ViewPager) view.findViewById(R.id.bottom_right_viewpager_5);

            this.topLayout5 = (LinearLayout) view.findViewById(R.id.top_banner_layout_5);
            this.middleLayout5 = (LinearLayout) view.findViewById(R.id.banner_middle_layout_5);
            this.bottomLayout5 = (LinearLayout) view.findViewById(R.id.bottom_banner_layout_5);

            this.viewPager6 = (ViewPager)view.findViewById(R.id.viewpager_6);
            this.topLeftPager6 = (ViewPager) view.findViewById(R.id.top_left_viewpager_6);
            this.topRightPager6 = (ViewPager) view.findViewById(R.id.top_right_viewpager_6);
            this.bottomLowerPager6 = (ViewPager)view.findViewById(R.id.bottom_lower_viewpager_6);
            this.bottomUpperPager6 = (ViewPager) view.findViewById(R.id.bottom_upper_viewpager_6);
            this.bottomRightPager6 = (ViewPager) view.findViewById(R.id.bottom_right_viewpager_6);

            this.topLayout6 = (LinearLayout) view.findViewById(R.id.top_banner_layout_6);
            this.middleLayout6 = (LinearLayout) view.findViewById(R.id.banner_middle_layout_6);
            this.bottomLayout6 = (LinearLayout) view.findViewById(R.id.bottom_banner_layout_6);

        }
    }

    public int getItemCount() {
        Log.d("count", (null != homeBannerArrayList ? homeBannerArrayList.size() : 0) + "");
        return (null != homeBannerArrayList ? 1 : 0);
    }

}