package com.zecast.live.om_saravanabava.helper;

import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.util.Log;
import com.zecast.live.om_saravanabava.utils.Sharedprefrences;

/**
 * Created by Salman on 15/9/17.
 */

public class RadioStreamService extends Service {
    private static final String TAG = "StreamService";
    public static MediaPlayer mp;
    String url;

    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onCreate() {
        super.onCreate();
        // It's very important that you put the IP/URL of your ShoutCast stream here
        mp = new MediaPlayer();
        mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
        playsong();
    }

    private void playsong() {
        {
            try {
                url = Sharedprefrences.getAudioUrl(this);
                Log.e("radio",url);
                mp.reset();
                mp.setDataSource(url);
                mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mp.prepareAsync();

                mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mediaPlayer) {
                        mp.start();
                        Log.e("radioError", "6");
                    }
                });
            } catch (Exception e) {
                Log.e("error", String.valueOf(e));
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onStart(Intent intent, int startId) {
        Log.e(TAG, "onStart");
        mp.start();
    }

    @Override
    public void onDestroy() {
        if (mp != null) {
            mp.stop();
            mp.release();
            mp = null;
        }
    }
}