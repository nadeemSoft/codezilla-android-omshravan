package com.zecast.live.om_saravanabava.helper;

public interface AsyncCallback {

    void setResponse(Integer responseCode, String responseStr);

    void setException(String e);
}
