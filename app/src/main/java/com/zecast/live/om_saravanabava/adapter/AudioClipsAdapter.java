package com.zecast.live.om_saravanabava.adapter;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.devbrackets.android.exomedia.listener.OnPreparedListener;
import com.devbrackets.android.exomedia.ui.widget.VideoView;
import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.activity.audio.AudioActivity;
import com.zecast.live.om_saravanabava.helper.RadioStreamService;
import com.zecast.live.om_saravanabava.model.AudioDataModel;
import com.zecast.live.om_saravanabava.utils.Sharedprefrences;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AudioClipsAdapter extends RecyclerView.Adapter<AudioClipsAdapter.MyViewHolder> {
    private Context context;
    ArrayList<AudioDataModel> arrayListData;
    public int clickedPosition = -1;
    public boolean val = false;

    public AudioClipsAdapter(Context context, ArrayList<AudioDataModel> mAudioList) {
        this.context = context;
        this.arrayListData = mAudioList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(context, R.layout.single_row_item_audio_adapter, null);
        return new AudioClipsAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {

        final AudioDataModel audioModel = arrayListData.get(position);
        holder.tvTitle.setText(audioModel.getAudioName());

        if (clickedPosition == -1) {
            val = true;
            clickedPosition = 0;
            holder.imageViewPlayIcon.setVisibility(View.VISIBLE);
            holder.imageViewPauseIcon.setVisibility(View.GONE);
        } else {
            if (position == clickedPosition) {
                val = true;
                holder.imageViewPlayIcon.setVisibility(View.GONE);
                holder.imageViewPauseIcon.setVisibility(View.VISIBLE);
            } else {
                holder.imageViewPlayIcon.setVisibility(View.VISIBLE);
                holder.imageViewPauseIcon.setVisibility(View.GONE);
            }
        }

        Picasso.with(context)
                .load(audioModel.getAudioLogo())
                .error(R.drawable.app_icon)
                .placeholder(R.drawable.app_icon)
                .into(holder.imageViewAudiologo);

        holder.imageViewAudiologo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        holder.imageViewPlayIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Sharedprefrences.setAudioURl(context, arrayListData.get(position).getAudioUrl());

                 holder.vv.setVideoURI(Uri.parse(audioModel.getAudioUrl()));
                holder.vv.start();

                holder.vv.getDuration();
// Intent intent = new Intent(context, RadioStreamService.class);
//                context.stopService(intent);
//                context.startService(intent);
                holder.imageViewPlayIcon.setVisibility(View.GONE);
                holder.imageViewPauseIcon.setVisibility(View.VISIBLE);

            }
        });

        holder.imageViewPauseIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                holder.vv.stopPlayback();
                holder.imageViewPlayIcon.setVisibility(View.VISIBLE);
                holder.imageViewPauseIcon.setVisibility(View.GONE);

            }
        });
        holder.seekBarVolume.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                holder.audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                        i, 0);
                holder.ivAudioVolume.setImageResource(R.drawable.speaker);
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (seekBar.getProgress() == 0) {

                    holder.ivAudioVolume.setImageResource(R.drawable.mute);
                    holder.ivAudioVolume.setSoundEffectsEnabled(false);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayListData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;
        ImageView imageViewAudiologo,ivAudioVolume;
        ImageView imageViewPlayIcon, imageViewPauseIcon;
        SeekBar seekBarAudioPos, seekBarVolume;
        AudioManager audioManager;
        VideoView vv;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.textViewNameAudio);
            imageViewAudiologo = (ImageView) itemView.findViewById(R.id.imageLogoAudio);
            imageViewPlayIcon = (ImageView) itemView.findViewById(R.id.vcv_img_play);
            imageViewPauseIcon = (ImageView) itemView.findViewById(R.id.imageButton_pause);
            seekBarAudioPos = (SeekBar) itemView.findViewById(R.id.appCompatSeekBarprogressaudio);
            seekBarVolume = (SeekBar) itemView.findViewById(R.id.seekbarProgressVoice);
            ivAudioVolume = (ImageView)itemView.findViewById(R.id.high_volume);
            audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
            vv = (VideoView)itemView.findViewById(R.id.video_view);

            seekBarVolume.setMax(audioManager
                    .getStreamMaxVolume(AudioManager.STREAM_MUSIC));
            seekBarVolume.setProgress(audioManager
                    .getStreamVolume(AudioManager.STREAM_MUSIC));
        }


    }


}