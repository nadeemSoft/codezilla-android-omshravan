package com.zecast.live.om_saravanabava.helper;

public interface MessageConstant {
     String MESSAGE_SOMETHING_WRONG = "OOPS! something went's wrong";
     String MESSAGE_INTERNET_CONNECTION = "Please check intenet connection";
     String MESSAGE_PLEASE_WAIT = "Please wait...";
}