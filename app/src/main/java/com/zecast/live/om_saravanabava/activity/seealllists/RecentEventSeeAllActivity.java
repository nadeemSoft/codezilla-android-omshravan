package com.zecast.live.om_saravanabava.activity.seealllists;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.Window;
import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.adapter.RecentEventsSeeAllAdapter;
import static com.zecast.live.om_saravanabava.fragments.LiveFragment.recentEventsModelList;

public class RecentEventSeeAllActivity extends AppCompatActivity {

    RecyclerView recyclerSeeAll;
    GridLayoutManager layoutManager;
    RecentEventsSeeAllAdapter adapter;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recent_event_see_all);
        setTitle("Recent Event");
        initWidgets();
        setRecyclerAdapter();
        Window window = this.getWindow();
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.colorPrimary));

        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
    }

    private void setRecyclerAdapter() {
        adapter = new RecentEventsSeeAllAdapter(getBaseContext(), recentEventsModelList);
        layoutManager = new GridLayoutManager(getBaseContext(),2);
        recyclerSeeAll.setLayoutManager(layoutManager);
        recyclerSeeAll.setAdapter(adapter);
        recyclerSeeAll.setNestedScrollingEnabled(false);
        adapter.notifyDataSetChanged();
    }

    private void initWidgets() {
        recyclerSeeAll = (RecyclerView)findViewById(R.id.recycler_recent_see_all);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        System.exit(0);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
