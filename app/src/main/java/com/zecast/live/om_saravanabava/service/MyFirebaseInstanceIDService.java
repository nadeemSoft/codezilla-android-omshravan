package com.zecast.live.om_saravanabava.service;

import android.app.ProgressDialog;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.zecast.live.om_saravanabava.utils.Const;
import com.zecast.live.om_saravanabava.utils.HTTPRequest;
import com.zecast.live.om_saravanabava.utils.Sharedprefrences;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static android.webkit.ConsoleMessage.MessageLevel.LOG;


/**
 * Created by codezilla-8 on 27/9/17.
 */

//AIzaSyBMdF7tU9PVsU2W2QljrWFuLWnsKjEbXKQ
public class MyFirebaseInstanceIDService  extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";
    ProgressDialog pd;
    String regId;

    @Override
    public void onTokenRefresh() {
        //Getting registration token
        regId = FirebaseInstanceId.getInstance().getToken();
        Sharedprefrences.getFCMRegistrationId(MyFirebaseInstanceIDService.this);
        Sharedprefrences.setFCMRegistrationId(MyFirebaseInstanceIDService.this,regId);
        //Displaying token on logcat
        Log.e(TAG, "MobileToken: " + regId);
       // Log.e(TAG, "onTokenRefresh: "+"TOKENN"+regId);

        if (!regId.isEmpty())
        {
            updateDevice();
        }

        }


    private void updateDevice()
    {
        RequestQueue queue = Volley.newRequestQueue(MyFirebaseInstanceIDService.this);

        StringRequest sr = new StringRequest(Request.Method.POST, HTTPRequest.SERVICE_URL + "updateDevice", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {

                    Log.e("response", response);
                    try {
                        JSONObject objects = new JSONObject(response);

                        JSONObject obj =  objects.optJSONObject("response");

                        String message = obj.optString("message");


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("error",String.valueOf(error));
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();

                try {

                    headers.put("vendorKey",Const.vendorKey);
                    headers.put("deviceId", Sharedprefrences.getDeviceId(MyFirebaseInstanceIDService.this));
                    headers.put("deviceType", Const.deviceType);
                    headers.put("deviceToken", regId);
                    headers.put("Content-Type", Const.contentType);


                }catch (Exception e){}


                return headers;
            }

            @Override
            public Map<String, String> getParams(){
                Map<String, String> params = new HashMap<>();




                return params;
            }
        };
        queue.add(sr);
    }
    }