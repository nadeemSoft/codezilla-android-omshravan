package com.zecast.live.om_saravanabava.activity.search;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.adapter.AdapterSearchPrograms;
import com.zecast.live.om_saravanabava.model.Search;
import com.zecast.live.om_saravanabava.utils.Const;
import com.zecast.live.om_saravanabava.utils.HTTPRequest;
import com.zecast.live.om_saravanabava.utils.Sharedprefrences;

import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by codezilla-8 on 23/8/17.
 */

public class SearchActivity extends AppCompatActivity  {
    ImageView iv_search;
    EditText exName;
    RecyclerView rvRecylerView;
    ArrayList<Search> mSearchList = new ArrayList<>();
    AdapterSearchPrograms adapSearch;
    int numberOfColumns = 2;
    String videoId;
    private String videoUrl,epiString;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.searching);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        init();

        iv_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager keyboard = (InputMethodManager)SearchActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                keyboard.hideSoftInputFromWindow(iv_search.getWindowToken(), 0);
                getSearchData();
            }
        });

        exName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH)
                {
                    InputMethodManager keyboard = (InputMethodManager)SearchActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    keyboard.hideSoftInputFromWindow(iv_search.getWindowToken(), 0);
                    getSearchData();
                    return true;
                }
                return false;
            }
        });
    }

    private void init() {
        iv_search = (ImageView)findViewById(R.id.iv_search);
        exName = (EditText)findViewById(R.id.search_key);
        rvRecylerView = (RecyclerView)findViewById(R.id.searchprograms_recyclerView);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getSearchData()
    {
        RequestQueue queue = Volley.newRequestQueue(SearchActivity.this);
        StringRequest sr = new StringRequest(Request.Method.POST, HTTPRequest.SERVICE_URL+"searchVideo", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null)
                {
                    Log.e("response",response);
                    try {


                        JSONObject obj = new JSONObject(response);
                        String message = obj.optString("message");
                        String errorType = obj.optString("error_type");
                        if (errorType.equals("206"))
                        {
                            mSearchList.clear();
                            adapSearch.notifyDataSetChanged();

                        }
                        Toast.makeText(SearchActivity.this,message,Toast.LENGTH_SHORT).show();

                        JSONObject objRes = obj.optJSONObject("response");
                        mSearchList.clear();
                        JSONArray arr = objRes.optJSONArray("VideoList");

                        for (int i = 0; i < arr.length(); i++) {

                            JSONObject objc = arr.optJSONObject(i);

                            String episodeId = objc.optString("episode_id");
                            String episodeName = objc.optString("episode_name");
                            String episodeImage = objc.optString("episode_image");
                            String episode_date = objc.optString("episode_date");
                            String video_id = objc.optString("video_id");
                            String episode_type = objc.optString("episode_type");
                            String episode_url = objc.optString("episode_url");
                            String episode_seen = objc.optString("episode_seen");
                            String episode_desc = objc.optString("episode_desc");
                            String episode_like_status = objc.optString("episode_like_status");
                            String episode_like = objc.optString("episode_like");

                            Search sr = new Search(episodeId,episodeName,episodeImage,episode_date,video_id,episode_type,episode_url,episode_seen,episode_desc,episode_like_status,episode_like);
                            mSearchList.add(sr);

                            adapSearch = new AdapterSearchPrograms(SearchActivity.this,mSearchList);
                            Log.e("SET SEARCH ", "onResponse: "+"REACHED HERE" );
                            rvRecylerView.setLayoutManager(new GridLayoutManager(SearchActivity.this, numberOfColumns));
                            rvRecylerView.setAdapter(adapSearch);
                            adapSearch.notifyDataSetChanged();

                        }
                    }catch (Exception e){}
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("error",String.valueOf(error));
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("vendorKey", Const.vendorKey);
                headers.put("deviceId", Sharedprefrences.getUserId(SearchActivity.this));
                headers.put("deviceType",Const.deviceType);
                headers.put("Authorization", Sharedprefrences.getUserKey(SearchActivity.this));
                return headers;
            }

            @Override
            public Map<String, String> getParams(){
                Map<String, String> params = new HashMap<>();
                params.put("keyword", exName.getText().toString());
                return params;
            }
        };
        queue.add(sr);
    }


   /* SearchListener searchListener = new SearchListener() {

        @Override
        public void video(String episode_type, String url,String videoId) {

            epiString = episode_type;
            Log.e("listener", episode_type + "===" + url + "===" + videoId);
            if (epiString != null && epiString.equalsIgnoreCase("direct")) {
                videoUrl = url;
            } else {
                videoUrl = videoId;
            }

            Intent intent = new Intent(SearchActivity.this, VideoDetailsActivity.class);
            startActivity(intent);
        }
    };
*/

}