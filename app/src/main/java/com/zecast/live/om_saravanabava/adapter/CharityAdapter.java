package com.zecast.live.om_saravanabava.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.model.CharityModel;

import java.util.ArrayList;

public class CharityAdapter extends RecyclerView.Adapter<CharityAdapter.MyViewHolder> {

    private Context context;
    ArrayList<CharityModel> arrayListData;
    public int clickedPosition = -1;
    public boolean val = false;
    int h,btnh,btnWidth;

    public CharityAdapter(Context context, ArrayList<CharityModel> mAudioList) {
        this.context = context;
        this.arrayListData = mAudioList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = View.inflate(context, R.layout.single_row_item_charity_adapter, null);
        return new CharityAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        DisplayMetrics dm = new DisplayMetrics();
        ((FragmentActivity)context).getWindowManager().getDefaultDisplay().getMetrics(dm);
        double x = Math.pow(dm.widthPixels/dm.xdpi,2);
        double y = Math.pow(dm.heightPixels/dm.ydpi,2);
        final double screenInches = Math.sqrt(x+y);
        Log.e("debug","Screen inches : " + screenInches);

        if (clickedPosition == -1) {
            val = true;
        } else {
            if  (position == clickedPosition) {
                val = true;
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.height = 580;
                params.width=params.WRAP_CONTENT;
                params.setMargins(15,5,15,5);
                holder.cardViewContainer.setLayoutParams(params);
                holder.cardViewContainer.setElevation(5);
                holder.tvDeatils1.setVisibility(View.VISIBLE);
                holder.tvViewDetails.setVisibility(View.GONE);

//                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//                params.height = h+280; //height recycleviewer
//                params.width=params.WRAP_CONTENT;
//                params.setMargins(15,15,15,15);
//                holder.cardViewContainer.setLayoutParams(params);
//                holder.cardViewContainer.setElevation(5);
//                holder.tvDeatils1.setVisibility(View.VISIBLE);
//                holder.tvViewDetails.setVisibility(View.GONE);

            } else {
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.height = -580;
                params.width=params.WRAP_CONTENT;
                params.setMargins(15,5,15,5);
                holder.cardViewContainer.setLayoutParams(params);
                holder.cardViewContainer.setElevation(5);
                holder.tvDeatils1.setVisibility(View.VISIBLE);
                holder.tvViewDetails.setVisibility(View.GONE);
            }
        }

        final CharityModel charityModel = arrayListData.get(position);
        holder.tvTitle.setText(charityModel.getCharityName());

        Picasso.with(context)
                .load(charityModel.getCharityLogo())
                .error(R.drawable.app_icon)
                .placeholder(R.drawable.app_icon)
                .fit()
                .into(holder.imgViewCharityLogo);

        holder.tvDescription.setText(charityModel.getCharityDesc());
        holder.donateNowBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://donate.justgiving.com/donation-amount?uri=aHR0cHM6Ly9kb25hdGUtYXBpLmp1c3RnaXZpbmcuY29tL2FwaS9kb25hdGlvbnMvOTM4ZDM3YTM0NDk1NDRhODhiYTViZWY4NTJkZjRjMGM="));
                context.startActivity(browserIntent);
            }
        });

        holder.tvViewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (screenInches < 5) {

                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    params.height = 650;
                    params.width = params.WRAP_CONTENT;
                    params.setMargins(15, 5, 15, 5);

                    holder.cardViewContainer.setLayoutParams(params);
                    holder.cardViewContainer.setElevation(5);
                    holder.tvDeatils1.setVisibility(View.VISIBLE);
                    holder.tvViewDetails.setVisibility(View.GONE);
                }

                if (screenInches > 5) {

                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    params.height = 1200;
                    params.width = params.WRAP_CONTENT;
                    params.setMargins(15, 5, 15, 5);

                    holder.cardViewContainer.setLayoutParams(params);
                    holder.cardViewContainer.setElevation(5);
                    holder.tvDeatils1.setVisibility(View.VISIBLE);
                    holder.tvViewDetails.setVisibility(View.GONE);
                }
            }
        });

        holder.tvDeatils1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                if (arrayListData.get(position).getCharityDesc().isEmpty()) {
                    return;
                }

                if (screenInches < 5) {
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    params.height = -650; //height recycleviewer
                    params.width = params.WRAP_CONTENT;
                    params.setMargins(15, 2, 15, 2);

                    holder.cardViewContainer.setLayoutParams(params);
                    holder.cardViewContainer.setElevation(5);
                    holder.tvDeatils1.setVisibility(View.GONE);
                    holder.tvViewDetails.setVisibility(View.VISIBLE);

                }

                if (screenInches > 5) {
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    params.height = -1200; //height recycleviewer
                    params.width = params.WRAP_CONTENT;
                    params.setMargins(15, 2, 15, 2);

                    holder.cardViewContainer.setLayoutParams(params);
                    holder.cardViewContainer.setElevation(5);
                    holder.tvDeatils1.setVisibility(View.GONE);
                    holder.tvViewDetails.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayListData.size();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle,tvDescription,tvViewDetails,tvDeatils1;
        ImageView imgViewCharityLogo;
        ImageView imageViewPlayIcon, imageViewPauseIcon;
        Button donateNowBtn;
        LinearLayout linearDescriptionContainer,llMain;
        LinearLayout linearButtonContainer;
        CardView cardViewContainer;

        public MyViewHolder(View itemView)  {

            super(itemView);

            tvTitle = (TextView) itemView.findViewById(R.id.title_tv);
            tvDescription = (TextView) itemView.findViewById(R.id.desc_tv);
            tvViewDetails = (TextView) itemView.findViewById(R.id.tv_view_details);
            imgViewCharityLogo = (ImageView) itemView.findViewById(R.id.image__charity_logo);
            imageViewPlayIcon = (ImageView) itemView.findViewById(R.id.vcv_img_play);
            imageViewPauseIcon = (ImageView) itemView.findViewById(R.id.imageButton_pause);
            donateNowBtn = (Button) itemView.findViewById(R.id.btn_donate_now);
            linearDescriptionContainer = (LinearLayout)itemView.findViewById(R.id.linearDescrContainer);
            linearButtonContainer = (LinearLayout)itemView.findViewById(R.id.linearButtonContainer);
            cardViewContainer = (CardView)itemView.findViewById(R.id.card_view_row_3);
            tvDeatils1 = (TextView)itemView.findViewById(R.id.tv_view_details1);
            llMain = (LinearLayout)itemView.findViewById(R.id.llmain);
            tvViewDetails.performClick();

        }
    }
}