package com.zecast.live.om_saravanabava.adapter;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Parcelable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.model.RecentEventsModel;
import com.zecast.live.om_saravanabava.fragments.LiveFragment;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class RecentEventsAdapter extends RecyclerView.Adapter<RecentEventsAdapter.MyViewHolder>  {

    private Context context;
    ArrayList<RecentEventsModel> arrayListData;
    LiveFragment fragment;

    public static int clickedPosition = -1;
    public boolean val = false;

    public RecentEventsAdapter(Context context, ArrayList<RecentEventsModel> mCategoryList, LiveFragment fragment) {
        this.context = context;
        this.arrayListData = mCategoryList;
        this.fragment = fragment;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = View.inflate(context, R.layout.single_row_item_recent_event_categories,null);
        return new RecentEventsAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder,final  int position) {

        final RecentEventsModel programs = arrayListData.get(position);




        if (!LiveFragment.recentEventsModelList.isEmpty()) {
            if (clickedPosition == -2) {
                val = true;
                clickedPosition = 2;
                holder.llMain.setBackgroundResource(0);

            }

            if (clickedPosition == -1) {
                val = true;
                clickedPosition = 0;
                holder.llMain.setBackgroundResource(R.color.colorPrimary);

            }
        }
        if (position == clickedPosition) {
            val = true;
            holder.llMain.setBackgroundResource(R.color.colorPrimary);
        } else {
            holder.llMain.setBackgroundResource(0);

        }

        holder.liveTagTextView.setVisibility(View.GONE);

        holder.tvEpisodeName.setText(programs.getEventName());
        holder.tvView.setText(programs.getEventSeens());




        if (!programs.getEventImageURL().isEmpty())
        {
            Picasso.with(context).
                    load(programs.getEventImageURL())
                    .error(R.drawable.app_icon)
                    .placeholder(R.drawable.app_icon)
                    .into(holder.ivHolderEpisodeLogo);
        }

        if (programs.getEventTypeStatus().contains("Live")) {
            holder.liveTagTextView.setVisibility(View.VISIBLE);

        } else {
            holder.liveTagTextView.setVisibility(View.GONE);
        }

        holder.llLive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (arrayListData.get(position).getEventDonateStatus().equals("Yes"))
                {
                    LiveFragment.dontateNowLayout.setVisibility(View.VISIBLE);
                } else {
                    //  LiveFragment.dontateNowLayout.setVisibility(View.GONE);
                }

                fragment.callAsnycTaskFromRecent(programs, String.valueOf(position));

                clickedPosition=position;
                notifyDataSetChanged();
            }
        });


    }


    @Override
    public int getItemCount() {
        return arrayListData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView ivHolderEpisodeLogo,ivshare;
        public TextView tvEpisodeName,tvLike,tvView,tvComment,liveTagTextView;
        ImageView playVideoIcon;
        FrameLayout frameLayoutImg;
        CardView cardViewContainer;
        LinearLayout llLive,ll,llMain;


        public MyViewHolder(View itemView) {

            super(itemView);

            ivHolderEpisodeLogo =(ImageView)itemView.findViewById(R.id.adapter_recent_programs_imageview);
            tvEpisodeName = (TextView) itemView.findViewById(R.id.tv_title_catchup);
          //  tvLike = (TextView) itemView.findViewById(R.id.tv_like);
            tvView = (TextView)itemView.findViewById(R.id.tv_views);
            playVideoIcon = (ImageView) itemView.findViewById(R.id.play_video_icon);
            frameLayoutImg = (FrameLayout)itemView.findViewById(R.id.frame_img);
            // tvComment = (TextView)itemView.findViewById(R.id.tv_comment);
            liveTagTextView = (TextView)itemView.findViewById(R.id.tv_live_tag_img_view);
            cardViewContainer = (CardView)itemView.findViewById(R.id.card_view);
           // ivshare = (ImageView)itemView.findViewById(R.id.liveshares);
            llLive = (LinearLayout)itemView.findViewById(R.id.llLayoutLive);
            ll = (LinearLayout)itemView.findViewById(R.id.llLayoutLive);
            llMain = (LinearLayout)itemView.findViewById(R.id.llMains);
        }
    }
}