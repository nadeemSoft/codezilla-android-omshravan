package com.zecast.live.om_saravanabava.activity.verify;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity;
import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.activity.search.SearchActivity;
import com.zecast.live.om_saravanabava.fragments.ProfileFragment;
import com.zecast.live.om_saravanabava.utils.Const;
import com.zecast.live.om_saravanabava.utils.HTTPRequest;
import com.zecast.live.om_saravanabava.utils.Sharedprefrences;
import com.zecast.live.om_saravanabava.activity.signup.SignupActivity;
import com.raycoarana.codeinputview.CodeInputView;
import com.raycoarana.codeinputview.OnCodeCompleteListener;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by codezilla-12 on 18/9/17.
 */

public class VerifyMobileActivity extends Fragment implements View.OnClickListener {

    TextView timerTv, tvResendSMS , tvMobileNumber, tv2Mobilenumber;
    private boolean isRunning = false;
    String OTPCode = "";
    CodeInputView codeInputView;


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view  = inflater.inflate(R.layout.activity_verify_mobile,container,false);

        codeInputView = (CodeInputView)view.findViewById(R.id.code_input_view);

        Window window = getActivity().getWindow();
        window.setStatusBarColor(ContextCompat.getColor(getActivity(),R.color.colorPrimary));

        initVidgets(view);
        countDownTimer();

        tvMobileNumber.setText("Verify"+"  "+SignupActivity.countrycode+" "+Sharedprefrences.getUserEnteredMobile(getActivity()));
        tv2Mobilenumber.setText("+"+"  "+SignupActivity.countrycode+" "+Sharedprefrences.getUserEnteredMobile(getActivity()));

        codeInputView.addOnCompleteListener(new OnCodeCompleteListener() {
            @Override
            public void onCompleted(String code) {

                if (!(codeInputView.getCode().isEmpty())) {

                    OTPCode = codeInputView.getCode();
                    verifyMobile();
                }
            }
        });

        codeInputView.addOnCompleteListener(new OnCodeCompleteListener() {
            @Override
            public void onCompleted(String code) {

            }
        });

        //  timerTv.setOnClickListener(this);
        tvResendSMS.setOnClickListener(this);

        return view;
    }


    private void resendOTP() {

            RequestQueue queue = Volley.newRequestQueue(getActivity());
            StringRequest sr = new StringRequest(Request.Method.POST, HTTPRequest.SERVICE_URL + "ReSendVerificationCode", new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    if (response != null) {
                        Log.e("RESEND OTP ", response);

                        try {
                            codeInputView.setVisibility(View.VISIBLE);
                            JSONObject response1 = new JSONObject(response);

                            String message = response1.optString("message");
                            String errorType = response1.optString("error_type");
                            String status = response1.optString("status");

                            if (status.equals("200")) {
                                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

                                FragmentTransaction fg5 = getActivity().getSupportFragmentManager().beginTransaction(); //to begin home page with fragment
                                fg5.replace(R.id.frame_drawer, new VerifyMobileActivity());
                                fg5.commit();

                            } else {

                                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }


                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    Log.e("EREROR", String.valueOf(error));
                    Toast.makeText(getActivity(), String.valueOf(error), Toast.LENGTH_SHORT).show();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<String, String>();

                    headers.put("vendorKey", Const.vendorKey);
                    headers.put("deviceId", Sharedprefrences.getDeviceId(getActivity()));
                    headers.put("Content-Type", Const.contentType);

                    return headers;
                }


                @Override
                public Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("userId", Sharedprefrences.getUserId(getActivity()));
                    params.put("userKey", Sharedprefrences.getUserKey(getActivity()));
                    return params;
                }
            };

            int socketTimeout = 20000;//30 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            sr.setRetryPolicy(policy);
            queue.add(sr);

    }

    protected void countDownTimer() {
        if (!isRunning) {
            new CountDownTimer(60000, 1000) {
                public void onTick(long millisUntilFinished) {
                    isRunning = true;

                    timerTv.setTextColor(Color.parseColor("#000000"));
                    timerTv.setText("0:" + millisUntilFinished / 1000);
                    timerTv.setEnabled(false);
                    tvResendSMS.setEnabled(false);
                    tvResendSMS.setTextColor(Color.parseColor("#616161"));

                }


                @Override
                public void onFinish() {
                    tvResendSMS.setEnabled(true);
                    tvResendSMS.setTextColor(Color.parseColor("#ff9900"));
                    timerTv.setText("0:0");
                    timerTv.setTextColor(Color.parseColor("#616161"));
                    isRunning = false;
                }
            }.start();
        }
    }

    public void initVidgets(View view) {
        timerTv = (TextView)view.findViewById(R.id.tv_resendsms_timer);
        tvResendSMS = (TextView)view.findViewById(R.id.tv_resendsms);
        tvMobileNumber = (TextView)view.findViewById(R.id.phone_number_tv);
        tv2Mobilenumber = (TextView)view.findViewById(R.id.verifyno_tvnumber);
    }

    private void verifyMobile() {

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest sr = new StringRequest(Request.Method.POST, HTTPRequest.SERVICE_URL + "NumberVerification", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.e("VERIFICATION OTP ", response);

                    try {

                        codeInputView.setVisibility(View.VISIBLE);
                        JSONObject response1 = new JSONObject(response);
                        String responseStatus = response1.getString("status");
                        String errorType = response1.optString("error_type");
                        String responseMessage = response1.optString("message");

                        if (!errorType.equals("203")) {
                        JSONObject openingObj = response1.optJSONObject("response");


                        for (int i =0;i<openingObj.length();i++) {

                            String userId = openingObj.getString("userId");
                            String userName = openingObj.optString("userName");
                            String userEmail = openingObj.optString("userEmail");
                            String userMobile = openingObj.getString("userMobile");
                            String userCountry = openingObj.optString("userCountry");
                            String userCountryId = openingObj.optString("userCountryId");
                            String userCountryCode = openingObj.getString("userCountryCode");
                            String userKey = openingObj.optString("userKey");


                            Sharedprefrences.setUserLogedInStatus(getActivity(), true);
                            Sharedprefrences.setUserId(getActivity(), userId);
                            Sharedprefrences.setUserKey(getActivity(), userKey);
                            Sharedprefrences.setUserName(getActivity(), userName);
                            Sharedprefrences.setUserMobile(getActivity(), userMobile);
                            Sharedprefrences.setUserEmail(getActivity(), userEmail);
                            Sharedprefrences.setUserCountry(getActivity(), userCountry);

                           }
                        }

                        VolleyLog.e("status response VERIFICATION OTP " + responseStatus + " msg " + responseMessage + "error msg " + errorType);
                        Toast.makeText(getActivity(), responseMessage, Toast.LENGTH_SHORT).show();

                        if (errorType.equals("203")) {

                            Toast.makeText(getActivity(), responseMessage, Toast.LENGTH_SHORT).show();

                            FragmentTransaction fg5 = getActivity().getSupportFragmentManager().beginTransaction(); //to begin home page with fragment
                            fg5.replace(R.id.frame_drawer, new VerifyMobileActivity());
                            fg5.commit();

//                            startActivity(getIntent());

                        } if (errorType.equals("200")) {

                            FragmentTransaction fg5 = getActivity().getSupportFragmentManager().beginTransaction(); //to begin home page with fragment
                            fg5.replace(R.id.frame_drawer, new ProfileFragment());
                            fg5.commit();


//                           Intent intentNavigateHome = new Intent(VerifyMobileActivity.this, HomeDrawerActivity.class);
//                            startActivity(intentNavigateHome);
//                            finish();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                Toast.makeText(getActivity(), String.valueOf(error), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();


                headers.put("vendorKey", Const.vendorKey);
                headers.put("deviceId", Sharedprefrences.getDeviceId(getActivity()));
                headers.put("Content-Type", Const.contentType);

                return headers;
            }


            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("userId", Sharedprefrences.getUserId(getActivity()));
                params.put("userKey", Sharedprefrences.getUserKey(getActivity()));
                if (!(OTPCode.isEmpty())) {
                    params.put("verificationCode", OTPCode);
                }
                return params;
            }
        };

        int socketTimeout = 20000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        sr.setRetryPolicy(policy);
        queue.add(sr);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.tv_resendsms:
                resendOTP();
                countDownTimer();
        }
    }

}
