package com.zecast.live.om_saravanabava.model;

/**
 * Created by codezilla-12 on 20/9/17.
 */

public class RegisteredUserDetailsModel {

    String userId ;
    String userName ;
    String userEmail ;
    String userMobile ;
    String userCountry ;
    String userCountryId ;
    String userCountryCode ;
    String userKey ;

    public RegisteredUserDetailsModel(String userId, String userName, String userEmail, String userMobile, String userCountry, String userCountryId, String userCountryCode, String userKey) {

        this.userId = userId;
        this.userName = userName;
        this.userEmail = userEmail;
        this.userMobile = userMobile;
        this.userCountry = userCountry;
        this.userCountryId = userCountryId;
        this.userCountryCode = userCountryCode;
        this.userKey = userKey;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserMobile() {
        return userMobile;
    }

    public void setUserMobile(String userMobile) {
        this.userMobile = userMobile;
    }

    public String getUserCountry() {
        return userCountry;
    }

    public void setUserCountry(String userCountry) {
        this.userCountry = userCountry;
    }

    public String getUserCountryId() {
        return userCountryId;
    }

    public void setUserCountryId(String userCountryId) {
        this.userCountryId = userCountryId;
    }

    public String getUserCountryCode() {
        return userCountryCode;
    }

    public void setUserCountryCode(String userCountryCode) {
        this.userCountryCode = userCountryCode;
    }

    public String getUserKey() {
        return userKey;
    }

    public void setUserKey(String userKey) {
        this.userKey = userKey;
    }
}
