package com.zecast.live.om_saravanabava.activity.audio;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.adapter.AudioClipsAdapter;
import com.zecast.live.om_saravanabava.helper.RadioStreamService;
import com.zecast.live.om_saravanabava.model.AudioDataModel;
import com.zecast.live.om_saravanabava.utils.Const;
import com.zecast.live.om_saravanabava.utils.HTTPRequest;
import com.zecast.live.om_saravanabava.utils.Sharedprefrences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AudioActivity extends AppCompatActivity {

    RecyclerView recyclerAudio;
    AudioClipsAdapter audioAdapter;
    public static final ArrayList<AudioDataModel> audioList = new ArrayList<>();
    LinearLayoutManager layoutManager;
    ProgressBar progressBar;
    LinearLayout linearLayoutContainerMain;
    LinearLayout linearDataContainer,linearLoaderContainer;

    ProgressBar pb;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio);
        setTitle("Audio");

        Window window = this.getWindow();
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

        initWidgets();
        fetchAudioData();
    }

    private void fetchAudioData() {

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest sr = new StringRequest(Request.Method.POST, HTTPRequest.SERVICE_URL+"getAudio", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.e("response_AUDIO", response);

                    try {
                        JSONObject response1 = new JSONObject(response);
                        String responseStatus = response1.getString("status");
                        String errorType = response1.optString("error_type");
                        String responseMessage = response1.optString("message");

                        VolleyLog.e("status response VIDEO "+responseStatus+" msg "+responseMessage+"error msg "+ errorType);

                        JSONObject openingObj = response1.optJSONObject("response");
                        audioList.clear();
                        JSONArray arrayAudio = openingObj.getJSONArray("audio");

                        for (int i =0;i<arrayAudio.length();i++) {


                            JSONObject objopeningaudioArray  = arrayAudio.getJSONObject(i);


                            String audioId = objopeningaudioArray.optString("audio_id");
                            String audioName = objopeningaudioArray.optString("audio_name");
                            String audioUrl = objopeningaudioArray.optString("audio_url");
                            String audioLogo = objopeningaudioArray.optString("audio_logo");

                            AudioDataModel model = new AudioDataModel(audioId,audioName,audioUrl,audioLogo);
                            audioList.add(model);


                        }

                        audioAdapter = new AudioClipsAdapter(AudioActivity.this, audioList);
                        layoutManager = new LinearLayoutManager(AudioActivity.this);
                        recyclerAudio.setLayoutManager(layoutManager);
                        recyclerAudio.setAdapter(audioAdapter);
                        recyclerAudio.setNestedScrollingEnabled(false);
                        audioAdapter.notifyDataSetChanged();

                        linearDataContainer.setVisibility(View.VISIBLE);
                        linearLoaderContainer.setVisibility(View.GONE);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("EREROR",String.valueOf(error));

                linearDataContainer.setVisibility(View.GONE);
                linearLoaderContainer.setVisibility(View.VISIBLE);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();

                headers.put("vendorKey", Const.vendorKey);
                headers.put("Authorization",  Sharedprefrences.getUserKey(getBaseContext()));
                headers.put("userID",  Sharedprefrences.getUserId(getBaseContext()));
                // headers.put("deviceId",  Const.deviceId);
                headers.put("Content-Type",Const.contentType);

                return headers;
            }

            @Override
            public Map<String, String> getParams(){
                Map<String, String> params = new HashMap<>();
               // params.put("channelId", "1");

                return params;
            }
        };

        int socketTimeout = 20000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        sr.setRetryPolicy(policy);
        queue.add(sr);


    }

    private void initWidgets() {
        recyclerAudio = (RecyclerView)findViewById(R.id.recyclerAudio);
        progressBar = (ProgressBar)findViewById(R.id.pbHeaderProgress);
        linearLayoutContainerMain = (LinearLayout) findViewById(R.id.linear_container_main);
        linearDataContainer = (LinearLayout) findViewById(R.id.linearDatacontainer);
        pb = (ProgressBar) findViewById(R.id.progressbar1);
        linearLoaderContainer = (LinearLayout) findViewById(R.id.linearpbContainer);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(new Intent(AudioActivity.this, RadioStreamService.class));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                stopService(new Intent(AudioActivity.this, RadioStreamService.class));
                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}