package com.zecast.live.om_saravanabava.activity.signup;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.helper.ValidationUtils;
import com.zecast.live.om_saravanabava.hh20.CountryCodeDialog;
import com.zecast.live.om_saravanabava.hh20.CountryCodePicker;
import com.zecast.live.om_saravanabava.model.CountryDao;
import com.zecast.live.om_saravanabava.model.RegisteredUserDetailsModel;
import com.zecast.live.om_saravanabava.utils.Const;
import com.zecast.live.om_saravanabava.utils.Sharedprefrences;
import com.zecast.live.om_saravanabava.activity.verify.VerifyOTPActivity;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;


public class SignupActivity extends Fragment implements View.OnClickListener{

    public static EditText etSpinner,etCountrycode;
    CountryCodePicker spinner_country;
    Button btnVerify;
    RadioButton radioTerms;
    public static TextView tvRadioterms,tvRadioterms2;
    public static EditText etEmail,etMobileNumber,etName;
    LinearLayout containerLinear;
    public static ArrayList<RegisteredUserDetailsModel> userDetailsArray =new ArrayList<>();
    public static ArrayList<CountryDao> mCountryListFinal = new ArrayList<>();
    public static String code,countrycode;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_signup,container,false);

        getActivity().setTitle("Sign up");


        Window window = getActivity().getWindow();
        window.setStatusBarColor(ContextCompat.getColor(getActivity(),R.color.colorPrimary));
        initWidgets(view);
        btnVerify.setOnClickListener(this);
        etSpinner.setOnClickListener(this);
        tvRadioterms.setOnClickListener(this);
        tvRadioterms2.setOnClickListener(this);

        radioTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (radioTerms.isChecked()&&etName.getText().toString().length()>0&&etMobileNumber.getText().toString().length()>0&&etEmail.getText().toString().length()>0)
                {
                    btnVerify.setEnabled(true);
                    //btnVerify.setBackgroundResource(R.drawable.buttonshapegreen);
                    radioTerms.setSelected(true);

                }

                else
                {
                    Toast.makeText(getActivity(),"Please fill all the details",Toast.LENGTH_SHORT).show();
                    //  btnVerify.setBackgroundResource(R.drawable.buttonshapegray);
                    radioTerms.setSelected(false);
                    //  rg.clearCheck();
                    // rg.clearChildFocus(rb);
                }
            }
        });


        return view;
    }



    public void jsonParser() {
        mCountryListFinal = new ArrayList<CountryDao>();
        try {
            JSONArray jArray = null;
            jArray = new JSONArray(loadJSONFromAsset());
            for (int i = 0; i < jArray.length(); i++) {
                JSONObject jObjects = jArray.getJSONObject(i);
                mCountryListFinal.add(new CountryDao(jObjects));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getActivity().getApplication().getAssets().open("country.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }

    //TODO: registering  user to server



    private void initWidgets(View view) {

        etSpinner = (EditText)view.findViewById(R.id.et_country_code_name);
        spinner_country = (CountryCodePicker)view.findViewById(R.id.spinner_country);
        btnVerify = (Button)view.findViewById(R.id.btn_verify);
        etCountrycode = (EditText)view. findViewById(R.id.et_countrycode);
        radioTerms = (RadioButton)view. findViewById(R.id.raido_terms);
        tvRadioterms = (TextView)view. findViewById(R.id.textView);
        tvRadioterms2 = (TextView)view. findViewById(R.id.textView2);
        etEmail = (EditText)view. findViewById(R.id.et_email_id);
        etMobileNumber = (EditText)view. findViewById(R.id.et_mobileNumber);
        etName = (EditText) view.findViewById(R.id.et_name);
        containerLinear = (LinearLayout) view.findViewById(R.id.container_linear);

        jsonParser();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btn_verify:

                if (TextUtils.isEmpty(etName.getText().toString())) {

                    etName.setError("Please enter name");
                    etName.requestFocus();
                    return;
                }



                if (TextUtils.isEmpty(etMobileNumber.getText().toString())) {

                    etMobileNumber.setError("Please enter mobile number");
                    etMobileNumber.requestFocus();
                    return;

                }

                if (etMobileNumber.getText().toString().length() <8 ) {
                    etMobileNumber.setError("Please enter digits between 8 - 13");
                    etMobileNumber.requestFocus();
                    return;
                }


                if (TextUtils.isEmpty(etEmail.getText().toString())) {

                    etEmail.setError("Please enter Email id");
                    etEmail.requestFocus();
                    return;

                }

                if (!ValidationUtils.isValidEmail(etEmail.getText().toString())) {
                    etEmail.setError("Please enter correct  Email id");
                    etEmail.requestFocus();
                    return;

                }


        if (!(radioTerms.isChecked())) {

            Toast.makeText(getActivity(), "please select terms and conditions !!", Toast.LENGTH_SHORT).show();
            return;
        }

            if (etSpinner.getText().toString().isEmpty()) {
                code = "228";
                countrycode = "44";
            }
            if (!etSpinner.getText().toString().isEmpty()) {
                for (int i = 0; i < mCountryListFinal.size(); i++) {
                    if (mCountryListFinal.get(i).mCountryCode.equalsIgnoreCase(Const.phoneCode)) {
                        code = mCountryListFinal.get(i).mCountryId;
                        etCountrycode.setText("+" + mCountryListFinal.get(i).mCountryCode);
                        countrycode = mCountryListFinal.get(i).mCountryCode;
                        break;
                    }
                }
            }

            Sharedprefrences.setUserEnteredMobile(getActivity(), etMobileNumber.getText().toString());

            FragmentTransaction fg5 = getActivity().getSupportFragmentManager().beginTransaction(); //to begin home page with fragment
                fg5.replace(R.id.frame_drawer, new VerifyOTPActivity());
                fg5.commit();

//            Intent intent = new Intent(getActivity(), VerifyOTPActivity.class);
//            startActivity(intent);

          break;

            case R.id.et_country_code_name:
                CountryCodeDialog.openCountryCodeDialog(spinner_country);
                break;


            case R.id.textView:
                radioTerms.setChecked(true);
                break;

            case R.id.textView2:
                radioTerms.setChecked(true);
                break;
        }
    }
}

/*private void fetchUserData() {
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest sr = new StringRequest(Request.Method.POST, HTTPRequest.SERVICE_URL+"UserRegister", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.e("response_catcup", response);


                    try {
                        JSONObject response1 = new JSONObject(response);
                        String responseStatus = response1.getString("status");
                        String errorType = response1.optString("error_type");
                        String responseMessage = response1.optString("message");

                        Snackbar snackbarresponsemsg = Snackbar.make(containerLinear, responseMessage, Snackbar.LENGTH_SHORT);
                        snackbarresponsemsg.show();

                        VolleyLog.e("status response VIDEO "+responseStatus+" msg "+responseMessage+"error msg "+ errorType);

                        JSONObject openingObj = response1.optJSONObject("response");
                        userDetailsArray.clear();

                          for (int i =0;i<openingObj.length();i++) {

                              String userId = openingObj.getString("userId");
                              String userName = openingObj.optString("userName");
                              String userEmail = openingObj.optString("userEmail");
                              String userMobile = openingObj.getString("userMobile");
                              String userCountry = openingObj.optString("userCountry");
                              String userCountryId = openingObj.optString("userCountryId");
                              String userCountryCode = openingObj.getString("userCountryCode");
                              String userKey = openingObj.optString("userKey");

                              Sharedprefrences.setUserName(getBaseContext(),userName);
                              Sharedprefrences.setUserMobile(getBaseContext(),userMobile);
                              Sharedprefrences.setUserEmail(getBaseContext(),userEmail);
                              Sharedprefrences.setUserCountry(getBaseContext(),userCountry);
                              Sharedprefrences.setUserLogedInStatus(getBaseContext(),true);
                              Sharedprefrences.setUserId(getBaseContext(),userId);
                              Sharedprefrences.setUserKey(getBaseContext(),userKey);
                              //Sharedprefrences.setUserAddress();

                              Log.d("SIGNUP CHECK SHARED PRE", "onResponse: "+Sharedprefrences.getUserName(getBaseContext())+
                                      "MOBILE " +Sharedprefrences.getUserMobile(getBaseContext())+
                                      "EMAIL " + Sharedprefrences.getUserEmail(getBaseContext())+
                                      "COUNTRY " +Sharedprefrences.getUserCountry(getBaseContext())+
                                      "LOGEDINSTATUS" + Sharedprefrences.getUserLogedInStatus(getBaseContext())+
                                      "USER ID " +Sharedprefrences.getUserId(getBaseContext()));

                              RegisteredUserDetailsModel model = new RegisteredUserDetailsModel(userId,userName,userEmail,userMobile,userCountry,userCountryId,userCountryCode,userKey);
                              userDetailsArray.add(model);
                          }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("EREROR",String.valueOf(error));

                Snackbar snackbarresponsemsg = Snackbar.make(containerLinear, String.valueOf(error), Snackbar.LENGTH_SHORT);
                snackbarresponsemsg.show();

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();


                headers.put("vendorKey", Const.vendorKey);
                headers.put("deviceId",  Const.deviceId);
                headers.put("Content-Type",Const.contentType);

                return headers;
            }


            @Override
            public Map<String, String> getParams(){
                Map<String, String> params = new HashMap<>();
                params.put("userCountry", code);
                params.put("userMobile", etMobileNumber.getText().toString());
                params.put("userName", etName.getText().toString());
                params.put("userEmail", etEmail.getText().toString());
              //  params.put("userPass", "123456");
               // Sharedprefrences.setUserPass(getBaseContext(),);
                return params;
            }
        };

        int socketTimeout = 20000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        sr.setRetryPolicy(policy);
        queue.add(sr);
    }*/