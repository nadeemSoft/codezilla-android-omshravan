package com.zecast.live.om_saravanabava.activity.verify;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.activity.welcome.WelcomeActivity;
import com.zecast.live.om_saravanabava.fragments.ProfileFragment;
import com.zecast.live.om_saravanabava.model.RegisteredUserDetailsModel;
import com.zecast.live.om_saravanabava.utils.Const;
import com.zecast.live.om_saravanabava.utils.HTTPRequest;
import com.zecast.live.om_saravanabava.utils.Sharedprefrences;
import com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity;
import com.zecast.live.om_saravanabava.activity.signup.SignupActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by codezilla-12 on 18/9/17.
 */

public class VerifyOTPActivity extends Fragment implements View.OnClickListener {

    TextView tvMobileNumberVerify;
    Button btnVerifyMob;
    TextView tvEdit;
    LinearLayout containerLinear;
    public static ArrayList<RegisteredUserDetailsModel> userDetailsArray =new ArrayList<>();
    ProgressDialog pd;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_verify_otp,container,false);

        getActivity().setTitle("Verify OTP");

        initWidgets(view);

        tvMobileNumberVerify.setText("+"+SignupActivity.countrycode+" "+Sharedprefrences.getUserEnteredMobile(getActivity()));

        Window window = getActivity().getWindow();
        window.setStatusBarColor(ContextCompat.getColor(getActivity(),R.color.colorPrimary));

        btnVerifyMob.setOnClickListener(this);
        tvEdit.setOnClickListener(this);

        return view;
    }



    private void initWidgets(View view) {

        tvMobileNumberVerify = (TextView)view.findViewById(R.id.verifyno_tvnumber);
        btnVerifyMob = (Button)view.findViewById(R.id.btn_verify_mobile);
        tvEdit = (TextView)view.findViewById(R.id.tvedit);
        containerLinear = (LinearLayout)view.findViewById(R.id.containerLinear);

    }

    private void fetchUserData() {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest sr = new StringRequest(Request.Method.POST, HTTPRequest.SERVICE_URL+"UserRegister", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.e("SIGNUP RESPONSE  ", response);
                    pd.dismiss();
                    try {

                        JSONObject response1 = new JSONObject(response);
                        String responseStatus = response1.getString("status");
                        String errorType = response1.optString("error_type");
                        String responseMessage = response1.optString("message");

                        Toast.makeText(getActivity(),responseMessage,Toast.LENGTH_SHORT).show();

                        JSONObject openingObj = response1.optJSONObject("response");
                        userDetailsArray.clear();

                        for (int i =0;i<openingObj.length();i++) {

                            String userId = openingObj.getString("userId");
                            String userName = openingObj.optString("userName");
                            String userEmail = openingObj.optString("userEmail");
                            String userMobile = openingObj.getString("userMobile");
                            String userCountry = openingObj.optString("userCountry");
                            String userCountryId = openingObj.optString("userCountryId");
                            String userCountryCode = openingObj.getString("userCountryCode");
                            String userKey = openingObj.optString("userKey");

                            Sharedprefrences.setUserLogedInStatus(getActivity(),true);
                            Sharedprefrences.setUserId(getActivity(),userId);
                            Sharedprefrences.setUserKey(getActivity(),userKey);
                            Sharedprefrences.setSignupErrorCode(getActivity(),errorType);


                            RegisteredUserDetailsModel model = new RegisteredUserDetailsModel(userId,userName,userEmail,userMobile,userCountry,userCountryId,userCountryCode,userKey);
                            userDetailsArray.add(model);

                            if (errorType.equals("201")) {

                                FragmentTransaction fg5 = getActivity().getSupportFragmentManager().beginTransaction(); //to begin home page with fragment
                                fg5.replace(R.id.frame_drawer, new VerifyMobileActivity());
                                fg5.commit();


                            }  if(errorType.equals("200"))  {
                                Sharedprefrences.setUserName(getActivity(),userName);
                                Sharedprefrences.setUserCountry(getActivity(),userCountry);
                                Sharedprefrences.setUserEmail(getActivity(),userEmail);
                                Sharedprefrences.setUserMobile(getActivity(),userMobile);

                                FragmentTransaction fg5 = getActivity().getSupportFragmentManager().beginTransaction(); //to begin home page with fragment
                                fg5.replace(R.id.frame_drawer, new ProfileFragment());
                                fg5.commit();

//                                Intent intent = new Intent(VerifyOTPActivity.this,HomeDrawerActivity.class);
//                                startActivity(intent);
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("EREROR",String.valueOf(error));

                Snackbar snackbarresponsemsg = Snackbar.make(containerLinear, String.valueOf(error), Snackbar.LENGTH_SHORT);
                snackbarresponsemsg.show();

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();

                headers.put("vendorKey", Const.vendorKey);
                headers.put("deviceId", Sharedprefrences.getDeviceId(getActivity()));
                Log.e("TOKEN CHECK SIGNUP", "getHeaders: "+Sharedprefrences.getFCMRegistrationId(getActivity()) );
                headers.put("deviceToken",Sharedprefrences.getFCMRegistrationId(getActivity()));
                headers.put("deviceType",Const.deviceType);
                headers.put("deviceMode", Const.deviceMode);
                headers.put("Content-Type",Const.contentType);
                Log.e("Token",Sharedprefrences.getFCMRegistrationId(getActivity()));
                return headers;
            }

            @Override
            public Map<String, String> getParams(){

                Map<String, String> params = new HashMap<>();
                params.put("userCountry", SignupActivity.code);
                params.put("userMobile", SignupActivity.etMobileNumber.getText().toString());
                params.put("userName", SignupActivity.etName.getText().toString());
                params.put("userEmail",SignupActivity.etEmail.getText().toString());
               // params.put("userPass", "123456");
                return params;
            }
        };

        int socketTimeout = 20000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        sr.setRetryPolicy(policy);
        queue.add(sr);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btn_verify_mobile:
                pd = new ProgressDialog(getActivity());
                pd.setMessage("Please Wait...!");
                pd.show();
                pd.setCancelable(false);
                fetchUserData();
                break;

            case R.id.tvedit:

                FragmentTransaction fg5 = getActivity().getSupportFragmentManager().beginTransaction(); //to begin home page with fragment
                fg5.replace(R.id.frame_drawer, new SignupActivity());
                fg5.commit();
                break;
        }
    }

//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        tvMobileNumberVerify.setText("");
//    }
}