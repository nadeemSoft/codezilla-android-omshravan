package com.zecast.live.om_saravanabava.activity.searchdetails;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatCallback;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.adapter.VideoDetailsSearchAdapter;
import com.zecast.live.om_saravanabava.model.Search;
import com.zecast.live.om_saravanabava.model.VideoProgramsModel;
import com.zecast.live.om_saravanabava.utils.Const;
import com.zecast.live.om_saravanabava.utils.HTTPRequest;
import com.zecast.live.om_saravanabava.utils.Sharedprefrences;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by codezilla-12 on 18/10/17.
 */


public class VideoDetailsSearchActivity  extends YouTubeBaseActivity implements View.OnClickListener,AppCompatCallback {

    public static VideoView vv;
    public static TextView tvEpisodeName, tvEpisodeDate, tvSeen, tvLikes, tvDescription;
    RecyclerView rv;
    String episodeName, episodeId;
    String httpLiveUrl;
/*    ArrayList<OtherVideos> mVideoDetails = new ArrayList<>();*/
    LinearLayoutManager layoutManager;
   /* VideoDetailsAdapter adap;*/
   VideoDetailsSearchAdapter adap;
    LinearLayout videoContainer, frameContainer;
    ImageView ivPlay, ivPause, ivFullscreenSearch, ivSpeaker;
    TextView tvtitle;
    SeekBar seekbar, progressSeekbar;
    private AudioManager mAudioManager;
    LinearLayout linearYuoutubePlayerContainer;
    public static ImageView imgPlayYoutubeVideo;
    VideoProgramsModel programs;
    ImageView ivLikeOrange, ivLikesGray,ivShare;
    TextView tvIds;
    String video_id;
    LinearLayout toolbar;
    ImageView imgBackIcon;
    TextView tvTitleToolbar;
    ArrayList<Search> mSearchList = new ArrayList<>();
    String videoId;

    //private AppCompatDelegate delegate;

    //youtube releted initialization


    private YouTubePlayer youTubePlayer;
    private YouTubePlayerFragment youTubePlayerFragment;
    private static final int RQS_ErrorDialog = 1;
    private VideoDetailsSearchActivity.MyPlayerStateChangeListener myPlayerStateChangeListener;
    private VideoDetailsSearchActivity.MyPlaybackEventListener myPlaybackEventListener;
    // public static final String VIDEO_ID = "o7VVHhK9zf0";
    //public static String VIDEO_ID = "";


    String log = "";


    //controlss for youtubee and video view

    //it will be visible by default
    public static FrameLayout videoviewFullContainer;
    public static  FrameLayout youtubePlayerContainer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_videodetails_search);

        Window window = this.getWindow();
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.colorPrimary));

        init_Components();
        httpLiveUrl = getIntent().getStringExtra("URL");
        getIntentData();
        VideoDetailsSearchActivity.this.setTitle(episodeName);
        mAudioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
        soundControl();
        setSoundControlFunction();
        getData();

        ivPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ivPause.setVisibility(View.GONE);
                ivPlay.setVisibility(View.VISIBLE);
                vv.pause();
            }
        });

        ivPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ivPause.setVisibility(View.VISIBLE);
                ivPlay.setVisibility(View.GONE);
                vv.start();
            }
        });

        progressSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (b) {
                    vv.seekTo(i);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        ivFullscreenSearch.setEnabled(false);
        ivFullscreenSearch.setOnClickListener(this);

        // imgPlayYoutubeVideo.setOnClickListener(this);
        tvLikes.setOnClickListener(this);

        if (Sharedprefrences.getEpisodeTypeStored(getApplicationContext()).equals("direct")) {
            //  VideoDetailsActivity.imgPlayYoutubeVideo.setVisibility(View.GONE);
            vv.setVisibility(View.VISIBLE);
        } else {
            // VideoDetailsActivity.imgPlayYoutubeVideo.setVisibility(View.VISIBLE);
            vv.setVisibility(View.INVISIBLE);
        }

        myPlayerStateChangeListener = new VideoDetailsSearchActivity.MyPlayerStateChangeListener();
        myPlaybackEventListener = new VideoDetailsSearchActivity.MyPlaybackEventListener();

        tvTitleToolbar.setText("Video details");
        tvTitleToolbar.setTextColor(ContextCompat.getColor(getBaseContext(),R.color.White));

    }  //end of on create

    private void getIntentData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            episodeName = extras.getString("EpisodeName");
            episodeId = extras.getString("EpisodeId");
            programs = (VideoProgramsModel) getIntent().getSerializableExtra("object");
        }
    }

    private void init_Components() {
        rv = (RecyclerView) findViewById(R.id.recycler_recent_events);
        vv = (VideoView) findViewById(R.id.videoview);
        tvEpisodeName = (TextView) findViewById(R.id.heading_event_live_1);
        tvEpisodeDate = (TextView) findViewById(R.id.date_event_live_1);
        tvLikes = (TextView) findViewById(R.id.tv_like);
        tvSeen = (TextView) findViewById(R.id.tv_views);
        tvDescription = (TextView) findViewById(R.id.description_event_live_1);
        videoContainer = (LinearLayout) findViewById(R.id.video_Container);
        frameContainer = (LinearLayout) findViewById(R.id.frame_linear_container);
        ivPlay = (ImageView) findViewById(R.id.iv_play1);
        ivPause = (ImageView) findViewById(R.id.iv_pause1);
        ivFullscreenSearch = (ImageView) findViewById(R.id.video_fullscreen_searchhh);
        seekbar = (SeekBar) findViewById(R.id.seekbar);
        ivSpeaker = (ImageView) findViewById(R.id.seekbarVolume);
        progressSeekbar = (SeekBar) findViewById(R.id.seekbarProgress);
        tvtitle = (TextView) findViewById(R.id.toolbar_title);
        linearYuoutubePlayerContainer = (LinearLayout) findViewById(R.id.linear_container_youtube_player);
        //youtube_view_video_details = (YouTubePlayer) findViewById(R.id.youtube_view_video_details);
        imgPlayYoutubeVideo = (ImageView) findViewById(R.id.ic_youtube_play);

        ivShare = (ImageView)findViewById(R.id.liveShare);
        tvIds = (TextView)findViewById(R.id.episodeId);

        ivLikeOrange = (ImageView) findViewById(R.id.img_like_live_orange);
        ivLikesGray = (ImageView) findViewById(R.id.img_like_live);


        youtubePlayerContainer = (FrameLayout)findViewById(R.id.youtube_player_container);
        videoviewFullContainer = (FrameLayout)findViewById(R.id.framecontrols);
        youTubePlayerFragment = (YouTubePlayerFragment)getFragmentManager()
                .findFragmentById(R.id.youtubeplayerfragment);

        toolbar = (LinearLayout) findViewById(R.id.toolbarSearchh);
        tvTitleToolbar = (TextView) findViewById(R.id.toolbar_title);


        imgBackIcon = (ImageView)findViewById(R.id.back_action);


        //  toolbar.setho
        ivLikeOrange.setOnClickListener(this);
        ivLikesGray.setOnClickListener(this);
        ivShare.setOnClickListener(this);
        imgBackIcon.setOnClickListener(this);
    }

    private void getData() {

        RequestQueue queue = Volley.newRequestQueue(VideoDetailsSearchActivity.this);
        StringRequest sr = new StringRequest(Request.Method.POST, HTTPRequest.SERVICE_URL + "getVideoDetails", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.e("response", response);
                    try {
                       /* mVideoDetails.clear();*/
                        mSearchList.clear();
                         JSONObject obj = new JSONObject(response);
                        String message = obj.optString("message");
                        String status = obj.optString("status");
                        JSONObject objResponse = obj.optJSONObject("response");
                        String episode_id = objResponse.optString("episode_id");
                        String episode_name = objResponse.optString("episode_name");
                        String episode_image = objResponse.optString("episode_image");
                        String episode_date = objResponse.optString("episode_date");
                        video_id = objResponse.optString("video_id");
                        String episode_type = objResponse.optString("episode_type");
                        String episode_url = objResponse.optString("episode_url");
                        String episode_seen = objResponse.optString("episode_seen");
                        String episode_desc = objResponse.optString("episode_desc");
                        String episode_like_status = objResponse.optString("episode_like_status");
                        String episode_like = objResponse.optString("episode_like");



                        tvIds.setText(episode_id);
                        tvEpisodeName.setText(episode_name);
                        tvLikes.setText(episode_like);
                        tvSeen.setText(episode_seen);
                        tvDescription.setText(episode_desc);
                        tvEpisodeDate.setText(episode_date);

                        if (episode_like_status.equals("0")) {
                            ivLikesGray.setVisibility(View.VISIBLE);
                            ivLikeOrange.setVisibility(View.GONE);
                        }

                        if (episode_like_status.equals("1")) {
                            ivLikesGray.setVisibility(View.GONE);
                            ivLikeOrange.setVisibility(View.VISIBLE);
                        }

                        Sharedprefrences.setVideoUrls(VideoDetailsSearchActivity.this, episode_url);
                        if (!(video_id.equals(""))) {
                            Sharedprefrences.setYoutubeVideoId(getBaseContext(), video_id);
                        }
                        // imgPlayYoutubeVideo.setVisibility(View.GONE);



                        if (episode_type.equals("youtube")) {
                            youtubePlayerContainer.setVisibility(View.VISIBLE);
                            videoviewFullContainer.setVisibility(View.GONE);
                            vv.setVisibility(View.INVISIBLE);
                            videoId = video_id;
                            PlayYoutubeVideos();
                            Sharedprefrences.setYoutubeVideoId(getBaseContext(),video_id);


                        } else {
                            youtubePlayerContainer.setVisibility(View.GONE);
                            videoviewFullContainer.setVisibility(View.VISIBLE);
                            vv.setVisibility(View.VISIBLE);
                            new VideoDetailsSearchActivity.BackgroundAsyncTask().execute(episode_url);
                        }




                      /*  }*/

                        JSONArray arr = objResponse.optJSONArray("otherVideos");

                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject ob = arr.getJSONObject(i);
                            String episode_ids = ob.optString("episode_id");
                            String episode_names = ob.optString("episode_name");
                            String episode_images = ob.optString("episode_image");
                            String episode_dates = ob.optString("episode_date");
                            String video_ids = ob.optString("video_id");
                            String episode_types = ob.optString("episode_type");
                            String episode_urls = ob.optString("episode_url");
                            String episode_seens = ob.optString("episode_seen");
                            String episode_descs = ob.optString("episode_desc");
                            String episode_like_statuss = ob.optString("episode_like_status");
                            String episode_likes = ob.optString("episode_like");
                           // OtherVideos vd = new OtherVideos(episode_ids, episode_names, episode_images, episode_dates, video_ids, episode_types, episode_urls, episode_seens, episode_descs, episode_like_statuss, episode_likes);
                            Search search = new Search(episode_ids, episode_names, episode_images, episode_dates, video_ids, episode_types, episode_urls, episode_seens, episode_descs, episode_like_statuss, episode_likes) ;
                            // mVideoDetails.add(vd);
                            mSearchList.add(search);
                        }

                        adap = new VideoDetailsSearchAdapter(VideoDetailsSearchActivity.this, VideoDetailsSearchActivity.this, mSearchList);
                        layoutManager = new LinearLayoutManager(VideoDetailsSearchActivity.this, LinearLayoutManager.HORIZONTAL, false);
                        rv.setLayoutManager(layoutManager);
                        rv.setAdapter(adap);
                        rv.setNestedScrollingEnabled(false);
                        adap.notifyDataSetChanged();
                    } catch (Exception e) {
                        Log.e("Exception", e + "");
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", String.valueOf(error));
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                headers.put("vendorKey", Const.vendorKey);
                headers.put("Authorization", Sharedprefrences.getUserKey(VideoDetailsSearchActivity.this));
                headers.put("userID", Sharedprefrences.getUserId(VideoDetailsSearchActivity.this));
                headers.put("deviceToken",Sharedprefrences.getFCMRegistrationId(VideoDetailsSearchActivity.this));
                headers.put("deviceType", Const.deviceType);
                headers.put("deviceId", Sharedprefrences.getDeviceId(VideoDetailsSearchActivity.this));
                headers.put("Content-Type", Const.contentType);
                return headers;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("videoId", episodeId);
                Log.e("ids",episodeId);

                return params;
            }
        };
        queue.add(sr);
    }

    public void playLiveChannel(Search ov, String position) {
        try {
           /* vv.setVisibility(View.VISIBLE);
       */

            tvIds.setText(ov.getEpisode_id());

            Sharedprefrences.setVideoUrls(VideoDetailsSearchActivity.this, ov.getEpisode_url());
            Sharedprefrences.setPositions(VideoDetailsSearchActivity.this, position);

            if (ov.getEpisode_like_status().equals("0")) {
                ivLikesGray.setVisibility(View.VISIBLE);
                ivLikeOrange.setVisibility(View.GONE);
            }
            if (ov.getEpisode_like_status().equals("1")) {
                ivLikesGray.setVisibility(View.GONE);
                ivLikeOrange.setVisibility(View.VISIBLE);
            }

            tvEpisodeName.setText(ov.getEpisode_name());
            tvLikes.setText(ov.getEpisode_like());
            tvDescription.setText(ov.getEpisode_desc());
            tvEpisodeDate.setText(ov.getEpisode_date());
            tvSeen.setText(ov.getEpisode_seen());

            //  imgPlayYoutubeVideo.setVisibility(View.GONE);


            if (ov.getEpisode_type().equals("youtube")) {
                youtubePlayerContainer.setVisibility(View.VISIBLE);
                videoviewFullContainer.setVisibility(View.GONE);
                videoId = ov.getVideo_id();
                PlayYoutubeVideos();

                // VIDEO_ID = video_id;
            } else {
                youtubePlayerContainer.setVisibility(View.GONE);
                videoviewFullContainer.setVisibility(View.VISIBLE);

                new VideoDetailsSearchActivity.BackgroundAsyncTask().execute(ov.getEpisode_url());
                Log.e("live", ov.getEpisode_url());
                progressSeekbar.setProgress(0);
            }
        } catch (Exception e) {
        }

    }



    //appcompact  callback methodss

    @Override
    public void onSupportActionModeStarted(ActionMode mode) {

    }

    @Override
    public void onSupportActionModeFinished(ActionMode mode) {

    }


    @Nullable
    @Override
    public ActionMode onWindowStartingSupportActionMode(ActionMode.Callback callback) {
        return null;
    }

    public class BackgroundAsyncTask extends AsyncTask<String, Uri, Void> {


        protected void onPreExecute() {

//            pb.setVisibility(View.VISIBLE);

        }

        protected void onProgressUpdate(final Uri... uri) {

            try {

                vv.setVideoURI(uri[0]);

                vv.requestFocus();

                vv.getLayoutParams().width = ViewGroup.LayoutParams.FILL_PARENT;

                vv.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

                    public void onPrepared(MediaPlayer mp) {
                        mp.setLooping(true);
                        vv.start();
                        vv.setVisibility(View.VISIBLE);
                        ivFullscreenSearch.setEnabled(true);
//                        ivFulllscreen.setVisibility(View.VISIBLE);
                        ivPlay.setVisibility(View.GONE);
                        ivPause.setVisibility(View.VISIBLE);
                        progressSeekbar.setMax(vv.getDuration());
                    }
                });

                vv.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                    @Override
                    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {

                        vv.setVisibility(View.VISIBLE);
                        vv.start();
                        Toast.makeText(VideoDetailsSearchActivity.this, "Cant play Video", Toast.LENGTH_SHORT).show();
//                        ivFulllscreen.setVisibility(View.GONE);
                        ivPlay.setVisibility(View.GONE);
                        ivPause.setVisibility(View.GONE);

                        Log.e("Error", "error");
                        return true;
                    }
                });

            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(String... params) {
            try {
                Uri uri = Uri.parse(params[0]);
                publishProgress(uri);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.video_fullscreen_searchhh:
                int orientation = this.getResources().getConfiguration().orientation;
                if (orientation == Configuration.ORIENTATION_PORTRAIT) {

                    //delegate.getSupportActionBar().hide();
                    this.toolbar.setVisibility(View.GONE);
                    //   toolbar.setTitle("Video details");


                    getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                    vv.getLayoutParams().width = ViewGroup.LayoutParams.FILL_PARENT;
                    frameContainer.getLayoutParams().height = ViewGroup.LayoutParams.FILL_PARENT;
                    vv.getLayoutParams().height = ViewGroup.LayoutParams.FILL_PARENT;
                    videoContainer.setVisibility(View.GONE);
                    frameContainer.getLayoutParams().height = ViewGroup.LayoutParams.FILL_PARENT;
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                } else {
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                    videoContainer.setVisibility(View.VISIBLE);
                    vv.getLayoutParams().width = ViewGroup.LayoutParams.FILL_PARENT;
                    frameContainer.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    vv.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    // delegate.getSupportActionBar().show();
                    this.toolbar.setVisibility(View.VISIBLE);
                    frameContainer.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }
                break;

           /* case R.id.ic_youtube_play:

                Intent intent = new Intent(this, YoutubeActivity.class);
                intent.putExtra("URL", httpLiveUrl);
                startActivity(intent);

                break;*/


            case R.id.tv_like:

                break;


            case R.id.img_like_live:

                getLike();

                break;

            case R.id.img_like_live_orange:

                getLike();

                break;

            case R.id.liveShare:

                List<Intent> targetShareIntents = new ArrayList<Intent>();
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                List<ResolveInfo> resInfos = getPackageManager().queryIntentActivities(shareIntent, 0);
                if (!resInfos.isEmpty()) {
                    System.out.println("Have package");
                    for (ResolveInfo resInfo : resInfos) {
                        String packageName = resInfo.activityInfo.packageName;
                        Log.i("Package Name", packageName);

                        if (!(packageName.contains("com.google.android.apps.translate") || packageName.contains("com.google.android.gms.drive") || packageName.contains("com.google.android.hangouts"))) {

                            Intent intents = new Intent();
                            intents.setComponent(new ComponentName(packageName, resInfo.activityInfo.name));
                            intents.setAction(Intent.ACTION_SEND);
                            intents.setType("text/plain");
                            intents.putExtra(Intent.EXTRA_TEXT,"watch the video- videoURL"+ Sharedprefrences.getVideoUrls(VideoDetailsSearchActivity.this));
                            intents.putExtra(Intent.EXTRA_SUBJECT, "watch the video- videoURL");
                            intents.setPackage(packageName);
                            targetShareIntents.add(intents);
                        }

                    }
                    if (!targetShareIntents.isEmpty()) {
                        System.out.println("Have Intent");
                        Intent chooserIntent = Intent.createChooser(targetShareIntents.remove(0), "Choose app to share");
                        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetShareIntents.toArray(new Parcelable[]{}));
                        startActivity(chooserIntent);
                    } else {
                        System.out.println("Do not Have Intent");
                    }
                }


                break;


            case R.id.back_action:
                this.finish();
                break;

            default:
                break;
        }
    }

    private void soundControl() {

        if (ivSpeaker != null)
            this.seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                    mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                            progress, 0);
                    ivSpeaker.setImageResource(R.drawable.speaker);
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                    if (seekBar.getProgress() == 0) {

                        ivSpeaker.setImageResource(R.drawable.mute);
                        ivSpeaker.setSoundEffectsEnabled(false);

                    }
                }
            });
    }

    private void setSoundControlFunction() {

        this.seekbar.setMax(mAudioManager
                .getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        this.seekbar.setProgress(mAudioManager
                .getStreamVolume(AudioManager.STREAM_MUSIC));
    }

    @Override
    public void onBackPressed() {

        int orientation = this.getResources().getConfiguration().orientation;

        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {

            videoContainer.setVisibility(View.VISIBLE);
            vv.getLayoutParams().width = ViewGroup.LayoutParams.FILL_PARENT;
            frameContainer.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
            vv.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
            //  delegate.getSupportActionBar().show();
           this.toolbar.setVisibility(View.VISIBLE);
            frameContainer.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            super.onBackPressed();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                this.finish();
                return true;

            case R.id.action_back:
                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void getLike() {
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest sr = new StringRequest(Request.Method.POST, HTTPRequest.SERVICE_URL + "likeVideo", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.e("like responseeee", response);
                    try {
                        JSONObject obj = new JSONObject(response);
                        String message = obj.optString("message");
                        String error_type = obj.optString("error_type");
                        String status = obj.optString("status");

                        //      Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT).show();

                        if (error_type.equals("200")) {
                            fetchLiveVideosAfterLike(/*position,likecount*/);
                        }
                    } catch (Exception e) {
                        Log.e("Exception", e + "");
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", String.valueOf(error));
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();

                headers.put("vendorKey",Const.vendorKey);
                headers.put("Authorization",Sharedprefrences.getUserKey(VideoDetailsSearchActivity.this));
                headers.put("userID",Sharedprefrences.getUserId(VideoDetailsSearchActivity.this));
                headers.put("deviceId", Sharedprefrences.getDeviceId(VideoDetailsSearchActivity.this));
                headers.put("deviceToken",Sharedprefrences.getFCMRegistrationId(VideoDetailsSearchActivity.this));
                headers.put("deviceType", Const.deviceType);
                headers.put("Content-Type", Const.contentType);
                return headers;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                Log.e("videoid",tvIds.getText().toString());
                params.put("videoId", tvIds.getText().toString());

                return params;
            }
        };
        queue.add(sr);
    }


    private void fetchLiveVideosAfterLike() {

        RequestQueue queue = Volley.newRequestQueue(VideoDetailsSearchActivity.this);

        StringRequest sr = new StringRequest(Request.Method.POST, HTTPRequest.SERVICE_URL + "getVideoDetails", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {

                    Log.e("responseAfterLikes", response);
                    try {

                        mSearchList.clear();
                        JSONObject obj = new JSONObject(response);

                        String message = obj.optString("message");
                        String status = obj.optString("status");

                        JSONObject objResponse = obj.optJSONObject("response");

                        String episode_id = objResponse.optString("episode_id");
                        String episode_name = objResponse.optString("episode_name");
                        String episode_image = objResponse.optString("episode_image");
                        String episode_date = objResponse.optString("episode_date");
                        String video_id = objResponse.optString("video_id");
                        String episode_type = objResponse.optString("episode_type");
                        String episode_url = objResponse.optString("episode_url");
                        String episode_seen = objResponse.optString("episode_seen");
                        String episode_desc = objResponse.optString("episode_desc");
                        String episode_like_status = objResponse.optString("episode_like_status");
                        String episode_like = objResponse.optString("episode_like");


                        JSONArray arr = objResponse.optJSONArray("otherVideos");

                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject ob = arr.getJSONObject(i);

                            String episode_ids = ob.optString("episode_id");
                            String episode_names = ob.optString("episode_name");
                            String episode_images = ob.optString("episode_image");
                            String episode_dates = ob.optString("episode_date");
                            String video_ids = ob.optString("video_id");
                            String episode_types = ob.optString("episode_type");
                            String episode_urls = ob.optString("episode_url");
                            String episode_seens = ob.optString("episode_seen");
                            String episode_descs = ob.optString("episode_desc");
                            String episode_like_statuss = ob.optString("episode_like_status");
                            String episode_likes = ob.optString("episode_like");

                            /*OtherVideos vd = new OtherVideos(episode_ids, episode_names, episode_images, episode_dates, video_ids, episode_types, episode_urls, episode_seens, episode_descs, episode_like_statuss, episode_likes);
                            mVideoDetails.add(vd);*/


                           // Search sr = new Search(episodeId,episodeName,episodeImage,episodeNo,videoId,episodeType,episodeUrl);

                            Search sr = new Search(episode_ids, episode_names, episode_images, episode_dates, video_ids, episode_types, episode_urls, episode_seens, episode_descs, episode_like_statuss, episode_likes) ;

                            mSearchList.add(sr);
                        }

                        try
                        {
                            changeLikes(mSearchList.get(Integer.parseInt(Sharedprefrences.getPostions(VideoDetailsSearchActivity.this))));

                        } catch (Exception e) {
                        }

                    } catch (Exception e) {
                        Log.e("Exception", e + "");
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("error", String.valueOf(error));
            }
        }) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                headers.put("vendorKey", Const.vendorKey);
                headers.put("Authorization", Sharedprefrences.getUserKey(VideoDetailsSearchActivity.this));
                headers.put("userID", Sharedprefrences.getUserId(VideoDetailsSearchActivity.this));
                headers.put("deviceToken",Sharedprefrences.getFCMRegistrationId(VideoDetailsSearchActivity.this));
                headers.put("deviceType", Const.deviceType);
                headers.put("deviceId", Sharedprefrences.getDeviceId(VideoDetailsSearchActivity.this));
                headers.put("Content-Type", Const.contentType);
                return headers;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("videoId", episodeId);


                return params;
            }
        };
        queue.add(sr);
    }

    private void changeLikes(Search otherVideos) {

        Log.e("Likes",otherVideos.getEpisode_like_status());

        if (otherVideos.getEpisode_like_status().equals("0")) {

            ivLikesGray.setVisibility(View.VISIBLE);
            ivLikeOrange.setVisibility(View.GONE);
        }
        if (otherVideos.getEpisode_like_status().equals("1")) {

            ivLikesGray.setVisibility(View.GONE);
            ivLikeOrange.setVisibility(View.VISIBLE);
        }

        tvDescription.setText(otherVideos.getEpisode_desc());
        tvLikes.setText(otherVideos.getEpisode_like());
        tvSeen.setText(otherVideos.getEpisode_seen());
        tvIds.setText(otherVideos.getEpisode_id());
        adap.notifyDataSetChanged();
        Log.e("asas",otherVideos.getEpisode_like());

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    private final class MyPlayerStateChangeListener implements YouTubePlayer.PlayerStateChangeListener {

        /*private void updateLog(String prompt){
            log +=  "MyPlayerStateChangeListener" + "\n" +
                    prompt + "\n\n=====";
            textVideoLog.setText(log);
        };*/

        @Override
        public void onAdStarted() {
         /*   updateLog("onAdStarted()");*/
        }

        @Override
        public void onError(
                com.google.android.youtube.player.YouTubePlayer.ErrorReason arg0) {
            //  updateLog("onError(): " + arg0.toString());
        }

        @Override
        public void onLoaded(String arg0) {
            // updateLog("onLoaded(): " + arg0);
        }

        @Override
        public void onLoading() {
            //updateLog("onLoading()");
        }

        @Override
        public void onVideoEnded() {
            //updateLog("onVideoEnded()");
        }

        @Override
        public void onVideoStarted() {
            //  updateLog("onVideoStarted()");
        }

    }

    private final class MyPlaybackEventListener implements YouTubePlayer.PlaybackEventListener {

      /*  private void updateLog(String prompt) {
            log +=  "MyPlaybackEventListener" + "\n-" +
                    prompt + "\n\n=====";
          //  textVideoLog.setText(log);
        };*/

        @Override
        public void onBuffering(boolean arg0) {
            // updateLog("onBuffering(): " + String.valueOf(arg0));
        }

        @Override
        public void onPaused() {
            //updateLog("onPaused()");
        }

        @Override
        public void onPlaying() {

            //updateLog("onPlaying()");
        }

        @Override
        public void onSeekTo(int arg0) {
            // updateLog("onSeekTo(): " + String.valueOf(arg0));
        }

        @Override
        public void onStopped() {
            // updateLog("onStopped()");
        }
    }






    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menubardetails, menu);
        return true;
    }

    public void PlayYoutubeVideos()

    {
        youTubePlayerFragment = (YouTubePlayerFragment)getFragmentManager()
                .findFragmentById(R.id.youtubeplayerfragment);

        youTubePlayerFragment.onDestroy();

        youTubePlayerFragment.initialize(Const.YOUTUBE_VIDEO_CODE,
                new YouTubePlayer.OnInitializedListener() {
                    @Override
                    public void onInitializationSuccess(YouTubePlayer.Provider provider,
                                                        YouTubePlayer youTubePlayer, boolean b) {
                        // do any work here to cue video, play video, etc.

                        youTubePlayer.loadVideo(videoId);
                        Log.e("start","1");

                        Log.e("start",videoId);

                    }
                    @Override
                    public void onInitializationFailure(YouTubePlayer.Provider provider,
                                                        YouTubeInitializationResult youTubeInitializationResult) {
                    }
                });
    }



}

/*
public class VideoDetailsSearchActivity extends AppCompatActivity implements View.OnClickListener {
     public static  VideoView vv;
     public static TextView tvEpisodeName, tvEpisodeDate, tvSeen, tvLikes, tvDescription;
    RecyclerView rv;
    String episodeName, episodeId;
    String httpLiveUrl;
    ArrayList<OtherVideos> mVideoDetails = new ArrayList<>();
    LinearLayoutManager layoutManager;
    VideoDetailsSearchAdapter adap;
    LinearLayout videoContainer, frameContainer;
    ImageView ivPlay, ivPause, ivFullScreen, ivSpeaker;
    TextView tvtitle;
    SeekBar seekbar, progressSeekbar;
    private AudioManager mAudioManager;
    LinearLayout linearYuoutubePlayerContainer;
    public static ImageView imgPlayYoutubeVideo;
    //VideoProgramsModel programs;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_videodetails_search);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        init_Components();
        httpLiveUrl = getIntent().getStringExtra("URL");
        getIntentData();
        VideoDetailsSearchActivity.this.setTitle(episodeName);
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        soundControl();
        setSoundControlFunction();
        getData();

        ivPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ivPause.setVisibility(View.GONE);
                ivPlay.setVisibility(View.VISIBLE);
                vv.pause();
            }
        });

        ivPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ivPause.setVisibility(View.VISIBLE);
                ivPlay.setVisibility(View.GONE);
                vv.start();
            }
        });

        progressSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (b) {

                    vv.seekTo(i);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

        });
        ivFullScreen.setOnClickListener(this);
        imgPlayYoutubeVideo.setOnClickListener(this);
        tvLikes.setOnClickListener(this);

        if (Sharedprefrences.getEpisodeTypeStored(getApplicationContext()).equals("direct")) {
            VideoDetailsSearchActivity.imgPlayYoutubeVideo.setVisibility(View.GONE);
            vv.setVisibility(View.VISIBLE);
        } else {
            VideoDetailsSearchActivity.imgPlayYoutubeVideo.setVisibility(View.VISIBLE);
            vv.setVisibility(View.INVISIBLE);
        }
    }

    private void getIntentData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            episodeName = extras.getString("EpisodeName");
            episodeId = extras.getString("EpisodeId");
           // programs = (VideoProgramsModel)getIntent().getSerializableExtra("object");

        }
    }

    private void init_Components() {
        rv = (RecyclerView) findViewById(R.id.recycler_recent_events);
        vv = (VideoView) findViewById(R.id.videoview);
        tvEpisodeName = (TextView) findViewById(R.id.heading_event_live_1);
        tvEpisodeDate = (TextView) findViewById(R.id.date_event_live_1);
        tvLikes = (TextView) findViewById(R.id.tv_like);
        tvSeen = (TextView) findViewById(R.id.tv_views);
        tvDescription = (TextView) findViewById(R.id.description_event_live_1);
        videoContainer = (LinearLayout) findViewById(R.id.video_Container);
        frameContainer = (LinearLayout) findViewById(R.id.frame_linear_container);
        ivPlay = (ImageView) findViewById(R.id.iv_play1);
        ivPause = (ImageView) findViewById(R.id.iv_pause1);
        ivFullScreen = (ImageView) findViewById(R.id.video_fullscreen);
        seekbar = (SeekBar) findViewById(R.id.seekbar);
        ivSpeaker = (ImageView) findViewById(R.id.seekbarVolume);
        progressSeekbar = (SeekBar) findViewById(R.id.seekbarProgress);
        tvtitle = (TextView) findViewById(R.id.toolbar_title);
        linearYuoutubePlayerContainer = (LinearLayout) findViewById(R.id.linear_container_youtube_player);
        //youtube_view_video_details = (YouTubePlayer) findViewById(R.id.youtube_view_video_details);
        imgPlayYoutubeVideo = (ImageView) findViewById(R.id.ic_youtube_play);
    }

    private void getData() {

        RequestQueue queue = Volley.newRequestQueue(VideoDetailsSearchActivity.this);
        StringRequest sr = new StringRequest(Request.Method.POST, HTTPRequest.SERVICE_URL + "getVideoDetails", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.e("response", response);
                    try {
                        mVideoDetails.clear();
                        JSONObject obj = new JSONObject(response);
                        String message = obj.optString("message");
                        String status = obj.optString("status");
                        JSONObject objResponse = obj.optJSONObject("response");
                        String episode_id = objResponse.optString("episode_id");
                        String episode_name = objResponse.optString("episode_name");
                        String episode_image = objResponse.optString("episode_image");
                        String episode_date = objResponse.optString("episode_date");
                        String video_id = objResponse.optString("video_id");
                        String episode_type = objResponse.optString("episode_type");
                        String episode_url = objResponse.optString("episode_url");
                        String episode_seen = objResponse.optString("episode_seen");
                        String episode_desc = objResponse.optString("episode_desc");
                        String episode_like_status = objResponse.optString("episode_like_status");
                        String episode_like = objResponse.optString("episode_like");

                            imgPlayYoutubeVideo.setVisibility(View.GONE);
                            new BackgroundAsyncTask().execute(episode_url);

                        JSONArray arr = objResponse.optJSONArray("otherVideos");

                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject ob = arr.getJSONObject(i);
                            String episode_ids = ob.optString("episode_id");
                            String episode_names = ob.optString("episode_name");
                            String episode_images = ob.optString("episode_image");
                            String episode_dates = ob.optString("episode_date");
                            String video_ids = ob.optString("video_id");
                            String episode_types = ob.optString("episode_type");
                            String episode_urls = ob.optString("episode_url");
                            String episode_seens = ob.optString("episode_seen");
                            String episode_descs = ob.optString("episode_desc");
                            String episode_like_statuss = ob.optString("episode_like_status");
                            String episode_likes = ob.optString("episode_like");

                            OtherVideos vd = new OtherVideos(episode_ids, episode_names, episode_images, episode_dates, video_ids, episode_types, episode_urls, episode_seens, episode_descs, episode_like_statuss, episode_likes);
                            mVideoDetails.add(vd);
                        }

                        tvEpisodeName.setText(episode_name);
                        tvLikes.setText(episode_like);
                        tvSeen.setText(episode_seen);
                        tvDescription.setText(episode_desc);
                        tvEpisodeDate.setText(episode_date);

                        adap = new VideoDetailsSearchAdapter(VideoDetailsSearchActivity.this, VideoDetailsSearchActivity.this,mVideoDetails);
                        layoutManager = new LinearLayoutManager(VideoDetailsSearchActivity.this, LinearLayoutManager.HORIZONTAL, false);
                        rv.setLayoutManager(layoutManager);
                        rv.setAdapter(adap);
                        rv.setNestedScrollingEnabled(false);
                        adap.notifyDataSetChanged();
                    } catch (Exception e) {
                        Log.e("Exception", e + "");
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", String.valueOf(error));
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                headers.put("vendorKey", Const.vendorKey);
                headers.put("Authorization", Sharedprefrences.getUserKey(getBaseContext()));
                headers.put("userID", Sharedprefrences.getUserId(VideoDetailsSearchActivity.this));
                headers.put("deviceId", Sharedprefrences.getDeviceId(VideoDetailsSearchActivity.this));
                return headers;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("videoId", episodeId);
                return params;
            }
        };
        queue.add(sr);
    }

    public void playLiveChannel(OtherVideos ov) {
        try {
           */
/* vv.setVisibility(View.VISIBLE);

       *//*
  imgPlayYoutubeVideo.setVisibility(View.GONE);
             new BackgroundAsyncTask().execute(ov.getEpisode_url());
            Log.e("live", ov.getEpisode_url());
            progressSeekbar.setProgress(0);
        } catch (Exception e) {
        }
    }

    public class BackgroundAsyncTask extends AsyncTask<String, Uri, Void> {


        protected void onPreExecute() {

//            pb.setVisibility(View.VISIBLE);

        }

        protected void onProgressUpdate(final Uri... uri) {

            try {

                vv.setVideoURI(uri[0]);

                vv.requestFocus();

                vv.getLayoutParams().width = ViewGroup.LayoutParams.FILL_PARENT;

                vv.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

                    public void onPrepared(MediaPlayer mp) {
                        mp.setLooping(true);
                        vv.start();
                        vv.setVisibility(View.VISIBLE);
//                        ivFulllscreen.setVisibility(View.VISIBLE);
                        ivPlay.setVisibility(View.GONE);
                        ivPause.setVisibility(View.VISIBLE);
                        progressSeekbar.setMax(vv.getDuration());
                    }
                });

                vv.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                    @Override
                    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {

                        vv.setVisibility(View.VISIBLE);
                        vv.start();
                        Toast.makeText(VideoDetailsSearchActivity.this, "Cant play Video", Toast.LENGTH_SHORT).show();
//                        ivFulllscreen.setVisibility(View.GONE);
                        ivPlay.setVisibility(View.GONE);
                        ivPause.setVisibility(View.GONE);

                        Log.e("Error", "error");
                        return true;
                    }
                });

            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(String... params) {
            try {
                Uri uri = Uri.parse(params[0]);
                publishProgress(uri);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.video_fullscreen:
                int orientation = this.getResources().getConfiguration().orientation;
                if (orientation == Configuration.ORIENTATION_PORTRAIT) {

                    getSupportActionBar().hide();
                    getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                    vv.getLayoutParams().width = ViewGroup.LayoutParams.FILL_PARENT;
                    frameContainer.getLayoutParams().height = ViewGroup.LayoutParams.FILL_PARENT;
                    vv.getLayoutParams().height = ViewGroup.LayoutParams.FILL_PARENT;
                    videoContainer.setVisibility(View.GONE);
                    frameContainer.getLayoutParams().height = ViewGroup.LayoutParams.FILL_PARENT;
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                } else {
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                    videoContainer.setVisibility(View.VISIBLE);
                    vv.getLayoutParams().width = ViewGroup.LayoutParams.FILL_PARENT;
                    frameContainer.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    vv.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    getSupportActionBar().show();
                    frameContainer.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }
                break;

            case R.id.ic_youtube_play:

                Intent intent = new Intent(this, YoutubeActivity.class);
                intent.putExtra("URL", httpLiveUrl);
                startActivity(intent);

                break;


            case R.id.tv_like:
                getLike(episodeId);

                break;

            default:
                break;
        }
    }

    private void soundControl() {

        if (ivSpeaker != null)
            this.seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                    mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                            progress, 0);
                    ivSpeaker.setImageResource(R.drawable.speaker);
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                    if (seekBar.getProgress() == 0) {

                        ivSpeaker.setImageResource(R.drawable.mute);
                        ivSpeaker.setSoundEffectsEnabled(false);

                    }
                }
            });
    }

    private void setSoundControlFunction() {

        this.seekbar.setMax(mAudioManager
                .getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        this.seekbar.setProgress(mAudioManager
                .getStreamVolume(AudioManager.STREAM_MUSIC));
    }

    @Override
    public void onBackPressed() {

        int orientation = this.getResources().getConfiguration().orientation;

        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {

            videoContainer.setVisibility(View.VISIBLE);
            vv.getLayoutParams().width = ViewGroup.LayoutParams.FILL_PARENT;
            frameContainer.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
            vv.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
            getSupportActionBar().show();
            frameContainer.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            super.onBackPressed();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void getLike(*/
/*final int position,*//*
 final String episodeid*/
/*, final String likecount*//*
) {
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest sr = new StringRequest(Request.Method.POST, HTTPRequest.SERVICE_URL + "likeVideo", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.e("like responseeee", response);
                    try {
                        JSONObject obj = new JSONObject(response);
                        String message = obj.optString("message");
                        String error_type = obj.optString("error_type");
                        String status = obj.optString("status");

                      //  Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT).show();

                        if (error_type.equals("200")) {
                            fetchLiveVideosAfterLike(*/
/*position,likecount*//*
);
                        }
                    } catch (Exception e) {
                        Log.e("Exception", e + "");
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", String.valueOf(error));
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();

                headers.put("vendorKey", Const.vendorKey);
                headers.put("Authorization", Sharedprefrences.getUserKey(getBaseContext()));
                headers.put("userID", Sharedprefrences.getUserId(getBaseContext()));
                headers.put("deviceId", Sharedprefrences.getDeviceId(getBaseContext()));
                headers.put("deviceType", Const.deviceType);
                headers.put("Content-Type", Const.contentType);
                return headers;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                Log.e("EPISODE IDD", "getParams: " + Sharedprefrences.getepisodeId(getBaseContext()));
                params.put("videoId",  episodeId);
                return params;
            }
        };
        queue.add(sr);
    }


    private void fetchLiveVideosAfterLike(*/
/*final int position, final String likecount*//*
) {

        RequestQueue queue = Volley.newRequestQueue(VideoDetailsSearchActivity.this);

        StringRequest sr = new StringRequest(Request.Method.POST, HTTPRequest.SERVICE_URL + "getVideoDetails", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {

                    Log.e("response", response);
                    try {

                        mVideoDetails.clear();
                        JSONObject obj = new JSONObject(response);

                        String message = obj.optString("message");
                        String status = obj.optString("status");

                        JSONObject objResponse = obj.optJSONObject("response");

                        String episode_id = objResponse.optString("episode_id");
                        String episode_name = objResponse.optString("episode_name");
                        String episode_image = objResponse.optString("episode_image");
                        String episode_date = objResponse.optString("episode_date");
                        String video_id = objResponse.optString("video_id");
                        String episode_type = objResponse.optString("episode_type");
                        String episode_url = objResponse.optString("episode_url");
                        String episode_seen = objResponse.optString("episode_seen");
                        String episode_desc = objResponse.optString("episode_desc");
                        String episode_like_status = objResponse.optString("episode_like_status");
                        String episode_like = objResponse.optString("episode_like");



                     */
/*   if (!(episode_type.equals("youtube"))) {
                            vv.setVisibility(View.VISIBLE);
                            imgPlayYoutubeVideo.setVisibility(View.GONE);
                  *//*
          new BackgroundAsyncTask().execute(episode_url);
                     */
/*   }*//*


                        JSONArray arr = objResponse.optJSONArray("otherVideos");

                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject ob = arr.getJSONObject(i);

                            String episode_ids = ob.optString("episode_id");
                            String episode_names = ob.optString("episode_name");
                            String episode_images = ob.optString("episode_image");
                            String episode_dates = ob.optString("episode_date");
                            String video_ids = ob.optString("video_id");
                            String episode_types = ob.optString("episode_type");
                            String episode_urls = ob.optString("episode_url");
                            String episode_seens = ob.optString("episode_seen");
                            String episode_descs = ob.optString("episode_desc");
                            String episode_like_statuss = ob.optString("episode_like_status");
                            String episode_likes = ob.optString("episode_like");
                            OtherVideos vd = new OtherVideos(episode_ids, episode_names, episode_images, episode_dates, video_ids, episode_types, episode_urls, episode_seens, episode_descs, episode_like_statuss, episode_likes);

                            mVideoDetails.add(vd);
                        }

                        tvEpisodeName.setText(episode_name);
                        tvLikes.setText(episode_like);
                        tvSeen.setText(episode_seen);
                        tvDescription.setText(episode_desc);
                        tvEpisodeDate.setText(episode_date);

                       // adap = new VideoDetailsAdapter(VideoDetailsSearchActivity.this,VideoDetailsSearchActivity.this, mVideoDetails);

                        layoutManager = new LinearLayoutManager(VideoDetailsSearchActivity.this, LinearLayoutManager.HORIZONTAL, false);
                        rv.setLayoutManager(layoutManager);
                        rv.setAdapter(adap);
                        rv.setNestedScrollingEnabled(false);
                        adap.notifyDataSetChanged();


                    } catch (Exception e) {
                        Log.e("Exception", e + "");
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("error", String.valueOf(error));
            }
        }) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                headers.put("vendorKey", Const.vendorKey);
                headers.put("Authorization", Sharedprefrences.getUserKey(getBaseContext()));
                headers.put("userID", Sharedprefrences.getUserId(VideoDetailsSearchActivity.this));
                headers.put("deviceId", Sharedprefrences.getDeviceId(VideoDetailsSearchActivity.this));

                return headers;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("videoId", episodeId);

                return params;
            }
        };
        queue.add(sr);
    }
}*/
