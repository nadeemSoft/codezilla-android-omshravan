package com.zecast.live.om_saravanabava.model;

import java.io.Serializable;

/**
 * Created by codezilla-12 on 25/9/17.
 */

public class CatchupModel implements Serializable{

        String eventId ;
        String eventName ;
        String eventStartDate;
        String eventEndDate ;
        String eventDescription;
        String eventLocation ;
        String eventDuration ;
        String eventCode ;
        String eventImageURL ;
        String eventURL ;
        String eventDonateStatus;
        String eventGiftTypeId ;
        String eventGiftRemarks ;
        String eventTypeStatusId ;
        String eventTypeStatus ;
        String eventLikeStatus;
        String eventLikes ;
        String eventSeens;
        String eventHasMultipleParts;

    public CatchupModel() {
    }

    public CatchupModel(String eventId, String eventName, String eventStartDate, String eventEndDate, String eventDescription, String eventLocation, String eventDuration, String eventCode, String eventImageURL, String eventURL, String eventDonateStatus, String eventGiftTypeId, String eventGiftRemarks, String eventTypeStatusId, String eventTypeStatus, String eventLikeStatus, String eventLikes, String eventSeens, String eventHasMultipleParts) {
        this.eventId = eventId;
        this.eventName = eventName;
        this.eventStartDate = eventStartDate;
        this.eventEndDate = eventEndDate;
        this.eventDescription = eventDescription;
        this.eventLocation = eventLocation;
        this.eventDuration = eventDuration;
        this.eventCode = eventCode;
        this.eventImageURL = eventImageURL;
        this.eventURL = eventURL;
        this.eventDonateStatus = eventDonateStatus;
        this.eventGiftTypeId = eventGiftTypeId;
        this.eventGiftRemarks = eventGiftRemarks;
        this.eventTypeStatusId = eventTypeStatusId;
        this.eventTypeStatus = eventTypeStatus;
        this.eventLikeStatus = eventLikeStatus;
        this.eventLikes = eventLikes;
        this.eventSeens = eventSeens;
        this.eventHasMultipleParts = eventHasMultipleParts;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventStartDate() {
        return eventStartDate;
    }

    public void setEventStartDate(String eventStartDate) {
        this.eventStartDate = eventStartDate;
    }

    public String getEventEndDate() {
        return eventEndDate;
    }

    public void setEventEndDate(String eventEndDate) {
        this.eventEndDate = eventEndDate;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public String getEventLocation() {
        return eventLocation;
    }

    public void setEventLocation(String eventLocation) {
        this.eventLocation = eventLocation;
    }

    public String getEventDuration() {
        return eventDuration;
    }

    public void setEventDuration(String eventDuration) {
        this.eventDuration = eventDuration;
    }

    public String getEventCode() {
        return eventCode;
    }

    public void setEventCode(String eventCode) {
        this.eventCode = eventCode;
    }

    public String getEventImageURL() {
        return eventImageURL;
    }

    public void setEventImageURL(String eventImageURL) {
        this.eventImageURL = eventImageURL;
    }

    public String getEventURL() {
        return eventURL;
    }

    public void setEventURL(String eventURL) {
        this.eventURL = eventURL;
    }

    public String getEventDonateStatus() {
        return eventDonateStatus;
    }

    public void setEventDonateStatus(String eventDonateStatus) {
        this.eventDonateStatus = eventDonateStatus;
    }

    public String getEventGiftTypeId() {
        return eventGiftTypeId;
    }

    public void setEventGiftTypeId(String eventGiftTypeId) {
        this.eventGiftTypeId = eventGiftTypeId;
    }

    public String getEventGiftRemarks() {
        return eventGiftRemarks;
    }

    public void setEventGiftRemarks(String eventGiftRemarks) {
        this.eventGiftRemarks = eventGiftRemarks;
    }

    public String getEventTypeStatusId() {
        return eventTypeStatusId;
    }

    public void setEventTypeStatusId(String eventTypeStatusId) {
        this.eventTypeStatusId = eventTypeStatusId;
    }

    public String getEventTypeStatus() {
        return eventTypeStatus;
    }

    public void setEventTypeStatus(String eventTypeStatus) {
        this.eventTypeStatus = eventTypeStatus;
    }

    public String getEventLikeStatus() {
        return eventLikeStatus;
    }

    public void setEventLikeStatus(String eventLikeStatus) {
        this.eventLikeStatus = eventLikeStatus;
    }

    public String getEventLikes() {
        return eventLikes;
    }

    public void setEventLikes(String eventLikes) {
        this.eventLikes = eventLikes;
    }

    public String getEventSeens() {
        return eventSeens;
    }

    public void setEventSeens(String eventSeens) {
        this.eventSeens = eventSeens;
    }

    public String getEventHasMultipleParts() {
        return eventHasMultipleParts;
    }

    public void setEventHasMultipleParts(String eventHasMultipleParts) {
        this.eventHasMultipleParts = eventHasMultipleParts;
    }

    @Override
    public String toString() {
        return "CatchupModel{" +
                "eventId='" + eventId + '\'' +
                ", eventName='" + eventName + '\'' +
                ", eventStartDate='" + eventStartDate + '\'' +
                ", eventEndDate='" + eventEndDate + '\'' +
                ", eventDescription='" + eventDescription + '\'' +
                ", eventLocation='" + eventLocation + '\'' +
                ", eventDuration='" + eventDuration + '\'' +
                ", eventCode='" + eventCode + '\'' +
                ", eventImageURL='" + eventImageURL + '\'' +
                ", eventURL='" + eventURL + '\'' +
                ", eventDonateStatus='" + eventDonateStatus + '\'' +
                ", eventGiftTypeId='" + eventGiftTypeId + '\'' +
                ", eventGiftRemarks='" + eventGiftRemarks + '\'' +
                ", eventTypeStatusId='" + eventTypeStatusId + '\'' +
                ", eventTypeStatus='" + eventTypeStatus + '\'' +
                ", eventLikeStatus='" + eventLikeStatus + '\'' +
                ", eventLikes='" + eventLikes + '\'' +
                ", eventSeens='" + eventSeens + '\'' +
                ", eventHasMultipleParts='" + eventHasMultipleParts + '\'' +
                '}';
    }
}
