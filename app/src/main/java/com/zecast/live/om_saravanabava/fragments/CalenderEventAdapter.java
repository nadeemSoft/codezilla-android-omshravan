package com.zecast.live.om_saravanabava.fragments;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.callback.RecyclerClickListener;
import com.zecast.live.om_saravanabava.helper.DateUtil;
import com.zecast.live.om_saravanabava.helper.KeyConstant;
import com.squareup.picasso.Picasso;

import java.util.Calendar;


public class CalenderEventAdapter extends RecyclerView.Adapter<CalenderEventViewHolder> {
    private JSONArray jsonArray;
    private Context context;
    private Activity activity;
    private RecyclerClickListener recyclerClickListener;

    Calendar calendar = Calendar.getInstance();
    int day = calendar.get(Calendar.DATE);
    int month = calendar.get(Calendar.MONTH) + 1;
    int year = calendar.get(Calendar.YEAR);
    int eventId = 0;

    public CalenderEventAdapter(Context context, FragmentActivity activity, JSONArray jsonArray, RecyclerClickListener recyclerClickListener) {
        this.activity = activity;
        this.jsonArray = jsonArray;
        this.context = context;
        this.recyclerClickListener = recyclerClickListener;
    }

    @Override
    public CalenderEventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CalenderEventViewHolder(LayoutInflater.from(parent.getContext()), parent);
    }

    @Override
    public void onBindViewHolder(final CalenderEventViewHolder holder, int position) {
        final JSONObject jsonObject = getItem(position);

        holder.heading.setText(jsonObject.optString(KeyConstant.KEY_EVENT_TITLE));
        holder.subHeading.setText(jsonObject.optString(KeyConstant.KEY_EVENT_DESCRIPTION));
        holder.timingText.setText("Time :"+DateUtil.getTime(jsonObject.optString(KeyConstant.KEY_EVENT_START_DATE)));
        String[] dateStr = jsonObject.optString(KeyConstant.KEY_EVENT_START_DATE).split(" ");
        String selectedDate = dateStr[0];
        String currentDate = String.valueOf(day + "-" + month + "-" + year);
        boolean dateIsGreater = DateUtil.isEndDateGreater(currentDate, selectedDate);
        eventId = jsonObject.optInt(KeyConstant.KEY_EVENT_ID);

        try {
            String liveStatus = jsonObject.getString("eventTypeStatus");
            if (liveStatus.equals("Live")) {
        holder.liveTag.setVisibility(View.VISIBLE);
            } else {
                holder.liveTag.setVisibility(View.GONE);
            }
        }catch (Exception e){}

        try {
            String imgLogo = jsonObject.getString(KeyConstant.KEY_POSTER_EVENT);
            Log.e("IMMGGG FETCHEDDD", "onBindViewHolder: "+ imgLogo);
            Picasso.with(context)
                    .load(imgLogo)
                    .error(R.drawable.app_icon)
                    .placeholder(R.drawable.app_icon)
                    .fit()
                    .into(holder.logoImg);

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("exception condition", "onBindViewHolder: "+e.getMessage());
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    public JSONObject getItem(int position) {
        return jsonArray.optJSONObject(position);
    }

}



