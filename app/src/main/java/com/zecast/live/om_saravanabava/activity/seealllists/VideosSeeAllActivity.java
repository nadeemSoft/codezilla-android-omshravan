package com.zecast.live.om_saravanabava.activity.seealllists;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.adapter.VideoProgramsAdapter;
import com.zecast.live.om_saravanabava.fragments.VideoFragment;
import com.zecast.live.om_saravanabava.utils.Sharedprefrences;
import static com.zecast.live.om_saravanabava.fragments.VideoFragment.modelList;

public class VideosSeeAllActivity extends AppCompatActivity {

    RecyclerView recyclerViewSeeAllCatachup;
    GridLayoutManager layoutManager;
    VideoProgramsAdapter adapter;
    //VideoCategoriesAdapter adapterMain;
    //LinearLayoutManager linearLayoutManagaer;
    VideoFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_see_all);

        String titlePage = getIntent().getStringExtra("title");
        setTitle(titlePage);
        initWidgets();
        setRecyclerAdapter();
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
    }

    private void setRecyclerAdapter() {
        String position =Sharedprefrences.getAdapterPosition(getBaseContext());
        adapter = new VideoProgramsAdapter(fragment,this, modelList.get(Integer.parseInt(position)).getSubcategorylist());
        layoutManager = new GridLayoutManager(getBaseContext(), 2);
        recyclerViewSeeAllCatachup.setLayoutManager(layoutManager);
        recyclerViewSeeAllCatachup.setAdapter(adapter);
        recyclerViewSeeAllCatachup.setNestedScrollingEnabled(false);
        adapter.notifyDataSetChanged();
    }

    private void initWidgets() {
        recyclerViewSeeAllCatachup = (RecyclerView)findViewById(R.id.rv_video_programs_see_all);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
