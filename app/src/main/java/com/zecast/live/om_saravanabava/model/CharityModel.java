package com.zecast.live.om_saravanabava.model;

/**
 * Created by codezilla-12 on 22/9/17.
 */

public class CharityModel {


    String charityId;
    String charityName;
    String charityDesc;
    String charityLink;
    String charityLogo;

    public CharityModel(String charityId, String charityName, String charityDesc, String charityLink, String charityLogo) {
        this.charityId = charityId;
        this.charityName = charityName;
        this.charityDesc = charityDesc;
        this.charityLink = charityLink;
        this.charityLogo = charityLogo;
    }

    public String getCharityId() {
        return charityId;
    }

    public void setCharityId(String charityId) {
        this.charityId = charityId;
    }

    public String getCharityName() {
        return charityName;
    }

    public void setCharityName(String charityName) {
        this.charityName = charityName;
    }

    public String getCharityDesc() {
        return charityDesc;
    }

    public void setCharityDesc(String charityDesc) {
        this.charityDesc = charityDesc;
    }

    public String getCharityLink() {
        return charityLink;
    }

    public void setCharityLink(String charityLink) {
        this.charityLink = charityLink;
    }

    public String getCharityLogo() {
        return charityLogo;
    }

    public void setCharityLogo(String charityLogo) {
        this.charityLogo = charityLogo;
    }
}
