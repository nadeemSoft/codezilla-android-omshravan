package com.zecast.live.om_saravanabava.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by codezilla-12 on 27/9/17.
 */
public class CountryDao {
    public String mCountryId = "";
    public String mCountryCode = "";
    public String mCountryName = "";
    public String mCountryImage = "";
    public String mCountryShortName = "";
    private boolean mCountryStatuse = false;

    public CountryDao(String mCountryId, String mCountryCode, String mCountryName, String mCountryShortName, boolean mCountryStatuse) {
        this.mCountryId = mCountryId;
        this.mCountryCode = mCountryCode;
        this.mCountryName = mCountryName;
        this.mCountryShortName = mCountryShortName;
        this.mCountryStatuse = mCountryStatuse;
    }

    public CountryDao(JSONObject jObjects) {
        try {
            this.mCountryId = jObjects.getString("id");
            this.mCountryCode = jObjects.getString("country_code");
            this.mCountryName = jObjects.getString("country_name");
            this.mCountryShortName = jObjects.getString("country_short_name").toLowerCase();
            this.mCountryImage = "flag_" + mCountryShortName;
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
