package com.zecast.live.om_saravanabava.activity.membership;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.helper.AsyncCallback;
import com.zecast.live.om_saravanabava.helper.ImagePickerHelper;
import com.zecast.live.om_saravanabava.helper.KeyConstant;
import com.zecast.live.om_saravanabava.helper.MessageConstant;
import com.zecast.live.om_saravanabava.helper.ProgressDialogUtils;
import com.zecast.live.om_saravanabava.utils.Const;
import com.zecast.live.om_saravanabava.utils.HTTPRequest;
import com.zecast.live.om_saravanabava.utils.Sharedprefrences;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.RequestBody;
import com.squareup.picasso.Picasso;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;


public class MembershipDetailActivity extends AppCompatActivity implements View.OnClickListener {

    EditText etName,etEmail,etCountry,etMobile,etAddress;
    TextView spinnerDob;
    ImageView profilePic;
    TextView tvTypeMembership,tvDescriptionMembership;
    RadioButton radiotermsBtn;
    LinearLayout linearContainerTerms;
    String titlemembershiptype,membershiTypeName;
    String membershiTypeId,membershiTypeDescription,membershiTypeStatus,membershiTypeStatusId;
    Button btnSendARequest;
    private int mYear, mMonth, mDay, mHour, mMinute;
    String message;
    String dobReverse = "";
    String TAG = "MEMBERSHIP DETAIL";
    final int CAMERA_REQUEST = 0;
    final int CROP_REQUEST = 1;
    final int GALLERY_REQUEST = 2;
    public final static int IMAGE_SIZE = 512;
    private static final int PERMISSIONS_MULTIPLE_REQUEST = 25;
    private static final int STORAGE_PERMISSION_REQUEST = 24;
    protected Uri pictureURI;
    protected File tempImagePath;
    protected Uri tempPictureURI;
    protected Uri prefimageUrl;
    private Bitmap bitmap = null;
    private AsyncCallback asyncCallback;
    Boolean isClickd = false;
    ImageView imgEdit;
    String errorType1;
     public static Boolean isClicked1 =true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_membership_details);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);


        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        titlemembershiptype =   getIntent().getStringExtra("membershiTypeTitle");
        membershiTypeName =  getIntent().getStringExtra("membershiTypeName");
        membershiTypeId =   getIntent().getStringExtra("membershiTypeId");
        membershiTypeDescription =   getIntent().getStringExtra("membershiTypeDescription");
        membershiTypeStatus =   getIntent().getStringExtra("membershiTypeStatus");
        membershiTypeStatusId =   getIntent().getStringExtra("membershiTypeStatusId");
        setTitle(titlemembershiptype);
        performCropImggg();
        initWidgets();
        setDataToWidgets();
        setUserProfilePickOnCreate();

        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        spinnerDob.setOnClickListener(this);
        linearContainerTerms.setOnClickListener(this);
        btnSendARequest.setOnClickListener(this);
//       imgEdit.setOnClickListener(this);

    }

    private void setUserProfilePickOnCreate()  {
            this.runOnUiThread(new Runnable() {
                public void run() {
                    Picasso.with(getBaseContext())
                            .load(Sharedprefrences.getUserProfile(getApplicationContext()))
                            .error(R.drawable.app_icon)
                            .placeholder(R.drawable.app_icon)
                            .into(profilePic);
                }
            });
    }

   /* private void setUserProfilePick(final String userprofilephotoupdated) {
        this.runOnUiThread(new Runnable() {
            public void run() {
                 Picasso.with(getBaseContext())
                        .load(userprofilephotoupdated)
                        .error(R.drawable.app_icon)
                        .placeholder(R.drawable.app_icon)
                        .into(profilePic);

                Picasso.with(getBaseContext())
                        .load(userprofilephotoupdated)
                        .error(R.drawable.app_icon)
                        .placeholder(R.drawable.app_icon)
                        .into(imgprofileSideDrawer);

                Picasso.with(getBaseContext())
                        .load(userprofilephotoupdated)
                        .error(R.drawable.app_icon)
                        .placeholder(R.drawable.app_icon)
                        .into(ProfileFragment.profilePic);
            }
        });

    }
  */
    private void performCropImggg() {

        final MediaType MEDIA_TYPE_JPG = MediaType.parse("image/jpg");
        try {
            byte[] data = null;
            if (/*createEvent != null && */pictureURI.getPath() != null) {
                // Get the dimensions of the View
                int targetW = profilePic.getWidth();
                int targetH = profilePic.getHeight();

                // Get the dimensions of the bitmap
                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                bmOptions.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(pictureURI.getPath(), bmOptions);
                int photoW = bmOptions.outWidth;
                int photoH = bmOptions.outHeight;
                int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
                bmOptions.inJustDecodeBounds = false;
                bmOptions.inSampleSize = scaleFactor;
                bmOptions.inPurgeable = true;
                bitmap = BitmapFactory.decodeFile(pictureURI.getPath(), bmOptions);
                Log.e("IMAGERERRRR SETTTT", "setImageView: " + bitmap);
                profilePic.setImageBitmap(bitmap);
                Log.e("FINALLL  IMAGE HERE ", "setImageView: " + bitmap);
            }
        } catch (Exception e) {

        }

    }

    private void setDataToWidgets() {

        if (!(titlemembershiptype.equals("") && membershiTypeName.equals("") &&
                membershiTypeId.equals("") && membershiTypeDescription.equals("") &&
                membershiTypeStatus.equals("") &&  membershiTypeStatusId.equals(""))) {

            tvTypeMembership.setText(titlemembershiptype+ " "+ "?");
            tvDescriptionMembership.setText(membershiTypeDescription);
            etName.setText(Sharedprefrences.getUserName(getBaseContext()));
            etEmail.setText(Sharedprefrences.getUserEmail(getBaseContext()));
            etCountry.setText(Sharedprefrences.getUserCountry(getBaseContext()));
            etMobile.setText(Sharedprefrences.getUserMobile(getBaseContext()));
              }

    }

    private void initWidgets() {

        tvTypeMembership = (TextView)findViewById(R.id.type_membership_text);
        etName =    (EditText)findViewById(R.id.etName);
        etEmail =   (EditText)findViewById(R.id.etEmail);
        etCountry = (EditText)findViewById(R.id.etCountry);
        etMobile =  (EditText)findViewById(R.id.etMobile);
        //etAddress = (EditText)findViewById(R.id.etAddress);
        spinnerDob =(TextView) findViewById(R.id.spinnerDob);
        etAddress = (EditText) findViewById(R.id.etAddress);
        profilePic = (ImageView) findViewById(R.id.account_profile_membership);
        btnSendARequest = (Button)findViewById(R.id.btn_send_a_request);
        tvDescriptionMembership = (TextView) findViewById(R.id.tvDescriptionMembership);
        radiotermsBtn = (RadioButton)findViewById(R.id.radio_terms_1);
        linearContainerTerms = (LinearLayout)findViewById(R.id.container_radio_terms);
     //   imgEdit = (ImageView)findViewById(R.id.img_edit11);
    }

    private void setMembershipDetailsToWidgets(final String userName, final String userCountry, final String userEmail, final String userMobile ) {

       this.runOnUiThread(new Runnable() {
           @Override
           public void run() {
               etName.setText(userName);
               etCountry.setText(userCountry);
               etEmail.setText(userEmail);
               etMobile.setText(userMobile);
           }
       });
     }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.container_radio_terms:
                radiotermsBtn.setChecked(true);
                break;

          /*  case R.id.img_edit11:
                //showImageDialog();
                break;*/

            case R.id.btn_send_a_request:

                if (etAddress.getText().toString().equals("")) {

                    // Toast.makeText(this, "please fill address field", Toast.LENGTH_SHORT).show();

                    etAddress.setError("please fill address field");
                    etAddress.requestFocus();
                    return;
                }


                if (dobReverse.equals("")) {

                    Toast.makeText(this, "please select Date Of Birth", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (!((radiotermsBtn.isChecked()))) {

                    Toast.makeText(this, "Please select terms and conditions", Toast.LENGTH_SHORT).show();
                    return;
                }

                if ((!(dobReverse.equals(""))&&(radiotermsBtn.isChecked()) && !(etAddress.getText().toString().equals("")))) {
                    Sharedprefrences.setUserAddress(getBaseContext(),etAddress.getText().toString());

                    Log.d("SUCCESSSSSS CONDITION","----");
                    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                    Bitmap bitmap = BitmapFactory.decodeFile(Sharedprefrences.getUserProfile(getBaseContext()),bmOptions);
                    setImageView(Sharedprefrences.getPictureuripathStored(getBaseContext()));
                } /*else {
                    Log.d("ERROR CONDITION","----");
                    Toast.makeText(this, "please fill all fields ", Toast.LENGTH_SHORT).show();
                }*/
                break;

            case R.id.spinnerDob:

                    final Calendar c = Calendar.getInstance();
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);

              //  dp.setMinDate(System.currentTimeMillis() - 1000);

                    DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                            R.style.DatepickerDialogTheme, new DatePickerDialog.OnDateSetListener() {


                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {

                                    spinnerDob.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                                  //  if (dobReverse.)
                                    dobReverse =  year+"-"+(monthOfYear + 1)+"-"+dayOfMonth;
                                    Log.d("LOG DATE REVERSE", "onDateSet: "+dobReverse);

                                }
                            }, mYear, mMonth, mDay);
                  //  datePickerDialog.getDatePicker().setMinDate(1800);
                    datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                    datePickerDialog.show();
                break;
        }
    }


    /* method from profile frag*/

    private void setImageView(String path) {
        // Get the dimensions of the View
        int targetW = profilePic.getWidth();
        int targetH = profilePic.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        //Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        //Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        bitmap = BitmapFactory.decodeFile(path, bmOptions);
        Log.e("IMAGERERRRR SETTTT", "setImageView: " + bitmap);
        //profilePic.setImageBitmap(bitmap);
        updateUserImage(bitmap);
        Log.e("FINALLL  IMAGE HERE ", "setImageView: " + bitmap);

    }











    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showAlertDialog() {

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setView(R.layout.alertdialog_subscribe_member_success);

        alert.setCancelable(false);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                MembershipDetailActivity.this.finish();

                           }
        });
        AlertDialog dialog = alert.create();
        dialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CAMERA_REQUEST) {
                performCrop();
            } else if (requestCode == GALLERY_REQUEST) {
                if (setURIFromGallery(imageReturnedIntent.getData())) {
                    performCrop();
                }
            } else if (requestCode == CROP_REQUEST) {
                //editGallery.setVisibility(View.VISIBLE);
             //   setImageView(pictureURI.getPath());
            }
        }
    }

   /* private void setImageView(String path) {
        // Get the dimensions of the View
        int targetW = profilePic.getWidth();
        int targetH = profilePic.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        //Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        //Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        bitmap = BitmapFactory.decodeFile(path, bmOptions);
        Log.e("IMAGERERRRR SETTTT", "setImageView: " + bitmap);
        profilePic.setImageBitmap(bitmap);

        Log.e("FINALLL  IMAGE HERE ", "setImageView: " + bitmap);

    }
*/
    protected void performCrop() {
        pictureURI = Uri.fromFile(ImagePickerHelper.getImageFile());
        Intent cropIntent = new Intent("com.android.camera.action.CROP");
        cropIntent.setDataAndType(tempPictureURI, "image/*");
        cropIntent.putExtra("crop", "true");
        cropIntent.putExtra("aspectX", 1);
        cropIntent.putExtra("aspectY", 1);
        cropIntent.putExtra("outputX", IMAGE_SIZE);
        cropIntent.putExtra("outputY", IMAGE_SIZE);
        cropIntent.putExtra("return-data", false);
        cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, pictureURI);
        startActivityForResult(cropIntent, CROP_REQUEST);
    }

    protected boolean setURIFromGallery(Uri selectedImage) {

        tempImagePath = null;
        String[] filePathColumn = {MediaStore.Images.Media.DATA};

        if (selectedImage.toString().startsWith(
                "content://com.android.gallery3d.provider")) {
            selectedImage = Uri.parse(selectedImage.toString().replace(
                    "com.android.gallery3d", "com.google.android.gallery3d"));
        }

        Cursor cursor = this.getApplicationContext().getContentResolver().query(selectedImage,
                filePathColumn, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            if (selectedImage.toString().startsWith(
                    "content://com.google.android.gallery3d")
                    || selectedImage.toString().startsWith(
                    "content://com.google.android.apps.photos.content")) {
                tempPictureURI = selectedImage;
            } else {
                tempPictureURI = Uri.fromFile(new File(cursor
                        .getString(columnIndex)));
            }
            try {
                Bitmap bmpPic = ImagePickerHelper.decodeSampledBitmapFromURI(
                        tempPictureURI, this, IMAGE_SIZE, IMAGE_SIZE, true);
                tempImagePath = ImagePickerHelper.getTemporaryFile();
                Log.e("IMAGE PATH ", "setURIFromGallery: " + tempImagePath + " temp picture uri " + tempPictureURI);

                tempPictureURI = Uri.fromFile(tempImagePath);
                Log.e("IMAGE PATH ", "setURIFromGallery: " + tempImagePath + " temp picture uri " + tempPictureURI);

                FileOutputStream bmpFile = new FileOutputStream(tempImagePath);
                bmpPic.compress(Bitmap.CompressFormat.JPEG, 60, bmpFile);
                bmpFile.flush();
                bmpFile.close();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        } //loop
        return false;
    }

    protected void showGalleryView() {
        Intent showGallery = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        showGallery.setType("image/*");
        startActivityForResult(
                Intent.createChooser(showGallery, "Select Photo from Gallery"),
                GALLERY_REQUEST);
    }

    private void showImageDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup_upload_image);
        LinearLayout cameraLayout = (LinearLayout) dialog.findViewById(R.id.popup_camera);
        LinearLayout galleryLayout = (LinearLayout) dialog.findViewById(R.id.popup_gallery);

        cameraLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1) {
                    showCameraView();
                } else {
                    checkMultiplePermission();
                }
            }
        });

        galleryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1) {
                    showGalleryView();
                } else {
                    checkStoragePermission();
                }
            }
        });

        dialog.show();
        dialog.setCancelable(true);
    }

    protected void showCameraView() {
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        tempImagePath = ImagePickerHelper.getTemporaryFile();
        tempPictureURI = Uri.fromFile(tempImagePath);
        takePicture.putExtra(MediaStore.EXTRA_OUTPUT, tempPictureURI);
        try {

            startActivityForResult(takePicture, CAMERA_REQUEST);
        } catch (Exception e) {
            Log.e("camera issue", e + "");
        }
    }

    private void checkMultiplePermission() {
        if (ContextCompat.checkSelfPermission(getBaseContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE) + ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale
                            (this, Manifest.permission.CAMERA)) {

                Snackbar.make(this.findViewById(android.R.id.content), "Please Grant Permissions to upload profile photo", Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, PERMISSIONS_MULTIPLE_REQUEST);
                                }
                            }
                        }).show();
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, PERMISSIONS_MULTIPLE_REQUEST);
                }
            }
        } else {
            showCameraView();
        }
    }

    private void checkStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                Snackbar.make(this.findViewById(android.R.id.content), "Please Grant Permissions to upload profile photo", Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_REQUEST);
                                }
                            }
                        }).show();
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_REQUEST);
                }
            }
        } else {
            showGalleryView();
        }
    }

    private void showToastMessages(final String message,  final String errorType) {

        this.runOnUiThread(new Runnable() {
            public void run() {
                if (errorType.equals("200")) {

                      showAlertDialog();

                }
            }
        });
    }

    private void updateUserImage(Bitmap bitmap) {
        Log.d(TAG, "update membership user imageeee: " + "name" + etName.getText().toString() + "emailll" + etEmail.getText().toString());
        Log.e(TAG, "nenbershpddd:checkkkkk "+bitmap);

        new UpdateUserDetailTask(this, bitmap, new AsyncCallback() {
        @Override
        public void setResponse(Integer responseCode, String response) {
            final String message, errorType;


            if (response!=null) {
                try {
                    JSONObject response1 = new JSONObject(response);
                    JSONObject responseStatus = response1.optJSONObject("response");

                    message = response1.getString("message");
                     errorType = response1.getString("error_type");
                     String Status = response1.getString("status");

                    String userId  = responseStatus.optString("userId");
                    String userName  = responseStatus.optString("userName");
                    String userEmail  = responseStatus.optString("userEmail");
                    String userMobile  = responseStatus.optString("userMobile");
                    String userCountry  = responseStatus.optString("userCountry");
                    String userCountryId  = responseStatus.optString("userCountryId");
                    String userCountryCode  = responseStatus.optString("userCountryCode");
                    String userKey  = responseStatus.optString("userKey");
                    String userAddress = responseStatus.getString("userAddress");
                    String userDob = responseStatus.getString("userDOB");
                    String userPhoto  = responseStatus.optString("userPhoto");

                    Sharedprefrences.setUserKey(getBaseContext(), userKey);
                    Sharedprefrences.setUserName(getBaseContext(), userName);
                    Sharedprefrences.setUserEmail(getBaseContext(), userEmail);
                    Sharedprefrences.setUserAddress(getBaseContext(), userAddress);
                    Sharedprefrences.setDateOfBirthMembershipSubs(getBaseContext(),userDob);
                    Sharedprefrences.setUserId(getBaseContext(),userId);


                    Sharedprefrences.setAdapterPositionMembership(getBaseContext(),errorType);

                    showToastMessages(message, errorType);
                 //   setUserProfilePick(userPhoto);

                 //   Sharedprefrences.setUserProfile(getBaseContext(),userPhoto);
                    setMembershipDetailsToWidgets(userName,userCountry,userEmail,userMobile);

                }  catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }

        @Override
        public void setException(String e) {

        }
    }).execute();

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    public class UpdateUserDetailTask extends AsyncTask<Void, Void, Void> {
        private String TAG = "NEW MEMBERSHIP";
        private AsyncCallback asyncCallback;
        private Context context;
        private Bitmap imageBitmap;
        String imgpath;

        public UpdateUserDetailTask(Context context, Bitmap bitmap, AsyncCallback asyncCallback) {
            this.asyncCallback = asyncCallback;
            this.context = context;
            this.imageBitmap = bitmap;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                ProgressDialogUtils.showProgressDialog(context, MessageConstant.MESSAGE_PLEASE_WAIT);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            userRegistration();
            return null;
        }

        private String userRegistration() {
            String fileName = "memberhip_profile_pic" + ".png";
            String url = HTTPRequest.SERVICE_URL + "subscribeMembership";
            Log.d(TAG, "MEMBERSHIPP REQUESTTHROUGH OKHTTP. URL :" + url+"membership type iddd"+membershiTypeId);
            String responseStr = null;
            final MediaType MEDIA_TYPE_JPG = MediaType.parse("image/jpg");

            try {

                byte[] data = null;
                if (imageBitmap != null) {
                    Log.d(TAG, "bitmap:" +imageBitmap.toString());
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    data = stream.toByteArray();
                }

                OkHttpClient client = new OkHttpClient();

                client.setConnectTimeout(Const.CONNECTION_TIME_OUT, TimeUnit.SECONDS);
                client.setReadTimeout(Const.CONNECTION_TIME_OUT, TimeUnit.SECONDS);

                RequestBody requestBody;


                if (data == null) {
                    requestBody = new MultipartBuilder()
                            .type(MultipartBuilder.FORM)
                            .addFormDataPart("userAddress",Sharedprefrences.getUserAddresss(getBaseContext()))
                            .addFormDataPart("dateOfBirth",dobReverse)
                            .addFormDataPart("membershipTypeId",membershiTypeId)
                    .build();
                } else {

                    requestBody = new MultipartBuilder()
                            .type(MultipartBuilder.FORM)
                             .addFormDataPart("userAddress",Sharedprefrences.getUserAddresss(getBaseContext()))
                            .addFormDataPart("dateOfBirth",dobReverse)
                            .addFormDataPart("membershipTypeId",membershiTypeId)
                            .addFormDataPart(KeyConstant.KEY_USER_FILE, fileName, RequestBody.create(MEDIA_TYPE_JPG, data))
                            .build();

                }
                com.squareup.okhttp.Request request = new com.squareup.okhttp.Request.Builder()
                        .url(url)
                        .addHeader("vendorKey", Const.vendorKey)
                        .addHeader("Authorization", Sharedprefrences.getUserKey(context))
                        .addHeader("userID", Sharedprefrences.getUserId(context))
                        .post(requestBody)
                        .build();

                com.squareup.okhttp.Response response = null;

                response = client.newCall(request).execute();
                responseStr = response.body().string().toString();
                Log.d(TAG, "responseStr" + responseStr);
                Log.d(TAG, "response.code():" + response.code());

                //callback of async task
                asyncCallback.setResponse(response.code(), responseStr);
                ProgressDialogUtils.hideProgressDialog();
            } catch (Exception e) {
                e.printStackTrace();
                ProgressDialogUtils.hideProgressDialog();
                asyncCallback.setException(e.getMessage());
            }
            return responseStr;
        }
    }



}


