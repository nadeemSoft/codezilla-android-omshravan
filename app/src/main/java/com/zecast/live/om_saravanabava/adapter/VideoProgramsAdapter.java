package com.zecast.live.om_saravanabava.adapter;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.fragments.VideoFragment;
import com.zecast.live.om_saravanabava.utils.Sharedprefrences;
import com.zecast.live.om_saravanabava.activity.videodetails.VideoDetailsActivity;
import com.zecast.live.om_saravanabava.model.VideoProgramsModel;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;

public class VideoProgramsAdapter extends RecyclerView.Adapter<VideoProgramsAdapter.MyViewHolder>  {

    private Context context;
    ArrayList<VideoProgramsModel> arrayListData;
    VideoFragment fragment;

    public VideoProgramsAdapter(VideoFragment fragment,Context context, ArrayList<VideoProgramsModel> mCategoryList) {
        this.context = context;
        this.arrayListData = mCategoryList;
        this.fragment = fragment;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(context, R.layout.single_row_item_video__programs_sub_categories,null);
        return new VideoProgramsAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder,final  int position) {

        final VideoProgramsModel programs = arrayListData.get(position);

        holder.tvEpisodeName.setText(programs.getGetEpisodeName());
        holder.tvLike.setText(programs.getEpisode_like());

        Picasso.with(context).
                load(programs.getEpisodeImage())
                .error(R.drawable.app_icon)
                .placeholder(R.drawable.app_icon)
                .into(holder.ivHolderEpisodeLogo);


        holder.tvView.setText(" "+programs.getEpisode_seen());
        holder.tvLike.setText(" "+programs.getEpisode_like());

        holder.playVideoIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

      Sharedprefrences.setEpisodeTypeStored(context,arrayListData.get(position).getEpisodeType());

        Intent in = new Intent(context, VideoDetailsActivity.class);
        in.putExtra("EpisodeName",programs.getGetEpisodeName());
        in.putExtra("EpisodeId",programs.getEpisodeId());
        in.putExtra ("URL", programs.getVideoId());
        in.putExtra("object",programs);
        Log.e("URL", programs.getVideoId());
        context.startActivity(in);
            }
        });


        holder.ivHolderEpisodeLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    Intent in = new Intent(context, VideoDetailsActivity.class);
                    in.putExtra("EpisodeName",programs.getGetEpisodeName());
                    in.putExtra("EpisodeId",programs.getEpisodeId());
                    in.putExtra ("URL", programs.getVideoId());
                    context.startActivity(in);
            }
        });


        holder.ivShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<Intent> targetShareIntents = new ArrayList<Intent>();
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                List<ResolveInfo> resInfos = context.getPackageManager().queryIntentActivities(shareIntent, 0);
                if (!resInfos.isEmpty()) {
                    System.out.println("Have package");
                    for (ResolveInfo resInfo : resInfos) {
                        String packageName = resInfo.activityInfo.packageName;
                        Log.i("Package Name", packageName);

                        if (!(packageName.contains("com.google.android.apps.translate") || packageName.contains("com.google.android.gms.drive") || packageName.contains("com.google.android.hangouts"))) {

                            Intent intent = new Intent();
                            intent.setComponent(new ComponentName(packageName, resInfo.activityInfo.name));
                            intent.setAction(Intent.ACTION_SEND);
                            intent.setType("text/plain");
                            intent.putExtra(Intent.EXTRA_TEXT,"watch the video- videoURL"+ programs.getEpisodeUrl()+"\n");
                            intent.putExtra(Intent.EXTRA_SUBJECT, "watch the video- videoURL");
                            intent.setPackage(packageName);
                            targetShareIntents.add(intent);
                        }

                    }
                    if (!targetShareIntents.isEmpty()) {
                        System.out.println("Have Intent");
                        Intent chooserIntent = Intent.createChooser(targetShareIntents.remove(0), "Choose app to share");
                        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetShareIntents.toArray(new Parcelable[]{}));
                        context.startActivity(chooserIntent);
                    } else {
                        System.out.println("Do not Have Intent");
                    }
                }
            }
        });



        if (programs.getEpisode_like_status().equals("1")) {

            holder.imgLike.setColorFilter(ContextCompat.getColor(context,R.color.colorPrimary));
           // notifyDataSetChanged();
         }

        holder.imgLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.e("LIKEE VIDEO IDDD", "onClick: " + programs.getEpisodeId() + "LIKEEE" + programs.getEpisode_like());

                   try {
                       fragment.likeButtonHit(position,programs.getEpisodeId(), programs.getEpisode_like());

                     } catch (NullPointerException e) {

                   }
            }
        });


        //Sharedprefrences.setEventLikeStatus(context,programs.getEpisode_like());

    }

    @Override
    public int getItemCount() {
        return arrayListData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView ivHolderEpisodeLogo;
        public TextView tvEpisodeName,tvLike,tvView;
        ImageView playVideoIcon,ivShare;
        FrameLayout frameLayoutImg;
         public  ImageView imgLike;

        public MyViewHolder(View itemView) {

            super(itemView);
            ivHolderEpisodeLogo =(ImageView)itemView.findViewById(R.id.adapter_categories_imageview);
            tvEpisodeName = (TextView) itemView.findViewById(R.id.tv_episode_name);
            tvLike = (TextView) itemView.findViewById(R.id.tv_like);
            tvView = (TextView)itemView.findViewById(R.id.tv_views);
            playVideoIcon = (ImageView) itemView.findViewById(R.id.imageViewPlayVideo_pro);
            frameLayoutImg = (FrameLayout)itemView.findViewById(R.id.frame_img);
            ivShare = (ImageView)itemView.findViewById(R.id.videoShare);
            imgLike = (ImageView)itemView.findViewById(R.id.img_like);
        }
    }
}
/*

Sharedprefrences.setEpisodeTypeStored(context,arrayListData.get(position).getEpisodeType());

        Intent in = new Intent(context, VideoDetailsActivity.class);
        in.putExtra("EpisodeName",programs.getGetEpisodeName());
        in.putExtra("EpisodeId",programs.getEpisodeId());
        in.putExtra ("URL", programs.getVideoId());
        Log.e("URL", programs.getVideoId());
        context.startActivity(in);*/
