package com.zecast.live.om_saravanabava.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.model.RecentEventsModel;
import com.zecast.live.om_saravanabava.players.VideoPlayFullScreenActivity;
import com.zecast.live.om_saravanabava.fragments.LiveFragment;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static com.zecast.live.om_saravanabava.fragments.LiveFragment.recentEventsModelList;

public class RecentEventsSeeAllAdapter extends RecyclerView.Adapter<RecentEventsSeeAllAdapter.MyViewHolder>  {

    private Context context;
    ArrayList<RecentEventsModel> arrayListData;


    public RecentEventsSeeAllAdapter(Context context, ArrayList<RecentEventsModel> mCategoryList) {
        this.context = context;
        this.arrayListData = mCategoryList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = View.inflate(context, R.layout.single_row_item_recent_event_categories_see_all,null);
        return new RecentEventsSeeAllAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder,final  int position) {

        final RecentEventsModel programs = arrayListData.get(position);
        holder.liveTagTextView.setVisibility(View.GONE);
        holder.tvEpisodeName.setText(programs.getEventName());
       // holder.tvLike.setText(programs.getEventLikes());
        holder.tvView.setText(programs.getEventSeens());
        Picasso.with(context).
                load(programs.getEventImageURL())
                .error(R.mipmap.ic_launcher)
                .placeholder(R.mipmap.ic_launcher)
                .into(holder.ivHolderEpisodeLogo);

        if (programs.getEventTypeStatus().contains("Live")) {
            holder.liveTagTextView.setVisibility(View.VISIBLE);

        } else {
            holder.liveTagTextView.setVisibility(View.GONE);
           }

           if (arrayListData.size()!=0) {
           if (arrayListData.get(position).getEventTypeStatus().equals("Live")) {

               if (position == 0) {
                   LiveFragment.tvHeadingEventLive.setText(recentEventsModelList.get(position).getEventName());
                   LiveFragment.tvDescriptionEventLive.setText(recentEventsModelList.get(position).getEventDescription());
                   LiveFragment.tvDateEventLive.setText(recentEventsModelList.get(position).getEventStartDate());
                   LiveFragment.tvViewsLiveEvent.setText(recentEventsModelList.get(position).getEventSeens());

               } else {

                   if (position == 1) {
                       LiveFragment.tvHeadingEventLive.setText(recentEventsModelList.get(position).getEventName());
                       LiveFragment.tvDescriptionEventLive.setText(recentEventsModelList.get(position).getEventDescription());
                       LiveFragment.tvDateEventLive.setText(recentEventsModelList.get(position).getEventStartDate());
                       LiveFragment.tvViewsLiveEvent.setText(recentEventsModelList.get(position).getEventSeens());
                   }
                 }
               }
             }  //end of size check if

        holder.cardViewContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*LiveFragment.tvHeadingEventLive.setText(recentEventsModelList.get(position).getEventName());
                LiveFragment.tvDescriptionEventLive.setText(recentEventsModelList.get(position).getEventDescription());
                LiveFragment.tvDateEventLive.setText(recentEventsModelList.get(position).getEventStartDate());
                LiveFragment.tvLikeEventLive.setText(recentEventsModelList.get(position).getEventLikeStatus());
           */

                Intent intent = new Intent(context,VideoPlayFullScreenActivity.class);
                intent.putExtra("VIDEOURL",arrayListData.get(position).getEventURL());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);

                  }
              });
           }

    @Override
    public int getItemCount() {
        return arrayListData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView ivHolderEpisodeLogo;
        public TextView tvEpisodeName,tvLike,tvView,tvComment,liveTagTextView;
        ImageView playVideoIcon;
        FrameLayout frameLayoutImg;
        CardView cardViewContainer;


        public MyViewHolder(View itemView) {

            super(itemView);

             ivHolderEpisodeLogo =(ImageView)itemView.findViewById(R.id.adapter_recent_programs_imageview);
             tvEpisodeName = (TextView) itemView.findViewById(R.id.tv_recent_event_name);
            // tvLike = (TextView) itemView.findViewById(R.id.tv_like);
             tvView = (TextView)itemView.findViewById(R.id.tv_views);
             playVideoIcon = (ImageView) itemView.findViewById(R.id.play_video_icon);
             frameLayoutImg = (FrameLayout)itemView.findViewById(R.id.frame_img);
             liveTagTextView = (TextView)itemView.findViewById(R.id.tv_live_tag_img_view);
             cardViewContainer = (CardView)itemView.findViewById(R.id.card_view);

        }
    }
}

/*
* #E0E0E0
*
*
*
*
*
*   @Override
            public void onClick(View view) {

                Log.v("Orientation_full", initialConfigOrientation + "");


                int orientation = getActivity().getResources().getConfiguration().orientation;

                if (orientation == Configuration.ORIENTATION_PORTRAIT) {

                    getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                     videoLayout.getLayoutParams().height= ViewGroup.LayoutParams.FILL_PARENT;
                     videoLayout.getLayoutParams().width= ViewGroup.LayoutParams.FILL_PARENT;
                     frameVideoControls.getLayoutParams().width = ViewGroup.LayoutParams.FILL_PARENT;
                     frameVideoControls.getLayoutParams().height = ViewGroup.LayoutParams.FILL_PARENT;

                     //noticeLayoutCurentEvent.setVisibility(View.GONE);
                     dontateNowLayout.setVisibility(View.GONE);
                     scrollView.setVisibility(View.GONE);
                     bottombarContainerLinear.setVisibility(View.GONE);
                     toolbar.setVisibility(View.GONE);
                     frameContainer.getLayoutParams().height = ViewGroup.LayoutParams.FILL_PARENT;
                     frameContainer.getLayoutParams().width = ViewGroup.LayoutParams.FILL_PARENT;
                     frameVideoControls.getLayoutParams().height= ViewGroup.LayoutParams.FILL_PARENT;
                    frameVideoControls.getLayoutParams().width= ViewGroup.LayoutParams.FILL_PARENT;

                    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

                } else {

                    videoLayout.getLayoutParams().height= ViewGroup.LayoutParams.WRAP_CONTENT;
                    videoLayout.getLayoutParams().width= ViewGroup.LayoutParams.FILL_PARENT;
                    frameContainer.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    frameContainer.getLayoutParams().width = ViewGroup.LayoutParams.FILL_PARENT;
                    noticeLayoutCurentEvent.setVisibility(View.VISIBLE);
                    dontateNowLayout.setVisibility(View.VISIBLE);
                    scrollView.setVisibility(View.VISIBLE);
                    toolbar.setVisibility(View.VISIBLE);
                    bottombarContainerLinear.setVisibility(View.VISIBLE);

                    getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                    getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
                    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }
            }
*
*
*
*
* */