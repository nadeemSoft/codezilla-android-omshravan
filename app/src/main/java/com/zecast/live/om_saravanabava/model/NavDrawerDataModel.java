package com.zecast.live.om_saravanabava.model;

/**
 * Created by codezilla-12 on 20/9/17.
 */

public class NavDrawerDataModel {

    String title;
/*    String img;*/
    String navItemId;

    public NavDrawerDataModel(String title/*, String img*/,String navItemId) {
        this.title = title;
       /* this.img = img;*/
        this.navItemId = navItemId;

    }


    public String getNavItemId() {
        return navItemId;
    }

    public void setNavItemId(String navItemId) {
        this.navItemId = navItemId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

  /*  public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }*/
}
