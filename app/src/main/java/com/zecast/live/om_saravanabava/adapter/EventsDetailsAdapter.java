package com.zecast.live.om_saravanabava.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.activity.eventdetails.EventDetailsActivity;
import com.zecast.live.om_saravanabava.activity.videodetails.VideoDetailsActivity;
import com.zecast.live.om_saravanabava.model.EventDetailsModel;
import com.zecast.live.om_saravanabava.model.OtherVideos;
import com.zecast.live.om_saravanabava.utils.Sharedprefrences;

import java.util.ArrayList;

import static com.zecast.live.om_saravanabava.activity.videodetails.VideoDetailsActivity.vv;

/**
 * Created by codezilla-12 on 18/10/17.
 */

public class EventsDetailsAdapter extends RecyclerView.Adapter<EventsDetailsAdapter.MyViewHolder> {

    private ArrayList<EventDetailsModel> mOtherVideos;
    private EventDetailsActivity context;
    public static int clickedPosition = -1;
    public boolean val = false;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView ivHolderEpisodeLogo;
        public TextView tvEpisodeName,tvLike,tvView;
        ImageView playVideoIcon,ivShare;
        FrameLayout frameLayoutImg;
        CardView cd;
        LinearLayout linearLayoutContainer;

        public MyViewHolder(View view) {
            super(view);

            ivHolderEpisodeLogo =(ImageView)itemView.findViewById(R.id.adapter_categories_imageview1);
            tvEpisodeName = (TextView) itemView.findViewById(R.id.tv_episode_name1);
            tvLike = (TextView) itemView.findViewById(R.id.tv_like1);
            tvView = (TextView)itemView.findViewById(R.id.tv_views1);
            playVideoIcon = (ImageView) itemView.findViewById(R.id.imageViewPlayVideo_pro1);
            frameLayoutImg = (FrameLayout) itemView.findViewById(R.id.frame_img1);
            ivShare = (ImageView)itemView.findViewById(R.id.videoShare1);
            cd = (CardView)itemView.findViewById(R.id.card_view);
            linearLayoutContainer = (LinearLayout)itemView.findViewById(R.id.layout_container);
        }
    }

    public EventsDetailsAdapter(EventDetailsActivity context, ArrayList<EventDetailsModel> mOtherVideos) {
        this.context = context;
        this.mOtherVideos = mOtherVideos;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.videodetailsadapter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

//        if (clickedPosition == -1) {
//            val = true;
//            clickedPosition = 0;
//            holder.cd.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
//        }
//        else {
//            if (position == clickedPosition) {
//                val = true;
//
//                holder.cd.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
//            } else {
//
//                holder.cd.setBackgroundColor(0);
//            }
//
//        }

        final EventDetailsModel ccn = mOtherVideos.get(position);

        holder.tvEpisodeName.setText(ccn.getEventTitle());
        holder.tvLike.setText(ccn.getEventLikes());
        holder.tvView.setText(ccn.getEventSeens());


        Picasso.with(context)
                .load(ccn.getEventImageURL())
                .error(R.drawable.app_icon)
                .fit()
                .placeholder(R.drawable.app_icon)
                .into(holder.ivHolderEpisodeLogo);

        holder.frameLayoutImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.playLiveChannel(ccn);
                clickedPosition = position;
                notifyDataSetChanged();
            }
        });

        holder.playVideoIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.playLiveChannel(ccn);
                clickedPosition = position;
                notifyDataSetChanged();
            }
        });

        holder.linearLayoutContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                context.playLiveChannel(ccn);
                clickedPosition = position;

                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return mOtherVideos.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }








}