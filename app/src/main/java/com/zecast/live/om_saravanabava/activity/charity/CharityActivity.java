package com.zecast.live.om_saravanabava.activity.charity;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.adapter.CharityAdapter;
import com.zecast.live.om_saravanabava.model.CharityModel;
import com.zecast.live.om_saravanabava.utils.Const;
import com.zecast.live.om_saravanabava.utils.HTTPRequest;
import com.zecast.live.om_saravanabava.utils.Sharedprefrences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CharityActivity extends AppCompatActivity {

    RecyclerView recyclerCharity;
    public static final ArrayList<CharityModel> charityModelArrayList = new ArrayList<>();
    CharityAdapter charityAdapter;
    LinearLayoutManager linearLayoutManager;
    LinearLayout linearContainerProgress;
    ProgressBar progressBar;
    LinearLayout linearLayoutContainerMain;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_charity);
        setTitle("Charity");

        Window window = this.getWindow();
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.colorPrimary));
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        initWidgets();
        fetchCharityData();

        linearContainerProgress.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    private void fetchCharityData() {
        RequestQueue queue = Volley.newRequestQueue(CharityActivity.this);
        StringRequest sr = new StringRequest(Request.Method.POST, HTTPRequest.SERVICE_URL+"getCharity", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.e("response_catcup", response);

                    try {

                        JSONObject response1 = new JSONObject(response);
                        String responseStatus = response1.getString("status");
                        String errorType = response1.optString("error_type");
                        String responseMessage = response1.optString("message");

                        VolleyLog.e("status response VIDEO "+responseStatus+" msg "+responseMessage+"error msg "+ errorType);

                        JSONObject openingObj = response1.optJSONObject("response");
                        charityModelArrayList.clear();
                        for (int i =0;i<openingObj.length();i++) {

                            JSONArray jsonCatchUpArray = openingObj.getJSONArray("charity");

                            for (int j=0; j<jsonCatchUpArray.length();j++) {

                                JSONObject charityOpeningObj = jsonCatchUpArray.optJSONObject(j);

                                String charityId  = charityOpeningObj.optString("charity_id");
                                String charityName  = charityOpeningObj.optString("charity_name");
                                String charityDesc  = charityOpeningObj.optString("charity_desc");
                                String charityLink  = charityOpeningObj.optString("charity_link");
                                String charityLogo  = charityOpeningObj.optString("charity_logo");

                                CharityModel model = new CharityModel(charityId,charityName,charityDesc,charityLink,charityLogo);
                                charityModelArrayList.add(model);

                                linearContainerProgress.setVisibility(View.GONE);
                                progressBar.setVisibility(View.GONE);

                                if (charityModelArrayList.size()!=0) {

                                    charityAdapter = new CharityAdapter(CharityActivity.this, charityModelArrayList);
                                    linearLayoutManager = new LinearLayoutManager(CharityActivity.this);
                                    recyclerCharity.setLayoutManager(linearLayoutManager);
                                    recyclerCharity.setAdapter(charityAdapter);
                                    recyclerCharity.setNestedScrollingEnabled(false);
                                    charityAdapter.notifyDataSetChanged();

                                } else {

                                    Snackbar snackbarresponsemsg = Snackbar.make(linearLayoutContainerMain, responseMessage, Snackbar.LENGTH_SHORT);
                                    snackbarresponsemsg.show();

                                }
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();


                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("EREROR",String.valueOf(error));

                linearContainerProgress.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);

                Toast.makeText(CharityActivity.this,String.valueOf(error), Toast.LENGTH_SHORT).show();

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();

                headers.put("vendorKey", Const.vendorKey);
                headers.put("Authorization", Sharedprefrences.getUserKey(getBaseContext()));
                headers.put("userID",  Sharedprefrences.getUserId(getBaseContext()));
                headers.put("deviceId", Sharedprefrences.getDeviceId(getBaseContext()));
                headers.put("Content-Type",Const.contentType);

                return headers;
            }

            @Override
            public Map<String, String> getParams(){
                Map<String, String> params = new HashMap<>();
                return params;
            }
        };

        int socketTimeout = 20000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        sr.setRetryPolicy(policy);
        queue.add(sr);


    }

    private void initWidgets() {
        recyclerCharity = (RecyclerView)findViewById(R.id.charity_recycler);
        //contentLoadingProgressBar = (ContentLoadingProgressBar)findViewById(R.id.contentProgressBar);
        linearContainerProgress = (LinearLayout)findViewById(R.id.linlaHeaderProgress);
        progressBar = (ProgressBar)findViewById(R.id.pbHeaderProgress);
        linearLayoutContainerMain = (LinearLayout) findViewById(R.id.linear_container_main);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;

            default:
                Log.e("SELECTEDDDDD", "onOptionsItemSelected: "+item.getItemId());
                return super.onOptionsItemSelected(item);
        }
    }



}