package com.zecast.live.om_saravanabava.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.fragments.VideoFragment;
import com.zecast.live.om_saravanabava.model.VideosCategoryModel;
import com.zecast.live.om_saravanabava.model.VideoProgramsModel;
import com.zecast.live.om_saravanabava.utils.Sharedprefrences;
import com.zecast.live.om_saravanabava.activity.seealllists.VideosSeeAllActivity;

import java.util.ArrayList;

public class VideoCategoriesAdapter extends RecyclerView.Adapter<VideoCategoriesAdapter.MyViewHolder>  {

    private Context context;
    ArrayList<VideosCategoryModel> arrayListData;
    VideoProgramsAdapter videoProgramsAdapter;
    LinearLayoutManager linearLayoutManager;
    VideoFragment fragment;

    public VideoCategoriesAdapter(VideoFragment fragment,Context context, ArrayList<VideosCategoryModel> mCategoryList) {
        this.context = context;
        this.arrayListData = mCategoryList;
        this.fragment = fragment;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = View.inflate(context, R.layout.single_row_item_video_categories,null);
        return new VideoCategoriesAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder,final  int position) {

        ArrayList <VideoProgramsModel> mList = arrayListData.get(position).getSubcategorylist();

        // holder.categoryTitle.setText(arrayListData.get(position).getCategoryName());
        Log.d("CATEGORY NAME", "onBindViewHolder: "+arrayListData.get(position).getCategoryName());

        holder.categoryTitle.setText(arrayListData.get(position).getCategoryName());
        videoProgramsAdapter = new VideoProgramsAdapter(fragment,context, mList);
        linearLayoutManager = new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL, false);
        holder.recyclerViewEpisodeVideo.setLayoutManager(linearLayoutManager);
        holder.recyclerViewEpisodeVideo.setAdapter(videoProgramsAdapter);
        holder.recyclerViewEpisodeVideo.setNestedScrollingEnabled(false);
        videoProgramsAdapter.notifyDataSetChanged();
        Sharedprefrences.setAdapterPosition(context, String.valueOf(position));

        holder.categorySeeAlltitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Sharedprefrences.setAdapterPosition(context, String.valueOf(position));
                Intent intent = new Intent(context, VideosSeeAllActivity.class);
                intent.putExtra("title",arrayListData.get(position).getCategoryName());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayListData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        RecyclerView recyclerViewEpisodeVideo;
        TextView categoryTitle,categorySeeAlltitle;

        public MyViewHolder(View itemView) {

            super(itemView);
            recyclerViewEpisodeVideo = (RecyclerView)itemView.findViewById(R.id.rv_video_programs);
            categoryTitle = (TextView)itemView.findViewById(R.id.tv_title_video_categories);
            categorySeeAlltitle = (TextView)itemView.findViewById(R.id.tv_title_video_categories_see_all);
        }
    }
}