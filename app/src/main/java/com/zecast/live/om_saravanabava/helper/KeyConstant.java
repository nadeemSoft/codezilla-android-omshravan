package com.zecast.live.om_saravanabava.helper;


public interface KeyConstant {

   String KEY_RESPONSE = "response";
   String KEY_MESSAGE = "message";
   String KEY_ERROR_TYPE = "error_type";
   String KEY_STATUS = "status";
   String KEY_RESPONSE_CODE_200 = "200";
   String KEY_MESSAGE_FALSE = "false";
   String KEY_USER_FILE = "userfile";
   String KEY_USER_ID = "userId";
   String KEY_PUBLIC_EVENTS = "organizationEvents";
   String KEY_EVENT_ID = "eventId";
   String KEY_EVENT_TITLE = "eventTitle";
   String KEY_EVENT_START_DATE = "eventStartDate";
   String KEY_EVENT_DESCRIPTION = "eventDescription";
   String KEY_EVENT_DATE = "eventDate";
   String KEY_PUBLIC_EVENT = "eventList";
   String KEY_POSTER_EVENT = "eventImageURL";
   }
