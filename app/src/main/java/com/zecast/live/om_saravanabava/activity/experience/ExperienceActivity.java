package com.zecast.live.om_saravanabava.activity.experience;

import android.app.ProgressDialog;
import android.content.Context;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.adapter.ExperiecnceAdapter;
import com.zecast.live.om_saravanabava.model.ExperienceModel;
import com.zecast.live.om_saravanabava.utils.Const;
import com.zecast.live.om_saravanabava.utils.HTTPRequest;
import com.zecast.live.om_saravanabava.utils.Sharedprefrences;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class ExperienceActivity extends AppCompatActivity {

    RecyclerView recyclerExperience;
    ExperienceModel experienceModel;
    ArrayList<ExperienceModel> experienceDataList = new ArrayList<>();
    ExperiecnceAdapter experiecnceAdapter;
    LinearLayoutManager linearLayoutManager;
    ProgressDialog progressDialog;
    public AudioManager mAudioManager;
    LinearLayout linearLayoutLoaderContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_experience);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        mAudioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);

        initWidgets();
        fetchExperienceData();

        progressDialog = new ProgressDialog(getBaseContext());
    }

    private void initWidgets() {
        recyclerExperience = (RecyclerView) findViewById(R.id.recyclerExperience);
        linearLayoutLoaderContainer = (LinearLayout)  findViewById(R.id.loadercontainerLinear);
    }

    private void fetchExperienceData() {
        RequestQueue queue = Volley.newRequestQueue(getBaseContext());
        StringRequest sr = new StringRequest(Request.Method.POST, HTTPRequest.SERVICE_URL+"getExperience", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.e("response about us ", response);
                    try {

                        JSONObject response1 = new JSONObject(response);
                        String responseStatus = response1.getString("status");
                        String errorType = response1.optString("error_type");
                        String responseMessage = response1.optString("message");
                        VolleyLog.e("status response Experience "+responseStatus+" msg "+responseMessage+"error msg "+ errorType);


                        if (errorType.equals("206"))
                        {
                            Toast.makeText(ExperienceActivity.this,responseMessage,Toast.LENGTH_SHORT).show();

                        }

                        else {

                        JSONObject openingObj = response1.optJSONObject("response");

                        for (int i = 0;i < openingObj.length();i++) {

                            JSONArray arrayExperience = openingObj.getJSONArray("experience");

                            for (int k = 0; k<arrayExperience.length();k++) {

                                JSONObject objectopeningArray = arrayExperience.getJSONObject(k);

                                String experienceId = objectopeningArray.getString("experience_id");
                                String experienceName = objectopeningArray.getString("experience_name");
                                String experienceUrl = objectopeningArray.getString("experience_url");
                                String experienceLogo = objectopeningArray.getString("experience_logo");
                                String experiencevideoId = objectopeningArray.getString("video_id");
                                String experienceDesc = objectopeningArray.getString("experience_desc");

                                experienceModel = new ExperienceModel(experienceId, experienceName, experienceUrl, experienceLogo, experiencevideoId, experienceDesc);
                                experienceDataList.add(experienceModel);

                                experiecnceAdapter = new ExperiecnceAdapter(ExperienceActivity.this, experienceDataList);
                                linearLayoutManager = new LinearLayoutManager(getBaseContext());
                                recyclerExperience.setLayoutManager(linearLayoutManager);
                                recyclerExperience.setAdapter(experiecnceAdapter);
                                recyclerExperience.setNestedScrollingEnabled(false);
                                experiecnceAdapter.notifyDataSetChanged();

                                linearLayoutLoaderContainer.setVisibility(View.GONE);
                            }
                            }

                        }

                    }  catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("EREROR",String.valueOf(error));
                linearLayoutLoaderContainer.setVisibility(View.GONE);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();


                headers.put("vendorKey", Const.vendorKey);
                headers.put("Authorization",  Sharedprefrences.getUserKey(getBaseContext()));
                headers.put("userID",  Sharedprefrences.getUserId(getBaseContext()));
                headers.put("Content-Type",Const.contentType);

                return headers;
            }

            @Override
            public Map<String, String> getParams(){
                Map<String, String> params = new HashMap<>();
                //  params.put("channelId", "1");

                return params;
            }
        };

        int socketTimeout = 20000; //30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        sr.setRetryPolicy(policy);
        queue.add(sr);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                this.finish();
                return true;

            default:
                Log.e("SELECTEDDDDD", "onOptionsItemSelected: "+item.getItemId());
                return super.onOptionsItemSelected(item);
        }
    }

}