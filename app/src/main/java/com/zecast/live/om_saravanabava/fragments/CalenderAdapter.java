package com.zecast.live.om_saravanabava.fragments;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.callback.CalenderClickListener;
import com.zecast.live.om_saravanabava.helper.KeyConstant;
import com.zecast.live.om_saravanabava.utils.Sharedprefrences;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class CalenderAdapter extends BaseAdapter {

    private static final String TAG = "GridCellAdapter";
    private static final int DAY_OFFSET = 1;
    private final Context context;
    private final List<String> list;
    private final String[] weekdays = new String[]{"S", "M", "T", "W", "T", "F", "S"};
    private final String[] months = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"};
    private final int[] daysOfMonth = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    private int leaveCount = 0;
    private int daysInMonth, prevMonthDays;
    private int currentDayOfMonth;
    private int currentWeekDay;
    private int todayYear;
    private int todayMonth;
    private CalenderClickListener calenderClickListener;
    private Button previousSelectedGridCell = null;
    public static TextView eventHighlightTv;
    String dateeeeee;

    // Days in Current Month
/*
    public CalenderAdapter(Context context, int month, int year) {
        super();
        this.context = context;
        this.list = new ArrayList<>();
        this.leaveCount = 0;
        this.calenderClickListener = calenderClickListener;

        Calendar calendar = Calendar.getInstance();
        setCurrentDayOfMonth(calendar.get(Calendar.DAY_OF_MONTH));
        setTodayMonth(calendar.get(Calendar.MONTH) + 1);
        setTodayYear(calendar.get(Calendar.YEAR));
        setCurrentWeekDay(calendar.get(Calendar.DAY_OF_WEEK));
        printMonth(month, year, getTodayMonth(), getTodayYear());

    }*/



    public CalenderAdapter(Context context, int month, int year, CalenderClickListener calenderClickListener) {
        super();
        this.context = context;
        this.list = new ArrayList<>();
        this.leaveCount = 0;
        this.calenderClickListener = calenderClickListener;

        Calendar calendar = Calendar.getInstance();
        setCurrentDayOfMonth(calendar.get(Calendar.DAY_OF_MONTH));
        setTodayMonth(calendar.get(Calendar.MONTH) + 1);
        setTodayYear(calendar.get(Calendar.YEAR));
        setCurrentWeekDay(calendar.get(Calendar.DAY_OF_WEEK));
        printMonth(month, year, getTodayMonth(), getTodayYear());

    }


    private String getMonthAsString(int i) {
        return months[i];
    }

    private String getWeekDayAsString(int i) {
        return weekdays[i];
    }

    private int getNumberOfDaysOfMonth(int i) {
        return daysOfMonth[i];
    }

    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    /**
     * Prints Month
     *
     * @param mm
     * @param yy
     */

    private void printMonth(int mm, int yy, int tm, int ty) {
        // The number of days to leave blank at
        // the start of this month.
        int trailingSpaces = 0;
        int daysInPrevMonth = 0;
        int prevMonth = 0;
        int prevYear = 0;
        int nextMonth = 0;
        int nextYear = 0;

        int currentMonth = mm - 1;
        String currentMonthName = getMonthAsString(currentMonth);
        daysInMonth = getNumberOfDaysOfMonth(currentMonth);

       // Log.d(TAG, "Current Month: " + " " + currentMonthName + " having "
            //    + daysInMonth + " days.");

        GregorianCalendar cal = new GregorianCalendar(yy, currentMonth, 1);


        if (currentMonth == 11) {
            prevMonth = currentMonth - 1;
            daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
            nextMonth = 0;
            prevYear = yy;
            nextYear = yy + 1;
      //      Log.d(TAG, "*->PrevYear: " + prevYear + " PrevMonth:"
             //       + prevMonth + " NextMonth: " + nextMonth
                 //   + " NextYear: " + nextYear);
        } else if (currentMonth == 0) {
            prevMonth = 11;
            prevYear = yy - 1;
            nextYear = yy;
            daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
            nextMonth = 1;
          //  Log.d(TAG, "**--> PrevYear: " + prevYear + " PrevMonth:"
             //       + prevMonth + " NextMonth: " + nextMonth
              //      + " NextYear: " + nextYear);
        } else {
            prevMonth = currentMonth - 1;
            nextMonth = currentMonth + 1;
            nextYear = yy;
            prevYear = yy;
            daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
            Log.d(TAG, "***---> PrevYear: " + prevYear + " PrevMonth:"
                    + prevMonth + " NextMonth: " + nextMonth
                    + " NextYear: " + nextYear);
        }

        // Compute how much to leave before the first day of the
        // month.
        // getDay() returns 0 for Sunday.
        int currentWeekDay = cal.get(Calendar.DAY_OF_WEEK) - 1;
        trailingSpaces = currentWeekDay;

       /* Log.d(TAG, "Week Day:" + currentWeekDay + " is "
                + getWeekDayAsString(currentWeekDay));
        Log.d(TAG, "No. Trailing space to Add: " + trailingSpaces);
        Log.d(TAG, "No. of Days in Previous Month: " + daysInPrevMonth);*/

        if (cal.isLeapYear(cal.get(Calendar.YEAR)) && mm == 2) {
            ++daysInMonth;
        }

        // Trailing Month days
        for (int i = 0; i < trailingSpaces; i++) {
          /*  Log.d(TAG,
                    "PREV MONTH:= "
                            + prevMonth
                            + " => "
                            + getMonthAsString(prevMonth)
                            + " "
                            + String.valueOf((daysInPrevMonth
                            - trailingSpaces + DAY_OFFSET)
                            + i));*/
            list.add(String
                    .valueOf((daysInPrevMonth - trailingSpaces + DAY_OFFSET)
                            + i)
                    + "-GREY"
                    + "-"
                    + getMonthAsString(prevMonth)
                    + "-"
                    + prevYear);
        }

        for (int i = 1; i <= daysInMonth; i++) {
            if (i == getCurrentDayOfMonth() && mm == getTodayMonth() && yy == getTodayYear()) {
                list.add(String.valueOf(i) + "-ORANGE" + "-"
                        + getMonthAsString(currentMonth) + "-" + yy);
            } else {
                list.add(String.valueOf(i) + "-WHITE" + "-"
                        + getMonthAsString(currentMonth) + "-" + yy);
            }
        }
        // Leading Month days
        for (int i = 0; i < list.size() % 7; i++) {
          /*  Log.d(TAG, "NEXT MONTH:= " + getMonthAsString(nextMonth));*/
            list.add(String.valueOf(i + 1) + "-GREY" + "-"
                    + getMonthAsString(nextMonth) + "-" + nextYear);
        }
      //  Log.d(TAG, list.toString());
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.custom_calender_grid_view, parent, false);
        }

        final Button gridcell = (Button) row.findViewById(R.id.calendar_day_gridcell);
      //   eventHighlightTv =   (TextView) row.findViewById(R.id.event_highlight_text);

        gridcell.setClickable(true);

        String[] day_color = list.get(position).split("-");
        String theday = day_color[0];
        String themonth = day_color[2];
        String theyear = day_color[3];

        int day = Integer.parseInt(day_color[0]);

        if(day<10){
            theday = 0+day_color[0];
        }

        gridcell.setText(theday);
        gridcell.setTag(theday + "-" + themonth + "-" + theyear);

        dateeeeee = theday + "-" + themonth + "-" + theyear;
        Log.e(TAG, "Setting GridCell" + gridcell.getTag().toString());

        /*if (day_color[1].equals("GREY")) {
            gridcell.setClickable(false);
            gridcell.setEnabled(false);
            gridcell.setTextColor(Color.WHITE);
        } else if (day_color[1].equals("WHITE")) {
            gridcell.setTextColor(Color.BLACK);
         } else if (day_color[1].equals("ORANGE")) {
            gridcell.setTextColor(context.getResources().getColor(R.color.Black));
            gridcell.setBackgroundResource(R.drawable.circle_filled_orange_color_border);
            gridcell.setTextColor(Color.WHITE);
        }*/


        if (day_color[1].equals("GREY")) {
            gridcell.setClickable(false);
            gridcell.setEnabled(false);

            gridcell.setTextColor(Color.WHITE);

            //    eventHighlightTv.setVisibility(View.GONE);

        } else if (day_color[1].equals("WHITE")) {
            gridcell.setTextColor(Color.BLACK);


        } else if (day_color[1].equals("ORANGE")) {
            gridcell.setTextColor(context.getResources().getColor(R.color.Black));
            gridcell.setBackgroundResource(R.drawable.circle_filled_oranage_color_border);
            gridcell.setTextColor(Color.WHITE);
        }


        String datesArrray =  Sharedprefrences.getEventDateObject(context);

     //   Log.e(TAG, "getView: "+datesArrray);

        try {

            JSONArray arr = new JSONArray(datesArrray);

            for (int i = 0; i< arr.length();i++) {
                final JSONObject jsonObject = arr.optJSONObject(i);
            //    Log.e(TAG, "getView: "+jsonObject);
                String date = jsonObject.optString(KeyConstant.KEY_EVENT_DATE);
             //   Log.e(TAG, "getView: "+date);
                if (dateeeeee.contains(date) && !(day_color[1].equals("GREY"))) {
                    gridcell.setTextColor(context.getResources().getColor(R.color.darkGray));
                    gridcell.setBackgroundResource(R.drawable.circle_filled_event_highlight_circle);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        gridcell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             //   Log.d(TAG, "clicked " + gridcell.getTag().toString());
                notifyDataSetChanged();
                if(previousSelectedGridCell != null){
                 //   Log.e(TAG, "if");
                    previousSelectedGridCell.setBackgroundResource(R.drawable.circle_filled_transparent_color_border);
                    previousSelectedGridCell = null;
                }
                if(previousSelectedGridCell == null){
                 //   Log.e(TAG, "else ");
                    previousSelectedGridCell = gridcell;
                }
                gridcell.setBackgroundResource(R.drawable.circle_filled_gray_color_border);
                calenderClickListener.setClickedDate(gridcell.getTag().toString());
            }
        });


       /* gridcell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "clicked " + gridcell.getTag().toString());
                notifyDataSetChanged();

                gridcell.setBackgroundResource(R.drawable.circle_filled_gray_color_border);
                Log.e(TAG, "checkkk dateeeeee here111: "+gridcell.getTag().toString());

                if(previousSelectedGridCell != null){
                    Log.e(TAG, "if");
                    previousSelectedGridCell.setBackgroundResource(R.drawable.circle_filled_transparent_color_border);
                    previousSelectedGridCell = null;
                }
                if(previousSelectedGridCell == null){
                    Log.e(TAG, "else");
                    previousSelectedGridCell = gridcell;
                }

                calenderClickListener.setClickedDate(gridcell.getTag().toString());
            }
        });*/


      //  Log.e(TAG, "DATESSSS: "+datess );

        return row;
    }

    public int getCurrentDayOfMonth() {
        return currentDayOfMonth;
    }

    private void setCurrentDayOfMonth(int currentDayOfMonth) {
        this.currentDayOfMonth = currentDayOfMonth;
    }

    public void setCurrentWeekDay(int currentWeekDay) {
        this.currentWeekDay = currentWeekDay;
    }

    public int getTodayMonth() {
        return todayMonth;
    }

    public void setTodayMonth(int todayMonth) {
        this.todayMonth = todayMonth;
    }

    public int getTodayYear() {
        return todayYear;
    }

    public void setTodayYear(int todayYear) {
        this.todayYear = todayYear;
    }

}
