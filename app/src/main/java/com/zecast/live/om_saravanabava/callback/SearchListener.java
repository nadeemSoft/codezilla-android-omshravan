package com.zecast.live.om_saravanabava.callback;

public interface SearchListener {
    public void video(String episode_type, String videoURL, String videoId);

}