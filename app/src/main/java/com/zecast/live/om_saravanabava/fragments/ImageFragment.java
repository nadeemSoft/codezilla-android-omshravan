package com.zecast.live.om_saravanabava.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity;
import com.zecast.live.om_saravanabava.R;
import com.squareup.picasso.Picasso;

public class ImageFragment extends Fragment {
    private ImageView ivImage;
    private static Context mContext;
    private String banner;
    private int type;
    private String adUnit;

    public static ImageFragment newInstance(Context context) {
        mContext=context;
        ImageFragment f = new ImageFragment();
        return f;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.imageview, container, false);
        ivImage = (ImageView) root.findViewById(R.id.ivImageView);
        HomeDrawerActivity.ivSearch.setVisibility(View.GONE);
        if(type == 2)
        {
            ivImage.setVisibility(View.GONE);
        }
        else
            {
            ivImage.setVisibility(View.VISIBLE);
            setImageInViewPager();
        }
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public void setImageList(String banner, int type, String adUnit) {
        this.banner = banner;
        this.type = type;
        this.adUnit = adUnit;
        Log.d("banner", banner.toString());
    }

    public void setImageInViewPager() {
        try {
            Picasso.with(mContext).load(encodeUrl(banner)).error(R.drawable.app_icon).placeholder(R.drawable.app_icon).into(ivImage);
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            System.gc();
        }
    }

    public String encodeUrl(String url){
        String str = url.replace("\"","");
        String remove = str.replace("[", "");
        String removeMore = remove.replace("]", "");
            String s = removeMore.replace("\\","");
            Log.d("banner", s);
            return s;
    }
}
