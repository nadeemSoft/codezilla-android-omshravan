package com.zecast.live.om_saravanabava.activity.splash;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.firebase.NotificationActivity;
import com.zecast.live.om_saravanabava.utils.Sharedprefrences;
import com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity;
import com.zecast.live.om_saravanabava.activity.welcome.WelcomeActivity;

public class SplashScreenActivity extends AppCompatActivity {

    private static final String TAG = NotificationActivity.class.getSimpleName();
    private BroadcastReceiver mRegistrationBroadcastReceiver;
   public static int isLogin = 0; //if Directly Login then it will be 1 otherwise it will 0

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        saveDeviceId();


    }

    @Override
    protected void onStart() {
        super.onStart();

        new Handler()
                .postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!(Sharedprefrences.getUserEmail(getBaseContext()).equals("") && Sharedprefrences.getUserMobile(getBaseContext()).equals(""))) {
                            Log.e("ENTERED IN CONDITION 1", "run: " );
                            Intent intent = new Intent(SplashScreenActivity.this, HomeDrawerActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Log.e("ENTERED IN CONDITION 2", "run: " );

                            isLogin = 1;
                            Sharedprefrences.setUserKey(SplashScreenActivity.this,"44552f68766e52356652585164305975374b794174673d3d");
                            Sharedprefrences.setUserId(SplashScreenActivity.this,"828");
                            Intent intent2 = new Intent(SplashScreenActivity.this, HomeDrawerActivity.class);
                            startActivity(intent2);
                            finish();
                        }



                    }
                }, 2000);

    }

    public void saveDeviceId() {

        String android_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        Sharedprefrences.setDeviceId(SplashScreenActivity.this,android_id);

        Log.e("DEVICE_ID", "saveDeviceId: "+Sharedprefrences.getDeviceId(SplashScreenActivity.this));
    }

//    private void displayFirebaseRegId() {
//        String regId =   Sharedprefrences.getFCMRegistrationId(getBaseContext());
//        Log.e(TAG, "Firebase reg id: " + regId);
//
//        if (!TextUtils.isEmpty(regId))
//            Toast.makeText(this,regId, Toast.LENGTH_SHORT).show();
//        else
//            Toast.makeText(this,"Firebase Reg Id is not received yet!" , Toast.LENGTH_SHORT).show();
//    }

    @Override
    protected void onResume() {
        super.onResume();

        // register GCM registration complete receiver
//        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
//                new IntentFilter(Const.REGISTRATION_COMPLETE));
//
//        // register new push message receiver
//        // by doing this, the activity will be notified each time a new message arrives
//        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
//                new IntentFilter(Const.PUSH_NOTIFICATION));
//
//        // clear the notification area when the app is opened
//        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }
}

