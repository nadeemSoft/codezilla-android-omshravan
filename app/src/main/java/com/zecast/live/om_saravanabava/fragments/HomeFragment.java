package com.zecast.live.om_saravanabava.fragments;



import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.zecast.live.om_saravanabava.helper.AppUtil;
import com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity;
import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.adapter.HomeRecyclerBannerAdapter;
import com.zecast.live.om_saravanabava.model.Banner;
import com.zecast.live.om_saravanabava.model.HomeBanner;
import com.zecast.live.om_saravanabava.utils.Const;
import com.zecast.live.om_saravanabava.utils.HTTPRequest;
import com.zecast.live.om_saravanabava.utils.Sharedprefrences;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


public class HomeFragment extends Fragment {

    private RecyclerView bannerRecy;
    private static ArrayList<HomeBanner> homeBannerArrayList = new ArrayList<>();
    HomeRecyclerBannerAdapter homeBannerAdapter;
    ImageView imageViewHomeBannerNotice;
    public static String messageNoticeBoardImage;
    //LinearLayout linearContainerProgress;
    LinearLayout linearMainContainer;
    FrameLayout containerLinear;
    ProgressBar pb;

    //linear_container_main

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.layout_fragment_homescreen, container, false);

        getActivity().setTitle("Home");
        initWidgets(view);
        HomeDrawerActivity.ivSearch.setVisibility(View.GONE);
        bannerRecy.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), OrientationHelper.VERTICAL, false);
        bannerRecy.setLayoutManager(linearLayoutManager);
        bannerRecy.setItemAnimator(new DefaultItemAnimator());


        updateDevice();
        Const.UPCOMING_FRAGMENT_KEY = 0;

        if(homeBannerArrayList.size() > 0) {
            homeBannerAdapter = new HomeRecyclerBannerAdapter(getChildFragmentManager(), getActivity(), getContext(), homeBannerArrayList);
            bannerRecy.setAdapter(homeBannerAdapter);

            Picasso.with(getContext())
                    .load(messageNoticeBoardImage)
                    .error(R.drawable.app_icon)
                    .placeholder(R.drawable.app_icon)
                    .into(imageViewHomeBannerNotice);

            pb.setVisibility(View.GONE);
            linearMainContainer.setVisibility(View.VISIBLE);

        } else {
            getHomeBanners();
        }

        return view;
    }

    private void initWidgets(View view) {
        bannerRecy = (RecyclerView) view.findViewById(R.id.banner_view_pager_list);
        imageViewHomeBannerNotice = (ImageView)view.findViewById(R.id.homeBannerNoticeImg);
        //  linearContainerProgress = (LinearLayout)view.findViewById(R.id.linlaHeaderProgress);
        linearMainContainer = (LinearLayout)view.findViewById(R.id.linear_container_main);
        pb = (ProgressBar)view.findViewById(R.id.progressbar);
        containerLinear = (FrameLayout) view.findViewById(R.id.container_linear);
    }

    private void updateDevice() {

        if (AppUtil.isNetworkAvailable(getContext())) {
            RequestQueue queue = Volley.newRequestQueue(getContext());
            StringRequest sr = new StringRequest(Request.Method.POST, HTTPRequest.SERVICE_URL + "updateDevice", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (response != null) {
                        Log.e("UPDATE DEVICE RESPONSE", response);

                        try {

                            JSONObject obj = new JSONObject(response);

                            JSONObject objMain = obj.optJSONObject("response");

                            String userMessage = objMain.optString("message");
                            String error_type = objMain.optString("error_type");
                            String status = objMain.optString("status");

                            Log.e("RESPONSE", "onResponse: "+ userMessage + "ERROR TYPE"
                                    + error_type + "status" + status);

                        } catch (Exception e) {
                            Log.e("Exception", String.valueOf(e));
                        }
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    Log.e("EREROR", String.valueOf(error));




                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<String, String>();

                   /* headers.put(HeaderConstants.VENDERKEY, HeaderDataConstants.vendorKey);
                    headers.put(HeaderConstants.CONTENT_TYPE,HeaderDataConstants.contentType);
                    headers.put(HeaderConstants.DEVICE_MODE,HeaderDataConstants.deviceMode);
*/

                    headers.put("vendorKey", Const.vendorKey);
                    headers.put("Authorization", Sharedprefrences.getUserKey(getActivity()));
                    headers.put("userID", Sharedprefrences.getUserId(getContext()));
                    headers.put("deviceId", Sharedprefrences.getDeviceId(getContext()));


                    return headers;
                }

                @Override
                public Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();


                  /*  params.put(HeaderConstants.DEVICEID, Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID));
                    Log.e("updatedevice", "homeFragment: "+Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID)+"and token"
                            +Sharedprefrences.getFCMRegistrationId(getContext()));
                    params.put(HeaderConstants.DEVICE_TOKEN,Sharedprefrences.getFCMRegistrationId(getContext()));
                    params.put(HeaderConstants.DEVICTYPE,"A");*/

                    Log.e("updatedevice", "homeFragment: "+Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID)+"and token"
                            +Sharedprefrences.getFCMRegistrationId(getContext()));

                    params.put("deviceId", Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID));
                    params.put("deviceToken",Sharedprefrences.getFCMRegistrationId(getContext()));
                    params.put("deviceType","A");

                    return params;
                }
            };

            int socketTimeout = 20000;//30 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            sr.setRetryPolicy(policy);
            queue.add(sr);
        }

    }

    private void getHomeBanners() {

        if (AppUtil.isNetworkAvailable(getContext())) {
            RequestQueue queue = Volley.newRequestQueue(getContext());
            StringRequest sr = new StringRequest(Request.Method.POST, HTTPRequest.SERVICE_URL + "getHomeBanners", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (response != null) {
                        Log.e("response_catcupsubcat", response);

                        try {
                            JSONObject obj = new JSONObject(response);

                            homeBannerArrayList.clear();



                            JSONObject objMain = obj.optJSONObject("response");

                            JSONArray userMessageArray = objMain.optJSONArray("user_message");

                            for (int i = 0; i < userMessageArray.length(); i++) {

                                JSONObject objectOPeningMsgArray = userMessageArray.optJSONObject(i);

                                String messageId = objectOPeningMsgArray.optString("msg_id");
                                messageNoticeBoardImage = objectOPeningMsgArray.optString("message");

                                Picasso.with(getContext())
                                        .load(messageNoticeBoardImage)
                                        .error(R.drawable.app_icon)
                                        .placeholder(R.drawable.app_icon)
                                        .into(imageViewHomeBannerNotice);

                                VolleyLog.e("INSIDE MSG ARRAY" + messageId + " and  msg is :" + messageNoticeBoardImage);
                            }

                            JSONArray homeBannerArray = objMain.optJSONArray("home_banner");

                            if (homeBannerArray.length()>0) {
                            for (int i = 0; i < homeBannerArray.length(); i++) {
                                Log.e("", "onResponse: " + homeBannerArray.length());

                                HomeBanner homeBanner = new HomeBanner();
                                JSONObject object = homeBannerArray.getJSONObject(i);

                                homeBanner.setBannerRowId(object.optInt("banner_row_id"));
                                JSONArray bannerArray = object.optJSONArray("banners");
                                Log.e("", "Banner Array: " + bannerArray.length());

                                ArrayList<Banner> bannerArrayList = new ArrayList<>();
                                for (int j = 0; j < bannerArray.length(); j++) {
                                    Banner banner = new Banner();
                                    JSONObject bannerObj = bannerArray.optJSONObject(j);
                                    if (bannerObj != null) {
                                        banner.setId(bannerObj.optInt("banner_id"));
                                        banner.setType(bannerObj.optInt("banner_type"));
                                        ArrayList<String> bannerSubArray = convertStringInArray(bannerObj.optString("banner_image"));
                                        banner.setImageUrl(bannerSubArray);
                                        banner.setAdKey(bannerObj.optString("banner_admob_id"));
                                        bannerArrayList.add(banner);
                                        Log.e("", "Array list " + bannerArrayList.size());
                                    }
                                }
                                homeBanner.setBannerArrayList(bannerArrayList);
                                homeBannerArrayList.add(homeBanner);

                            }
                            }
                            if (getActivity() != null) {

                                //  linearContainerProgress.setVisibility(View.GONE);

                                homeBannerAdapter = new HomeRecyclerBannerAdapter(getChildFragmentManager(), getActivity(), getContext(), homeBannerArrayList);
                                bannerRecy.setAdapter(homeBannerAdapter);

                                pb.setVisibility(View.GONE);
                                linearMainContainer.setVisibility(View.VISIBLE);
                            }
                        } catch (Exception e) {
                            Log.e("Exception", String.valueOf(e));
                        }

                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    Log.e("EREROR", String.valueOf(error));
                    //    linearContainerProgress.setVisibility(View.GONE);

//                    Toast.makeText(getContext(),String.valueOf(error), Toast.LENGTH_SHORT).show();

                    pb.setVisibility(View.GONE);
                    linearMainContainer.setVisibility(View.VISIBLE);


                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<String, String>();

                    headers.put("vendorKey", Const.vendorKey);
                    headers.put("Authorization", Sharedprefrences.getUserKey(getActivity()));
                    headers.put("userID", Sharedprefrences.getUserId(getContext()));
                    headers.put("deviceId", Sharedprefrences.getDeviceId(getContext()));
                    headers.put("Content-Type", Const.contentType);

                    return headers;
                }

                @Override
                public Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    //params.put("sub_category_id", subId);

                    return params;
                }
            };

            int socketTimeout = 20000;//30 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            sr.setRetryPolicy(policy);
            queue.add(sr);
        }  else {

          //  AppUtil.showMsgAlert(containerLinear,MessageConstant.MESSAGE_INTERNET_CONNECTION);

        }
    }

    private ArrayList<String> convertStringInArray(String array) {

        String str = array.replace("\"","");
        String remove = str.replace("[", "");
        String removeMore = remove.replace("]", "");
        ArrayList<String> strings = new ArrayList<String>(Arrays.asList(removeMore.split(",")));
        return strings;
    }

    @Override
    public void onPause() {
        Log.e("currentPage","pause");
        super.onPause();

       /* pb.setVisibility(View.GONE);
        linearMainContainer.setVisibility(View.VISIBLE);*/

        // getHomeBanners();

        //  linearContainerProgress.setVisibility(View.GONE);

    }

    @Override
    public void onResume() {
        Log.e("currentPage","resume");
        super.onResume();
        getHomeBanners();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

}

/*

        new Handler().postDelayed(new Runnable() {
@Override
public void run() {

        if (!ValidationUtils.isInternetAvailable(getContext())) {

        linearMainContainer.setVisibility(View.GONE);
        pb.setVisibility(View.VISIBLE);

        //  buttonJoinNow.setEnabled(false);
        Snackbar snackbar = Snackbar.make(containerLinear, "Please check your internet connection", Snackbar.LENGTH_LONG);

                    */
/*snackbar.setAction("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);

                            linearMainContainer.setVisibility(View.VISIBLE);
                            pb.setVisibility(View.GONE);
                        }
                    });*//*

        snackbar.setActionTextColor(ContextCompat.getColor(getContext(),R.color.colorPrimary));
        snackbar.show();
        }
        if (ValidationUtils.isInternetAvailable(getContext())){
        //  buttonJoinNow.setEnabled(true);
                   */
/* linearMainContainer.setVisibility(View.VISIBLE);
                    pb.setVisibility(View.GONE);*//*

        }
        }
        },600);*/