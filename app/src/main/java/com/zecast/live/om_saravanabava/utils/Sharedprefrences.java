package com.zecast.live.om_saravanabava.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by codezilla-12 on 16/9/17.
 */

public class Sharedprefrences {

    private static final String OM_SARAVANABAVA_SHARED_PREF_FILE_NAME = "com.om_saravanabava.sharedpref";
    private static final String DEVICE_ID = "device_id";
    private static final String AUDIO_URL = "position";
    private static final String USER_NAME = "username";
    private static final String USER_EMAIL = "email";
    private static final String USER_COUNTRY = "country";
    private static final String USER_MOBILE = "mobile";
    private static final String USER_IS_LOGINED = "islogin";
    private static final String USER_ID = "userid";
    private static final String USER_KEY = "userkey";
    private static final String ADAPTER_POSITION = "position";
    private static final String USER_ADDRESS = "useraddress";
    private static final String USER_ENTERED_MOBILE = "userenteredmobile";
    private static final String BANNER_POSITION = "bannerposition";
    private static final String ERROR = "error";
    private static final String DONATE_STATUS = "donatestatus";
    private static final String USER_PROFILE_PICTURE = "profilepic";
    private static final String LiveVideoUrl = "liveurll";
    private static final String DATEOFBIRTH = "dob";
    private static final String MEMBERSHIPADAPTERPOS = "dob";
    private static final String FCMTOKENID = "tokennID";
    private static final String VIDEOEPISODETYPE = "tokennID";
    private static final String PICTUREURIPATH = "pathh";
    private static final String CHANGELIKESBUTTONCOLOR = "colorr";
    private static final String STOREEPISODEID = "id";
    private static final String LIKEBUTTONLIVEID = "idlivelike";
    private static final String LIKEBUTTONCOLORCHANGER = "color";
    private static final String VIDEOEPISODETYPESEARCH = "searchh";
    private static final String EVENTLIKESTATUS = "likestatus";
    private static final String EVENTDATEARRAY = "array";
    private static final String EVENTDATEOBJECT = "objjj";
    private static final String VideosURL= "videosUrl";
    private static final String VideoPosition = "positions";
    private static final String ISTEXTCOLORWHITEEE = "tvcolorwhiteee";
    private static final String SAVEDATECLICKED = "ddaatteee";
    private static final String SETPUBLICEVENT = "evevee";
    private static final String SETSHAREVIDEOURL = "VideoUrl";
    //pictureURI.getPath()

    public static void setDeviceId(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(DEVICE_ID, value).apply();
    }

    public static String getDeviceId(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(DEVICE_ID, "");
    }

    public static String setAudioURl(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(AUDIO_URL, value).apply();

        return value;
    }

    public static String getAudioUrl(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(AUDIO_URL, "");
    }

    public static void setUserName(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(USER_NAME, value).apply();
    }

    public static String getUserName(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(USER_NAME, "");
    }

    public static void setUserEmail(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(USER_EMAIL, value).apply();
    }

    public static String getUserEmail(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(USER_EMAIL, "");
    }


    public static void setUserMobile(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(USER_MOBILE, value).apply();
    }

    public static String getUserMobile(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(USER_MOBILE, "");
    }

    public static void setUserEnteredMobile(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(USER_ENTERED_MOBILE, value).apply();
    }

    public static String getUserEnteredMobile(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(USER_ENTERED_MOBILE, "");
    }

    public static void setUserCountry(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(USER_COUNTRY, value).apply();
    }

    public static String getUserCountry(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(USER_COUNTRY, "");
    }

    public static void setUserLogedInStatus(Context context, Boolean value) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().putBoolean(USER_IS_LOGINED, value).apply();
    }

    public static Boolean getUserLogedInStatus(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getBoolean(USER_IS_LOGINED, false);
    }

    public static void setUserId(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(USER_ID, value).apply();
    }

    public static String getUserId(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(USER_ID, "USER_ID");
    }

    public static void setUserKey(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(USER_KEY, value).apply();
    }

    public static String getUserKey(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(USER_KEY, "");
    }

    public static void setAdapterPosition(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(ADAPTER_POSITION, value).apply();
    }

    public static String getAdapterPosition(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(ADAPTER_POSITION, "");
    }

    public static void setUserAddress(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(USER_ADDRESS, value).apply();
    }

    public static String getUserAddresss(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(USER_ADDRESS, "");
    }

    public static void setBAnnerpositionStored(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(BANNER_POSITION, value).apply();
    }

    public static String getBAnnerpositionStored(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(BANNER_POSITION, "");
    }


    public static void setSignupErrorCode(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(ERROR, value).apply();
    }

    public static String getSignupErrorCode(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(ERROR, "");
    }

    public static void setDonateStatus(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(DONATE_STATUS, value).apply();
    }

    public static String getDonateStatus(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(DONATE_STATUS, "");
    }



    public static void setUserProfile(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(USER_PROFILE_PICTURE, value).apply();
    }

    public static String getUserProfile(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(USER_PROFILE_PICTURE, "");
    }

    public static String getVideoUrl(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(LiveVideoUrl, "");
    }

    public static void setVideoUrl(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(LiveVideoUrl, value).apply();
    }


    public static String getDateOfBirthMembershipSubs(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(DATEOFBIRTH, "");
    }

    public static void setDateOfBirthMembershipSubs(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(DATEOFBIRTH, value).apply();
    }

    public static String getAdapterPositionMembership(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(MEMBERSHIPADAPTERPOS, "");
    }

    public static void setAdapterPositionMembership(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(MEMBERSHIPADAPTERPOS, value).apply();
    }

    /*
    * firebase releted  data storage
    * */

    public static String getFCMRegistrationId(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(FCMTOKENID, "");
    }

    public static void setFCMRegistrationId(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(FCMTOKENID, value).apply();
    }

    public static String getEpisodeTypeStored(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(VIDEOEPISODETYPE, "");
    }

    public static void setEpisodeTypeStored(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(VIDEOEPISODETYPE, value).apply();
    }

    public static String getPictureuripathStored(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(VIDEOEPISODETYPE, "");
    }

    public static void setPictureuripathStored(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(VIDEOEPISODETYPE, value).apply();
    }

    public static String getcolorButtonLike(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(CHANGELIKESBUTTONCOLOR, "");
    }

    public static void setcolorButtonLike(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(CHANGELIKESBUTTONCOLOR, value).apply();
    }

    public static String getepisodeId(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(STOREEPISODEID, "");
    }

    public static void setepisodeId(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(STOREEPISODEID, value).apply();
    }

    public static String getliveLikeId(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(LIKEBUTTONLIVEID, "");
    }

    public static void setliveLikeId(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(LIKEBUTTONLIVEID, value).apply();
    }
    public static String getchangelikeposition(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(LIKEBUTTONCOLORCHANGER, "");
    }

    public static void setchangelikePosition(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(LIKEBUTTONCOLORCHANGER, value).apply();
    }

    public static String getEpisodeTypeStoredSearch(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(VIDEOEPISODETYPESEARCH, "");
    }

    public static void setEpisodeTypeStoredSearch(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(VIDEOEPISODETYPESEARCH, value).apply();
    }


    public static String getEventlikestatus(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(EVENTLIKESTATUS, "");
    }

    public static void setEventLikeStatus(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(EVENTLIKESTATUS, value).apply();
    }

    public static String getEventDateArray(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(EVENTDATEARRAY, "");
    }

    public static void setEventDateArray(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(EVENTDATEARRAY, value).apply();
    }

    public static Boolean clearEventDateArray(Context context) {

       // Log.e("method calledddddddddd", "clearEventDateArray: ");
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(EVENTDATEARRAY);
        editor.commit();
        return true;
    }

    public static String getEventDateObject(Context context) {
     /*   Log.e("method calledddddddddd", "clearEventDateObject: ");*/

        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(EVENTDATEOBJECT, "");
    }

    public static void setEventDateObject(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(EVENTDATEOBJECT, value).apply();
    }


    public static void setPublicEventArray(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(SETPUBLICEVENT, value).apply();
    }


    public static String getPublicEventArray(Context context) {

        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(SETPUBLICEVENT, "");
    }

    public static Boolean clearEventDateObject(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(EVENTDATEOBJECT);
        editor.commit();
        return true;
    }


    public static void setVideoUrls(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(VideosURL, value).apply();
    }

    public static String getVideoUrls(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(VideosURL, "");
    }

    public static void setPositions(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(VideoPosition, value).apply();
    }

    public static String getPostions(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(VideoPosition, "");
    }


    public static void setYoutubeVideoId(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(ISTEXTCOLORWHITEEE, value).apply();
    }

    public static String getYoutubeVideoId(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(ISTEXTCOLORWHITEEE, "");
    }


    public static void setDateOfEVEMatch(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(SAVEDATECLICKED, value).apply();
    }

    public static String getDateOfEVEMatch(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(SAVEDATECLICKED, "");
    }


    public static Boolean clearDateOfEVEMatch(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OM_SARAVANABAVA_SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(SAVEDATECLICKED);
        editor.commit();
        return true;
    }

}
