package com.zecast.live.om_saravanabava.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity;
import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.adapter.ProfileMembershipAdapter;
import com.zecast.live.om_saravanabava.helper.AsyncCallback;
import com.zecast.live.om_saravanabava.helper.ImagePickerHelper;
import com.zecast.live.om_saravanabava.helper.KeyConstant;
import com.zecast.live.om_saravanabava.helper.MessageConstant;
import com.zecast.live.om_saravanabava.helper.ProgressDialogUtils;
import com.zecast.live.om_saravanabava.model.MembershipTypeDetailsModel;
import com.zecast.live.om_saravanabava.utils.Const;
import com.zecast.live.om_saravanabava.utils.HTTPRequest;
import com.zecast.live.om_saravanabava.utils.Sharedprefrences;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.RequestBody;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity.baseHomeText;
import static com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity.baseLivetext;
import static com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity.baseProfileText;
import static com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity.baseUpcomingtext;
import static com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity.basevideostext;
import static com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity.iconTabHome;
import static com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity.iconTabLive;
import static com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity.iconTabProfile;
import static com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity.iconTabUpcoming;
import static com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity.iconTabVideo;
import static com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity.imgprofileSideDrawer;
import static com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity.lineHome;
import static com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity.lineLive;
import static com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity.lineProfile;
import static com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity.lineUpcoming;
import static com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity.lineVideo;
import static com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity.tvUserCountry;
import static com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity.tvUserEamil;
import static com.zecast.live.om_saravanabava.activity.homescreen.HomeDrawerActivity.tvUserName;

public class ProfileFragment extends Fragment implements View.OnClickListener/*,View.OnTouchListener*/ {

    public static ImageView profilePic;
    final int CAMERA_REQUEST = 0;
    final int CROP_REQUEST = 1;
    final int GALLERY_REQUEST = 2;
    public final static int IMAGE_SIZE = 512;
    private static final int PERMISSIONS_MULTIPLE_REQUEST = 25;
    private static final int STORAGE_PERMISSION_REQUEST = 24;
    protected Uri pictureURI;
    protected File tempImagePath;
    protected Uri tempPictureURI;
    public static EditText etName, etEmail, etCountry, etMobile;
    RecyclerView recyclerViewMembershipType;
    TextView tvSave, tvEdit;
    ArrayList<MembershipTypeDetailsModel> membershipDetailsArrayList = new ArrayList<>();
    ProfileMembershipAdapter adapter;
    LinearLayoutManager linearLayoutManager;
    TextView textViewAnd;
   public static String updateduserName, updatedUserEmail, updatedUserProfile;
    String updatedUserCountry, updatedUserMobile;
    ImageView imgEdit;
    LinearLayout linearEtNameContainer,linearEtEmailContainer;
    public static String mobileEdited, nameEdited, countryEdited, emailEdited;
    private String TAG = "NEW PROFILE FRAGMENTTTT";
    private Bitmap bitmap=null;
    LinearLayout linearLayoutLoaderContainer,linearDatacontainer;


    public ProfileFragment() {
        // Required empty public constructor
    }



    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTabColor();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_12, container, false);
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        initWidgets(view);

        Const.UPCOMING_FRAGMENT_KEY = 0;

        HomeDrawerActivity.ivSearch.setVisibility(View.GONE);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        getActivity().setTitle("Profile");

        tvEdit.setOnClickListener(this);
        tvSave.setOnClickListener(this);
        imgEdit.setOnClickListener(this);
        getUserAndSubscriptionDetails();
        setUpdatedUserDataToWidgets();
        setUserPrifilePick();
        etName.getBackground().setColorFilter(getResources().getColor(R.color.White), PorterDuff.Mode.SRC_ATOP);
        etEmail.getBackground().setColorFilter(getResources().getColor(R.color.White), PorterDuff.Mode.SRC_ATOP);

        return view;
    }

    private void setTabColor() {

        lineLive.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.White));
        iconTabLive.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_live_unselected));
        lineHome.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.White));
        iconTabHome.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_home_unselected));
        lineUpcoming.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.White));
        iconTabUpcoming.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_upcoming_unselected));
        lineVideo.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.White));
        iconTabVideo.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_video_tab_unselected));
        lineProfile.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
        iconTabProfile.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_profile_tab));
        baseHomeText.setTextColor(ContextCompat.getColor(getActivity(), R.color.gray));
        basevideostext.setTextColor(ContextCompat.getColor(getActivity(), R.color.gray));
        baseUpcomingtext.setTextColor(ContextCompat.getColor(getActivity(), R.color.gray));
        baseProfileText.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
        baseLivetext.setTextColor(ContextCompat.getColor(getActivity(), R.color.gray));

    }

    private void setUserPrifilePick() {
        if (Sharedprefrences.getUserProfile(getContext())!="") {
            Picasso.with(getContext())
                    .load(Sharedprefrences.getUserProfile(getContext()))
                    .error(R.drawable.app_icon)
                    .fit()
                    .placeholder(R.drawable.app_icon)
                    .into(profilePic);
        }
    }

    private void setUserProfilePick(final String userprofilephotoupdated) {

        try {
            Sharedprefrences.setUserProfile(getContext(), userprofilephotoupdated);
        } catch (Exception e) {
            e.getMessage();
            Log.d(TAG, "setUserProfilePick: " + "no img url" + e.getMessage());
        }

        try {
        ((Activity) getContext()).runOnUiThread(new Runnable() {
            public void run() {
                Picasso.with(getContext())
                        .load(userprofilephotoupdated)
                        .error(R.drawable.app_icon)
                        .placeholder(R.drawable.app_icon)
                        .into(profilePic);

                Picasso.with(getContext())
                        .load(userprofilephotoupdated)
                        .error(R.drawable.app_icon)
                        .placeholder(R.drawable.app_icon)
                        .into(imgprofileSideDrawer);
            }
        });
    }
        catch(Exception e) {

        }
    }

    private void initWidgets(View view) {
        profilePic = (ImageView) view.findViewById(R.id.account_profile);
        tvSave = (TextView) view.findViewById(R.id.save_profile);
        tvEdit = (TextView) view.findViewById(R.id.edit_profile);
        etName = (EditText) view.findViewById(R.id.etName_1);
        etMobile = (EditText) view.findViewById(R.id.etMobile_1);
        etCountry = (EditText) view.findViewById(R.id.etCountry);
        etEmail = (EditText) view.findViewById(R.id.etEmail);
        recyclerViewMembershipType = (RecyclerView) view.findViewById(R.id.recycler_profile_membership);
        textViewAnd = (TextView) view.findViewById(R.id.and_icon_tv);
        imgEdit = (ImageView) view.findViewById(R.id.img_edit);
        linearLayoutLoaderContainer = (LinearLayout)view.findViewById(R.id.loadercontainerLinear);
        linearDatacontainer = (LinearLayout)view.findViewById(R.id.linearDatacontainer1);


    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.img_edit:
                showImageDialog();
                break;

            case R.id.edit_profile:
                etName.setFocusable(true);
                etName.setFocusableInTouchMode(true);
                etEmail.setFocusable(true);
                etEmail.setFocusableInTouchMode(true);
                etName.setLongClickable(false);
                etEmail.setLongClickable(false);
                etCountry.setLongClickable(false);
                etMobile.setLongClickable(false);

                etName.getBackground().setColorFilter(getResources().getColor(R.color.gray), PorterDuff.Mode.SRC_ATOP);
                etEmail.getBackground().setColorFilter(getResources().getColor(R.color.gray), PorterDuff.Mode.SRC_ATOP);
                break;

            case R.id.save_profile:

                if (!(TextUtils.isEmpty(etMobile.getText().toString())) &&
                        !(TextUtils.isEmpty(etName.getText().toString())) &&
                        !(TextUtils.isEmpty(etCountry.getText().toString())) &&
                        !(TextUtils.isEmpty(etCountry.getText().toString()))) {

                    etName.setFocusable(false);
                    etName.setFocusableInTouchMode(false);
                    etEmail.setFocusable(false);
                    etEmail.setFocusableInTouchMode(false);

                    etName.getBackground().setColorFilter(getResources().getColor(R.color.White), PorterDuff.Mode.SRC_ATOP);
                    etEmail.getBackground().setColorFilter(getResources().getColor(R.color.White), PorterDuff.Mode.SRC_ATOP);

                    mobileEdited = etMobile.getText().toString();
                    nameEdited = etName.getText().toString();
                    countryEdited = etCountry.getText().toString();
                    emailEdited = etEmail.getText().toString();

                    Log.e(TAG, "onClick: IMAGE PROFILEEEee"+bitmap );

                    updateUserImage(bitmap);
                    hideSoftKeyboard(getActivity());
                }
                break;
        }
    }

    private void updateUserImage(Bitmap bitmap) {

        Log.d(TAG, "updateUserImage: " + "name" +"bitmappp "+ bitmap +etName.getText().toString() + "emailll" + etEmail.getText().toString());
        new UpdateUserDetailTask(getContext(), bitmap, new AsyncCallback() {

            @Override
            public void setResponse(Integer responseCode, String response) {
                final String message, errorType;

                if (response != null) {
                    Log.e("USER DATA UPDATE CALLED", response);
                    try {
                        JSONObject response1 = new JSONObject(response);
                        JSONObject objresponseOpening = response1.optJSONObject("response");

                        String responseStatus = objresponseOpening.getString("result");
                        message = response1.getString("message");
                        errorType = response1.getString("error_type");

                        Log.e("DATA FETCHED", "onResponse: " + responseStatus);
                        JSONArray jsonCatchUpArray = objresponseOpening.getJSONArray("userDetails");

                        for (int j = 0; j < jsonCatchUpArray.length(); j++) {
                            JSONObject charityOpeningObj = jsonCatchUpArray.optJSONObject(j);
                            String userId = charityOpeningObj.optString("userId");
                            String userName = charityOpeningObj.optString("userName");
                            String userEmail = charityOpeningObj.optString("userEmail");
                            String userPhotoUpdated = charityOpeningObj.optString("userPhoto");
                            String userMobile = charityOpeningObj.optString("userMobile");
                            String userCountry = charityOpeningObj.optString("userCountry");
                            String userCountryCode = charityOpeningObj.optString("userCountryCode");
                            String userKey = charityOpeningObj.optString("userKey");

                            Log.e("PPRROOFFILE RESPONSE", "onResponse: " + userId + "u name" + userName + " email" + userEmail + " photo" + userPhotoUpdated + " mobile" + userMobile +
                                    "user country " + userCountry + " country code " + userCountryCode + " keyyyy" + userKey);


                            showToastMessages(message, errorType);

                            Sharedprefrences.setUserId(getContext(), userId);
                            Sharedprefrences.setUserName(getContext(), userName);
                            Sharedprefrences.setUserEmail(getContext(), userEmail);
                            Sharedprefrences.setUserProfile(getContext(), userPhotoUpdated);
                            Sharedprefrences.setUserMobile(getContext(), userMobile);
                            Sharedprefrences.setUserCountry(getContext(), userCountry);
                            Sharedprefrences.setUserKey(getContext(), userKey);

                            updateduserName = Sharedprefrences.getUserName(getContext());
                            updatedUserEmail = Sharedprefrences.getUserEmail(getContext());
                            updatedUserProfile = Sharedprefrences.getUserProfile(getContext());
                            updatedUserCountry = Sharedprefrences.getUserCountry(getContext());
                            updatedUserMobile = Sharedprefrences.getUserMobile(getContext());
                            updatedUserCountry = Sharedprefrences.getUserCountry(getContext());


                           setUpdstedDataToWidgets(updatedUserCountry,updateduserName,updatedUserEmail);
                            Log.d(TAG, "setResponse: "+Sharedprefrences.getUserProfile(getContext()));
                            setUserProfilePick(userPhotoUpdated);
                            Log.e("REACHED ", "onResponse: ");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void setException(String e) {
               try {
                  // AppUtil.showMsgAlert(profilePic, MessageConstant.MESSAGE_SOMETHING_WRONG);
                   Snackbar.make(profilePic, MessageConstant.MESSAGE_SOMETHING_WRONG, Snackbar.LENGTH_SHORT).show();

               } catch (Exception e1) {
                   Log.e(TAG, "setException: "+e1.getMessage() );
               }
               }
        }).execute();

    }

    private void setUpdstedDataToWidgets(final String updatedUserCountry, final String updateduserName, final String updatedUserEmail ) {

        ((Activity) getContext()).runOnUiThread(new Runnable() {
            public void run() {

                tvUserCountry.setText(updatedUserCountry);
                tvUserName.setText(updateduserName);
                tvUserEamil.setText(updatedUserEmail);
            }
    });

    }

    protected void showGalleryView() {
        Intent showGallery = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        showGallery.setType("image/*");
        startActivityForResult(
                Intent.createChooser(showGallery, "Select Photo from Gallery"),
                GALLERY_REQUEST);
    }

    private void showImageDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup_upload_image);
        LinearLayout cameraLayout = (LinearLayout) dialog.findViewById(R.id.popup_camera);
        LinearLayout galleryLayout = (LinearLayout) dialog.findViewById(R.id.popup_gallery);

        cameraLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1) {
                    showCameraView();
                } else {
                    checkMultiplePermission();
                }
            }
        });

        galleryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1) {
                    showGalleryView();
                } else {
                    checkStoragePermission();
                }
            }
        });

        dialog.show();
        dialog.setCancelable(true);
    }

    protected void showCameraView() {
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        tempImagePath = ImagePickerHelper.getTemporaryFile();
        tempPictureURI = Uri.fromFile(tempImagePath);
        takePicture.putExtra(MediaStore.EXTRA_OUTPUT, tempPictureURI);
        try {

            startActivityForResult(takePicture, CAMERA_REQUEST);
        } catch (Exception e) {
            Log.e("camera issue", e + "");
        }
    }

    private void checkMultiplePermission() {
        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE) + ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale
                            (getActivity(), Manifest.permission.CAMERA)) {

                Snackbar.make(getActivity().findViewById(android.R.id.content), "Please Grant Permissions to upload profile photo", Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, PERMISSIONS_MULTIPLE_REQUEST);
                                }
                            }
                        }).show();
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, PERMISSIONS_MULTIPLE_REQUEST);
                }
            }
        } else {
            showCameraView();
        }
    }

    private void checkStoragePermission() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                Snackbar.make(getActivity().findViewById(android.R.id.content), "Please Grant Permissions to upload profile photo", Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_REQUEST);
                                }
                            }
                        }).show();
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_REQUEST);
                }
            }
        } else {
            showGalleryView();
        }
    }

    private void getUserAndSubscriptionDetails() {

        RequestQueue queue = Volley.newRequestQueue(getContext());
        StringRequest sr = new StringRequest(Request.Method.POST, HTTPRequest.SERVICE_URL + "getUserDetails", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.e("RESPONSE PROFIDATASHOW", response);

                    try {

                        JSONObject response1 = new JSONObject(response);
                        String responseStatus = response1.getString("result");

                        Log.e("DATA FETCHED", "onResponse: " + responseStatus);

                        membershipDetailsArrayList.clear();
                        JSONArray jsonCatchUpArray = response1.getJSONArray("userDetails");

                        for (int j = 0; j < jsonCatchUpArray.length(); j++) {

                            JSONObject charityOpeningObj = jsonCatchUpArray.optJSONObject(j);

                            String userId = charityOpeningObj.optString("userId");
                            String userName = charityOpeningObj.optString("userName");
                            String userEmail = charityOpeningObj.optString("userEmail");
                            String userPhoto = charityOpeningObj.optString("userPhoto");
                            String userMobile = charityOpeningObj.optString("userMobile");
                            String userCountry = charityOpeningObj.optString("userCountry");
                            String userCountryCode = charityOpeningObj.optString("userCountryCode");
                            String userKey = charityOpeningObj.optString("userKey");

                            Log.e("PPRROOFFILE RESPONSE", "onResponse: " + userId + "u name" + userName + " email" + userEmail + " photo" + userPhoto + " mobile" + userMobile +
                                    "user country " + userCountry + " country code " + userCountryCode + " keyyyy" + userKey);

                            etName.setText(userName);
                            etCountry.setText(userCountry);
                            etEmail.setText(userEmail);
                            etMobile.setText(userMobile);

                            Picasso.with(getActivity())
                                    .load(userPhoto)
                                    .error(R.drawable.app_icon)
                                    .placeholder(R.drawable.app_icon)
                                    .into(profilePic);

                            setUserProfilePick(userPhoto);

                        }

                        JSONArray jsonCatchUpArrayMembership = response1.getJSONArray("membershiType");

                        for (int k = 0; k < jsonCatchUpArrayMembership.length(); k++) {

                            JSONObject membershipOpeningObj = jsonCatchUpArrayMembership.optJSONObject(k);

                            String membershiTypeId = membershipOpeningObj.optString("membershiTypeId");
                            String membershiTypeTitle = membershipOpeningObj.optString("membershiTypeTitle");
                            String membershiTypeName = membershipOpeningObj.optString("membershiTypeName");
                            String membershiTypeDescription = membershipOpeningObj.optString("membershiTypeDescription");
                            String membershiTypeStatusId = membershipOpeningObj.optString("membershiTypeStatusId");
                            String membershiTypeStatus = membershipOpeningObj.optString("membershiTypeStatus");
                            Log.e("build",membershiTypeStatus);

                            MembershipTypeDetailsModel model = new MembershipTypeDetailsModel(membershiTypeId, membershiTypeTitle, membershiTypeName, membershiTypeDescription, membershiTypeStatusId, membershiTypeStatus);
                            membershipDetailsArrayList.add(model);


                            ArrayList<Integer> mdataList = new ArrayList<>();

                            for (int i=0;i<membershipDetailsArrayList.size();i++) {
                                mdataList.add(i);
                            }

                            adapter = new ProfileMembershipAdapter(getActivity(), membershipDetailsArrayList,mdataList);
                            linearLayoutManager = new LinearLayoutManager(getActivity());
                            recyclerViewMembershipType.setLayoutManager(linearLayoutManager);
                            recyclerViewMembershipType.setAdapter(adapter);
                            recyclerViewMembershipType.setNestedScrollingEnabled(false);
                            adapter.notifyDataSetChanged();

                            linearLayoutLoaderContainer.setVisibility(View.GONE);
                            linearDatacontainer.setVisibility(View.VISIBLE);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("EREROR", String.valueOf(error));
                linearLayoutLoaderContainer.setVisibility(View.GONE);
                linearDatacontainer.setVisibility(View.VISIBLE);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("vendorKey", Const.vendorKey);
                headers.put("Authorization", Sharedprefrences.getUserKey(getContext()));
                headers.put("userID",Sharedprefrences.getUserId(getContext()));
                headers.put("deviceId", Sharedprefrences.getDeviceId(getContext()));
                headers.put("Content-Type", Const.contentType);
                return headers;

            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                return params;
            }
        };

        int socketTimeout = 20000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        sr.setRetryPolicy(policy);
        queue.add(sr);
    }

    private void getUserAndSubscriptionDetails2() {

        RequestQueue queue = Volley.newRequestQueue(getContext());
        StringRequest sr = new StringRequest(Request.Method.POST, HTTPRequest.SERVICE_URL + "getUserDetails", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.e("RESPONSE PROFIDATASHOW", response);

                    try {

                        JSONObject response1 = new JSONObject(response);
                        String responseStatus = response1.getString("result");

                        Log.e("DATA FETCHED", "onResponse: " + responseStatus);

                        membershipDetailsArrayList.clear();
                        JSONArray jsonCatchUpArray = response1.getJSONArray("userDetails");

                        for (int j = 0; j < jsonCatchUpArray.length(); j++) {

                            JSONObject charityOpeningObj = jsonCatchUpArray.optJSONObject(j);

                            String userId = charityOpeningObj.optString("userId");
                            String userName = charityOpeningObj.optString("userName");
                            String userEmail = charityOpeningObj.optString("userEmail");
                            String userPhoto = charityOpeningObj.optString("userPhoto");
                            String userMobile = charityOpeningObj.optString("userMobile");
                            String userCountry = charityOpeningObj.optString("userCountry");
                            String userCountryCode = charityOpeningObj.optString("userCountryCode");
                            String userKey = charityOpeningObj.optString("userKey");

                            Log.e("PPRROOFFILE RESPONSE", "onResponse: " + userId + "u name" + userName + " email" + userEmail + " photo" + userPhoto + " mobile" + userMobile +
                                    "user country " + userCountry + " country code " + userCountryCode + " keyyyy" + userKey);

                            etName.setText(userName);
                            etCountry.setText(userCountry);
                            etEmail.setText(userEmail);
                            etMobile.setText(userMobile);
                        }

                        JSONArray jsonCatchUpArrayMembership = response1.getJSONArray("membershiType");

                        for (int k = 0; k < jsonCatchUpArrayMembership.length(); k++) {

                            JSONObject membershipOpeningObj = jsonCatchUpArrayMembership.optJSONObject(k);

                            String membershiTypeId = membershipOpeningObj.optString("membershiTypeId");
                            String membershiTypeTitle = membershipOpeningObj.optString("membershiTypeTitle");
                            String membershiTypeName = membershipOpeningObj.optString("membershiTypeName");
                            String membershiTypeDescription = membershipOpeningObj.optString("membershiTypeDescription");
                            String membershiTypeStatusId = membershipOpeningObj.optString("membershiTypeStatusId");
                            String membershiTypeStatus = membershipOpeningObj.optString("membershiTypeStatus");
                            Log.e("build",membershiTypeStatus);

                            MembershipTypeDetailsModel model = new MembershipTypeDetailsModel(membershiTypeId, membershiTypeTitle, membershiTypeName, membershiTypeDescription, membershiTypeStatusId, membershiTypeStatus);
                            membershipDetailsArrayList.add(model);

                            ArrayList<Integer> mdataList = new ArrayList<>();

                            for (int i=0;i<membershipDetailsArrayList.size();i++) {
                                mdataList.add(i);
                            }

                            adapter = new ProfileMembershipAdapter(getActivity(), membershipDetailsArrayList,mdataList);
                            linearLayoutManager = new LinearLayoutManager(getActivity());
                            recyclerViewMembershipType.setLayoutManager(linearLayoutManager);
                            recyclerViewMembershipType.setAdapter(adapter);
                            recyclerViewMembershipType.setNestedScrollingEnabled(false);
                            adapter.notifyDataSetChanged();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("EREROR", String.valueOf(error));
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("vendorKey", Const.vendorKey);
                headers.put("Authorization", Sharedprefrences.getUserKey(getContext()));
                headers.put("userID", Sharedprefrences.getUserId(getContext()));
                headers.put("deviceId", Sharedprefrences.getDeviceId(getContext()));
                headers.put("Content-Type", Const.contentType);
                return headers;

            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                return params;
            }
        };

        int socketTimeout = 20000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        sr.setRetryPolicy(policy);
        queue.add(sr);
    }

    private void setUpdatedUserDataToWidgets() {
        etName.setText(updateduserName);
        etCountry.setText(updatedUserCountry);
        etEmail.setText(updatedUserEmail);
        etMobile.setText(updatedUserMobile);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CAMERA_REQUEST) {
                performCrop();
            } else if (requestCode == GALLERY_REQUEST) {
                if (setURIFromGallery(imageReturnedIntent.getData())) {
                    performCrop();
                }
            } else if (requestCode == CROP_REQUEST) {
                Sharedprefrences.setPictureuripathStored(getContext(),pictureURI.getPath());
                setImageView(pictureURI.getPath());

            }
        }
    }

    private void setImageView(String path) {
        // Get the dimensions of the View
        int targetW = profilePic.getWidth();
        int targetH = profilePic.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        //Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        //Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        bitmap = BitmapFactory.decodeFile(path, bmOptions);
        Log.e("IMAGERERRRR SETTTT", "setImageView: " + bitmap);
        profilePic.setImageBitmap(bitmap);

        Log.e("FINALLL  IMAGE HERE ", "setImageView: " + bitmap);

    }

    protected void performCrop() {
        pictureURI = Uri.fromFile(ImagePickerHelper.getImageFile());
        Intent cropIntent = new Intent("com.android.camera.action.CROP");
        cropIntent.setDataAndType(tempPictureURI, "image/*");
        cropIntent.putExtra("crop", "true");
        cropIntent.putExtra("aspectX", 1);
        cropIntent.putExtra("aspectY", 1);
        cropIntent.putExtra("outputX", IMAGE_SIZE);
        cropIntent.putExtra("outputY", IMAGE_SIZE);
        cropIntent.putExtra("return-data", false);
        cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, pictureURI);
        startActivityForResult(cropIntent, CROP_REQUEST);
    }

    protected boolean setURIFromGallery(Uri selectedImage) {
        tempImagePath = null;
        String[] filePathColumn = {MediaStore.Images.Media.DATA};

        Cursor cursor = this.getContext().getContentResolver().query(selectedImage,
                filePathColumn, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);

                tempPictureURI = Uri.fromFile(new File(cursor
                        .getString(columnIndex)));

            try {

                Log.e(TAG, "setURIFromGallery: Pathhhh"+tempImagePath );
                Bitmap bmpPic = ImagePickerHelper.decodeSampledBitmapFromURI(
                        tempPictureURI, getContext(), IMAGE_SIZE, IMAGE_SIZE, true);

                tempImagePath = ImagePickerHelper.getTemporaryFile();
                Log.e("IMAGE PATH ", "setURIFromGallery: " + tempImagePath + " temp picture uri " + tempPictureURI);

                tempPictureURI = Uri.fromFile(tempImagePath);
                Log.e("IMAGE PATH ", "setURIFromGallery: " + tempImagePath + " temp picture uri " + tempPictureURI);

                FileOutputStream bmpFile = new FileOutputStream(tempImagePath);
                bmpPic.compress(Bitmap.CompressFormat.JPEG, 60, bmpFile);
                bmpFile.flush();
                bmpFile.close();

                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        } //loop
          return false;
    }  //end of uri from galarry

    private void showAlertDialog() {

        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
        alert.setView(R.layout.alertdialog_success);
        alert.setCancelable(false);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                hideSoftKeyboard(getActivity());
            }
        });
        AlertDialog dialog = alert.create();
        dialog.show();
    }

    private void showToastMessages(final String message, final String errorType) {

        ((Activity) getContext()).runOnUiThread(new Runnable() {
            public void run() {

                if (errorType.equals("200")) {
                    showAlertDialog();
                }
            }
        });
    }

    public class UpdateUserDetailTask extends AsyncTask<Void, Void, Void> {

        private String TAG = "NEW PROFILE FRAGMENTTTT";

        private AsyncCallback asyncCallback;
        private Context context;
        private Bitmap imageBitmap;

        public UpdateUserDetailTask(Context context, Bitmap bitmap, AsyncCallback asyncCallback) {
            this.asyncCallback = asyncCallback;
            this.context = context;
            this.imageBitmap = bitmap;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                ProgressDialogUtils.showProgressDialog(context, MessageConstant.MESSAGE_PLEASE_WAIT);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            userRegistration();
            return null;
        }

        private String userRegistration() {
            String fileName = "profile_pic" + ".png";
            String url = HTTPRequest.SERVICE_URL + "updateUserDetails";
            Log.d(TAG, "PROFILE UPDATE REQUEST THROUGH OKHTTP. URL :" + url);
            String responseStr = null;
            final MediaType MEDIA_TYPE_JPG = MediaType.parse("image/jpg");

            try {

                byte[] data = null;
                if (imageBitmap != null) {
                    Log.d(TAG, "bitmap: " + imageBitmap.toString());
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    data = stream.toByteArray();
                }

                OkHttpClient client = new OkHttpClient();

                client.setConnectTimeout(Const.CONNECTION_TIME_OUT, TimeUnit.SECONDS);
                client.setReadTimeout(Const.CONNECTION_TIME_OUT, TimeUnit.SECONDS);

                RequestBody requestBody;

                String nammee = etName.getText().toString();
                String emailll = etEmail.getText().toString();

                if (data == null) {
                    requestBody = new MultipartBuilder()
                            .type(MultipartBuilder.FORM)
                            .addFormDataPart("userName", etName.getText().toString())
                            .addFormDataPart("userEmail", etEmail.getText().toString()/*ProfileFragment.etEmail.getText().toString()*/)
                            .build();
                } else {
                    requestBody = new MultipartBuilder()
                            .type(MultipartBuilder.FORM)
                            .addFormDataPart("userName", etName.getText().toString())   /*"pargunanR"*/
                            .addFormDataPart("userEmail", etEmail.getText().toString()/* emailEdited*/ /*"pargunan@hnfuk.com1"*/)
                            .addFormDataPart(KeyConstant.KEY_USER_FILE,fileName,RequestBody.create(MEDIA_TYPE_JPG, data))
                            .build();

                }
                com.squareup.okhttp.Request request = new com.squareup.okhttp.Request.Builder()
                        .url(url)
                        .addHeader("vendorKey", Const.vendorKey)
                        .addHeader("Authorization",Sharedprefrences.getUserKey(context))
                        .addHeader("userID", Sharedprefrences.getUserId(context))
                        .addHeader("deviceId", Sharedprefrences.getDeviceId(context))
                        .post(requestBody)
                        .build();

                com.squareup.okhttp.Response response = null;

                response = client.newCall(request).execute();
                responseStr = response.body().string().toString();
                Log.d(TAG, "responseStr " + responseStr);
                Log.d(TAG, "response.code() :" + response.code());

                //callback of async task
                asyncCallback.setResponse(response.code(), responseStr);
                ProgressDialogUtils.hideProgressDialog();
            } catch (Exception e) {
                e.printStackTrace();
                ProgressDialogUtils.hideProgressDialog();
                asyncCallback.setException(e.getMessage());
            }
            return responseStr;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getUserAndSubscriptionDetails2();


        HomeDrawerActivity.tvUserName.setText(Sharedprefrences.getUserName(getActivity()));
        HomeDrawerActivity.tvUserEamil.setText(Sharedprefrences.getUserEmail(getActivity()));
        HomeDrawerActivity.tvUserCountry.setText(Sharedprefrences.getUserCountry(getActivity()));

        HomeDrawerActivity.lineLive.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.White));
        HomeDrawerActivity.iconTabLive.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_live_unselected));
        HomeDrawerActivity.lineHome.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.White));
        HomeDrawerActivity.iconTabHome.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_home_unselected));
        HomeDrawerActivity.lineUpcoming.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.White));
        HomeDrawerActivity.iconTabUpcoming.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_upcoming_unselected));
        HomeDrawerActivity.baseUpcomingtext.setTextColor(ContextCompat.getColor(getContext(), R.color.gray));
        HomeDrawerActivity.lineVideo.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.White));
        HomeDrawerActivity.iconTabVideo.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_video_tab_unselected));
        HomeDrawerActivity.lineProfile.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        HomeDrawerActivity.iconTabProfile.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_profile_tab));
        HomeDrawerActivity.baseHomeText.setTextColor(ContextCompat.getColor(getContext(), R.color.gray));
        HomeDrawerActivity.basevideostext.setTextColor(ContextCompat.getColor(getContext(), R.color.gray));
        HomeDrawerActivity.baseProfileText.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        HomeDrawerActivity.baseLivetext.setTextColor(ContextCompat.getColor(getContext(), R.color.gray));
    }

    public void hideSoftKeyboard(Activity activity) {

        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Service.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(tvSave.getWindowToken(), 0);
    }
}

