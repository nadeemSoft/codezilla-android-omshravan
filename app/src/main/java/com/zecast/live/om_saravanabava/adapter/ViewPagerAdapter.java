package com.zecast.live.om_saravanabava.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.view.ViewGroup;
import com.zecast.live.om_saravanabava.fragments.ImageFragment;
import java.util.ArrayList;


public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private ArrayList<String> itemData;
    private Context mContext;
    private String adUnit;
    private int type;

    public ViewPagerAdapter(FragmentManager fm, ArrayList<String> itemData, int type, String adUnit, Context mContext) {
        super(fm);
        this.mContext=mContext;
        this.itemData = itemData;
        this.type = type;
        this.adUnit = adUnit;
        Log.d("banner", "item data: " + itemData.get(0).toString());
    }

    @Override
    public int getCount() {
        return itemData.size();
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
    }
    @Override
    public Fragment getItem(int position) {
        Log.d("banner", "view pager" + itemData.get(position));
        ImageFragment f = ImageFragment.newInstance(mContext);
        f.setImageList(itemData.get(position), type, adUnit);


        return f;
    }
}

