package com.zecast.live.om_saravanabava.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.zecast.live.om_saravanabava.R;
import com.zecast.live.om_saravanabava.model.VideosCategoryModel;
import com.zecast.live.om_saravanabava.model.VideoProgramsModel;
import com.zecast.live.om_saravanabava.players.YoutubeActivity;
import com.zecast.live.om_saravanabava.fragments.VideoFragment;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class VideoCategoriesSeeAllAdapter extends RecyclerView.Adapter<VideoCategoriesSeeAllAdapter.MyViewHolder>  {

    private Context context;
    ArrayList<VideosCategoryModel> arrayListData;

    public VideoCategoriesSeeAllAdapter(Context context, ArrayList<VideosCategoryModel> mCategoryList) {
        this.context = context;
        this.arrayListData = mCategoryList;
    }

    @Override
    public VideoCategoriesSeeAllAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(context, R.layout.single_row_item_video__programs_sub_categories,null);
        return new VideoCategoriesSeeAllAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final VideoCategoriesSeeAllAdapter.MyViewHolder holder, final  int position) {

        final VideosCategoryModel programs = arrayListData.get(position);

        holder.tvEpisodeName.setText(programs.getSubcategorylist().get(position).getGetEpisodeName());
        Picasso.with(context).
                load(programs.getSubcategorylist().get(position).getEpisodeImage())
                .error(R.mipmap.ic_launcher).placeholder(R.mipmap.ic_launcher).
                into(holder.ivHolderEpisodeLogo);
        final ArrayList <VideoProgramsModel> mList = VideoFragment.modelList.get(position).getSubcategorylist();

        holder.ivHolderEpisodeLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, YoutubeActivity.class);
                Log.e("URL", mList.get(position).getEpisodeUrl());
                intent.putExtra ("URL", mList.get(position).getVideoId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayListData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView ivHolderEpisodeLogo;
        public TextView tvEpisodeName,tvLike,tvView;
        ImageView playVideoIcon;
        FrameLayout frameLayoutImg;

        public MyViewHolder(View itemView) {

            super(itemView);
            ivHolderEpisodeLogo =(ImageView)itemView.findViewById(R.id.adapter_categories_imageview);
            tvEpisodeName = (TextView) itemView.findViewById(R.id.tv_episode_name);
            tvLike = (TextView) itemView.findViewById(R.id.tv_like);
            tvView = (TextView)itemView.findViewById(R.id.tv_views);
            playVideoIcon = (ImageView) itemView.findViewById(R.id.play_video_icon);
            frameLayoutImg = (FrameLayout)itemView.findViewById(R.id.frame_img);
        }
    }
}
