package com.zecast.live.om_saravanabava.utils;

import android.app.Activity;
import android.widget.FrameLayout;

/**
 * Created by codezilla-12 on 16/9/17.
 */

public class Const {

    public static String countryName = "";
    public static String phoneCode = "";
    public static String vendorKey = "51e5abc4-7d7b-4d50-90ee-761769eff59e";
   // public static String authorizationKey = "5938694d4b4a76772b65512b4433382f7048745955673d3d";
    public static int CONNECTION_TIME_OUT = 40;
    public static String contentType = "application/x-www-form-urlencoded";
    public static Activity liveFragmentActivity;
    public static FrameLayout flLive, flAll;
    public static final String shar_AudioUrl="";
 //"/*AIzaSyDhqbeDgl-S6vG1Ty91VeEcssQCYkRfjoo*/"
    public static final String YOUTUBE_VIDEO_CODE = "AIzaSyCckNSRznSRZBXYGHNHPDHiyUTV4mmyo9k";
    public static String deviceType = "A";
    public static  String deviceMode = "1";
    public static  String donateLink = "https://donate.justgiving.com/donation-amount?uri=aHR0cHM6Ly9kb25hdGUtYXBpLmp1c3RnaXZpbmcuY29tL2FwaS9kb25hdGlvbnMvOTM4ZDM3YTM0NDk1NDRhODhiYTViZWY4NTJkZjRjMGM="/*"https://donate.justgiving.com/login?uri=aHR0cHM6Ly9kb25hdGUtYXBpLmp1c3RnaXZpbmcuY29tL2FwaS9kb25hdGlvbnMvYmQzYmM5YWUzNTI4NGRkM2JlMzFlMmE1ZTFkN2QyNDk="*/;

    /*
    * firebase releted stringss
    * */

    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    public static final String SHARED_PREF = "ah_firebase";
    public static int UPCOMING_FRAGMENT_KEY = 0;

}
