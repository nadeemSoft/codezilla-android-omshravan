package com.zecast.live.om_saravanabava.model;

/**
 * Created by codezilla-12 on 22/9/17.
 */

public class AudioDataModel {

    String audioId;
    String audioName;
    String audioUrl ;
    String audioLogo;

    public AudioDataModel(String audioId, String audioName, String audioUrl, String audioLogo) {
        this.audioId = audioId;
        this.audioName = audioName;
        this.audioUrl = audioUrl;
        this.audioLogo = audioLogo;
    }

    public String getAudioId() {
        return audioId;
    }

    public void setAudioId(String audioId) {
        this.audioId = audioId;
    }

    public String getAudioName() {
        return audioName;
    }

    public void setAudioName(String audioName) {
        this.audioName = audioName;
    }

    public String getAudioUrl() {
        return audioUrl;
    }

    public void setAudioUrl(String audioUrl) {
        this.audioUrl = audioUrl;
    }

    public String getAudioLogo() {
        return audioLogo;
    }

    public void setAudioLogo(String audioLogo) {
        this.audioLogo = audioLogo;
    }
}
