package com.zecast.live.om_saravanabava.callback;

import org.json.JSONObject;

public interface RecyclerClickListener {

    public void setCellClicked(JSONObject jsonObject);
}